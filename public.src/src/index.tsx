import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'; 
import thunk from 'redux-thunk';

import { ConfigProvider } from 'antd';

import 'antd/dist/antd.css';
import './styles/index.less';
import App from './components/';
import reducers from './ducks/';

const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(
    <ConfigProvider componentSize="middle">
        <Provider store={store}>
            <App />
        </Provider>
    </ConfigProvider>,
    document.getElementById('root')
);