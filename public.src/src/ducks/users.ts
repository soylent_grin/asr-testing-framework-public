import { handleActions, createAction } from 'redux-actions';
import { assign } from 'lodash-es';

import { notify } from '../utils';

import * as api from '../api/users';
import { Dispatch } from 'redux';

function comparator(a: User, b: User): -1|0|1 {
    if (a.name > b.name) {
        return 1;
    }
    if (a.name < b.name) {
        return -1;
    }
    return 0;
}

// action types
const LOADED = 'users/loaded';
const SAVED = 'users/saved';

// sync actions
export const loaded = createAction(LOADED);
export const saved = createAction(SAVED);

// async actions
export const loadUsers = () => (dispatch: Dispatch) => {
    api.loadUsers().then((res) => {
        dispatch(loaded(res));
    }, () => {
        notify("error", "Failed to load users");
        dispatch(loaded([]));
    });
};
export const saveUser = (u: User) => (dispatch: Dispatch) => {
    return api.updateUser(u).then(() => {
        notify("success", "User updated");
        dispatch(saved(u));
    }, () => {
        notify("error", "Failed to update user");
    });
};

// default state

type State = {
    users: User[]
}

const initialState: State = {
    users: []
};

// reducer
const reducer = handleActions({
    [LOADED]: (state, { payload }: any) => {
        const sorted = payload.sort(comparator);
        return assign({}, state, {
            users: sorted
        });
    },
    [SAVED]: (state, { payload }: any) => {
        return assign({}, state, {
            users: state.users.map((u) => {
                if (u.id === payload.id) {
                    return payload;
                }
                return u;
            })
        });
    }
}, initialState);
export default reducer;

// selectors
export const selectors = {
    getUsers: (state: State) => {
        return state.users;
    }
};

export const hookSelectors = {
    getUsers: (state: any) => {
        return selectors.getUsers(state.users);
    }
}