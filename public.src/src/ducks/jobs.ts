import { handleActions, createAction } from 'redux-actions';
import { assign } from 'lodash-es';

import { notify } from '../utils';

import * as api from '../api/jobs';
import { Dispatch, Action } from 'redux';
import { ThunkDispatch } from 'redux-thunk';

function comparator(a: AsrJob, b: AsrJob): -1|0|1 {
    if (a.timeCreate! < b.timeCreate!) {
        return 1;
    }
    if (a.timeCreate! > b.timeCreate!) {
        return -1;
    }
    return 0;
}

// action types
const LIST_LOADED = 'jobs/list/loaded';
const EXECUTORS_LOADED = 'jobs/executors/loaded';
const METRICS_LOADED = 'jobs/metrics/loaded';
const TAGS_LOADED = 'jobs/tags/loaded';
const CREATED = 'jobs/created';
const SELECTED = 'jobs/selected';
const CHECKED = 'jobs/checked';
const UPDATED = 'jobs/updated';
const DELETED = 'jobs/deleted';
const TERMINATED = 'jobs/terminated';
const LABEL_UPDATED = 'jobs/labelUpdated';

// sync actions
export const listLoaded = createAction(LIST_LOADED);
export const executorsLoaded = createAction(EXECUTORS_LOADED);
export const metricsLoaded = createAction(METRICS_LOADED);
export const tagsLoaded = createAction(TAGS_LOADED);
export const created = createAction(CREATED);
export const selected = createAction(SELECTED);
export const checked = createAction(CHECKED);
export const deleted = createAction(DELETED);
export const updated = createAction(UPDATED);
export const terminated = createAction(TERMINATED);
export const labelUpdated = createAction(LABEL_UPDATED);

// async actions
export const loadInitialData = () => (dispatch: ThunkDispatch<{}, {}, Action<any>>) => {
    dispatch(loadJobs());
    dispatch(loadExecutors());
    dispatch(loadMetrics());
    dispatch(loadTags());
    dispatch(subscribeToJobUpdate());
};
export const subscribeToJobUpdate = () => (dispatch: Dispatch) => {
    api.subscribeToJobUpdate((job: AsrJob) => {
        dispatch(updated(job));
    });
};
export const loadJobs = () => (dispatch: Dispatch) => {
    api.loadAllJobs().then((res) => {
        dispatch(listLoaded(res));
    }, () => {
        notify("error", "Failed to load job list");
        dispatch(listLoaded([]));
    });
};
export const loadExecutors = () => (dispatch: Dispatch) => {
    api.loadAvailableExecutors().then((res) => {
        dispatch(executorsLoaded(res));
    });
};
export const loadMetrics = () => (dispatch: Dispatch) => {
    api.loadAvailableMetrics().then((res) => {
        dispatch(metricsLoaded(res));
    });
};
export const loadTags = () => (dispatch: Dispatch) => {
    api.loadAvailableTags().then((res) => {
        dispatch(tagsLoaded(res));
    });
};
export const loadJobDetail = (id: string) => (dispatch: Dispatch) => {
    api.loadAllJobs().then((res) => {
        dispatch(listLoaded(res));
    }, () => {
        notify("error", "Failed to load job list");
        dispatch(listLoaded([]));
    });
};
export const create = (newJob: AsrJob) => (dispatch: Dispatch) => {
    return api.createJob(newJob).then((res) => {
        dispatch(
            created(
                res
            )
        );
        notify('success', 'Job created');
    }, () => {
        notify("error", 'Failed to create job');
    });
};
export const remove = (id: string) => (dispatch: Dispatch) => {
    return api.removeJobById(id).then(() => {
        dispatch(deleted(id));
        notify('success', 'Job removed');
    }, () => {
        notify("error", 'Failed to remove job');
    });
};
export const terminate = (id: string) => (dispatch: Dispatch) => {
    return api.terminateJobById(id).then(() => {
        dispatch(terminated(id));
        notify('success', 'Job terminated');
    }, () => {
        notify("error", 'Failed to terminate job');
    });
};
export const updateLabel = (id: string, label: string) => (dispatch: Dispatch) => {
    return api.updateJobLabel(id, label).then(() => {
        dispatch(labelUpdated({
            id,
            label
        }));
        notify('success', 'Job label updated');
    }, () => {
        notify("error", 'Failed to update job label');
    });
};

// default state
type State = {
    jobs: AsrJob[],
    checkedJobs: string[],
    currentJob: string | null,
    executors: AsrExecutor[],
    metrics: AsrJobMetric[],
    tags: string[],
    isLoading: boolean

}
const initialState: State = {
    jobs: [],
    checkedJobs: [],
    currentJob: null,
    executors: [],
    metrics: [],
    tags: [],
    isLoading: true
};

// reducer
const reducer = handleActions({
    [LIST_LOADED]: (state, { payload }: any) => {
        const sorted = payload.sort(comparator);
        return assign({}, state, {
            jobs: sorted,
            isLoading: false
        });
    },
    [EXECUTORS_LOADED]: (state, { payload }) => {
        return assign({}, state, {
            executors: payload
        });
    },
    [METRICS_LOADED]: (state, { payload }) => {
        return assign({}, state, {
            metrics: payload
        });
    },
    [TAGS_LOADED]: (state, { payload }) => {
        return assign({}, state, {
            tags: payload
        });
    },
    [CREATED]: (state, { payload }: any) => {
        const sorted = [...state.jobs, payload].sort(comparator);
        return assign({}, state, {
            jobs: sorted
        });
    },
    [UPDATED]: (state, { payload }: any) => {
        return assign({}, state, {
            jobs: state.jobs.map((j) => {
                if (j.id === payload.id) {
                    return payload;
                }
                return j;
            })
        });
    },
    [LABEL_UPDATED]: (state, { payload }: any) => {
        return assign({}, state, {
            jobs: state.jobs.map((j) => {
                if (j.id === payload.id) {
                    return assign({}, j, {
                        label: payload.label
                    });
                }
                return j;
            })
        });
    },
    [DELETED]: (state, { payload }: any) => {
        const newState = assign({}, state, {
            jobs: state.jobs.filter(t => t.id !== payload),
            checkedJobs: state.checkedJobs.filter(id => id !== payload)
        });
        return assign({}, state, newState);
    },
    [SELECTED]: (state, { payload }) => {
        return assign({}, state, { current: payload });
    },
    [CHECKED]: (state, { payload }) => {
        return assign({}, state, { checkedJobs: payload });
    }
}, initialState);
export default reducer;

// selectors
export const selectors = {
    getList: (state: State) => {
        return state.jobs;
    },
    getChecked: (state: State) => {
        return state.checkedJobs;
    },
    getCurrent: (state: State) => {
        return state.currentJob;
    },
    isLoading: (state: State) => {
        return state.isLoading;
    },
    getExecutors: (state: State) => {
        return state.executors;
    },
    getMetrics: (state: State) => {
        return state.metrics;
    },
    getTags: (state: State) => {
        return state.tags;
    },
    getReadyToCompare: (state: State) => {
        const { checkedJobs, jobs } = state;
        const jobsWithMeasureStep = checkedJobs.map((id) => {
            return jobs.filter((j) => {
                return j.id === id;
            })[0];
        }).filter((j) => {
            return j && j.status === "success" && j.steps.filter((step) => {
                return step.stepType === "measure";
            })[0];
        });
        return jobsWithMeasureStep;
    }
};

export const hookSelectors = {
    getMetrics(state: any) {
        return selectors.getMetrics(state.jobs)
    }
};