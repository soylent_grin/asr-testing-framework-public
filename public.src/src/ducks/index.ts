import { combineReducers } from 'redux';

import jobs from './jobs';
import soundbank from './soundbank';
import auth from './auth';
import users from './users';
import appConfig from './app-config';

export default combineReducers({
    jobs,
    soundbank,
    auth,
    users,
    appConfig
});