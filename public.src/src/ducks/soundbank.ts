import { handleActions, createAction } from 'redux-actions';
import { assign } from 'lodash-es';

import { notify, renderDuration } from '../utils';

import * as api from '../api/soundbank';
import { Dispatch } from 'redux';

function comparator(a: SoundbankFolder, b: SoundbankFolder): -1|0|1 {
    if (a.title > b.title) {
        return 1;
    }
    if (a.title < b.title) {
        return -1;
    }
    return 0;
}

// action types
const FOLDERS_LOADED = 'soundbank/folders/loaded';
const SAMPLES_LOADED = 'soundbank/samples/loaded';
const FOLDER_UPLOADED = 'soundbank/folders/uploaded';
const FOLDER_REMOVED = 'soundbank/folders/removed';
const SAMPLE_RECORDED = 'soundbank/samples/recorded';

// sync actions
export const foldersLoaded = createAction(FOLDERS_LOADED);
export const samplesLoaded = createAction(SAMPLES_LOADED);
export const folderUploaded = createAction(FOLDER_UPLOADED);
export const folderRemoved = createAction(FOLDER_REMOVED);
export const sampleRecorded = createAction(SAMPLE_RECORDED);

// async actions
export const loadFolders = () => (dispatch: Dispatch) => {
    api.loadFolders().then((res) => {
        dispatch(foldersLoaded(res));
    }, () => {
        notify("error", "Failed to load soundbank folders");
        dispatch(foldersLoaded([]));
    });
};
export const loadSamples = (folderKey: string) => (dispatch: Dispatch) => {
    api.loadSamples(folderKey).then((res) => {
        dispatch(samplesLoaded({
            samples: res,
            folderKey
        }));
    }, () => {
        notify("error", "Failed to load soundbank samples");
    });
};
export const uploadFolder = (file: File) => (dispatch: Dispatch) => {
    return api.uploadFolder(file).then((res) => {
        dispatch(folderUploaded(res));
        if (res) {
            if (res.samples && res.samples.length > 0) {
                notify("success", `Successfully uploaded new folder (found ${res.samples.length} samples)`);
            } else {
                notify("warn", "Folder uploaded, but 0 samples were found; check folder structure (e.g. there is no intermediate folder)")
            }
        }
    }, (e) => {
        let errorText = "unknown error";
        if (e && e.response) {
            if (e.response.status === 413) {
                errorText = "File is too large";
            } else if (e.response.data) {
                errorText = e.response.data;
            }
        }
        notify("error", `Failed to upload load soundbank folder; error: ${errorText}`);
    });
};
export const recordSample = (f: RecordSample) => (dispatch: Dispatch) => {
    return api.record(f).then((res) => {
        dispatch(sampleRecorded({
            sample: res,
            folderKey: f.folder
        }));
        if (res) {
            notify("success", `Successfully uploaded new recorded sample`);
        }
    }, (e) => {
        let errorText = "unknown error";
        if (e && e.response) {
            if (e.response.status === 413) {
                errorText = "File is too large";
            } else if (e.response.data) {
                errorText = e.response.data;
            }
        }
        notify("error", `Failed to upload recorded sample; error: ${errorText}`);
    });
};
export const addFolder = (key: string) => (dispatch: Dispatch) => {
    return api.addFolder(key).then((res) => {
        dispatch(folderUploaded(res));
        notify("success", "Successfully added folder. Now you can record samples for it");
    }, (e) => {
        notify("error", `Failed to add new folder`);
    });
};

export const removeFolder = (key: String) => (dispatch: Dispatch) => {
    return api.removeFolder(key).then(() => {
        dispatch(folderRemoved(key));
        notify("success", "Successfully removed folder");
    }, (e) => {
        notify("error", `Failed to remove soundbank folder`);
    });
};

// default state

type State = {
    publicFolders: SoundbankFolder[]
    privateFolders: SoundbankFolder[]
}

const initialState: State = {
    publicFolders: [],
    privateFolders: []
};

// reducer
const reducer = handleActions({
    [FOLDERS_LOADED]: (state, { payload }: any) => {
        const sorted = payload.sort(comparator);
        return assign({}, state, {
            publicFolders: sorted.filter((f: SoundbankFolder) => {
                return !f.user;
            }),
            privateFolders: sorted.filter((f: SoundbankFolder) => {
                return f.user;
            }),
            isLoading: false
        });
    },
    [FOLDER_UPLOADED]: (state, { payload }: any) => {
        debugger;
        const sorted = [...state.privateFolders, payload].sort(comparator);
        return assign({}, state, {
            privateFolders: sorted
        });
    },
    [FOLDER_REMOVED]: (state, { payload }: any) => {
        const newFolder = state.privateFolders.filter((f) => {
            return f.key !== payload;
        });
        return assign({}, state, {
            privateFolders: newFolder
        });
    },
    [SAMPLE_RECORDED]: (state, { payload }: any) => {
        const newFolder = state.privateFolders.map((f) => {
            if (f.key === payload.folderKey) {
                return assign({}, f, {
                    samples: [...f.samples, payload.sample],
                    children: [
                        ...f.children,
                        assign({}, payload.sample, {
                            title: `${payload.sample.key} (${renderDuration(payload.sample.duration)})`
                        })
                    ]
                });
            }
            return f;
        });
        return assign({}, state, {
            privateFolders: newFolder
        });
    }
}, initialState);
export default reducer;

// selectors
export const selectors = {
    getFolders: (state: State) => {
        return [...state.publicFolders, ...state.privateFolders];
    },
    getPublicFolders: (state: State) => {
        return state.publicFolders;
    },
    getPrivateFolders: (state: State) => {
        return state.privateFolders;
    },
    isPrivateFoldersLimitExceeded: (state: State, maxPrivateFoldersPerUser: number) => {
        return selectors.getPrivateFolders(state).length >= maxPrivateFoldersPerUser;
    }
};

// hook selectors
export const hookSelectors = {
    isPrivateFoldersLimitExceeded: (state: any) => {
        return selectors.isPrivateFoldersLimitExceeded(state.soundbank, state.appConfig.config.maxPrivateFoldersPerUser);
    }
};