import { handleActions, createAction } from 'redux-actions';
import { assign } from 'lodash-es';

import { notify } from '../utils';

import * as api from '../api/auth';
import { Dispatch } from 'redux';

function comparator(a: SoundbankFolder, b: SoundbankFolder): -1|0|1 {
    if (a.title > b.title) {
        return 1;
    }
    if (a.title < b.title) {
        return -1;
    }
    return 0;
}

// action types
const USER_INFO_LOADED = 'auth/user/loaded';

// sync actions
export const userInfoLoaded = createAction(USER_INFO_LOADED);

// async actions
export const loadUserInfo = () => (dispatch: Dispatch) => {
    api.getUser().then((res) => {
        dispatch(userInfoLoaded(res));
    }, () => {
        dispatch(userInfoLoaded(null));
    });
};

// default state

type State = {
    user: User | null,
    isLoading: boolean
}

const initialState: State = {
    user: null,
    isLoading: true
};

// reducer
const reducer = handleActions({
    [USER_INFO_LOADED]: (state, { payload }: any) => {
        return assign({}, state, {
            user: payload,
            isLoading: false
        });
    }
}, initialState);
export default reducer;

// selectors
export const selectors = {
    getUser: (state: State) => {
        return state.user;
    },
    isLoggedIn: (state: State) => {
        return !!state.user;
    },
    isLoading: (state: State) => {
        return state.isLoading;
    }
};

// selectors
export const hookSelectors = {
    getUser: (state: any) => {
        return selectors.getUser(state.auth);
    },
    isLoggedIn: (state: any) => {
        return selectors.isLoggedIn(state.auth);
    },
    isLoading: (state: any) => {
        return selectors.isLoading(state.auth);
    }
};