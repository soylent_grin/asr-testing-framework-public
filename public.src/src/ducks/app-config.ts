import { handleActions, createAction } from 'redux-actions';
import { assign } from 'lodash-es';

import * as api from '../api/app-config';
import { Dispatch } from 'redux';

// action types
const LOADED = 'app-config/loaded';

// sync actions
export const loaded = createAction(LOADED);

// async actions
export const load = () => (dispatch: Dispatch) => {
    api.loadConfig().then((res) => {
        dispatch(loaded(res));
    });
};

// default state

type State = {
    config: AppConfig
}

const initialState: State = {
    config: {
        googleAuthEnabled: false,
        githubAuthEnabled: false,
        maxJobsPerUser: 0,
        maxPrivateFoldersPerUser: 0,
        maxSamplesPerJob: 0
    }
};

// reducer
const reducer = handleActions({
    [LOADED]: (state, { payload }: any) => {
        return assign({}, state, {
            config: payload
        });
    }
}, initialState);
export default reducer;

// selectors
export const selectors = {
    getConfig: (state: State) => {
        return state.config;
    }
};

// selectors
export const hookSelectors = {
    getConfig: (state: any) => {
        return selectors.getConfig(state.appConfig);
    }
};