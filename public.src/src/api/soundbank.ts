import { renderDuration } from './../utils';
import axios from 'axios';

import { assign } from 'lodash-es';

// prepare for using in TreeSelect
function adaptFolder(f: any): SoundbankFolder {
    return assign({}, f, {
        value: f.key,
        children: f.samples.map((s: any) => {
            return assign({}, s, {
                value: s.id,
                key: s.id, // TODO: this is original key override
                title: `${s.key} (${renderDuration(s.duration)})`
            });
        })
    });
}

export const loadFolders = (): Promise<SoundbankFolder[]> => axios({
    url: '/api/soundbank/folders/'
}).then((res) => {
    return res.data.map(adaptFolder) as SoundbankFolder[];
});

export const loadSamples = (folder: string): Promise<SoundbankSample[]> => axios({
    url: `/api/soundbank/folders/${folder}/samples/`
}).then((res) => {
    return res.data as SoundbankSample[];
});

export const loadSamplesByIds = (ids: string[]): Promise<SoundbankSample[]> => axios({
    url: `/api/soundbank/samples/`,
    method: "POST",
    data: JSON.stringify(ids),
    headers: {
        "Content-Type": "application/json"
    }
}).then((res) => {
    return res.data as SoundbankSample[];
});

export const loadSample = (sampleId: string): Promise<SoundbankSample> => axios({
    url: `/api/soundbank/samples/${sampleId}`
}).then((res) => {
    return res.data as SoundbankSample;
});

export const uploadFolder = (file: File): Promise<SoundbankFolder> => {
    const form = new FormData();
    form.append('file', file);
    return axios.post(`/api/soundbank/folders/`, form, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    }).then((res) => {
        return adaptFolder(res.data) as SoundbankFolder;
    });
};

export const removeFolder = (folderKey: String): Promise<any> => axios({
    url: `/api/soundbank/folders/${folderKey}`,
    method: "DELETE"
});

export const addFolder = (key: String): Promise<any> => axios({
    url: `/api/soundbank/add_folder/`,
    method: "POST",
    data: JSON.stringify({
        key
    }),
    headers: {
        "Content-Type": "application/json"
    }
}).then((res) => {
    return adaptFolder(res.data) as SoundbankFolder;
});

export const loadAugmentatorParams = (key: string = "generic"): Promise<AsrExecutorParam[]> => axios({
    url: `/api/soundbank/augmentation/scenarios/${key}/params/`
}).then((res) => {
    return res.data as AsrExecutorParam[];
});

export const augment = (folder: string, params: any, scenario: string = "generic"): Promise<SoundbankFolder> => {
    const { title } = params;
    Object.keys(params).forEach((k) => {
        params[k] = params[k].toString();
    });
    return axios({
        url: `/api/soundbank/folders/${folder}/augmentate/${scenario}/`,
        method: 'post',
        headers: { 'content-type': 'application/json' },
        timeout: 24 * 3600 * 1000,
        data: JSON.stringify({ title, params })
    }).then((res) => {
        return adaptFolder(res.data) as SoundbankFolder;
    });
};

export const record = (r: RecordSample):  Promise<SoundbankSample> => {
    const form = new FormData();
    form.append('folder', r.folder);
    form.append('name', r.name);
    form.append('transcription', r.transcription);
    form.append('audio', r.audio);
    form.append('meta', r.meta || "{}");
    return axios.post(`/api/soundbank/record/`, form, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    }).then((res) => {
        return res.data as SoundbankSample;
    });
};

/*
export const loadNoisers = (): any => axios({
    url: '/api/soundbank/augmentation/scenarios/',
});

export const loadNoiserSettings = (key: string): any => axios({
    url: `/api/soundbank/augmentation/scenarios/${key}/params/`
});
*/

/*
export const augment = (folder: string, scenario: string, title: any, params: any): any => axios({
    url: `/api/soundbank/folders/${folder}/augmentate/${scenario}/`,
    method: 'post',
    headers: { 'content-type': 'application/json' },
    timeout: 24 * 3600 * 1000,
    data: JSON.stringify({ title, params })
});
export const normalize = (folder: string, smapleRate: 8000 | 16000 | 48000): any => axios({
    url: `/api/soundbank/folders/${folder}/normalize/${smapleRate}/`,
    timeout: 24 * 3600 * 1000,
    method: 'post'
});
*/