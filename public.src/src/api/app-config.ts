import axios from 'axios';

export const loadConfig = (): Promise<AppConfig> => axios({
    url: '/api/app-config/'
}).then((res) => {
    return res.data as AppConfig;
});