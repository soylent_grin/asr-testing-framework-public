import axios from 'axios';

/*
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error && error.response && error.response.status == 401) {
        logout();
    } else {
        return Promise.reject(error);
    }
});

const get_cookie = function(name: string) {
    var cookies = document.cookie;
    var start = cookies.indexOf(' ' + name + '=');
    if (start === -1) {
        start = cookies.indexOf(name + '=');
    }
    if (start !== -1) {
        start = cookies.indexOf('=', start) + 1;
        var end = cookies.indexOf(';', start);
        if (end === -1) {
            end = cookies.length;
        }
        return unescape(cookies.substring(start, end));
    }
    return null;
};

function delete_cookie(name: string) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

const COOKIE_NAME = 'auth_token';

let isLoggedInFlag = true;

*/

export const getUser = (): Promise<User> => axios({
    url: '/api/user/'
}).then((res) => {
    return res.data as User;
});

export const signIn = (username: string, password: string): Promise<any> => axios({
    url: '/api/sign-in/',
    method: 'post',
    headers: { 'content-type': 'application/json' },
    data: JSON.stringify({
        username,
        password
    }),
}).then((res) => {
    return res.data;
});