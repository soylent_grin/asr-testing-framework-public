import axios from 'axios';
import { assign } from 'lodash-es';
import MessageBroker from './ws-broker';

function stringSorter(collection: any[], field?: string) {
    const res = collection.sort((a: any, b: any) => {
        const aField = field ? a[field] : a;
        const bField = field ? b[field] : b;
        if (aField > bField) {
            return 1;
        }
        if (aField < bField) {
            return -1;
        }
        return 0;
    });

    return res;
}

export const loadAvailableExecutors = (): Promise<AsrExecutor[]> => axios({
    url: '/api/asr/executors/'
}).then((res) => {
    return res.data as AsrExecutor[];
});

export const loadAvailableParamsByAsr = (asr: string): Promise<AsrExecutorParam[]> => axios({
    url: `/api/asr/executors/${asr}/params/`,
}).then((res) => {
    return res.data as AsrExecutorParam[];
});

const launchJob = (id: string): Promise<void> => axios({
    url: `/api/asr/jobs/${id}/launch`,
    method: "post"
}).then((res) => {
    return res.data;
});

export const createJob = (data: AsrJob): Promise<AsrJob> => axios({
    url: `/api/asr/jobs/`,
    method: 'post',
    headers: { 'content-type': 'application/json' },
    data: JSON.stringify(data),
}).then((res) => {
    const job = res.data as AsrJob;
    launchJob(job.id!);
    return job;
});

export const loadAllJobs = (): Promise<AsrJob[]> => axios({
    url: `/api/asr/jobs/`,
}).then((res) => {
    return res.data as AsrJob[];
});

export const terminateJobById = (jobId: string): Promise<any> => axios({
    url: `/api/asr/jobs/${jobId}/terminate/`,
    method: 'put',
});

export const updateJobLabel = (jobId: string, label: string): Promise<any> => axios({
    url: `/api/asr/jobs/${jobId}/label/${label}`,
    method: 'put',
});

export const removeJobById = (jobId: string): Promise<any> => axios({
    url: `/api/asr/jobs/${jobId}/`,
    method: 'delete',
});

export const loadAvailableMetrics = (): Promise<AsrJobMetric[]> => axios({
    url: `/api/asr/metrics/`,
}).then((res) => {
    return stringSorter(res.data, "title") as AsrJobMetric[];
});

export const loadAvailableTags = (): Promise<string[]> => axios({
    url: `/api/asr/tags/`,
}).then((res) => {
    return stringSorter(res.data) as string[];
});

export const loadStepResult = (stepId: string): Promise<any> => axios({
    url: `/api/asr/jobs/result/${stepId}/`,
}).then((res) => {
    return res.data as any;
});

export const loadMeasureStepResult = (stepId: string): Promise<any> => {
    return loadStepResult(stepId).then(({ result }) => {
        let selectedMetrics: string[] = [];
        const overallMetrics = result && result.overall && result.overall[0] && result.overall[0].result;
        const singleMetrics = result && result.single && result.single.map((pair: any) => {
            const data = (pair[1] && pair[1].result) || {};
            if (selectedMetrics.length === 0 && Object.keys(data).length > 0) {
                selectedMetrics = Object.keys(data);
            }
            return assign({}, data, {
                sample: pair[0]
            })
        });
        return {
            selectedMetrics: selectedMetrics.sort(),
            result: [
                assign({}, overallMetrics, {
                    sample: "Average"
                }),
                ...singleMetrics.sort((a: any, b: any) => {
                    if (a.sample > b.sample) {
                        return 1;
                    }
                    if (a.sample < b.sample) {
                        return -1;
                    }
                    return 0;
                })
            ]
        };
    });
}

export const loadStepLogs = (stepId: string): Promise<string[]> => axios({
    url: `/api/asr/jobs/result/${stepId}/log/`,
}).then((res) => {
    return res.data as string[];
});

export const subscribeToJobUpdate = (callback: (a: AsrJob) => any) => {
    MessageBroker.subscribe("/topic/jobs/update", callback);
};

export const loadDefaultModels = (asrKey: string): Promise<AsrModel[]> => axios({
    url: `/api/asr/models/${asrKey}/default/`
}).then((res) => {
    return res.data as AsrModel[];
});
