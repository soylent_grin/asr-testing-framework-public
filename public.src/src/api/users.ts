import axios from 'axios';
import { assign } from 'lodash-es';

export const loadUsers = (): Promise<User[]> => axios({
    url: '/api/users/'
}).then((res) => {
    return res.data.map((u: any) => {
        return assign(u, {
            group: (u && u.data && u.data.group) || undefined
        })
    }) as User[];
});

export const updateUser = (u: User): Promise<User[]> => axios({
    url: `/api/users/${u.id}`,
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    data: JSON.stringify(assign({}, u, {
        data: {
            group: u.group
        }
    }))
}).then((res) => {
    return res.data;
});
