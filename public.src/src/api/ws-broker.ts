interface Consumer {
    topic: string;
    handler: Function;
  }
  
class Broker {
    private consumers: Consumer[] = [];
    private ws: any = null;
  
    public constructor() {
        this.ws = new WebSocket(`ws://${window.location.host}/api/websocket/`);
        this.ws.onmessage = (event: any): void => {
            const data = JSON.parse(event.data);
            this.consumers.filter((c: Consumer): boolean => {
             return c.topic === data.topic;
        }).forEach((c: Consumer): void => {
            c.handler(data.message);
        });
      }
    }
  
    public subscribe(topic: string, handler: Function) {
        this.consumers = [...this.consumers.filter((c: Consumer): boolean => c.topic !== topic), { topic, handler }];
    }
    public unsubscribe(topic: string) {
        this.consumers = this.consumers.filter((c: Consumer): boolean => c.topic !== topic);
    }
  }
  
  export default new Broker();