import moment from 'moment';
import { message } from 'antd';

export const noop = () => {};

export const notify = (status: "error" | "success" | "warn", text: string) => {
    // switch(status) {
    // case "error":
    //     message.error(text)
    //     break;
    // }
    message[status](text);
    // console.log(`[${status}]: ${message}`);
};

export const renderTime = (t: Date | number) => {
    return moment(t).format('DD.MM.YYYY HH:mm:ss');
};

export const renderJobName = (j: AsrJob) => {
    if (j.label) {
        return j.label;
    }
    return `Job ${renderTime(j.timeBegin!)}`;
};

export const validateAsrJob = (j: AsrJob, appConfig: AppConfig, folders: SoundbankFolder[]) => {
    let error = null;

    const datasetStep = j.steps.filter((s) => {
        return s.stepType === "dataset";
    })[0];
    const asrTrainStep = j.steps.filter((s) => {
        return s.stepType === "train";
    })[0];
    const asrTestStep = j.steps.filter((s) => {
        return s.stepType === "test";
    })[0];
    const measureStep = j.steps.filter((s) => {
        return s.stepType === "measure";
    })[0];

    if (asrTrainStep && asrTrainStep.samples.type === "current" && !datasetStep) {
        error = "Configure dataset step to use it's result in train step";
    } else if (asrTrainStep && asrTrainStep.samples.list.length === 0) {
        error = "Select at least one sample to train";
    } if (asrTestStep && asrTestStep.samples.type === "current" && !datasetStep) {
        error = "Configure dataset step to use it's result in test step";
    } else if (asrTestStep && asrTestStep.samples.list.length === 0) {
        error = "Select at least one sample to train";
    } else if (asrTestStep && asrTestStep.inputType === "previous" && !asrTestStep.inputJob) {
        error = "Select previous job to get model";
    } else if (asrTestStep && asrTestStep.inputType === "current" && !asrTrainStep) {
        error = "Set up current ASR train step";
    } else if (measureStep && measureStep.inputType === "previous" && !measureStep.inputJob) {
        error = "Select previous job to measure";
    } else if (measureStep && measureStep.inputType === "current" && !asrTestStep) {
        error = "Set up current ASR test step";
    } else if (measureStep && (!measureStep.config.metrics || measureStep.config.metrics.length === 0)) {
        error = "Select at least one metric";
    } else if (j.steps.length === 0) {
        error = "Select at least one job step";
    } else if (asrTestStep && asrTestStep.samples.type === "previous" && asrTestStep.samples.list.length > appConfig.maxSamplesPerJob) {
        error = `Selected ${asrTestStep.samples.list.length} samples for test step, but only ${appConfig.maxSamplesPerJob} is allowed`;
    } else if (datasetStep && folders.filter((f) => {
        return f.title === datasetStep.config.augmentationConfig?.title;
    }).length > 0) {
        error = `Unable to augmentate: folder with name ${datasetStep.config.augmentationConfig?.title} already exists`;
    } else if (datasetStep && !datasetStep.config.folder) {
        error = `Select folder to augmentate`;
    } else if (datasetStep && !datasetStep.config.augmentationConfig?.title) {
        error = `Set new folder title (must be unique)`;
    }

    return error;
}
export const diffBetweenDates = (from: Date | number, to: Date | number): string => {
    var ms = moment(to).diff(moment(from));
    var d = moment.duration(ms);
    return Math.floor(d.asHours()) + moment(ms).format(":mm:ss");
};

export const renderDuration = (seconds: number): string => {
    if (seconds < 60) {
        return `${Math.ceil(seconds)}s`;
    } else {
        const minutes = Math.floor(seconds / 60);
        const remainSeconds = seconds % 60;
        if (minutes < 60) {
            return `${minutes}m ${Math.ceil(remainSeconds)}s`;
        } else {
            const hours = Math.floor(minutes / 60);
            const remainMinutes = minutes % 60;
            return `${hours}h ${Math.ceil(remainMinutes)}m`;
        }
    }
}

export const chainPromises = (funcs: Array<() => Promise<any>>, delay: number) => {
    return funcs.reduce((promise, func) => {
        return promise.then(() => {
            return new Promise((resolve, reject) => {
                func().then((res) => {
                    if (delay) {
                        setTimeout(() => {
                            resolve(res);
                        }, delay);
                    } else {
                        resolve(res);
                    }
                }, () => {
                    reject();
                });
            });
        });
    }, Promise.resolve([]));
};

export const isAllowed = (
    user: User | null,
    permission: "viewUsers" | "editUsers"
): boolean => {
    const role = user && user.role;
    if (!role) {
        return false;
    }
    let isAllowed = false;
    switch (permission) {
    case "viewUsers":
        isAllowed = role === "admin";
        break;
    case "editUsers":
        isAllowed = role === "admin";
        break;
    default:
        break;
    }
    return isAllowed;
}

export const getRoles: () => Record<string, {
    name: string,
    key: string
}> = () => {
    return {
        admin: {
            name: "Administrator",
            key: "admin"
        },
        user: {
            name: "User",
            key: "user"
        }
    };
};