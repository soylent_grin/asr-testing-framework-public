import React, { useEffect } from 'react';
import {
    BrowserRouter as Router,
    Switch
}
from "react-router-dom";
import { connect, useDispatch } from 'react-redux';

import * as authDuck from '../ducks/auth';
import PrivateRoute from './common/private-layout-route';
import Route from './common/layout-route';
import Landing from './landing/';
import Main from './main/';
import Login from './login/';
import Help from './main/help';
import { load as loadAppConfig } from '../ducks/app-config';

/*

import * as api from '../api/auth';

import { notify } from '../utils';

import Login from './login';
import Main from './main';

type Props = {};
type State = {};

class App extends React.Component<Props, State> {
    state = {
        isLoading: false,
        loggedIn: api.isLoggedIn()
    };

    handleSubmit = (data: any) => {
        this.setState({
            isLoading: true
        }, () => {
            api.login(data.username, data.password).then(() => {
                this.setState({
                    isLoading: false,
                    loggedIn: true
                });
            }).catch(() => {
                this.setState({
                    isLoading: false
                }, () => {
                    notify("error", "Не удалось авторизоваться. Проверьте имя пользователя и пароль");
                });
            });
        });
    };
    render() {
        const { isLoading, loggedIn } = this.state;
        return (
            <div className="App">
                {
                    loggedIn ?
                        <Main /> :
                        <Login />
                }
            </div>
        );
    }
}

export default App;

*/


const App = ({ isLoading } : any) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadAppConfig());
        dispatch(authDuck.loadUserInfo());
    }, []);
    return (
        <Router>
            <Switch>
                <Route path="/login">
                    <Login />
                </Route>
                <PrivateRoute path="/main">
                    <Main />
                </PrivateRoute>

                <Route path="/help" isPrivate>
                    <Help />
                </Route>
                <Route path="/">
                    <Landing />
                </Route>
            </Switch>
        </Router>
    );
};

function mapStateToProps({ auth }: any) {
    return {
        isLoading: authDuck.selectors.isLoading(auth)
    };
}

export default connect(mapStateToProps)(App);
