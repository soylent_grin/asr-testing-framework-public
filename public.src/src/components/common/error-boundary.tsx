import React, { ReactElement } from 'react';

type Props = {
    renderFallback: () => ReactElement
}

export default class ErrorBoundary extends React.Component<Props> {
    state = { hasError: false }
  
    static getDerivedStateFromError(error: any) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }
  
    componentDidCatch(error: any, errorInfo: any) {
        // You can also log the error to an error reporting service
        console.error(error, errorInfo);
    }
  
    render() {
    if (this.state.hasError) {
        // You can render any custom fallback UI
        return this.props.renderFallback();
    }
  
      return this.props.children; 
    }
}