import React from 'react';
import { Route } from 'react-router-dom';
import Layout from '../layout/';

export default function LayoutRoute({ children, isPrivate, ...rest }: any) {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                <Layout isPrivate={isPrivate}>
                    {children}
                </Layout>
            }
        />
    );
}