import React from 'react';

import { Avatar } from 'antd';

type Props = {
    user: User | null
};

const UserAvatar = ({ user }: Props) => {
    return (
        <Avatar src={user!.avatarUrl} size="small">
            {user!.name && user!.name[0]}
        </Avatar>
    );
};

export default UserAvatar;

