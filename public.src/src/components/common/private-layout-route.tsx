import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { connect } from 'react-redux';

import * as authDuck from '../../ducks/auth';
import Layout from '../layout/';

type Props = {
    isLoading?: boolean,
    isLoggedIn?: boolean,
    path: string,
    children?: React.ReactNode
}

const PrivateLayoutRoute: React.FunctionComponent<Props> = ({ children, isLoggedIn, isLoading, ...rest }) => {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                (isLoggedIn) ?
                    <Layout isPrivate>
                        {children}
                    </Layout> : 
                    !isLoading && <Redirect
                        to={{
                            pathname: "/login",
                            state: { from: location }
                        }}
                    />
            }
        />
    );
}

function mapStateToProps({ auth }: any) {
    return {
        isLoggedIn: authDuck.selectors.isLoggedIn(auth),
        isLoading: authDuck.selectors.isLoading(auth)
    };
}

export default connect(mapStateToProps)(PrivateLayoutRoute);