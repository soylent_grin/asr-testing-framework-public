import React from 'react';
import Banner from './banner';
import Page from './page';
import Video from './video';
import Footer from './footer';

type Props = {};
type State = {};

class Home extends React.PureComponent {
  render() {
    return (
        <div className="layout">
            <Banner key="banner" />
            <Page key="page1" />
            <Video />
            <Footer />
        </div>
    );
  }
}
export default Home;