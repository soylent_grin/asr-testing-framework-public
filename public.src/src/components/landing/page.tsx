import React from 'react';

import {
    AudioOutlined,
    PlayCircleOutlined,
    BarChartOutlined
} from '@ant-design/icons';

class Page extends React.PureComponent {
  render() {
    return (
        <div className="layout__page">
            <div>
                <AudioOutlined style={{
                    fontSize: "40px",
                    textShadow: "1px 1px 2px #1890ff3b",
                    color: "#1890ff"
                }} />
                <p>
                    Augmentate dataset
                </p>
                <span>With audio processing algorithms</span>
            </div>
            <div>
                <PlayCircleOutlined style={{
                    fontSize: "40px",
                    textShadow: "1px 1px 2px #1890ff3b",
                    color: "#1890ff"
                }} />
                <p>
                    Evaluate ASR
                </p>
                <span>Receive transcriptions in unified way</span>
            </div>
            <div>
                <BarChartOutlined style={{
                    fontSize: "40px",
                    textShadow: "1px 1px 2px #1890ff3b",
                    color: "#1890ff"
                }} />
                <p>
                    Analyze results
                </p>
                <span>
                    Measure STT with 17 metrics
                </span>
            </div>
        </div>
    );
  }
}

export default Page;