import React from 'react';

class Video extends React.PureComponent {
  render() {
    return (
        <div className="layout__video">
            <div>
                <h4>Watch 5 minute online tour</h4>
                <iframe width="400" height="315"
                    src="https://www.youtube.com/embed/HtQMdYQT8EA">
                </iframe>
            </div>
        </div>
    );
  }
}

export default Video;