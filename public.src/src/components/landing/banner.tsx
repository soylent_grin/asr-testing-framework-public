import React from 'react';
import { Button } from 'antd';

import bannerImg from '../../assets/banner.png';
import logo from '../../assets/logo-full.svg';

import { Link } from 'react-router-dom';

type Props = {};
type State = {};

class Banner extends React.PureComponent {
  render() {
    return (
        <div className="layout__banner">
            <div>
                <h1 key="h1">
                    <img
                        src={logo}
                        style={{
                            height: "200px"
                        }}
                    />
                    Cloud Voice Assistant Framework
                </h1>
                <p style={{
                    marginBottom: "30px"
                }}>
                    SaaS tool to evaluate, analyze and compare across different ASR 
                </p>
                <Link to="/main/jobs">
                    <Button type="primary" size="large" style={{
                        borderRadius: "20px"
                    }}>
                        Explore
                    </Button>
                </Link>
            </div>
        </div>
    );
  }
}

export default Banner;