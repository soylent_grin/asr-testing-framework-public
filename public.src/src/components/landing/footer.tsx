import React from 'react';

import mtsLogo from '../../assets/mts-logo.png';
import itmoLogo from '../../assets/itmo-logo.png';

class Footer extends React.PureComponent {
  render() {
    return (
        <div className="layout__footer">
            <h4>Powered by</h4>
            <div className="layout__footer__container">
                <a href="https://mts.ru/">
                  <img src={mtsLogo} alt=""/>
                </a>
                <a href="https://itmo.ru/">
                  <img src={itmoLogo} alt=""/>
                </a>
            </div>
            <span>
               2020&nbsp;&copy;
            </span>
        </div>
    );
  }
}

export default Footer;