import React from 'react';

import { Typography } from 'antd';

const { Text } = Typography;

const Metadata = () => {
    return (
        <div>
            <p><Text strong>Sample metadata</Text> is arbitrary list of pairs "key-value", that can be used during metric calculation.</p>
            <ul>
                <li><Text keyboard>alpha</Text> — parameter for calculating the IWER metric, type: float</li>
                <li>
                    <Text keyboard>concepts-BIO</Text> — an array of "key-value" pairs, where the key is the reference transcription token during default tokenization, and the value is a label according to the BIO / IOB markup format. The concept label list is of an enumerated type. Example:
                    <pre>
                    {JSON.stringify({
                        "concepts-BIO": {
                            "type": "array",
                            "items": [
                                {
                                    "type": "string",
                                    "enum": ["O", "B-TITLE", "I-TITLE",
                                    "B-DATE", "I-DATE", "B-LOC", "I-LOC", "B-PER",
                                    "I-PER", "B-ORG", "I-ORG", "B-MONEY", "I-MONEY",
                                    "B-LOC-ORG", "I-LOC-ORG"]
                                }
                            ]
                        }
                    }, null, 4)}
                    </pre>
                </li>
                <li>
                    <Text keyboard>concepts-indices</Text> —  is used to set the indices of concepts in the reference transcription by default; in the absence of this field, the value of the “concepts-BIO” parameter described above is used. The meaning of “concepts-indices” is a multidimensional array. Each internal array contains indices of tokens related to one concept, the elements of the internal array are of integer type. Example:
                    <pre>
                    {JSON.stringify({
                        "concepts-indices": {
                            "type": "array",
                            "items": {
                            "type": "array",
                            "items": [
                            {
                            "type": "integer"
                            }
                            ]
                            }
                        }
                    }, null, 4)}
                    </pre>
                </li>
                <li>
                    <Text keyboard>speech-repairs</Text> — specifies the indices of speech failures tokens, if any, in the reference transcription at default tokenization. The value of the “speech-repairs” key is a multidimensional array. Each internal array contains indices of tokens related to one speech failure, the elements of the internal array are of integer type. Example:
                    <pre>
                    {JSON.stringify({
                        "speech-repairs": {
                            "type": "array",
                            "items": [
                            {
                            "type": "integer"
                            }
                            ]
                        }
                    }, null, 4)}
                    </pre>
                </li>
                <li>
                    <Text keyboard>ref</Text>, <Text keyboard>tagged-ref</Text> — associated with reference transcriptions (unannotated and annotated with tags, respectively), the value of both keys is a character string. Example:
                    <pre>
                    {JSON.stringify({
                        "ref": {
                            "type": "array",
                            "items": [
                            {
                            "type": "string"
                            }
                            ]
                            },
                        "tagged-ref": {
                            "type": "array",
                            "items": [
                            {
                            "type": "string"
                            }
                            ]
                        }
                    }, null, 4)}
                    </pre>
                </li>
            </ul>
        </div>
    )
};

export default Metadata;