import React, { useEffect } from 'react';
import { Typography, Tabs, List } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { hookSelectors, loadMetrics } from '../../../ducks/jobs';

import Metadata from './metadata';

const { TabPane } = Tabs;

const { Text } = Typography;

const GeneralHelp = () => {
    return (
        <div>
            <p><Text strong>CVoAF</Text> is a software-as-a-service tool, which aims to help voice assistant developers and researchers to measure various aspects of speech-to-text recognition, performed by different <Text strong>ASR</Text> (Automatic Speech Recognition) frameworks on prepared <Text strong>dataset</Text></p>
            <p>Main concept of CVoAF platform is <Text strong>job</Text> -- pipeline of one or more steps:</p>
            <ol>
                <li>
                    Dataset <Text strong>augmentation</Text>, in which each sample in one of existing datasets is processing by configurable augmentation algorithm, which allows to add background / pause noises, change speedm reverberation level or white noise.
                </li>
                <li>
                    ASR STT <Text strong>evaluation</Text>, when samples from chosen dataset are used as input for ASR, and the output (recognized transcription) is saved for further processing.
                </li>
                <li>
                    <Text strong>Measurement</Text> of result transcriptions, which performs nesessary text preprocessing (tokenization, lemmatization, etc) and calculates various metrics by comparing reference transcription and one obtained from ASR.
                </li>
            </ol>
            <p><Text strong>Dataset</Text> consists of one or more <Text strong>samples</Text>. Each sample consists of:</p>
            <ol>
                <li>
                    <Text strong>Audio file</Text> with recorded speech
                </li>
                <li>
                    Reference <Text strong>transcription</Text>
                </li>
                <li>
                    Arbitrary <Text strong>metadata</Text>, which can be used for advanced metrics calculation.
                </li>
            </ol>
        </div>
    );
}

const Help = () => {
    const metrics = useSelector(hookSelectors.getMetrics);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadMetrics());
    }, []);
    return (
        <div>
            <Typography.Title level={2}>
                Help
            </Typography.Title>
            <Tabs defaultActiveKey="general">
                <TabPane tab="General" key="general">
                    <GeneralHelp />
                </TabPane>
                <TabPane tab="Metrics" key="metrics">
                    <List
                        itemLayout="horizontal"
                        dataSource={metrics}
                        renderItem={item => (
                            <List.Item>
                            <List.Item.Meta
                                title={
                                    <div>
                                        <span style={{
                                            fontWeight: "bold"
                                        }}>{item.key}</span>,&nbsp;{item.title}
                                    </div>
                                }
                                description={item.description}
                            />
                            </List.Item>
                        )}
                    />
                </TabPane>
                <TabPane tab="Metadata" key="metadata">
                    <Metadata />
                </TabPane>
                <TabPane tab="Datasets" key="datasets" disabled>
                    <div>
                        <p></p>
                    </div>
                </TabPane>
            </Tabs>
        </div>
    );
};

export default Help;