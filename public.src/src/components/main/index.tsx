import React, { useEffect } from 'react';
import { Route } from 'react-router';
import { useRouteMatch, Switch } from 'react-router-dom';
import { useDispatch, connect, useSelector } from 'react-redux';
import { Spin } from 'antd';
import Jobs from './jobs/';
import Soundbank from './soundbank/';
import Users from './users/';
import * as jobsDuck from '../../ducks/jobs';
import * as soundbankDuck from '../../ducks/soundbank';
import * as userDuck from '../../ducks/users';
import * as authDuck from '../../ducks/auth';
import { isAllowed } from '../../utils';

type Props = {
    isLoading: boolean
};

const Main: React.FunctionComponent<Props> = ({ isLoading }) => {
    let { url } = useRouteMatch();
    const dispatch = useDispatch();
    const user = useSelector(authDuck.hookSelectors.getUser);
    useEffect(() => {
        if (user) {
            if (isLoading) {
                dispatch(jobsDuck.loadInitialData());
                dispatch(soundbankDuck.loadFolders());
                if (isAllowed(user, "viewUsers")) {
                    dispatch(userDuck.loadUsers());
                }
            }
        }
    }, [user]);
    return (
        <Spin spinning={isLoading}>
            {
                !isLoading &&
                    <Switch>
                        <Route path={`${url}/soundbank`}>
                            <Soundbank />
                        </Route>
                        <Route path={`${url}/jobs`}>
                            <Jobs />
                        </Route>
                        <Route path={`${url}/users`}>
                            <Users />
                        </Route>
                    </Switch>
            }
        </Spin>
    );
}

function mapStateToProps({ jobs }: any) {
    return {
        isLoading: jobsDuck.selectors.isLoading(jobs)
    }
};

export default connect(mapStateToProps)(Main);