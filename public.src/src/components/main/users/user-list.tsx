import React from 'react';

import { Table, Input, Button, Space, Typography, Menu, Dropdown } from 'antd';
import {
    SearchOutlined,
    SettingOutlined
} from '@ant-design/icons';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { ColumnsType } from 'antd/lib/table';
import Avatar from '../../common/avatar';
import { isAllowed } from '../../../utils';
import EditDialog from './edit-user-dialog';

type Props = {
    users: User[],
    currentUser: User | null,
    onSave: (user: User) => Promise<any>
} & RouteComponentProps;

type State = {
    editingUser?: User,
    searchText: string,
    searchedColumn: string,
};

class List extends React.Component<Props, State> {
    state = {
        editingUser: undefined,
        searchText: "",
        searchedColumn: ""
    }
    searchInput: Input | null = null;
    getMenu(user: User) {
        return (
            <Menu onClick={(a: any) => {
                switch(a.key) {
                    case "list-jobs":
                        this.props.history.push(`/main/jobs?user=${user.id}`);
                    case "edit-user":
                        this.setState({
                            editingUser: user
                        });
                default:
                    break;
                }
            }}>
                <Menu.Item key="list-jobs">
                    View jobs
                </Menu.Item>
                {
                    isAllowed(this.props.currentUser, "editUsers") &&
                        <Menu.Item key="edit-user">
                            Edit user
                        </Menu.Item>
                }
            </Menu>
        );
    }
    getColumns(users: User[]) {
        return [
            {
                title: 'ID',
                dataIndex: 'id',
                key: 'id',
                ...this.getColumnSearchProps('id', 'id')
            },
            {
                title: 'User',
                dataIndex: 'name',
                key: 'name',
                ...this.getColumnSearchProps('name', 'name'),
                sorter: {
                    compare: (a: User, b: User) => {
                        const aVal = a.name || "";
                        const bVal = b.name || "";
                        if (aVal > bVal) {
                            return 1;
                        }
                        if (aVal < bVal) {
                            return -1;
                        }
                        return 0;
                    },
                    multiple: 2,
                },
                render(text: string, data: User) {
                    return (
                        <span>
                            <Space>
                                <Avatar user={data} />
                                <span>
                                    {data!.name}
                                </span>
                            </Space>
                        </span>
                    )
                }
            },
            {
                title: 'Group',
                dataIndex: 'group',
                key: 'group',
                ...this.getColumnSearchProps('group', 'group'),
                sorter: {
                    compare: (a: User, b: User) => {
                        const aVal = a.group || "";
                        const bVal = b.group || "";
                        if (aVal > bVal) {
                            return 1;
                        }
                        if (aVal < bVal) {
                            return -1;
                        }
                        return 0;
                    },
                    multiple: 2,
                },
                render(text: string, data: User) {
                    return (
                        data && data.group
                    )
                }
            },
            {
                title: 'Role',
                dataIndex: 'role',
                key: 'role',
                ...this.getColumnSearchProps('role', 'role'),
                sorter: {
                    compare: (a: User, b: User) => {
                        const aVal = a.role || "";
                        const bVal = b.role || "";
                        if (aVal > bVal) {
                            return 1;
                        }
                        if (aVal < bVal) {
                            return -1;
                        }
                        return 0;
                    },
                    multiple: 3,
                }
            },
            {
                title: 'Actions',
                key: 'operation',
                width: 60,
                render: (data: User) => {
                    const iconStyle = {
                        fontSize: "18px"
                    };
                    return (
                        <div style={{
                            textAlign: "center"
                        }}>
                            <Dropdown overlay={this.getMenu(data)}>
                                <a>
                                    <SettingOutlined style={iconStyle} />
                                </a>
                            </Dropdown>
                        </div>
                    );
                },
            },
        ];
    }
    getColumnSearchProps = (dataIndex: string, title: string) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }: any) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search by ${title}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined />}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button
                    onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}
                >
                    Clear
                </Button>
            </div>
        ),
        filterIcon: (filtered: any) =>
            <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value: string, record: any) => {
            return (record[dataIndex] || "")
                    .toString()
                    .toLowerCase()
                    .includes(value.toLowerCase())
        },
        onFilterDropdownVisibleChange: (visible: any) => {
            if (visible) {
                setTimeout(() => this.searchInput?.select());
            }
        },
        render: (text: string) => text,
    });

    handleSearch = (selectedKeys: string[], confirm: any, dataIndex: string) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = (clearFilters: any) => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    render() {
        const { users } = this.props;
        const { editingUser } = this.state;
        return (
            <React.Fragment>
                <Typography.Title level={3}>
                    <Space>
                        <span>
                            Users
                        </span>
                    </Space>
                </Typography.Title>
                <Table
                    bordered
                    size="small"
                    columns={this.getColumns(users) as ColumnsType<User>}
                    rowKey="id"
                    dataSource={users}
                />
                {
                    editingUser &&
                        <EditDialog
                            user={editingUser as any}
                            onSave={(user) => {
                                return this.props.onSave(user).then(() => {
                                    this.setState({
                                        editingUser: undefined
                                    });
                                });
                            }}
                            onCancel={() => {
                                this.setState({
                                    editingUser: undefined
                                })
                            }}
                        />
                }
            </React.Fragment>
        );
    }
}

export default withRouter(List);
