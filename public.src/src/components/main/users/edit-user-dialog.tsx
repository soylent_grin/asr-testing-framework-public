import React, { useState } from 'react';

import { Modal, Button, Form, Select, Input } from 'antd';
import { getRoles } from '../../../utils';
import { assign } from 'lodash-es';
import { useForm } from 'antd/lib/form/Form';

type Props = {
    onSave: (user: User) => Promise<any>,
    onCancel: () => void,
    user: User
};

const EditUserDialog: React.FunctionComponent<Props> = (props) => {
    const { onSave, onCancel, user } = props;
    const [ isSaving, setIsSaving ] = useState(false);
    const roles = getRoles();
    const [form] = useForm();
    return (
        <Modal
            title="Edit user"
            visible
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancel
                </Button>,
                <Button
                    key="save"
                    type="primary"
                    loading={isSaving}
                    onClick={() => {
                        setIsSaving(true);
                        const cb = () => {
                            setIsSaving(false);
                        };
                        onSave(assign({}, user, {
                            role: form.getFieldValue("role"),
                            group: form.getFieldValue("group")
                        })).then(cb, cb);
                    }}
                >
                    Save
                </Button>,
            ]}
        >
            <Form
                initialValues={{
                    role: user.role
                }}
                form={form}
            >
                <Form.Item
                    label="Group"
                    name="group"
                >
                    <Input
                        value={user.group}
                    />
                </Form.Item>
                <Form.Item
                    label="Role"
                    name="role"
                >
                    <Select
                        options={Object.keys(roles).map((k) => {
                            return {
                                value: k,
                                label: roles[k].name
                            };
                        })}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default EditUserDialog;