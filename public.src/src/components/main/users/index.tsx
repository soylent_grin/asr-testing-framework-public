import React, { FunctionComponent, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import UserList from './user-list';

import * as duck from '../../../ducks/users';
import * as authDuck from '../../../ducks/auth';

type Props = {};

const Users: FunctionComponent<Props> = () => {
    const users = useSelector(duck.hookSelectors.getUsers);
    const currentUser = useSelector(authDuck.hookSelectors.getUser);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(duck.loadUsers());
    }, []);
    return (
        <UserList
            users={users}
            currentUser={currentUser}
            onSave={(u: User) => {
                return dispatch(
                    duck.saveUser(u)
                ) as unknown as Promise<any>;
            }}
        />
    );
}

export default Users;