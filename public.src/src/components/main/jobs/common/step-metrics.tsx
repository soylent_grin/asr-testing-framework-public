import React, { useState, useImperativeHandle } from 'react';

import { Form, Select, Divider, Tooltip } from 'antd';
import { CheckSquareTwoTone } from '@ant-design/icons';

import ResultMeasure from './result-measure';
import InputJobSelector from '../common/input-job-selector';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

type MetricSelectionProps = {
    metrics: AsrJobMetric[],
    selectedMetrics: string[],
    ref: any
}

const MetricSelection: React.FunctionComponent<MetricSelectionProps> = React.forwardRef(({ metrics, selectedMetrics }, ref) => {
    const [ currentMetrics, setSelectedMetrics ] = useState<string[]>(selectedMetrics);

    useImperativeHandle(ref, () => ({
        getData() {
            return {
                metrics: currentMetrics
            };
        }
    }));

    return (
        <Form
            {...layout}
            initialValues={{ 
                remember: false,
                metrics: selectedMetrics
            }}
        >
            <Form.Item
                label={
                    <div>
                        Selected metrics
                        {/* <Tooltip title="Select all metrics">
                            <CheckSquareTwoTone onClick={() => {
                                setSelectedMetrics(metrics.map((m) => {
                                    return m.key;
                                }));
                            }}/>
                        </Tooltip> */}
                    </div>
                }
                name="metrics"
                rules={[{ required: false }]}
            >
                <Select
                    mode="multiple"
                    value={currentMetrics}
                    placeholder="Please select"
                    allowClear
                    maxTagCount={5}
                    onChange={(e) => {
                        debugger;
                        setSelectedMetrics(e);
                    }}
                    options={metrics.map((m) => {
                        return {
                            label: m.title,
                            value: m.key
                        }
                    })}
                />
            </Form.Item>
        </Form>
    );
});

type TagSelectionProps = {
}

const TagSelection: React.FunctionComponent<TagSelectionProps> = () => {
    return (
        <div>not implemented</div>
    );
}

type Props = {
    metrics: AsrJobMetric[],
    tags: string[],
    jobs: AsrJob[],
    inputType?: "current" | "previous",
    inputJob?: string,
    config?: any,
    mode: "create" | "view",
    stepId?: string,
    ref: any
};

const StepMetrics: React.FunctionComponent<Props> = React.forwardRef<any, Props>(({ jobs, metrics, stepId, inputJob, inputType, config, mode } , ref) => {
    const jobSelectNode = React.useRef<any>(null);
    const metricSelectNode = React.useRef<any>(null);
    useImperativeHandle(ref, () => ({
        getData() {
            const jobData = jobSelectNode.current.getData();
            const metricData = metricSelectNode.current.getData();
            return {
                inputType: jobData.inputType,
                inputJob: jobData.inputJob,
                inputJobAsrKey: jobData.inputJobAsrKey,
                config: {
                    metrics: metricData.metrics,
                    tagGroups: []
                }
            };
        }
    }));
    return (
        <div>
            {
                stepId && (mode === "view") &&
                    <ResultMeasure stepId={stepId} />
            }
            <Divider>Configuration (leave empty to skip step)</Divider>
            <InputJobSelector
                jobs={jobs}
                ref={jobSelectNode}
                inputJob={inputJob}
                inputType={inputType}
                stepType="test"
            />
            <MetricSelection
                metrics={metrics}
                selectedMetrics={config && config.metrics}
                ref={metricSelectNode}
            />
        </div>
    );
});

export default StepMetrics;