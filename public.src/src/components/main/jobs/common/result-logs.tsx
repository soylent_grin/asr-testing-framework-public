import React, { useState, useEffect } from 'react';

import { Modal, Spin } from 'antd';

import { notify } from "../../../../utils";

import * as api from '../../../../api/jobs';

type Props = {
    stepId: string,
    handleClose: () => void
};

const ResultLogs: React.FunctionComponent<Props> = ({ stepId, handleClose }) => {
    const [ logs, setLogs ] = useState<string[] | null>(null);
    useEffect(() => {
        api.loadStepLogs(stepId).then((res) => {
            setLogs(res);
        }, () => {
            setLogs([]);
        });
    }, []);
    return (
        <Modal
            title="Logs"
            visible
            onCancel={handleClose}
            footer={null}
        >
            <Spin spinning={logs === null}>
                {
                    (logs && logs.length > 0) ?
                        <pre style={{
                            fontFamily: "monospace",
                            fontSize: "12px"
                        }}>{logs}</pre> :
                        <span>Not found any logs</span>
                }
            </Spin>
        </Modal>
    );
};

export default ResultLogs;