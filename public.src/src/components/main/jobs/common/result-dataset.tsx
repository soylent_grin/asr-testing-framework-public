import React, { useState, useEffect } from 'react';

import { Form, Divider } from 'antd';
import * as api from '../../../../api/jobs';
import { notify } from '../../../../utils';
import FolderList from '../../soundbank/folder-list';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

type Props = {
    stepId: string,
    folders: SoundbankFolder[]
};

export const ResultDatasetFolder: React.FunctionComponent<Props> = ({ stepId, folders }) => {
    const [ folder, setFolder ] = useState<SoundbankFolder[] | null>(null);
    useEffect(() => {
        api.loadStepResult(stepId).then((res) => {
            if (res && res.result && res.result.folder) {
                const foundFolder = folders.filter((f) => {
                    return f.key === res.result.folder; 
                });
                foundFolder && setFolder(foundFolder)
            }
        }, () => {
            notify("error", "Failed to load step results");
        });
    }, [folders]);
    return (
        folder && 
            <FolderList
                canAugmentate={false}
                isPrivate
                folders={folder}
                readonly
            />
    );
};

const ResultDataset: React.FunctionComponent<Props> = ({ stepId, folders }) => {
    return (
        <React.Fragment>
            <Divider>Results</Divider>
            <Form
                {...layout}
            >
                <Form.Item
                    label="Dataset"
                >
                    <ResultDatasetFolder
                        stepId={stepId}
                        folders={folders}
                    />
                </Form.Item>
            </Form>
        </React.Fragment>
    );
};

export default ResultDataset;