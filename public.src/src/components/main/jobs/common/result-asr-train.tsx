import React, { useState } from 'react';

import { Form, Button, Divider } from 'antd';

import ResultLogs from './result-logs';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

type Props = {
    stepId: string
};

const ResultAsrTrain: React.FunctionComponent<Props> = ({ stepId }) => {
    const [ logOpened, setLogOpened ] = useState(false);
    return (
        <React.Fragment>
            <Divider>Results</Divider>
            <Form
                {...layout}
                initialValues={{
                    remember: false
                }}
            >
                <Form.Item
                    label="Logs"
                >
                    <Button type="link" onClick={() => { setLogOpened(true); }}>show</Button>
                </Form.Item>
            </Form>
            {
                logOpened &&
                    <ResultLogs stepId={stepId} handleClose={() => { setLogOpened(false); }} />
            }
        </React.Fragment>
    );
};

export default ResultAsrTrain;