import React, { useState, useImperativeHandle } from 'react';

import { find } from 'lodash-es';

import { Form, Select, Divider } from 'antd';

import ParamList from './param-list';
import ResultAsrTrain from './result-asr-train';
import SampleSelector from './sample-selector';

type Props = {
    folders: SoundbankFolder[],
    executors: AsrExecutor[],
    asrKey?: string,
    config?: any,
    stepId?: string,
    samples?: AsrJobStepSamples,
    mode: "create" | "view",
    ref: any
};

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const StepAsrTrain: React.FunctionComponent<Props> = React.forwardRef(({ executors, asrKey, config, stepId, mode, samples, folders }, ref) => {
    const [ selectedExecutor, setSelectedExecutor ] = useState<string | undefined>(asrKey);
    const [ selectedSamples, setSelectedSamples ] = useState<AsrJobStepSamples | undefined>(samples);
    const currentExecutor = find(executors, (e: AsrExecutor) => {
        return e.key === selectedExecutor;
    });
    const paramListNode: any = React.createRef();

    useImperativeHandle(ref, () => ({
        getData() {
            return {
                asrKey: selectedExecutor,
                config: paramListNode.current && paramListNode.current.getData(),
                samples: selectedSamples
            };
        }
    }));

    return (
        <React.Fragment>
            {
                stepId && (mode === "view") &&
                    <ResultAsrTrain stepId={stepId} />
            }
            <Divider>Configuration (leave empty to skip step)</Divider>
            <Form
                {...layout}
                initialValues={{
                    remember: false,
                    asrKeyTrain: asrKey,
                    samples: selectedSamples
                }}
            >
               <SampleSelector
                    folders={folders}
                    value={selectedSamples}
                    onChange={(value) => {
                        setSelectedSamples(value);
                    }}
                />
                <Form.Item
                    label="ASR Executor"
                    name="asrKeyTrain"
                    rules={[{ required: false }]}
                >
                    <Select
                        value={selectedExecutor}
                        placeholder="Please select"
                        allowClear
                        onChange={(e) => {
                            setSelectedExecutor(e);
                        }}
                        options={executors.filter((e) => {
                            return e.capabilities.train;
                        }).map((e) => {
                            return {
                                label: e.title,
                                value: e.key
                            }
                        })}
                    />
                </Form.Item>
            </Form>
            {
                currentExecutor &&
                    <ParamList
                        ref={paramListNode}
                        key={selectedExecutor}
                        params={currentExecutor.params.filter((p) => {
                            return p.scope && p.scope.indexOf("train") > -1;
                        })}
                        values={config}
                    />
            }
        </React.Fragment>
    );
});

export default StepAsrTrain;