import React, { useState, useRef, useEffect, useImperativeHandle } from 'react';

import ParamList from './param-list';
import * as soundbankApi from '../../../../api/soundbank';

type Props = {
    folder?: SoundbankFolder,
    values?: any
}

const AugmentateDialog = React.forwardRef(({ values, folder }: Props, ref) => {
    const [ isLoadError, setLoadError ] = useState(false);
    const [ params, setParams ] = useState<AsrExecutorParam[]>([]);
    useEffect(() => {
        soundbankApi.loadAugmentatorParams().then((res) => {
            setParams([
                {
                    key: "title",
                    title: "Folder name",
                    paramType: "string",
                    defaultValue: folder && `${folder.title}-augmented`,
                    description: "Name of new private folder (must be unique)"
                },
                ...res
            ]);
        }, () => {
            setLoadError(true);
        });
    }, []);
    const paramListNode: any = useRef(null);

    useImperativeHandle(ref, () => ({
        getData() {
            return paramListNode.current && paramListNode.current.getData();
        }
    }));

    return (
        isLoadError ?
            <p>Failed to load augmentator params</p> :
            (
                params.length > 0 ?
                    <ParamList
                        values={values}
                        ref={paramListNode}
                        params={params}
                    /> :
                    <p>Loading...</p>
            )
    );
});

export default AugmentateDialog;