import React, { useState, useImperativeHandle } from 'react';

import { find } from 'lodash-es';

import { Form, Select, Divider, TreeSelect } from 'antd';

import ParamList from './param-list';
import ResultAsrTest from './result-asr-test';
import InputJobSelector from '../common/input-job-selector';
import SampleSelector from './sample-selector';

type Props = {
    jobs: AsrJob[],
    folders: SoundbankFolder[],
    executors: AsrExecutor[],
    inputJob?: string,
    inputType?: "current" | "previous" | "default",
    asrKey?: string,
    config?: any,
    stepId?: string,
    samples?: AsrJobStepSamples,
    mode: "create" | "view",
    ref: any
};

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const StepAsrTest: React.FunctionComponent<Props> = React.forwardRef(({ executors, asrKey, config, stepId, mode, samples, folders, jobs, inputJob, inputType }, ref) => {
    const [ selectedExecutor, setSelectedExecutor ] = useState<string | undefined>(asrKey);
    const [ selectedSamples, setSelectedSamples ] = useState<AsrJobStepSamples | undefined>(samples);
    const currentExecutor = find(executors, (e: AsrExecutor) => {
        return e.key === selectedExecutor;
    });
    const paramListNode: any = React.createRef();
    const inputJobNode: any = React.createRef();

    useImperativeHandle(ref, () => ({
        getData() {
            const inputJobData = inputJobNode.current && inputJobNode.current.getData();
            return {
                asrKey: selectedExecutor,
                config: paramListNode.current && paramListNode.current.getData(),
                samples: selectedSamples,
                inputType: inputJobData && inputJobData.inputType,
                inputJob: inputJobData && inputJobData.inputJob,
                defaultModelName: inputJobData && inputJobData.defaultModelName
            };
        }
    }));

    return (
        <React.Fragment>
            {
                stepId && (mode === "view") &&
                    <ResultAsrTest stepId={stepId} />
            }
            <Divider>Configuration (leave empty to skip step)</Divider>
            <Form
                {...layout}
                initialValues={{
                    remember: false,
                    asrKeyTest: asrKey,
                    samples: selectedSamples
                }}
            >
                <SampleSelector
                    folders={folders}
                    value={selectedSamples}
                    onChange={(value) => {
                        setSelectedSamples(value);
                    }}
                />
                <Form.Item
                    label="ASR Executor"
                    name="asrKeyTest"
                    rules={[{ required: false }]}
                >
                    <Select
                        value={selectedExecutor}
                        placeholder="Please select"
                        allowClear
                        onChange={(e) => {
                            setSelectedExecutor(e);
                        }}
                        options={executors.map((e) => {
                            return {
                                label: e.title,
                                value: e.key
                            }
                        })}
                    />
                </Form.Item>
            </Form>
            {
                currentExecutor &&
                    <ParamList
                        ref={paramListNode}
                        key={selectedExecutor}
                        params={currentExecutor.params.filter((p) => {
                            return p.scope && p.scope.indexOf("test") > -1;
                        })}
                        values={config}
                    />
            }
            {
                currentExecutor && currentExecutor.capabilities && currentExecutor.capabilities.train &&
                    
                    <InputJobSelector
                        jobs={jobs}
                        ref={inputJobNode}
                        inputJob={inputJob}
                        inputType={inputType}
                        stepType="train"
                        asrKey={currentExecutor.key}
                        label="Model"
                        withDefaultModels
                    />
            }
        </React.Fragment>
    );
});

export default StepAsrTest;