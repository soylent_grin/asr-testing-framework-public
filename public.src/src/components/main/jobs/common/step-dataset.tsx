import React, { useState, useImperativeHandle } from 'react';

import { Form, Switch, Select, Divider } from 'antd';

import AugmentationParamList from './augmentation-param-list';
import ResultDataset from './result-dataset';

type Props = {
    folders: SoundbankFolder[],
    stepId?: string,
    mode: "create" | "view",
    isPrivateFolderLimitExceeded: boolean,
    config?: {
        folder: string,
        augmentationConfig?: any
    }
};

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const StepDataset: React.FunctionComponent<Props> = React.forwardRef(({ config, folders, stepId, mode, isPrivateFolderLimitExceeded }, ref) => {
    const [ enabled, setEnabled ] = useState(config && !!config.folder);
    const [ folder, setFolder ] = useState(config && config.folder);

    const paramListNode: any = React.createRef();

    useImperativeHandle(ref, () => ({
        getData() {
            const params = paramListNode.current && paramListNode.current.getData() || {};
            Object.keys(params).forEach((k) => {
                params[k] = params[k].toString();
            });
            const data = {
                config: folder && {
                    folder,
                    augmentationConfig: {
                        title: params.title,
                        params
                    }
                }
            };

            return data;
        }
    }));

    return (
        <React.Fragment>
            {
                stepId && (mode === "view") &&
                    <ResultDataset stepId={stepId} folders={folders} />
            }
            <Divider>Configuration (leave empty to skip step)</Divider>
            <Form
                {...layout}
                initialValues={{
                    folder,
                    enabled
                }}
            >
                <Form.Item
                    label="Augmentate new dataset"
                >
                    <Switch
                        checked={enabled}
                        disabled={isPrivateFolderLimitExceeded}
                        title={
                            isPrivateFolderLimitExceeded ?
                                "Private folders limit exceeded, could not augmentate and create new dataset" :
                                ""
                        }
                        onChange={(e) => {
                            setEnabled(e);
                        }}
                    />
                </Form.Item>
                {
                    enabled &&
                        <Form.Item
                            label="Source folder"
                            name="folder"
                        >
                            <Select
                                options={folders.map((f) => {
                                    return {
                                        value: f.key,
                                        label: f.title
                                    };
                                })}
                                value={folder}
                                onChange={(e) => {
                                    setFolder(e);
                                }}
                            />
                        </Form.Item>
                }
            </Form>
            {
                enabled &&
                    <AugmentationParamList
                        ref={paramListNode}
                        values={config?.augmentationConfig}
                    />
            }
        </React.Fragment>
    );
});

export default StepDataset;