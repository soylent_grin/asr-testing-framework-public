import React from 'react';
import ReactAudioPlayer from 'react-audio-player';

const AudioPlayer: React.FunctionComponent<{sampleId: string}> = ({ sampleId }: any) => {
    return <ReactAudioPlayer
        style={{
            height: "30px"
        }}
        src={`/api/soundbank/load/${sampleId}`}
        controls
    />;
}

export default AudioPlayer;