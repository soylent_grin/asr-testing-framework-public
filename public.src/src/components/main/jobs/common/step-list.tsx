import React, { useImperativeHandle, useState, useEffect } from 'react';
import { Steps } from 'antd';

import DatasetStep from './step-dataset';
import AsrTrainStep from './step-asr-train';
import AsrTestStep from './step-asr-test';
import MetricStep from './step-metrics';
import { find } from 'lodash-es';

import { useSelector } from 'react-redux';
import * as soundbankDuck from '../../../../ducks/soundbank';

const { Step } = Steps;

type Props = {
    folders: SoundbankFolder[],
    executors: AsrExecutor[],
    metrics: AsrJobMetric[],
    currentJob?: AsrJob,
    jobs: AsrJob[],
    tags: string[],
    ref: any,
    mode: "create" | "view"
}

function adaptStepStatus(status: string): any {
    switch(status) {
    case "success":
        return "finish";
    case "waiting":
        return "wait";
    case "running":
        return "process";
    default:
        return status;
    }
}

function fromJobToProps(job: AsrJob): any {
    const data: any = {
        datasetData: {},
        asrTrainData: {},
        asrTestData: {},
        metricsData: {}
    };

    const datasetStep = find(job.steps, (s) => {
        return s.stepType === "dataset";
    });
    const asrTrainStep = find(job.steps, (s) => {
        return s.stepType === "train";
    });
    const asrTestStep = find(job.steps, (s) => {
        return s.stepType === "test";
    });
    const measureStep = find(job.steps, (s) => {
        return s.stepType === "measure";
    });

    if (datasetStep) {
        data.datasetData = {
            stepId: datasetStep.id,
            status: datasetStep.status,
            config: datasetStep.config
        };
    }
    if (asrTrainStep) {
        data.asrTrainData = {
            stepId: asrTrainStep.id,
            asrKey: job.asrKey,
            config: asrTrainStep.config,
            status: asrTrainStep.status,
            samples: asrTrainStep.samples
        };
    }
    if (asrTestStep) {
        data.asrTestData = {
            stepId: asrTestStep.id,
            asrKey: job.asrKey,
            config: asrTestStep.config,
            status: asrTestStep.status,
            samples: asrTestStep.samples,
            inputType: asrTestStep.inputType,
            inputJob: asrTestStep.inputJob,
            defaultModelName: asrTestStep.defaultModelName
        };
    }
    if (measureStep) {
        data.metricsData = {
            stepId: measureStep.id,
            inputType: measureStep.inputType,
            inputJob: measureStep.inputJob,
            config: measureStep.config,
            status: measureStep.status
        }
    }

    return data;
}

function fromPropsToJob({ datasetData, asrTrainData, asrTestData, metricsData }: any): AsrJob {
    const job: AsrJob = {
        steps: []
    };
    if (datasetData && datasetData.config && datasetData.config.folder) {
        const datasetStep: AsrJobStep = {
            index: 1,
            inputType: "current",
            stepType: "dataset",
            config: datasetData.config,
            samples: {
                type: 'current',
                list: []
            }
        };
        job.steps.push(datasetStep);
    }
    if (asrTrainData && asrTrainData.asrKey) {
        job.asrKey = asrTrainData.asrKey;
        const testStep: AsrJobStep = {
            index: 2,
            inputType: "default",
            stepType: "train",
            config: asrTrainData.config,
            samples: asrTrainData.samples
        };
        job.steps.push(testStep);
    }
    if (asrTestData && asrTestData.asrKey) {
        job.asrKey = asrTestData.asrKey;
        const testStep: AsrJobStep = {
            index: 3,
            inputType: asrTestData.inputType || "default",
            inputJob: asrTestData.inputJob,
            defaultModelName: asrTestData.defaultModelName,
            stepType: "test",
            config: asrTestData.config,
            samples: asrTestData.samples
        };
        job.steps.push(testStep);
    }
    if (metricsData && 
        metricsData.config && 
        metricsData.config.metrics &&
        metricsData.config.metrics.length > 0) {
        const metricsStep: AsrJobStep = {
            index: 4,
            inputType: metricsData.inputType,
            inputJob: metricsData.inputJob,
            stepType: "measure",
            config: metricsData.config,
            samples: {
                type: 'current',
                list: []
            }
        };
        if (!job.asrKey && metricsData.inputJobAsrKey) {
            job.asrKey = metricsData.inputJobAsrKey;
        }
        job.steps.push(metricsStep);
    }
    if (!job.asrKey) {
        job.asrKey = "";
    }
    return job;
}

const StepList: React.FunctionComponent<Props> = React.forwardRef((props, ref) => {
    const [hasTrainAsr, setHasTrainAsr] = useState(props.executors.filter((e) => {
        return e.capabilities.train;
    }).length > 0);
    const [current, setCurrent] = useState(0);

    const isPrivateFolderLimitExceeded = useSelector(soundbankDuck.hookSelectors.isPrivateFoldersLimitExceeded);

    useEffect(() => {
        setHasTrainAsr(props.executors.filter((e) => {
            return e.capabilities.train;
        }).length > 0);
    }, [props.executors]);

    useImperativeHandle(ref, () => {
        return {
            getData: () => {
                const datasetData = datasetNode.current && datasetNode.current!.getData();
                const asrTrainData = asrTrainNode.current && asrTrainNode.current!.getData();
                const asrTestData = asrTestNode.current && asrTestNode.current!.getData();
                const metricsData = metricsNode.current && metricsNode.current!.getData();
                const job = fromPropsToJob({ datasetData, asrTrainData, asrTestData, metricsData });
                return job;
            }
        };
    })

    const datasetNode = React.useRef<any>();
    const asrTrainNode = React.useRef<any>();
    const asrTestNode = React.useRef<any>();
    const metricsNode = React.useRef<any>();

    let datasetData: any = {}, asrTrainData: any = {}, asrTestData: any = {}, metricsData: any = {};
    if (props.currentJob) {
        const parsedProps = fromJobToProps(props.currentJob);
        datasetData = parsedProps.datasetData;
        asrTrainData = parsedProps.asrTrainData;
        asrTestData = parsedProps.asrTestData;
        metricsData = parsedProps.metricsData;
    }

    type BlockType = any;

    const blocks: BlockType[] = [
        {
            key: "dataset",
            step: <Step
                key="dataset"
                title="Dataset"
                status={props.mode == "view" && datasetData && adaptStepStatus(datasetData.status)}
            />,
            content: <DatasetStep
                key="dataset"
                ref={datasetNode}
                {...datasetData}
                mode={props.mode}
                folders={props.folders}
                isPrivateFolderLimitExceeded={isPrivateFolderLimitExceeded}
            />
        },
        hasTrainAsr ? {
            key: "train",
            step: <Step key="train" title="ASR train" status={props.mode == "view" && asrTrainData && adaptStepStatus(asrTrainData.status)} />,
            content: <AsrTrainStep
                key="train"
                executors={props.executors}
                ref={asrTrainNode}
                {...asrTrainData}
                mode={props.mode}
                folders={props.folders}
            />
        } : undefined,
        {
            key: "test",
            step: <Step key="test" title="ASR test" status={props.mode == "view" && asrTestData && adaptStepStatus(asrTestData.status)} />,
            content: <AsrTestStep
                key="test"
                executors={props.executors}
                ref={asrTestNode}
                {...asrTestData}
                mode={props.mode}
                jobs={props.jobs}
                folders={props.folders}
            />
        },
        {
            key: "measure",
            step: <Step key="measure" title="Metrics" status={props.mode == "view" && metricsData && adaptStepStatus(metricsData.status)} />,
            content: <MetricStep
                key="measure"
                metrics={props.metrics}
                tags={props.tags}
                jobs={props.jobs}
                ref={metricsNode}
                {...metricsData}
                mode={props.mode}
            />
        }
    ].filter(a => a).map(a => a as BlockType);

    return (
        <React.Fragment>
            <Steps current={current} onChange={setCurrent} type="navigation">
                {blocks.map(b => b.step)}
            </Steps>
            <div style={{
                marginTop: "15px"
            }}>
                {
                    blocks.map((b, index) => {
                        return (<div style={{
                            display: current === index ? "block" : "none"
                        }} key={b.key}>
                            {b.content}
                        </div>)
                    })
                }
            </div>
        </React.Fragment>
    );
});

export default StepList;