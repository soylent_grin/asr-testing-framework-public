import React, { useState, useImperativeHandle, useEffect } from 'react'

import { Form, Select } from 'antd';

import { find } from 'lodash-es';
import * as api from '../../../../api/jobs';

import { renderJobName } from '../../../../utils';
import { useForm } from 'antd/lib/form/Form';

type Props = {
    jobs: AsrJob[],
    inputJob?: string,
    inputType?: "current" | "previous" | "default",
    stepType: "train" | "test" | "measure",
    asrKey?: string,
    label?: string,
    defaultModelName?: string,
    withDefaultModels?: boolean
};

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const InputJobSelector = React.forwardRef((props: Props, ref) => {
    const [form] = useForm();
    const [ inputType, setInputType ] = useState(props.inputType || (
        props.withDefaultModels ? "default" : "current"
    ));
    const [ inputJob, setInputJob ] = useState(props.inputJob || null);
    const [ defaultModels, setDefaultModels ] = useState<AsrModel[]>([]);
    const [ currentDefaultModel, setCurrentDefaultModel ] = useState(props.defaultModelName);

    useEffect(() => {
        if (inputType === "default") {
            api.loadDefaultModels(props.asrKey!).then((res) => {
                setDefaultModels(res);
                if (res.length > 0) {
                    setCurrentDefaultModel(res[0].name);
                    form.setFieldsValue({
                        defaultModelName: res[0].name
                    });
                }
            });
        }
    }, [inputType]);

    useImperativeHandle(ref, () => ({
        getData() {
            return {
                inputType,
                inputJob,
                inputJobAsrKey: inputJob && find(props.jobs, (j) => {
                    return j.id === inputJob;
                })?.asrKey,
                defaultModelName: currentDefaultModel
            };
        }
    }));

    return (
        <Form
            {...layout}
            form={form}
            initialValues={{
                inputType,
                inputJob
            }}
        >
            <Form.Item
                label={props.label || "Get output from job"}
                name="inputType"
                rules={[{ required: false }]}
            >
                <Select
                    value={inputType}
                    placeholder="Please select"
                    onChange={(e) => {
                        setInputType(e);
                    }}
                    options={[
                        props.withDefaultModels && {
                            label: "Default",
                            value: "default"
                        },
                        {
                            label: "Current",
                            value: "current"
                        },
                        {
                            label: "Previous",
                            value: "previous"
                        }
                    ].filter(a => a).map(a => a as { label: string, value: string })}
                />
            </Form.Item>
            {
                inputType === "default" &&
                    <Form.Item
                        label="Select default model"
                        name="defaultModelName"
                        rules={[{ required: false }]}
                    >
                        <Select
                            maxTagCount={10}
                            value={currentDefaultModel}
                            placeholder="Please select"
                            onChange={(e) => {
                                setCurrentDefaultModel(e);
                            }}
                            options={defaultModels.map((m) => {
                                return {
                                    label: m.name,
                                    value: m.name
                                }
                            })}
                        />
                    </Form.Item>
            }
            {
                inputType === "previous" &&
                    <Form.Item
                        label="Previous job (must be succeeded)"
                        name="inputJob"
                        rules={[{ required: false }]}
                    >
                        <Select
                            maxTagCount={10}
                            value={inputJob || undefined}
                            placeholder="Please select"
                            onChange={(e) => {
                                setInputJob(e);
                            }}
                            options={props.jobs.filter((j: AsrJob) => {
                                return j.steps.filter((s) => {
                                    return s.stepType === props.stepType;
                                }).length > 0 && j.status === "success" && (!props.asrKey || props.asrKey === j.asrKey);
                            }).map((j) => {
                                return {
                                    label: renderJobName(j),
                                    value: j.id!
                                }
                            })}
                        />
                    </Form.Item>
            }
        </Form>
    );
});

export default InputJobSelector;