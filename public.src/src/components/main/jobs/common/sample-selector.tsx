import React, { useState, useImperativeHandle, useEffect } from 'react';

import { Form, Select, TreeSelect } from 'antd';

type Props = {
    folders: SoundbankFolder[],
    onChange: (s: AsrJobStepSamples) => void,
    value?: AsrJobStepSamples,
}

const SampleSelector = React.forwardRef(({ value, folders, onChange }: Props, ref) => {
    const [ currentType, setType ] = useState(value && value.type || "previous");
    const [ currentList, setList ] = useState(value && value.list || []);

    useImperativeHandle(ref, () => ({
        getData() {
            return {
                type: currentType,
                list: currentList
            };
        }
    }));

    useEffect(() => {
        onChange({
            type: currentType,
            list: currentList
        });
    }, [currentList, currentType]);

    return (
        <>
            <Form.Item
                label="Get samples from"
                name="type"
                initialValue={currentType}
            >
                <Select
                    value={currentType}
                    placeholder="Please select"
                    onChange={(e) => {
                        setType(e);
                    }}
                    options={[
                        {
                            label: "Dataset, created by this job on previous step",
                            value: "current"
                        },
                        {
                            label: "Existing dataset",
                            value: "previous"
                        }
                    ]}
                />
            </Form.Item>
            {
                currentType === "previous" &&
                    <Form.Item
                        label="Samples"
                        name="list"
                        initialValue={currentList}
                    >
                        <TreeSelect
                            treeData={folders}
                            value={currentList}
                            maxTagCount={10}
                            onChange={(value) => {
                                setList(value);
                            }}
                            treeCheckable
                            showCheckedStrategy={TreeSelect.SHOW_CHILD}
                            placeholder="Please select"
                            allowClear
                        />
                    </Form.Item>
            }
        </>
    );
});

export default SampleSelector;