import React, { useImperativeHandle, useState, useMemo, useRef } from 'react';
import { Form, Input, InputNumber, Checkbox, Select, Tooltip, Button, Space } from 'antd';
import { saveAs } from 'file-saver';
import {
    DownloadOutlined,
    UploadOutlined,
    InfoCircleOutlined
} from '@ant-design/icons';
import { notify } from '../../../../utils';
import { FormInstance } from 'antd/lib/form';
import { assign } from 'lodash-es';

type ParamProps = {
    param: AsrExecutorParam,
    value: any
}

const { Option } = Select;

const Param: React.FunctionComponent<ParamProps> = ({ param, value }) => {
    let content = null;
    let label = param.title;
    let isCheckbox = false;
    const { key } = param; 
    if (param) {
        label = param.title;
        switch (param.paramType) {
            case "string":
                content = <Input />;
                break;
            case "double":
                content = <InputNumber />;
                break;
            case "boolean":
                isCheckbox = true;
                const props = {
                    // onChange: this.handleChange(key, "boolean")
                };
                content = <Checkbox {...props} />;
                break;
            case "text":
                content = <Input.TextArea />;
                break;
            case "select":
                content = (
                    <Select
                        placeholder="Select value"
                        showSearch
                        filterOption={(input: any, option: any) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {
                            (param.options || []).map((o) => {
                                return (
                                    <Option key={o.key} value={o.key}>{o.title || o.key}</Option>
                                );
                            })
                        }
                    </Select>
                );
                break;
            default:
                content = <Input />;
                break;
        }
    } else {
        content = <Input />;
    }
    let labelNode;
    if (param.description) {
        labelNode = (<Tooltip title={param.description}>
            <span style={{
                borderBottom: "1px dashed rgb(0,0,0,.3)",
                cursor: "help"
            }}>{label}</span>
        </Tooltip>);
    } else {
        labelNode = <span>{label}</span>;
    }
    return (
        <Form.Item
            key={key}
            label={labelNode}
            name={key}
            valuePropName={isCheckbox ? "checked" : "value"}
        >
            {content}
        </Form.Item>
    );
};

type Props = {
    params: AsrExecutorParam[],
    values?: Record<string, string>,
    ref?: any
};

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const ParamList: React.FunctionComponent<Props> = React.forwardRef(({ params, values }, ref) => {
    const initialValues = useMemo(() => {
        const data: any = {};

        params.forEach((p) => {
            const currentValue = (values || {})[p.key];
            if (currentValue) {
                data[p.key] = currentValue;
            } else if (p.defaultValue) {
                data[p.key] = p.defaultValue;
            }

            if (data[p.key]) {
                if (p.paramType === "double") {
                    data[p.key] = parseFloat(data[p.key]);
                } else if (p.paramType === "boolean" && (typeof data[p.key] !== "boolean")) {
                    data[p.key] = data[p.key] === "true";
                }
            }
        });

        return data;
    }, [params, values]);

    const [ currentValues, setCurrentValues ] = useState(initialValues);

    useImperativeHandle(ref, () => ({
        getData() {
            return currentValues;
        }
    }));

    const fileNode = useRef<HTMLInputElement>(null);
    const formNode = useRef<FormInstance>(null);

    const docParam = params.filter((p) => {
        return p.paramType === "docs";
    })[0];
    const otherParams = params.filter((p) => {
        return p.paramType !== "docs";
    });
    
    return (
        <Form
            name="basic"
            {...layout}
            initialValues={initialValues}
            onValuesChange={(changed, all) => {
                setCurrentValues(all);
            }}
            ref={formNode}
        >
            {
                docParam &&
                    <Form.Item
                        label="Docs"
                        name="_docs"
                    >
                        <a
                            target="_blank"
                            href={docParam.title}
                        >{docParam.title}</a>
                    </Form.Item>
            }
            {
                otherParams.map((param) => {
                    return (
                        <Param
                            key={param.key}
                            param={param}
                            value={(currentValues || {})[param.key]}
                        />
                    );
                })
            }
            <Form.Item
                label={
                    <span>
                        Preset <Tooltip title="You can download current form parameters as plain JSON file, or import your parameters from file">
                            <InfoCircleOutlined />
                        </Tooltip>
                    </span>
                }
            >
                <Space>
                    <Button onClick={() => {
                        const blob = new Blob(
                            [JSON.stringify(currentValues, null, 4)],
                            {type: "text/plain;charset=utf-8"}
                        );
                        saveAs(blob, "params.json");
                    }}>
                        <DownloadOutlined />
                        Export
                    </Button>
                    <Button onClick={() => {
                        fileNode.current && fileNode.current!.click && fileNode.current!.click();
                    }}>
                        <UploadOutlined />
                        Import
                    </Button>
                    <input ref={fileNode} type="file" style={{
                        display: "none"
                    }} onChange={(fileEvent) => {
                        // read the file
                        const reader = new FileReader();

                        // file reading finished successfully
                        reader.addEventListener('load', function(e) {
                            // contents of file in variable
                            var text = e.target!.result;
                            try {
                                const json = JSON.parse(text as string);
                                formNode.current && formNode.current.setFieldsValue(json);
                                setCurrentValues(assign({}, currentValues, json));
                                notify("success", "Preset imported");
                            } catch(e) {
                                notify("warn", "Invalid JSON file");
                            }
                        });

                        // file reading failed
                        reader.addEventListener('error', function() {
                            notify("warn", "Unable to parse file");
                        });

                        // read as text file
                        reader.readAsText(fileEvent!.target!.files![0]);
                    }} />
                </Space>
            </Form.Item>
        </Form>
    );
});

export default ParamList;