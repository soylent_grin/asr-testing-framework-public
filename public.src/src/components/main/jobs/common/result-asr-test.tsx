import React, { useState, useEffect } from 'react';

import { find } from 'lodash-es';

import { Form, Table, Button, Divider, Modal, Spin, Popover } from 'antd';

import { PlayCircleTwoTone } from "@ant-design/icons";

import * as api from '../../../../api/jobs';
import * as soundbankApi from '../../../../api/soundbank';
import { notify } from '../../../../utils';

import ResultLogs from './result-logs';
import AudioPlayer from './audio-player';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

type TrProps = {
    stepId: string,
    handleClose: () => void
};

function renderTranscription(text: string) {
    return (
        <div style={{ fontFamily: "monospace", fontSize: "12px", lineHeight: "12px" }}>
            {text}
        </div>
    );
}

export const TranscriptionTable: React.FunctionComponent<{
    data: ResultTranscription[],
    printMode?: boolean
}> = ({ data, printMode }) => {
    return (
        <Table
            bordered
            pagination={printMode ? false : undefined}
            size="small"
            columns={
                [
                    {
                        title: 'Sample',
                        dataIndex: 'sample',
                        key: 'sample',
                        width: 150,
                        render: (text) => {
                            return (
                                <span>
                                    {text}&nbsp;
                                    <Popover title={<AudioPlayer sampleId={text} />}>
                                        <PlayCircleTwoTone />
                                    </Popover>
                                </span>
                            );
                        },
                        sorter: {
                            compare: (a: any, b: any): 0 | 1 | -1 => {
                                const aVal = a.sample || "";
                                const bVal = b.sample || "";
                                if (aVal > bVal) {
                                    return 1;
                                }
                                if (aVal < bVal) {
                                    return -1;
                                }
                                return 0;
                            },
                            multiple: 1
                        }
                    },
                    {
                        title: 'Reference',
                        dataIndex: 'reference',
                        key: 'reference',
                        width: 200,
                        render: renderTranscription
                    },
                    {
                        title: 'Result',
                        dataIndex: 'transcription',
                        key: 'transcription',
                        width: 200,
                        render: renderTranscription
                    }
                ]
            }
            rowKey="id"
            dataSource={data}
        />
    );
};

const ResultTranscriptions: React.FunctionComponent<TrProps> = ({ stepId, handleClose }) => {
    const [ tr, setTr ] = useState<ResultTranscription[] | null>(null);
    useEffect(() => {
        api.loadStepResult(stepId).then((res) => {
            soundbankApi.loadSamplesByIds(Object.keys(res.transcription)).then((samples) => {
                const transcriptions: any = Object.keys(res.transcription).map((k) => {
                    const referenceSample = find(samples, (s) => {
                        return s.id === k;
                    });
                    return {
                        sample: k,
                        reference: referenceSample && referenceSample.transcription,
                        transcription: res.transcription[k]
                    }
                });
                setTr(transcriptions);
            });
        }, () => {
            notify("error", "Failed to load step results");
            setTr([]);
        });
    }, []);
    return (
        <Modal
            title="Transcriptions"
            visible
            onCancel={handleClose}
            footer={null}
            width="800px"
        >
            <Spin spinning={tr === null}>
                <TranscriptionTable data={tr!} />
            </Spin>
        </Modal>
    );
};

type Props = {
    stepId: string
};

const ResultAsrTest: React.FunctionComponent<Props> = ({ stepId }) => {
    const [ logOpened, setLogOpened ] = useState(false);
    const [ trOpened, setTrOpened ] = useState(false);
    return (
        <React.Fragment>
            <Divider>Results</Divider>
            <Form
                {...layout}
                initialValues={{
                    remember: false
                }}
            >
                <Form.Item
                    label="Transcriptions"
                >
                    <Button type="link" onClick={() => { setTrOpened(true); }}>show</Button>
                </Form.Item>
                <Form.Item
                    label="Logs"
                >
                    <Button type="link" onClick={() => { setLogOpened(true); }}>show</Button>
                </Form.Item>
            </Form>
            {
                logOpened &&
                    <ResultLogs stepId={stepId} handleClose={() => { setLogOpened(false); }} />
            }
            {
                trOpened &&
                    <ResultTranscriptions
                        stepId={stepId}
                        handleClose={() => { setTrOpened(false); }}
                    />
            }
        </React.Fragment>
    );
};

export default ResultAsrTest;