import React, { useState, useEffect } from 'react';

import { Form, Button, Divider, Modal, Spin, Tooltip, Table, Typography, Popover } from 'antd';

import * as api from '../../../../api/jobs';
import { notify } from '../../../../utils';

import ResultLogs from './result-logs';
import { assign } from 'lodash-es';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

type TrProps = {
    stepId: string,
    handleClose: () => void
};

type MetricProps = {
    value: any
};

const Metric: React.FunctionComponent<MetricProps> = ({ value }) => {
    let content;
    let withPopover = false;
    if (value === null || value === undefined || value === "-") {
        content = "";
    } else if (typeof value == "number" || !isNaN(parseFloat(value))) {
        content = value;
    } else {
        withPopover = true;
        content = <Typography.Link>view</Typography.Link>;
    }
    if (!withPopover) {
        return content;
    }
    let parsedValue = null;
    try {
        parsedValue = JSON.stringify(
            JSON.parse(value),
            null,
            4
        );
    } catch(e) {
        parsedValue = `invalid JSON (${value})`;
    }
    return (
        <Popover content={
            <pre style={{
                fontSize: "12px",
                lineHeight: "16px"
            }}>
                {parsedValue}
            </pre>
        }>
            {content}
        </Popover>
    );
};

export const ResultMetricsTable: React.FunctionComponent<{
    metrics: any,
    printMode?: boolean
}> = ({ metrics, printMode }) => {
    const selectedMetrics = (metrics && metrics.selectedMetrics) || [];
    return (
        <Table
            pagination={printMode ? false : undefined}
            columns={[
                {
                    title: 'Sample',
                    dataIndex: 'sample',
                    key: 'sample',
                    fixed: printMode ? undefined : 'left',
                    width: printMode ? undefined : 250
                },
                ...selectedMetrics.map((sm: string) => {
                    return {
                        title: sm,
                        dataIndex: sm,
                        key: sm,
                        width: printMode ? undefined : 100,
                        render: (value: any) => {
                            return <Metric value={value} />;
                        }
                    };
                })
            ]}
            dataSource={(metrics && metrics.result) || []}
            scroll={
                printMode ?
                    undefined :
                    selectedMetrics.length > 2 ?
                        { x: 1500, y: 300 } :
                        undefined
                }
        />
    );
};

const ResultMetrics: React.FunctionComponent<TrProps> = ({ stepId, handleClose }) => {
    const [ metrics, setMetrics ] = useState<any | null>(null);
    useEffect(() => {
        api.loadMeasureStepResult(stepId).then((result) => {
            setMetrics(result);
        }, () => {
            notify("error", "Failed to load step results");
            setMetrics([]);
        });
    }, []);
    return (
        <Modal
            title="Metrics"
            visible
            onCancel={handleClose}
            footer={null}
            width={700}
        >
            <Spin spinning={metrics === null}>
                <ResultMetricsTable
                    metrics={metrics}
                />
            </Spin>
        </Modal>
    );
};

type Props = {
    stepId: string
};

const ResultMeasure: React.FunctionComponent<Props> = ({ stepId }) => {
    const [ logOpened, setLogOpened ] = useState(false);
    const [ trOpened, setTrOpened ] = useState(false);
    return (
        <React.Fragment>
            <Divider>Results</Divider>
            <Form
                {...layout}
                initialValues={{
                    remember: false
                }}
            >
                <Form.Item
                    label="Metrics"
                >
                    <Button type="link" onClick={() => { setTrOpened(true); }}>show</Button>
                </Form.Item>
                <Form.Item
                    label="Logs"
                >
                    <Button type="link" onClick={() => { setLogOpened(true); }}>show</Button>
                </Form.Item>
            </Form>
            {
                logOpened &&
                    <ResultLogs stepId={stepId} handleClose={() => { setLogOpened(false); }} />
            }
            {
                trOpened &&
                    <ResultMetrics stepId={stepId} handleClose={() => { setTrOpened(false); }} />
            }
        </React.Fragment>
    );
};

export default ResultMeasure;