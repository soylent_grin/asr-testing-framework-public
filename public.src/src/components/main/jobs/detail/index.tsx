import React, { useState } from 'react';
import { Space, Typography, Button } from 'antd';
import { useParams } from 'react-router-dom';
import { find } from 'lodash-es';
import { Link } from 'react-router-dom';
import {
    ReloadOutlined,
    FilePdfOutlined
} from '@ant-design/icons';

import Steps from '../common/step-list';

import { renderTime, renderJobName } from '../../../../utils';
import Report from './report';

type Props = {
    jobs: AsrJob[],
    folders: SoundbankFolder[],
    executors: AsrExecutor[],
    metrics: AsrJobMetric[],
    tags: string[],
    canCreateJob: boolean
};

const JobDetail: React.FunctionComponent<Props> = ({ ...props }) => {
    const stepsNode = React.createRef<any>();
    const [ preparingReport, setPreparingReport ] = useState(false);
    let { id } = useParams();
    const currentJob = find(props.jobs, (j) => {
        return j.id === id;
    });
    if (!currentJob) {
        return (
            <Typography.Title level={3}>
                <Space>
                    <span>
                        Unknown job (id = {id})
                    </span>
                </Space>   
            </Typography.Title>
        );
    }
    return (
        <React.Fragment>
            <Typography.Title level={3} className="no-print" style={{
                marginBottom: "40px"
            }}>
                    <span>
                        Job "{renderJobName(currentJob)}"
                    </span>
                    <div style={{
                        float: "right"
                    }}>
                        <Space>
                            <Link to={`/main/jobs/create?reference=${currentJob.id}`}>
                                <Button type="default" disabled={!props.canCreateJob}>
                                    Relaunch
                                    <ReloadOutlined />
                                </Button>
                            </Link> 
                            <Button type="default" loading={preparingReport} onClick={() => {
                                setPreparingReport(true);
                            }}>
                                Download report
                                <FilePdfOutlined />
                            </Button>
                        </Space>
                    </div>
            </Typography.Title>
            {
                preparingReport && 
                    <Report
                        folders={props.folders}
                        job={currentJob}
                        executors={props.executors}
                        onReady={() => {
                            setPreparingReport(false);
                            window.print();
                        }}
                    />
            }
            <div className="no-print">
                <Steps {...props} ref={stepsNode} currentJob={currentJob} mode="view" />
            </div>
        </React.Fragment>
    );
}

export default JobDetail;