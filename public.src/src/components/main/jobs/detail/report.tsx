import React, { useState, useEffect } from 'react';
import { Spin, Typography, Divider } from 'antd';
import { find } from 'lodash-es';
import ParamList from '../common/param-list';
import { TranscriptionTable } from '../common/result-asr-test';
import FolderList from '../../soundbank/folder-list';
import AugmenationParamList from '../common/augmentation-param-list';
import { ResultMetricsTable } from '../common/result-measure';
import * as api from '../../../../api/jobs';
import * as soundbankApi from '../../../../api/soundbank';
import { renderJobName, renderTime } from '../../../../utils';

const DatasetReport: React.FunctionComponent<{
    step: AsrJobStep,
    folders: SoundbankFolder[],
    onReady: () => void
}> = ({ step, onReady, folders }) => {
    const [ folder, setFolder ] = useState<SoundbankFolder[] | null>(null);
    useEffect(() => {
        api.loadStepResult(step.id!).then((res) => {
            if (res && res.result && res.result.folder) {
                const foundFolder = folders.filter((f) => {
                    return f.key === res.result.folder; 
                });
                setFolder(foundFolder);
            } else {
                setFolder([]);
            }
        });
    }, []);
    useEffect(() => {
        if (folder) {
            onReady();
        }
    }, [folder]);
    return (
        <div>
            <Typography.Title level={4}>
                Config
            </Typography.Title>
            {
                <AugmenationParamList
                    values={step.config}
                />
            }
        </div>
    );
}

const MeasureReport: React.FunctionComponent<{
    step: AsrJobStep,
    onReady: () => void
}> = ({ step, onReady }) => {
    const [ metrics, setMetrics ] = useState<any | null>(null);
    useEffect(() => {
        api.loadMeasureStepResult(step.id!).then((result) => {
            setMetrics(result);
        }, () => {
            setMetrics([]);
        });
    }, []);
    useEffect(() => {
        if (metrics) {
            onReady();
        }
    }, [metrics]);
    return (
        <div>
            <Typography.Title level={4}>
                Result
            </Typography.Title>
            {
                metrics &&
                    <ResultMetricsTable
                        metrics={metrics}
                        printMode
                    />
            }
        </div>
    );
}

const AsrTestReport: React.FunctionComponent<{
    job: AsrJob,
    step: AsrJobStep,
    executors: AsrExecutor[],
    onReady: () => void
}> = ({ job, step, executors, onReady }) => {
    const currentExecutor = find(executors, (e) => {
        return e.key === job.asrKey;
    });
    const [ tr, setTr ] = useState<ResultTranscription[] | null>(null);
    useEffect(() => {
        api.loadStepResult(step.id!).then((res) => {
            soundbankApi.loadSamplesByIds(Object.keys(res.transcription)).then((samples) => {
                const transcriptions: any = Object.keys(res.transcription).map((k) => {
                    const referenceSample = find(samples, (s) => {
                        return s.id === k;
                    });
                    return {
                        sample: k,
                        reference: referenceSample && referenceSample.transcription,
                        transcription: res.transcription[k]
                    }
                });
                setTr(transcriptions);
            });
        }, () => {
            setTr([]);
        });
    }, []);
    useEffect(() => {
        if (tr) {
            onReady();
        }
    }, [tr]);
    return (
        <div>
            <Typography.Title level={4}>
                ASR: {currentExecutor ? currentExecutor.title : "unknown ASR"}
            </Typography.Title>
            <Typography.Title level={4}>
                Config
            </Typography.Title>
            {
                <ParamList
                    params={currentExecutor!.params.filter((p) => {
                        return p.scope && p.scope.indexOf("test") > -1;
                    })}
                    values={step.config}
                />
            }
            <Typography.Title level={4}>
                Result
            </Typography.Title>
            {
                tr &&
                    <TranscriptionTable
                        data={tr}
                        printMode
                    />
            }
        </div>
    );
}

const AsrTrainReport: React.FunctionComponent<{
    job: AsrJob,
    step: AsrJobStep,
    executors: AsrExecutor[]
}> = ({ job, step, executors }) => {
    const currentExecutor = find(executors, (e) => {
        return e.key === job.asrKey;
    });
    return (
        <div>
            <Typography.Title level={4}>
                ASR: {currentExecutor ? currentExecutor.title : "unknown ASR"}
            </Typography.Title>
            <Typography.Title level={4}>
                Config
            </Typography.Title>
            {
                <ParamList
                    params={currentExecutor!.params.filter((p) => {
                        return p.scope && p.scope.indexOf("train") > -1;
                    })}
                    values={step.config}
                />
            }
        </div>
    );
}

const Report: React.FunctionComponent<{
    job: AsrJob,
    executors: AsrExecutor[],
    folders: SoundbankFolder[],
    onReady: () => void
}> = ({ job, executors, onReady, folders }) => {
    const datasetStep = find(job.steps, (s) => {
        return s.stepType === "dataset";
    });
    const [ isDatasetReady, setDatasetIsReady ] = useState(!datasetStep);
    const asrTrainStep = find(job.steps, (s) => {
        return s.stepType === "train";
    });
    const asrTestStep = find(job.steps, (s) => {
        return s.stepType === "test";
    });
    const [ isTestReady, setTestIsReady ] = useState(!asrTestStep);
    const measureStep = find(job.steps, (s) => {
        return s.stepType === "measure";
    });
    const [ isMeasureReady, setMeasureIsReady ] = useState(!measureStep);
    useEffect(() => {
        console.log(`calling effect, isTestReady: ${isTestReady}, isMeasureReady: ${isMeasureReady}`);
        if (isTestReady && isMeasureReady && isDatasetReady) {
            onReady();
        }
    }, [ isTestReady, isMeasureReady, isDatasetReady ]);
    return (
        <div className="report-container">
            <div>
                <Divider>
                    Summary
                </Divider>
                <Typography.Paragraph>
                    Job: {renderJobName(job)}
                </Typography.Paragraph>
                <Typography.Paragraph>
                    Created at: {renderTime(job.timeCreate!)}
                </Typography.Paragraph>
                <Typography.Paragraph>
                    Started at: {renderTime(job.timeBegin!)}
                </Typography.Paragraph>
                <Typography.Paragraph>
                    Finished at: {renderTime(job.timeEnd!)}
                </Typography.Paragraph>
            </div>
            {
                datasetStep &&
                    <div>
                        <Divider>
                            Dataset
                        </Divider>
                        <DatasetReport
                            step={datasetStep}
                            folders={folders}
                            onReady={() => {
                                setDatasetIsReady(true);
                            }}
                        />
                    </div>
            }
            {
                asrTrainStep &&
                    <div>
                        <Divider>
                            Train
                        </Divider>
                        <AsrTrainReport
                            step={asrTrainStep}
                            executors={executors}
                            job={job}
                        />
                    </div>
            }
            {
                asrTestStep &&
                    <div>
                        <Divider>
                            Test
                        </Divider>
                        <AsrTestReport
                            step={asrTestStep}
                            executors={executors}
                            job={job}
                            onReady={() => {
                                setTestIsReady(true);
                            }}
                        />
                    </div>
            }
            {
                measureStep &&
                    <div>
                        <Divider>
                            Measure
                        </Divider>
                        <MeasureReport
                            step={measureStep}
                            onReady={() => {
                                setMeasureIsReady(true);
                            }}
                        />
                    </div>
            }
        </div>
    );
}

export default Report;