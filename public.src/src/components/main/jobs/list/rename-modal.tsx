import React, { useState } from 'react';

import { Modal, Input, Button } from 'antd';

type Props = {
    label?: string,
    onSave: (label: string) => Promise<any>,
    onCancel: () => void
};

const RenameModal: React.FunctionComponent<Props> = (props) => {
    const { onSave, onCancel } = props;
    const [ isSaving, setIsSaving ] = useState(false);
    const [ label, setLabel ] = useState(props.label || "");
    return (
        <Modal
            title="Rename job"
            visible
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancel
                </Button>,
                <Button
                    key="save"
                    type="primary"
                    loading={isSaving}
                    onClick={() => {
                        setIsSaving(true);
                        onSave(label).then(() => {
                            setIsSaving(false);
                        }, () => {
                            setIsSaving(false);
                        });
                    }}
                >
                    Save
                </Button>,
            ]}
        >
            <Input
                value={label}
                placeholder="Enter job label"
                onChange={(e) => {
                    setLabel(e.target.value);
                }}
            />
        </Modal>
    );
};

export default RenameModal;