import React, { useEffect, useState } from 'react';
import queryString from 'query-string';

import {
    Tooltip,
    Table,
    Input,
    Button,
    Space,
    Typography,
    Menu,
    Dropdown,
    Modal,
    Tag,
    Select
} from 'antd';
import {
    SearchOutlined,
    SettingOutlined,
    PlusCircleOutlined,
    BarChartOutlined,
    ReloadOutlined,
    EditOutlined,
    DeleteOutlined,
    ZoomInOutlined
} from '@ant-design/icons';
import { assign } from 'lodash-es';
import { withRouter, RouteComponentProps, Link } from 'react-router-dom';

import Avatar from '../../../common/avatar';

import { renderTime, diffBetweenDates, isAllowed } from '../../../../utils';
import { ColumnsType } from 'antd/lib/table';

import RenameModal from './rename-modal';

type Props = {
    jobs: AsrJob[],
    executors: AsrExecutor[],
    checkedJobs: string[],
    readyToCompareJobs: AsrJob[],
    onJobCheck: (checked: string[]) => void,
    onJobDelete: (id: string) => void,
    onJobCopy: (job: AsrJob) => any,
    onJobRename: (id: string, label: string) => Promise<any>,
    currentUser: User | null,
    users: User[],
    canCreateJob: boolean
} & RouteComponentProps;

type State = {
    renameModalJob: AsrJob | null,
    renameModalVisible: boolean,
    searchText: string,
    searchedColumn: any,
    selectedUser: string
}

function Timer({ timeBegin }: { timeBegin: number }) {
    const [ currentTime, setCurrentTime ] = useState(Date.now());
    useEffect(() => {
        const interval = setInterval(() => {
            setCurrentTime(Date.now());
        }, 1000);
        return () => {
            clearInterval(interval);  
        };
    });
    return <span>{diffBetweenDates(timeBegin, currentTime)}</span>;
}

const MAX_VISIBLE_METRICS = 5;

class List extends React.Component<Props, State> {
    state = {
        renameModalVisible: false,
        renameModalJob: null,
        searchText: "",
        searchedColumn: null,
        selectedUser: "all"
    };
    searchInput: Input | null = null;
    componentDidMount() {
        const { user } = queryString.parse(this.props.location!.search);
        if (user) {
            this.setState({
                selectedUser: user as string
            });
        }
    }
    handleRename = (job: AsrJob) => {
        this.setState({
            renameModalVisible: true,
            renameModalJob: job
        });
    }
    handleRelaunch = (id: string) => () => {
        // this.props.onConfig(id);
    }
    handleDelete = (id: string) => {
        Modal.confirm({
            content: "Do you really want to delete this job?",
            onOk: () => {
                this.props.onJobDelete(id);
            },
            onCancel() {
                // do nothing
            },
        });
    }
    getMenu(job: AsrJob) {
        const { history, onJobCopy } = this.props;
        return (
            <Menu onClick={(a: any) => {
                switch(a.key) {
                case "detail":
                    history.push(`/main/jobs/detail/${job.id}`);
                    break;
                case "rename":
                    this.handleRename(job);
                    break;
                case "relaunch":
                    Modal.confirm({
                        content: "Do you want to redefine some of job settings?",
                        okText: "Yes",
                        cancelText: "No, create full job copy",
                        onOk: () => {
                            history.push(`/main/jobs/create?reference=${job.id}`);
                        },
                        onCancel() {
                            onJobCopy(
                                assign({}, job, {
                                    id: undefined,
                                    timeBegin: undefined,
                                    timeCreate: undefined,
                                    timeEnd: undefined,
                                    status: "waiting",
                                    steps: job.steps.map((s: AsrJobStep) => {
                                        return assign({}, s, {
                                            id: undefined,
                                            jobId: undefined,
                                            status: "waiting"
                                        })
                                    })
                                })
                            )
                        },
                    });
                    break;
                case "delete":
                    this.handleDelete(job.id!);
                    break;
                default:
                    break;
                }   
            }}>
                <Menu.Item key="detail" icon={<ZoomInOutlined />}>
                    View details
                </Menu.Item>
                <Menu.Item key="rename" icon={<EditOutlined />}>
                    Rename
                </Menu.Item>
                <Menu.Item key="relaunch" icon={<ReloadOutlined />} disabled={!this.props.canCreateJob}>
                    Relaunch
                </Menu.Item>
                {
                    job.steps.filter((s) => {
                        return s.stepType === "test";
                    }).length > 0 &&
                        <Menu.Item key="measure" disabled title="Not implemented" icon={<BarChartOutlined />}>
                            Measure
                        </Menu.Item>
                }
                <Menu.Item key="delete" danger icon={<DeleteOutlined />}>
                    Delete
                </Menu.Item>
            </Menu>
        );
    }
    getColumns(jobs: AsrJob[]) {
        let columns = [
            {
                title: 'Label',
                dataIndex: 'label',
                key: 'label',
                ...this.getColumnSearchProps('label', 'label'),
                render: (text: string, data: AsrJob) => {
                    return text || <span>
                        <Button style={{ paddingLeft: "5px" }}type="link" onClick={() => {
                            this.handleRename(data);
                        }}>Edit <EditOutlined /></Button>
                    </span>;
                }
            },
            {
                title: 'ASR',
                dataIndex: 'asrKey',
                key: 'asrKey',
                ...this.getColumnSearchProps('asrKey', 'ASR'),
                sorter: {
                    compare: (a: AsrJob, b: AsrJob) => {
                        const aVal = a.asrKey || "";
                        const bVal = b.asrKey || "";
                        if (aVal > bVal) {
                            return 1;
                        }
                        if (aVal < bVal) {
                            return -1;
                        }
                        return 0;
                    },
                    multiple: 2,
                },
                render: (text: string, data: AsrJob) => {
                    const executor = this.props.executors.filter((ex) => {
                        return ex.key === data.asrKey;
                    })[0];
                    return executor && executor.key;
                }
            },
            {
                title: 'Status',
                render: (data: AsrJob) => {
                    const { status } = data;
                    let color = "default";
                    switch(status) {
                    case "running":
                        color = "processing";
                        break;
                    case "success":
                        color = "green";
                        break;
                    case "error":
                    case "terminated":
                        color = "red";
                        break;
                    }
                    return (
                        <span className={`job-status--${status}`}>
                            <Tag color={color}>{status}</Tag>
                        </span>
                    );
                }
            },
            {
                title: 'Steps',
                render: (data: AsrJob) => {
                    return (
                        <Space>
                            {
                                data.steps.map((s) => {
                                    let title = "";
                                    if (s.stepType === "measure") {
                                        const metrics = (s.config && s.config.metrics) || [];
                                        title = metrics.join(", ");
                                    } else if (s.stepType === "dataset") {

                                    } else {
                                        title = `selected samples: ${s.samples.type}, ${s.samples.list.length}`;
                                    }
                                    return <Tooltip title={title} key={s.stepType}>
                                        <span style={{
                                            borderBottom: "1px dashed gray",
                                            cursor: "pointer"
                                        }}>
                                            {s.stepType}
                                        </span>
                                    </Tooltip>;
                                })
                            }
                        </Space>
                    );
                }
            },
            {
                title: 'Created at',
                key: 'timeCreate',
                sorter: {
                    compare: (a: AsrJob, b: AsrJob) => a.timeCreate! - b.timeCreate!,
                    multiple: 3,
                },
                render: (data: AsrJob) => {
                    return renderTime(data.timeCreate!);
                }
            },
            {
                title: 'Duration',
                key: 'duration',
                sorter: {
                    compare: (a: AsrJob, b: AsrJob) => -1,
                    multiple: 3,
                },
                render: (text: string, data: AsrJob) => {
                    const { timeBegin, timeEnd } = data;
                    if (!timeBegin) {
                        return "N/A"
                    }
                    if (!timeEnd) {
                        return (
                            <Timer timeBegin={timeBegin} />
                        );
                    }
                    return diffBetweenDates(timeBegin, timeEnd);
                }
            },
            {
                title: (
                    <div style={{
                        textAlign: "center"
                    }}>
                        <SettingOutlined style={{
                            margin: "auto"
                        }} />
                    </div>
                ),
                key: 'operation',
                render: (data: AsrJob) => {
                    const iconStyle = {
                        fontSize: "18px"
                    };
                    return (
                        <div style={{
                            textAlign: "center"
                        }}>
                            <Dropdown overlay={this.getMenu(data)}>
                                <a>
                                    <SettingOutlined style={iconStyle} />
                                </a>
                            </Dropdown>
                        </div>
                    );
                },
            },
        ];

        return columns;
    }
    getColumnSearchProps = (dataIndex: string, title: string) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }: any) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search by ${title}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined />}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button
                    onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}
                >
                    Clear
                </Button>
            </div>
        ),
        filterIcon: (filtered: any) =>
            <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value: string, record: any) => {
            return record[dataIndex]
                    .toString()
                    .toLowerCase()
                    .includes(value.toLowerCase())
        },
        onFilterDropdownVisibleChange: (visible: any) => {
            if (visible) {
                setTimeout(() => this.searchInput?.select());
            }
        },
        render: (text: string) => text,
    });

    handleSearch = (selectedKeys: string[], confirm: any, dataIndex: string) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = (clearFilters: any) => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    render() {
        const { jobs, readyToCompareJobs, onJobRename, users, currentUser, canCreateJob } = this.props;
        const { renameModalVisible, renameModalJob, selectedUser } = this.state;
        const userOptions = [
            {
                value: "all",
                label: "All users",
                user: null
            },
            ...users.map((u) => {
                return (
                    {
                        value: u.id,
                        label: u.name,
                        user: u
                    }
                );
            })
        ];
        return (
            <React.Fragment>
                <Typography.Title level={3}>
                    <Space>
                        <span>
                            Jobs
                        </span>
                        <Link to="/main/jobs/create">
                            <Button type="primary" disabled={!canCreateJob}>
                                Create <PlusCircleOutlined />
                            </Button>
                        </Link>
                        <Link to="/main/jobs/compare">
                            <Tooltip title="Select two or more jobs with measure step and at least one common metric">
                                <Button type="default" disabled={readyToCompareJobs.length < 2}>
                                    Compare <BarChartOutlined />
                                </Button>
                            </Tooltip>
                        </Link>
                    </Space>
                    {
                        isAllowed(currentUser, "viewUsers") &&
                            <Select
                                style={{
                                    float: "right",
                                    minWidth: "200px"
                                }}
                                showSearch
                                filterOption={(input, option) => {
                                    return option!.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }}
                                onChange={(e) => {
                                    this.setState({
                                        selectedUser: e
                                    });
                                }}
                                value={selectedUser}
                            >
                                {
                                    userOptions.map((o) => {
                                        return (
                                            <Select.Option value={o.value} key={o.value}>
                                                {
                                                    o.user && 
                                                        <>
                                                            <Avatar user={o.user} />
                                                            <span>&nbsp;</span>
                                                        </>
                                                }
                                                <span>{o.label}</span>
                                            </Select.Option>
                                        )
                                    })
                                }
                            </Select>
                    }
                </Typography.Title>
                <Table
                    bordered
                    size="small"
                    columns={this.getColumns(jobs) as ColumnsType<AsrJob>}
                    rowKey="id"
                    dataSource={
                        selectedUser === "all" ?
                            jobs :
                            jobs.filter((j) => {
                                return j.user === selectedUser;
                            })
                    }
                    rowSelection={{
                        type: "checkbox",
                        onChange: (selectedRowKeys, selectedRows) => {
                            this.props.onJobCheck(selectedRows.map(j => j.id!));
                        },
                        // getCheckboxProps: (record: AsrJob) => {
                        //     return {
                        //         checked: this.props.checkedJobs.indexOf(record.id!) > -1
                        //     };
                        // }
                    }}
                    onRow={(record: any) => {
                        return {
                        };
                    }}
                />
                {
                    renameModalVisible &&
                        <RenameModal
                            label={renameModalJob ? (renameModalJob as any).label : ""}
                            onSave={(newLabel) => {
                                return onJobRename(
                                    (renameModalJob as any).id,
                                    newLabel
                                ).then(() => {
                                    this.setState({
                                        renameModalVisible: false
                                    });
                                });
                            }}
                            onCancel={() => {
                                this.setState({
                                    renameModalVisible: false
                                });
                            }}
                        />
                }
            </React.Fragment>
        );
    }
}

export default withRouter(List);
