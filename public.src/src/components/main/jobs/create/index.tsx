import React from 'react';
import { Button, Space, message, Typography } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';

import queryString from 'query-string';

import Steps from '../common/step-list';

import { validateAsrJob } from '../../../../utils';
import { hookSelectors } from '../../../../ducks/app-config';
import { find } from 'lodash-es';
import { useSelector } from 'react-redux';

type Props = {
    jobs: AsrJob[],
    folders: SoundbankFolder[],
    executors: AsrExecutor[],
    metrics: AsrJobMetric[],
    tags: string[],
    location?: any,
    onJobCreate: (job: AsrJob) => void,
    canCreateJob: boolean
};

const JobCreate: React.FunctionComponent<Props> = ({ onJobCreate, ...props }) => {
    const stepsNode = React.createRef<any>();
    const { reference } = queryString.parse(props.location!.search);
    const appConfig = useSelector(hookSelectors.getConfig);
    let referenceJob: AsrJob | undefined = undefined;
    if (reference) {
        referenceJob = find(props.jobs, (j) => {
            return j.id === reference;
        });
    }  
    return (
        <React.Fragment>
            <Typography.Title level={3} style={{
                marginBottom: "40px"
            }}>
                <Space>
                    <span>
                        New job
                    </span>
                    <Button type="primary" disabled={!props.canCreateJob} onClick={() => {
                        const job = stepsNode.current && stepsNode.current!.getData();
                        const error = validateAsrJob(job, appConfig, props.folders);
                        if (error) {
                            message.warn(`Could not create job. ${error}`);
                            return;
                        }
                        onJobCreate(job);
                    }}>
                        Create <PlusCircleOutlined />
                    </Button>
                </Space>   
            </Typography.Title>
            <Steps {...props} ref={stepsNode} currentJob={referenceJob} mode="create" />
        </React.Fragment>
    );
}

export default JobCreate;