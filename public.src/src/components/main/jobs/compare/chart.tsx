import React, { useEffect } from 'react';
import Plotly from 'plotly.js-dist';
import { renderJobName } from '../../../../utils';
import { find } from 'lodash-es';

type Result = {
    job: AsrJob,
    results: number[]
}

type Props = {
    metric: string,
    metrics: AsrJobMetric[],
    results: Result[]
};

const Chart: React.FunctionComponent<Props> = ({ metric, results, metrics }) => {
    const containerId = `compare-chart-${metric}`;
    useEffect(() => {
        const data = results.map((r) => {
            return {
                y: r.results,
                boxpoints: 'all',
                jitter: 0.3,
                pointpos: -1.8,
                type: 'box',
                name: renderJobName(r.job)
            };
        });
        const targetMetric = find(metrics, (m) => {
            return m.key === metric;
        });
        const layout = {
            title: targetMetric ? targetMetric.title : "Unknown metric" 
        };
        Plotly.newPlot(containerId, data, layout);
    }, results);
    return (
        <div id={containerId} />
    );
}

export default Chart;