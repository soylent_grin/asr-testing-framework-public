import React, { useEffect, useState } from 'react';
import { Typography, Layout, Spin } from 'antd';
import { Link } from 'react-router-dom';
import { find, assign } from 'lodash-es';
import { loadStepResult } from '../../../../api/jobs';
import Chart from './chart';

type Props = {
    jobs: AsrJob[],
    metrics: AsrJobMetric[],
};

const { Sider, Content } = Layout;

const Compare: React.FunctionComponent<Props> = ({ jobs, metrics }) => {
    const [ measureResults, setMeasureResults ] = useState<any | null>(null);
    useEffect(() => {
        console.log(`loading all metrics result from jobs: `, jobs);
        const measureSteps = jobs.map((j) => {
            return find(j.steps, (s) => {
                return s.stepType === "measure";
            });
        }).filter(a => a);
        Promise.all(
            measureSteps.map((s) => {
                return loadStepResult(s!.id!);
            })
        ).then((results) => {
            const resultsWithJobs = results.map((r) => {
                const boundStep = find(measureSteps, (s) => {
                    return s!.id === r.stepId;
                });
                return assign({}, r, {
                    job: boundStep && boundStep!.jobId && find(jobs, (j) => {
                        return j.id === boundStep!.jobId;
                    })
                });
            });
            const commonMetrics: Record<string, number> = {};
            measureSteps.forEach((s) => {
                s?.config && s.config.metrics && s.config.metrics.map((m: string) => {
                    if (!commonMetrics[m]) {
                        commonMetrics[m] = 0;
                    }
                    commonMetrics[m] = commonMetrics[m] + 1;
                });
            });
            const data = Object.keys(commonMetrics).filter((m) => {
                return commonMetrics[m] === jobs.length;
            }).map((m) => {
                return {
                    metric: m,
                    results: resultsWithJobs.map((r) => {
                        const singleResults = (r.result &&
                            r.result.single &&
                            r.result.single.map((s: any) => {
                                return (s[1] && s[1].result && s[1].result[m]) || 0;
                            })) || [];
                        const overallResult = r.result &&
                            r.result.overall &&
                            r.result.overall[0] &&
                            r.result.overall[0].result &&
                            r.result.overall[0].result[m];
                        if (isNaN(overallResult)) {
                            return null;
                        }
                        return {
                            job: r.job,
                            results: [
                                ...singleResults,
                                overallResult
                            ].filter(a => a)
                        }
                    }).filter(a => a)
                }
            });
            setMeasureResults(data.filter((r) => {
                return r.results && r.results.length > 0;
            }));
        });
    }, []);
    return (
        <React.Fragment>
            {
                jobs.length > 1 ?
                    <Spin spinning={measureResults === null}>
                        <Typography.Title level={3}>
                            Compare
                        </Typography.Title>
                        <div style={{
                            minHeight: "500px"
                        }}>
                            <div className="compare__chart-container">
                                {
                                    measureResults && measureResults.map((k: any) => {
                                        return (
                                            <Chart
                                                metrics={metrics}
                                                key={k.metric}
                                                {...k}
                                            />
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </Spin> :
                    <div>Please, <Link to="/main/jobs">select</Link> at least 2 succeeded jobs with at least one common metric</div>
            }
        </React.Fragment>
    );
}

export default Compare;