import React, { FunctionComponent, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { useRouteMatch, Switch, Route, useHistory } from 'react-router-dom';

import JobList from './list';
import JobDetail from './detail';
import JobCompare from './compare';
import JobCreate from './create';

import { selectors, create, checked, remove, updateLabel } from '../../../ducks/jobs'; 
import * as soundbankDuck from '../../../ducks/soundbank';
import * as authDuck from '../../../ducks/auth';
import * as userDuck from '../../../ducks/users';
import * as appConfigDuck from '../../../ducks/app-config';
import { Spin } from 'antd';

type Props = {
    currentUser: User | null,
    metrics: AsrJobMetric[],
    tags: string[],
    jobs: AsrJob[],
    checkedJobs: string[],
    folders: SoundbankFolder[],
    executors: AsrExecutor[],
    readyToCompareJobs: AsrJob[],
    users: User[],
    canCreateJob: boolean
};

const Jobs: FunctionComponent<Props> = ({ children, executors, jobs, checkedJobs, folders, metrics, tags, readyToCompareJobs, currentUser, users, canCreateJob }) => {
    let { url } = useRouteMatch();
    const history = useHistory();
    const dispatch = useDispatch();
    const [ isCreating, setIsCreating ] = useState(false);
    return (
        <Spin spinning={isCreating}>
            <Switch>
                <Route path={`${url}/detail/:id`}>
                    {({ location }) => {
                        return (
                            <JobDetail
                                canCreateJob={canCreateJob}
                                folders={folders}
                                executors={executors}
                                metrics={metrics}
                                jobs={jobs}
                                tags={tags}
                            />
                        );
                    }}
                </Route>
                <Route path={`${url}/create`}>
                    {({ location }) => {
                        return (
                            <JobCreate
                                canCreateJob={canCreateJob}
                                location={location}
                                folders={folders}
                                executors={executors}
                                metrics={metrics}
                                jobs={jobs}
                                tags={tags}
                                onJobCreate={(newJob: AsrJob) => {
                                    setIsCreating(true);
                                    (dispatch(create(newJob)) as any).then(() => {
                                        setIsCreating(false);
                                        history.push("/main/jobs");
                                    }).catch(() => {
                                        setIsCreating(false);
                                    });
                                }}
                            />
                        );
                    }}
                </Route>
                <Route path={`${url}/compare`}>
                    <JobCompare
                        jobs={readyToCompareJobs}
                        metrics={metrics}
                    />
                </Route>
                <Route path={`${url}`}>
                    <JobList
                        canCreateJob={canCreateJob}
                        currentUser={currentUser}
                        users={users}
                        jobs={jobs}
                        checkedJobs={checkedJobs}
                        readyToCompareJobs={readyToCompareJobs}
                        executors={executors}
                        onJobCheck={(checkedIds: string[]) => {
                            dispatch(checked(checkedIds));
                        }}
                        onJobDelete={(id: string) => {
                            dispatch(remove(id));
                        }}
                        onJobCopy={(job: AsrJob) => {
                            return dispatch(create(job));
                        }}
                        onJobRename={(id: string, label: string) => {
                            return dispatch(updateLabel(id, label)) as any;
                        }}
                    />
                </Route>
            </Switch>
        </Spin>
    );
}

function mapStateToProps({ jobs, soundbank, auth, users, appConfig }: any) {
    const allJobs = selectors.getList(jobs);
    return {
        jobs: allJobs,
        currentJob: selectors.getCurrent(jobs),
        checkedJobs: selectors.getChecked(jobs),
        readyToCompareJobs: selectors.getReadyToCompare(jobs),
        executors: selectors.getExecutors(jobs),
        metrics: selectors.getMetrics(jobs),
        tags: selectors.getTags(jobs),
        folders: soundbankDuck.selectors.getFolders(soundbank),
        currentUser: authDuck.selectors.getUser(auth),
        users: userDuck.selectors.getUsers(users),
        canCreateJob: appConfigDuck.selectors.getConfig(appConfig).maxJobsPerUser > allJobs.length
    }
};

export default connect(mapStateToProps)(Jobs);