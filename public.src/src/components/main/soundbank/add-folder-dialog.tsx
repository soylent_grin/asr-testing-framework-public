import React, { useState } from 'react';

import { Modal, Spin, Input } from 'antd';

type Props = {
    onAdd: (name: string) => Promise<any>,
    onClose: () => void
}

const AugmentateDialog: React.FunctionComponent<Props> = ({ onAdd, onClose }) => {
    const [ isRunning, setIsRunning ] = useState(false);
    const [ name, setName ] = useState("");
    return (
        <Modal
            title="Add new private folder"
            visible
            onCancel={onClose}
            onOk={() => {
                setIsRunning(true);
                onAdd(name).then(() => {
                    setIsRunning(false);
                    onClose();
                }, () => {
                    setIsRunning(false);
                });
            }}
            okButtonProps={{
                disabled: isRunning
            }}
            cancelButtonProps={{
                disabled: isRunning
            }}
            width={650}
        >
            <Spin spinning={isRunning}>
                <Input
                    value={name}
                    onChange={(e) => {
                        setName(e.target.value);
                    }}
                />
            </Spin>
        </Modal>
    );
};

export default AugmentateDialog;