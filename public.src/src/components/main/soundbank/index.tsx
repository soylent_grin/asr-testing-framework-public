import React, { useState } from 'react';
import { Divider, Button, Typography, Space } from 'antd';
import { connect, useDispatch, useSelector } from 'react-redux';

import { UploadOutlined, PlusCircleOutlined } from '@ant-design/icons';

import * as soundbankDuck from '../../../ducks/soundbank';
import * as appConfigDuck from '../../../ducks/app-config';
import * as userDuck from '../../../ducks/users';
import * as authDuck from '../../../ducks/auth';
import UploadFolderDialog from './upload-folder-dialog';
import AugmentateDialog from './augmentate-dialog';
import FolderList from './folder-list';
import confirm from 'antd/lib/modal/confirm';
import RecordDialog from './record-dialog';
import AddDialog from './add-folder-dialog';

import * as soundbankApi from '../../../api/soundbank';
import { notify } from '../../../utils';

const { Title } = Typography;

type Props = {
    publicFolders: SoundbankFolder[],
    privateFolders: SoundbankFolder[],
    isPrivateFolderLimitExceeded: boolean
};

const PrivateFolderList: React.FunctionComponent<{
    folders: SoundbankFolder[],
    setCurrentFolder: (folder: SoundbankFolder) => any,
    setShowAugmentateDialog: (flag: boolean) => any,
    setShowRecordDialog: (flag: boolean) => any,
    isPrivateFolderLimitExceeded: boolean
}> = ({ folders, setCurrentFolder, setShowAugmentateDialog, isPrivateFolderLimitExceeded, setShowRecordDialog }) => {
    const dispatch = useDispatch();
    const currentUser = useSelector(authDuck.hookSelectors.getUser);
    const users = useSelector(userDuck.hookSelectors.getUsers);
    const renderFolders = (f: SoundbankFolder[]) => {
        return (
            f.length > 0 ?
                <FolderList
                    handleRemove={(key: string) => {
                        confirm({
                            content: "Are you sure you want to delete this folder?",
                            onOk() {
                                dispatch(soundbankDuck.removeFolder(key));
                            }
                        })
                    }}
                    canAugmentate={!isPrivateFolderLimitExceeded}
                    handleAugmentate={(folder: SoundbankFolder) => {
                        setCurrentFolder(folder);
                        setShowAugmentateDialog(true);
                    }}
                    handleRecord={(folder: SoundbankFolder) => {
                        setCurrentFolder(folder);
                        setShowRecordDialog(true);
                    }}
                    isPrivate={true}
                    folders={f}
                /> :
                <span>Not found any folder</span>
        );
    };
    return (
        currentUser?.role === "admin" ?
            <>
                {
                    users.map((u) => {
                        return (
                            <div>
                                <Title level={4}>
                                    Folders for user {u.name}
                                </Title>
                                {renderFolders(folders.filter((f) => {
                                    return f.user === u.id;
                                }))}
                            </div>
                        );
                    })
                }
            </> :
            renderFolders(folders)
    );
};

const Soundbank: React.FunctionComponent<Props> = ({ publicFolders, privateFolders, isPrivateFolderLimitExceeded }) => {
    const [showUploadDialog, setShowUploadDialog] = useState(false);
    const [showAddDialog, setShowAddDialog] = useState(false);
    const [showAugmentateDialog, setShowAugmentateDialog] = useState(false);
    const [showRecordDialog, setShowRecordDialog] = useState(false);
    const [currentFolder, setCurrentFolder] = useState<SoundbankFolder | null>(null);
    const dispatch = useDispatch();
    return (
        <div>
            <Title level={3}>
                Soundbank
            </Title>
            {
                publicFolders.length > 0 &&
                    <>
                        <Divider orientation="left">
                            Public folders ({publicFolders.length})
                        </Divider>
                        <FolderList
                            folders={publicFolders}
                            canAugmentate={!isPrivateFolderLimitExceeded}
                            handleAugmentate={(folder: SoundbankFolder) => {
                                setCurrentFolder(folder);
                                setShowAugmentateDialog(true);
                            }}
                        />
                    </>
            }
            <Divider orientation="left">
                <Space>
                    <span>Private folders ({privateFolders.length})</span>
                    <Button
                        type="primary"
                        size="small"
                        onClick={() => {
                            setShowUploadDialog(!showUploadDialog);
                        }}
                        disabled={isPrivateFolderLimitExceeded}
                    >
                        Upload <UploadOutlined />
                    </Button>
                    <Button
                        type="ghost"
                        size="small"
                        onClick={() => {
                            setShowAddDialog(!showAddDialog);
                        }}
                        disabled={isPrivateFolderLimitExceeded}
                    >
                        Create <PlusCircleOutlined />
                    </Button>
                </Space>
            </Divider>
            <PrivateFolderList
                folders={privateFolders}
                isPrivateFolderLimitExceeded={isPrivateFolderLimitExceeded}
                setShowAugmentateDialog={setShowAugmentateDialog}
                setShowRecordDialog={setShowRecordDialog}
                setCurrentFolder={setCurrentFolder}
            />
            {
                showUploadDialog &&
                    <UploadFolderDialog
                        onAdd={(file: File) => {
                            return dispatch(soundbankDuck.uploadFolder(file)) as any;
                        }}
                        onClose={() => {
                            setShowUploadDialog(false);
                        }}
                    />
            }
            {
                showAugmentateDialog &&
                    <AugmentateDialog
                        folder={currentFolder!}
                        onAugmentate={(params) => {
                            return soundbankApi.augment(currentFolder!.key, params).then((res) => {
                                notify("success", `Successfully augmented new private folder '${res.key}'`);
                                dispatch(soundbankDuck.folderUploaded(res));
                            }, (res) => {
                                notify("error", `Failed to augmentate dataset; error: ${res && res.response && res.response.data || "unknown error"}`);
                            });
                        }}
                        onClose={() => {
                            setShowAugmentateDialog(false);
                        }}
                    />
            }
            {
                showRecordDialog &&
                    <RecordDialog
                        folder={currentFolder!}
                        onRecorded={(r: RecordSample) => {
                            return dispatch(soundbankDuck.recordSample(r)) as any;
                        }}
                        onClose={() => {
                            setShowRecordDialog(false);
                        }}
                    />
            }
            {
                showAddDialog &&
                    <AddDialog
                        onAdd={(name: string) => {
                            const isExisting = privateFolders.filter((f) => {
                                return f.title === name;
                            }).length > 0;
                            if (isExisting) {
                                notify("warn", `Folder with name ${name} already exists`);
                                return Promise.reject();
                            }
                            return dispatch(soundbankDuck.addFolder(name)) as any;
                        }}
                        onClose={() => {
                            setShowAddDialog(false);
                        }}
                    />
            }
        </div>
    );
}

function mapStateToProps({ soundbank, appConfig }: any) {
    const currentAppConfig = appConfigDuck.selectors.getConfig(appConfig);
    return {
        publicFolders: soundbankDuck.selectors.getPublicFolders(soundbank),
        privateFolders: soundbankDuck.selectors.getPrivateFolders(soundbank),
        isPrivateFolderLimitExceeded: soundbankDuck.selectors.isPrivateFoldersLimitExceeded(soundbank, currentAppConfig.maxPrivateFoldersPerUser)
    }
};

export default connect(mapStateToProps)(Soundbank);