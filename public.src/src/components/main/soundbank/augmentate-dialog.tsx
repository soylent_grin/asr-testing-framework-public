import React, { useState, useRef, useEffect } from 'react';

import { Modal, Spin } from 'antd';
import AugmentationParamList from '../jobs/common/augmentation-param-list';

type Props = {
    folder: SoundbankFolder,
    onAugmentate: (params: any[]) => Promise<any>,
    onClose: () => void
}

const AugmentateDialog: React.FunctionComponent<Props> = ({ onAugmentate, onClose, folder }) => {
    const [ isRunning, setIsRunning ] = useState(false);
    const paramListNode: any = useRef(null);
    return (
        <Modal
            title="Augmentation"
            visible
            onCancel={onClose}
            onOk={() => {
                const params = paramListNode.current && paramListNode.current.getData();
                setIsRunning(true);
                onAugmentate(params).then(() => {
                    setIsRunning(false);
                    onClose();
                }, () => {
                    setIsRunning(false);
                });
            }}
            okButtonProps={{
                disabled: isRunning
            }}
            cancelButtonProps={{
                disabled: isRunning
            }}
            width={650}
        >
            <Spin spinning={isRunning}>
                <AugmentationParamList
                    folder={folder}
                    ref={paramListNode}
                />
            </Spin>
        </Modal>
    );
};

export default AugmentateDialog;