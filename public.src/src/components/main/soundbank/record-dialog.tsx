import React, { useState } from 'react';

import axios from 'axios';

import { Modal, Button, Form, Input, Spin, Alert } from 'antd';
import { ReactMediaRecorder } from 'react-media-recorder';
import { LoadingOutlined } from '@ant-design/icons';
import { notify } from '../../../utils';
import ErrorBoundary from '../../../components/common/error-boundary';

type Props = {
    folder: SoundbankFolder,
    onRecorded: (sample: RecordSample) => Promise<any>,
    onClose: () => void
}

const AddFolderDialog: React.FunctionComponent<Props> = ({ onRecorded, onClose, folder }) => {
    const [ name, setName ] = useState("");
    const [ isUploading, setIsUploading ] = useState(false);
    const [ transcription, setTranscription ] = useState("");
    return (
        <Modal
            title="Record new sample"
            visible
            onCancel={onClose}
            footer={null}
        >
            <Spin spinning={isUploading}>
                <Form>
                    <Form.Item
                        label="Sample key"
                        name="name"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input
                            value={name}
                            onChange={(e) => {
                                setName(e.target.value)
                            }}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Transcription"
                        name="transcription"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input.TextArea
                            value={transcription}
                            onChange={(e) => {
                                setTranscription(e.target.value)
                            }}
                        />
                    </Form.Item>
                </Form>
                <ErrorBoundary renderFallback={() => {
                    return (
                        <Alert
                            type="warning"
                            message="Sorry, audio recording is not suported in this version"
                        />
                    );
                }}>
                    <ReactMediaRecorder
                        audio={{
                            sampleRate: 48000,
                            channelCount: 2
                        }}
                        onStop={(blobUrl) => {
                            setIsUploading(true);
                            axios.get(blobUrl, {
                                responseType: 'blob'
                            }).then(r => {
                                const audio = new File([r.data], "audio", {
                                    type: r.data.type
                                });
                                onRecorded({
                                    folder: folder.key,
                                    audio,
                                    name,
                                    transcription
                                }).then(() => {
                                    setIsUploading(false);
                                    onClose();
                                }, () => {
                                    setIsUploading(false);
                                });
                            }).catch((e) => {
                                notify("error", "Failed to pack recorded audio to file; error: " + e);
                                setIsUploading(false);
                            });
                        }}
                        render={({ status, startRecording, stopRecording, error, mediaBlobUrl }) => {
                            // "media_aborted" | "permission_denied" | "no_specified_media_found" | "media_in_use" | "invalid_media_constraints" | "no_constraints" | "recorder_error" | "idle" | "acquiring_media" | "delayed_start" | "recording" | "stopping" | "stopped";
                            let content;
                            if (status === "idle") {
                                content = <Button type="primary" onClick={startRecording}>Start Recording</Button>;
                            } else if (status === "recording") {
                                content = <Button type="primary" onClick={stopRecording}>
                                    <LoadingOutlined />&nbsp;Stop Recording
                                </Button>
                            } else if (status === "stopping" || status === "stopped") {
                                content = "finished, uploading...";
                            } else {
                                content = "Sorry, in-browser audio recording is not supported in current version";
                            }
                            return (
                                <div style={{
                                    display: "flex",
                                    justifyContent: "center"
                                }}>
                                    {content}
                                </div>
                            );
                        }}
                    />
                </ErrorBoundary>
            </Spin>
        </Modal>
    );
};

export default AddFolderDialog;