import React, { useState, useEffect } from 'react';
import { Tree, Dropdown, Menu, Space, Button, Popover } from 'antd';
import {
    SlidersOutlined,
    SettingOutlined,
    DeleteOutlined,
    CiCircleOutlined,
    AudioOutlined
} from '@ant-design/icons';

import AudioPlayer from '../jobs/common/audio-player';
import * as soundbankApi from '../../../api/soundbank';
import { renderDuration } from '../../../utils';

type Props = {
    folders: SoundbankFolder[],
    canAugmentate: boolean,
    isPrivate?: boolean,
    handleRecord?: (folder: SoundbankFolder) => any,
    handleAugmentate?: (folder: SoundbankFolder) => any,
    handleRemove?: (key: string) => any,
    readonly?: boolean
};

interface DataNode {
    title: string;
    key: string;
    isLeaf?: boolean;
    children?: DataNode[];
}

const Transcription: React.FunctionComponent<{transcription: string[]}> = ({ transcription }) => {
    return (
        <span>
            Transcription:
            <Popover placement="right" content={
                <span style={{
                    fontSize: "12px",
                    lineHeight: "16px",
                    maxHeight: "300px",
                    maxWidth: "400px"
                }}>
                    {transcription.join("\n")}
                </span>
            }>
                <Button type="link">show</Button>
            </Popover>
        </span>
    )
}

const Metadata: React.FunctionComponent<{meta: any}> = ({ meta }) => {
    return (
        <span>
            Metadata:
            <Popover placement="right" content={
                <pre style={{
                    fontSize: "12px",
                    lineHeight: "16px",
                    maxHeight: "300px",
                    maxWidth: "400px"
                }}>
                    {JSON.stringify(meta, null, 4)}
                </pre>
            }>
                <Button type="link">show</Button>
            </Popover>
        </span>
    )
}

// this is not optimal
function updateTreeData(list: any[], id: string, children: any[]): any[] {
    return list.map(node => {
      if (node.id === id) {
        return {
          ...node,
          children,
        };
      } else if (node.children) {
        return {
          ...node,
          children: updateTreeData(node.children, id, children),
        };
      }
      return node;
    });
}

const FolderList: React.FunctionComponent<Props> = (props) => {
    const [ folders, setFolders ] = useState(props.folders);
    useEffect(() => {
        setFolders(props.folders);
    }, [props.folders]);
    return (
        <Tree
            showLine={{
                showLeafIcon: false
            }}
            height={700}
            treeData={folders}
            titleRender={(node: any) => {
                if (node.samples) {
                    const duration = node.samples.reduce((acc: number, curr: SoundbankSampleDto) => acc + curr.duration, 0);
                    return (
                        <Space>
                            <span>{node.title} ({node.samples.length} samples, {renderDuration(duration)})</span>
                            {
                                !props.readonly &&
                                    <Dropdown overlay={
                                        <Menu onClick={(a: any) => {
                                            switch(a.key) {
                                            case "augmentate":
                                                props.handleAugmentate && props.handleAugmentate(node as SoundbankFolder)
                                                break;
                                            case "record":
                                                props.handleRecord && props.handleRecord(node as SoundbankFolder)
                                                break;
                                            case "delete":
                                                props.handleRemove && props.handleRemove(node.key)
                                                break;
                                            default:
                                                break;
                                            }
                                        }}>
                                            {
                                                node.samples && node.samples.length > 0 &&
                                                    <Menu.Item key="augmentate" icon={<SlidersOutlined />} disabled={!props.canAugmentate}>
                                                        Augmentate and create new folder
                                                    </Menu.Item>
                                            }
                                            {
                                                props.isPrivate && node.meta && (node.meta.recordedBy === "user") &&
                                                    <Menu.Item key="record" icon={<AudioOutlined />}>
                                                        Record new sample
                                                    </Menu.Item>
                                            }
                                            {
                                                props.isPrivate &&
                                                    <Menu.Item key="delete" icon={<DeleteOutlined />} danger>
                                                        Delete
                                                    </Menu.Item>
                                            }
                                        </Menu>
                                    }>
                                        <a>
                                            <SettingOutlined />
                                        </a>
                                    </Dropdown>
                            }
                        </Space>
                    );
                }
                return node.title;
            }}
            loadData={(node: any) => {
                const key = node.id;
                if (key && !node.loaded) {
                    console.log(`loading data for node: `, node);
                    return soundbankApi.loadSample(node.id).then((res) => {
                        const children = [
                            { title: <AudioPlayer sampleId={res.id} />, key: `${key}-audio`, isLeaf: true },
                            { title: <Transcription transcription={res.transcription} />, key: `${key}-transcription`, isLeaf: true },
                            res.meta && { title: <Metadata meta={res.meta} />, key: `${key}-meta`, isLeaf: true }
                        ].filter(a => a);
                        setFolders((origin: any): any => {
                            return updateTreeData(
                                origin,
                                key,
                                children
                            );
                        });
                    }).catch((e) => {
                        console.error(e);
                    });
                } else return Promise.resolve();
            }}
        />
    );
}

export default FolderList;