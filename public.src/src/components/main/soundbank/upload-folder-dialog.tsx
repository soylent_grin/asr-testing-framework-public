import React, { useState } from 'react';

import { Modal, Spin, Upload, Button, Alert, Typography } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

type Props = {
    onAdd: (file: File) => Promise<any>,
    onClose: () => void
}

const AddFolderDialog: React.FunctionComponent<Props> = ({ onAdd, onClose }) => {
    const [ isAdding, setIsAdding ] = useState(false);
    return (
        <Modal
            title="Upload folder"
            visible
            onCancel={onClose}
            footer={null}
        >
            <Spin spinning={isAdding}>
                <Alert
                    message={
                        <div>
                            <p>Valid soundbank folder is <Typography.Text code>.zip</Typography.Text> file with following structure:</p>
                            <pre style={{
                                fontSize: "12px",
                                fontFamily: "monospace"
                            }}>{`    folder-name.zip
    |
    |----any-sample-name
    |    |
    |    |any-sample-name.txt  # required
    |    |any-sample-name.wav  # required
    |    |any-sample-name.json # optional metadata file
    |
    |----other-sample-name
    ...`}</pre>
                        </div>
                    }
                    type="info"
                />
                <div style={{
                    textAlign: "center",
                    margin: "20px"
                }}>
                    <Upload
                        beforeUpload={(file: File) => {
                            setIsAdding(true);
                            onAdd(file).then(() => {
                                setIsAdding(false);
                                onClose();
                            }, () => {
                                setIsAdding(false);
                            });
                            return false
                        }}
                    >
                        <Button>
                            <UploadOutlined /> Click to Upload
                        </Button>
                    </Upload>
                </div>
            </Spin>
        </Modal>
    );
};

export default AddFolderDialog;