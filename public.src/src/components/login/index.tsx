import React, { useState } from 'react';
import { Form, Button, Card, Row, Col, Input, Divider } from 'antd';
import { GithubOutlined, GoogleOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';

import * as authApi from '../../api/auth';
import { hookSelectors } from '../../ducks/app-config';
import { notify } from '../../utils';
import { useSelector } from 'react-redux';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

type Props = {
    onSubmit?: () => void
};

type State = {};

function Login() {
    let history = useHistory();
    const appConfig = useSelector(hookSelectors.getConfig);
    const [ username, setUsername ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ isAuth, setIsAuth ] = useState(false);
    return (
        <React.Fragment>
            <Row>
                <Col flex="auto"></Col>
                <Col flex="500px">
                    <Card
                        title="Login"
                        bordered
                        style={{
                            width: 400,
                            marginTop: "40px"
                        }}
                    >
                        <div>
                            {
                                appConfig.githubAuthEnabled &&
                                    <a href="/api/authenticate/github">
                                        <Button
                                            style={{
                                                width: "100%",
                                                marginBottom: "10px"
                                            }}
                                            type="primary"
                                        >
                                            Sign in with GitHub <GithubOutlined />
                                        </Button>
                                    </a>
                            }
                            {
                                appConfig.googleAuthEnabled &&
                                    <a href="/api/authenticate/google">
                                        <Button
                                            style={{
                                                width: "100%"
                                            }}
                                            type="primary"
                                        >
                                            Sign in with Google <GoogleOutlined />
                                        </Button>
                                    </a>
                            }
                        </div>
                        <Form
                            {...layout}
                            name="basic"
                        >
                            <Divider />
                            <Form.Item
                                label="Username"
                                name="username"
                                rules={[{ required: true, message: 'Please input your username!' }]}
                            >
                                <Input
                                    value={username}
                                    onChange={(e) => {
                                        setUsername(e.target.value)
                                    }}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                            >
                                <Input.Password
                                    value={password}
                                    onChange={(e) => {
                                        setPassword(e.target.value)
                                    }}
                                />
                            </Form.Item>
                            <Form.Item {...tailLayout}>
                                <Button
                                    type="primary"
                                    disabled={isAuth}
                                    onClick={() => {
                                        setIsAuth(true);
                                        authApi.signIn(username, password).then(() => {
                                            setIsAuth(false);
                                            window.location.href = "/main/jobs";
                                        }, () => {
                                            notify("error", "Failed to sign in");
                                            setIsAuth(false);
                                        });
                                    }}
                                >
                                    Sign in
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Col>
                <Col flex="auto"></Col>
            </Row>
        </React.Fragment>
    );
}

export default Login;