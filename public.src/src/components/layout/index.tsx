import React, { FunctionComponent } from 'react';
import { Layout, Row, Col } from 'antd';
import Header from './header';

type Props = {
    isPrivate?: boolean
};

const { Content } = Layout;

const AppLayout: FunctionComponent<Props> = ({ children, isPrivate }) => {
    return (
        <Layout className="layout" style={{
            background: "white",
        }}>
            <Header />
            {
                isPrivate ?
                    <Row style={{
                        paddingTop: "20px"
                    }}>
                        <Col flex="auto" />
                        <Col flex="1100px">
                            <Content style={{ padding: '0 50px' }}>
                                    {children}
                            </Content>
                        </Col>
                        <Col flex="auto" />
                    </Row> :
                    children
            }
        </Layout>
    )
}

export default AppLayout;