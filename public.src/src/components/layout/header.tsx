import React from 'react';
import { DownOutlined } from '@ant-design/icons';

import {
    Link,
}
from "react-router-dom";

import { useSelector } from 'react-redux';
import * as authDuck from '../../ducks/auth';
import { isAllowed } from '../../utils';

import { Menu, Dropdown, Space, Row, Col } from 'antd';

import Avatar from '../common/avatar';

function AppHeader() {
    const user = useSelector(authDuck.hookSelectors.getUser);
    const isLoggedIn = useSelector(authDuck.hookSelectors.isLoggedIn);
    return (
        <header>
            <Row>
                <Col flex="1 1 200px">
                    <Space size="large">
                        <Link to="/" style={{
                            fontWeight: "bold",
                            textShadow: "1px 1px 2px #1890ff3b",
                            color: "#1890ff"
                        }}>
                            CVoAF
                        </Link>
                        {
                            isLoggedIn &&
                                <React.Fragment>
                                    <Link to="/main/jobs">Jobs</Link>
                                    <Link to="/main/soundbank">Soundbank</Link>
                                    {
                                        isAllowed(user, "viewUsers") &&
                                            <Link to="/main/users">Users</Link>
                                    }
                                </React.Fragment>
                        }
                        <Link to="/help">Help</Link>
                    </Space>
                </Col>
                <Col flex="0 1 300px" style={{
                    textAlign: "right"
                }}>
                    {
                        isLoggedIn ?
                            <Dropdown overlay={
                                <Menu>
                                    <Menu.Item>
                                        <a href="/api/logout/">
                                            Sign out
                                        </a>
                                    </Menu.Item>
                                </Menu>
                            }>
                                <Space>
                                    <span>
                                        {user!.name}
                                    </span>
                                    <Avatar user={user} />
                                    <DownOutlined />
                                </Space>
                            </Dropdown> :
                            <Link to="/login">Sign in</Link>
                    }
                </Col>
            </Row>
        </header>
    );
};

export default AppHeader;