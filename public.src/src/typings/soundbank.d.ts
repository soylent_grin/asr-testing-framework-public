interface SoundbankSampleDto {
    id: string,
    key: string,
    duration: number,
    value: string,
    title: string
}

interface SoundbankSample {
    id: string,
    key: string,
    transcription: string[],
    duration: number,
    meta?: any
}

interface RecordSample {
    folder: string,
    name: string,
    transcription: string,
    audio: Blob,
    meta?: any
}

interface SoundbankFolder {
    key: string,
    title: string,
    value: string,
    children: SoundbankSampleDto[],
    samples: SoundbankSampleDto[],
    user?: string
}

declare module 'react-audio-player';
declare module 'plotly.js-dist';