interface AsrExecutor {
    title: string,
    key: string,
    params: AsrExecutorParam[],
    capabilities: {
        train?: boolean
    }
}

interface AsrExectuorParamOption {
    key: string,
    title: string
}

interface AsrExecutorParam {
    key: string,
    paramType: string,
    defaultValue?: string,
    title: string,
    description?: string,
    scope?: string[],
    options?: AsrExectuorParamOption[]
}

interface AsrJobStepMetricConfig {
    metrics: string[]
}

/*
id: Option[UUID],
jobId: Option[UUID],
index: Int,
stepType: AsrStepType,
status: Option[AsrExecutionStatus],
config: JsValue,
samples: List[String],
inputType: Option[AsrJobStepInputType],
inputJob: Option[UUID],
defaultModelName: Option[String],
duration: Option[Long]
*/

interface AsrJobStepSamples {
    type: "current" | "previous",
    list: string[]
}

interface AsrJobStep {
    id?: string,
    index: number,
    inputType?: "default" | "previous" | "current",
    stepType: "dataset" | "train" | "test" | "measure",
    config: any,
    samples: AsrJobStepSamples,
    status?: string,
    inputJob?: string,
    duration?: number,
    defaultModelName?: string,
    jobId?: string
}

interface AsrJob {
    id?: string, 
    asrKey?: string,
    timeCreate?: number, 
    timeBegin?: number, // опционально
    timeEnd?: number, // опционально
    status?: "waiting" | "running" | "success" | "error" | "terminated",
    steps: AsrJobStep[],
    user?: string,
    label?: string
}

interface AsrJobMetric {
    key: string,
    title: string,
    description?: string
}

interface HasGetData {
    getData: () => any
}

interface ResultTranscription  {
    sample: string,
    transcription: string,
    reference: string
}
interface User {
    id: string,
    name: string,
    email?: string,
    avatarUrl?: string,
    role: "admin" | "user",
    group?: string
}

interface AppConfig {
    githubAuthEnabled: boolean,
    googleAuthEnabled: boolean,
    maxJobsPerUser: number,
    maxPrivateFoldersPerUser: number,
    maxSamplesPerJob: number
}

interface AsrModel {
    name: string
}