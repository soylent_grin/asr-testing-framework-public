const CracoAntDesignPlugin = require("craco-antd");

module.exports = {
  plugins: [{ plugin: CracoAntDesignPlugin }],
  devServer: {
      proxy: {
          '/api/websocket': {
              target: 'ws://lvh.me:9000',
              ws: true,
          },
          '/api': 'http://lvh.me:9000'
      }
  }
};
