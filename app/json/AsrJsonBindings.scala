package json

import dtos._
import enums.MorphoFeature
import models.metrics.morpho.{MorphoInfoChunk, MorphoInfoExtendedChunk}
import models.{AsrJob, AsrMetricInput, AsrMetricOutput, AsrMetricOutputResult, AsrMetricOutputTagsResult, AsrMetricTagsInput, AsrStep, AsrStepResult, AsrStepSamples, SoundbankFolder, SoundbankSample}
import play.api.libs.json.{JsPath, JsSuccess, JsValue, Json, Reads, Writes}
import play.api.libs.functional.syntax._

object AsrJsonBindings {

  import TimestampJsonBindings._

  implicit val asrParamOptionFormat = Json.format[ParamOption]

  implicit val asrParamTypeDtoFormat = Json.format[ParamDto]

  implicit val augmentationParamsDtoJsonFormat = Json.format[AugmentationParamsDto]

  implicit val asrMetricTagsInputJsonFormat = Json.format[AsrMetricTagsInput]

  implicit val asrMetricInputJsonFormat = Json.format[AsrMetricInput]

  implicit val asrMetricOutputTagsResultJsonFormat = Json.format[AsrMetricOutputTagsResult]

  implicit val asrMetricOutputResultJsonFormat = Json.format[AsrMetricOutputResult]

  implicit val asrMetricOutputJsonFormat = Json.format[AsrMetricOutput]

  implicit val asrExecutorCapabilitiesJsonFormat = Json.format[AsrExecutorCapabilities]

  implicit val asrStepSamplesJsonFormat = Json.format[AsrStepSamples]

  implicit val asrStepJsonFormat = Json.format[AsrStep]

  implicit val asrParamListDtoWrites = Json.writes[AsrParamListDto]

  implicit val asrJobDtoFormat = Json.format[AsrJobDto]

  implicit val asrExecutorInfoJsonFormat = Json.format[AsrExecutorInfoDto]

  implicit val asrStepResultJsonFormat = Json.format[AsrStepResult]

  implicit val sampleDtoJsonFormat = Json.format[SampleDto]

  implicit val folderJsonWrites = new Writes[SoundbankFolder] {
    def writes(folder: SoundbankFolder) = Json.obj(
      "key" -> folder.key,
      "title" -> folder.title,
      "samples" -> Json.toJson(folder.samples.map(new SampleDto(_))),
      "meta" -> Json.toJson(folder.meta),
      "user" -> Json.toJson(folder.user)
    )
  }

  implicit val sampleJsonWrites = new Writes[SoundbankSample] {
    def writes(sample: SoundbankSample) = Json.obj(
      "id" -> sample.id,
      "key" -> sample.key,
      "transcription" -> sample.transcription,
      "duration" -> sample.duration,
      "sampleRate" -> sample.sampleRate,
      "meta" -> sample.meta
    )
  }

  implicit val asrJobJsonFormat = Json.format[AsrJob]


  implicit val mapReads: Reads[Map[String, Option[String]]] = {
    jv: JsValue =>
      JsSuccess(jv.as[Map[String, String]].map {
        case (k, v) =>
          var fixedValue = None: Option[String]
          if (v != null) {
            fixedValue = Option[String](v.asInstanceOf[String])
          }
          k.asInstanceOf[String] -> fixedValue
      })
  }

  implicit val mapOfFeaturesReads: Reads[Map[MorphoFeature.Value, String]] = {
    jv: JsValue =>
      JsSuccess(jv.as[Map[String, String]].map {
        case (k, v) =>
          var fixedValue = None: Option[String]
          if (v != null) {
            fixedValue = Some(v)
          }
          MorphoFeature.fromString(k.asInstanceOf[String]) -> fixedValue
      }.filter(pair => pair._2.isDefined).mapValues(_.get))
  }

  implicit val mapToExtendedChunkReads: Reads[Map[String, MorphoInfoChunk]] = {
    jv: JsValue =>
      JsSuccess({
        var result = Map[String, MorphoInfoChunk]()
        for (chunk <- jv.as[List[MorphoInfoExtendedChunk]]) {
          result += (chunk.token -> MorphoInfoChunk(chunk.lemma, chunk.features))
        }
        result
      })
  }

  implicit val morphoInfoChunkReads: Reads[MorphoInfoChunk] = (
    (JsPath \ "lemma").read[String] and
      (JsPath \ "features").read[Map[MorphoFeature.Value, String]]
    ) (MorphoInfoChunk.apply _)

  implicit val morphoInfoExtendedChunkReads: Reads[MorphoInfoExtendedChunk] = (
    (JsPath \ "token").read[String] and
      (JsPath \ "lemma").read[String] and
      (JsPath \ "features").read[Map[MorphoFeature.Value, String]]
    ) (MorphoInfoExtendedChunk.apply _)

  implicit val appConfigDtoJsonFormat = Json.format[AppConfigDto]
}
