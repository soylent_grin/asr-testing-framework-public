package json

import dtos.SignInDto
import models.User
import play.api.libs.json.Json

object UserJsonBinding {
  implicit val signInDtoJsonFormat = Json.format[SignInDto]
  implicit val userJsonFormat = Json.format[User]
}
