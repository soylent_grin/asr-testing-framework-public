package json

import org.joda.time.DateTime
import play.api.libs.json._

object TimestampJsonBindings {

  implicit val calendarJsonFormat = new Format[DateTime] {
    override def reads(json: JsValue): JsResult[DateTime] = {
      JsSuccess(new DateTime(json.as[Long]))
    }
    override def writes(o: DateTime): JsValue = {
      JsNumber(o.getMillis)
    }
  }

}
