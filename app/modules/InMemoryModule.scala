package modules

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.libs.akka.AkkaGuiceSupport
import repositories._
import repositories.interpreters.memory._

class InMemoryModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind[UserRepository].to(classOf[InMemoryUserRepository]).asEagerSingleton()
    bind[AsrJobRepository].to(classOf[InMemoryAsrJobRepository]).asEagerSingleton()
    bind[AsrStepResultRepository].to(classOf[InMemoryAsrStepResultRepository]).asEagerSingleton()
    bind[AsrStepRepository].to(classOf[InMemoryAsrStepRepository]).asEagerSingleton()
  }
}
