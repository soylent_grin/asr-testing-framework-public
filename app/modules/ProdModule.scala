package modules

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.libs.akka.AkkaGuiceSupport
import services.python.{PythonExecutionService, PythonExecutionServiceImpl}

class ProdModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind[PythonExecutionService].to(classOf[PythonExecutionServiceImpl]).asEagerSingleton()
  }
}
