package modules

import com.google.inject.{AbstractModule, Provides}
import com.mohiva.play.silhouette.api.actions.SecuredErrorHandler
import com.mohiva.play.silhouette.api.crypto.{Base64AuthenticatorEncoder, Signer}
import com.mohiva.play.silhouette.api.repositories.{AuthInfoRepository, AuthenticatorRepository}
import com.mohiva.play.silhouette.api.services.{AuthenticatorService, IdentityService}
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.api.{Environment, EventBus, Silhouette, SilhouetteProvider}
import com.mohiva.play.silhouette.crypto.{JcaSigner, JcaSignerSettings}
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, CookieAuthenticatorService, CookieAuthenticatorSettings}
import com.mohiva.play.silhouette.impl.providers.oauth1.services.PlayOAuth1Service
import com.mohiva.play.silhouette.impl.providers.{DefaultSocialStateHandler, OAuth1Settings, OAuth2Settings, SocialProviderRegistry, SocialStateHandler}
import com.mohiva.play.silhouette.impl.providers.oauth2.{GitHubProvider, GoogleProvider}
import com.mohiva.play.silhouette.impl.providers.state.{CsrfStateItemHandler, CsrfStateSettings}
import com.mohiva.play.silhouette.impl.util.{DefaultFingerprintGenerator, SecureRandomIDGenerator}
import com.mohiva.play.silhouette.password.BCryptSha256PasswordHasher
import com.mohiva.play.silhouette.persistence.daos.{DelegableAuthInfoDAO, InMemoryAuthInfoDAO}
import com.mohiva.play.silhouette.persistence.repositories.DelegableAuthInfoRepository
import javax.inject.{Named, Singleton}
import models.UserIdentity
import models.auth.SessionEnv
import net.codingwell.scalaguice.ScalaModule
import play.api.Configuration
import play.api.libs.ws.WSClient
import play.api.mvc.CookieHeaderEncoding
import services.{UserSyncService, UserSyncServiceImpl}
import services.auth.{InMemoryUserAuthenticatorRepository, PasswordInfoDAO, UserIdentityService}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

class AuthenticationModule extends AbstractModule with ScalaModule {
  override def configure(): Unit = {
    bind[AuthenticatorRepository[CookieAuthenticator]].to[InMemoryUserAuthenticatorRepository]
    bind[DelegableAuthInfoDAO[PasswordInfo]].toInstance(new InMemoryAuthInfoDAO[PasswordInfo]())
    bind[IdentityService[UserIdentity]].to[UserIdentityService]
    bind[Silhouette[SessionEnv]].to[SilhouetteProvider[SessionEnv]]
    bind[UserSyncService].to(classOf[UserSyncServiceImpl]).asEagerSingleton()
  }

  @Provides
  def provideIDGenerator(implicit ec: ExecutionContext): IDGenerator = {
    new SecureRandomIDGenerator()
  }

  @Provides
  @Singleton
  def provideAuthInfoRepository(passwordInfoDAO: DelegableAuthInfoDAO[PasswordInfo])
                               (implicit ec: ExecutionContext): AuthInfoRepository = {
    new DelegableAuthInfoRepository(passwordInfoDAO)
  }

  @Provides
  def provideEnvironment(userService: UserIdentityService,
                         authenticatorService: AuthenticatorService[CookieAuthenticator],
                         eventBus: EventBus)
                        (implicit ec: ExecutionContext): Environment[SessionEnv] =
    Environment[SessionEnv](userService, authenticatorService, Seq(), eventBus)

  @Provides
  @Singleton
  def provideAuthenticatorService(repository: AuthenticatorRepository[CookieAuthenticator],
                                  cookieHeaderEncoding: CookieHeaderEncoding,
                                  configuration: Configuration)
                                 (implicit ec: ExecutionContext): AuthenticatorService[CookieAuthenticator] = {
    val signerKey = "12345678901234567890"

    val jcaSignerSettings = JcaSignerSettings(signerKey)
    val signer = new JcaSigner(jcaSignerSettings)
    val fingerprintGenerator = new DefaultFingerprintGenerator()
    val authenticatorEncoder = new Base64AuthenticatorEncoder()

    val settings: CookieAuthenticatorSettings = CookieAuthenticatorSettings(
      secureCookie = false
    )

    new CookieAuthenticatorService(
      settings,
      Some(repository),
      signer,
      cookieHeaderEncoding,
      authenticatorEncoder,
      fingerprintGenerator,
      new SecureRandomIDGenerator(),
      Clock()
    )
  }

  @Provides
  @Singleton
  def providePasswordHasherRegistry(): PasswordHasherRegistry = {
    PasswordHasherRegistry(new BCryptSha256PasswordHasher())
  }

  @Provides
  def provideSocialProviderRegistry(gitHubProvider: GitHubProvider,
                                    googleProvider: GoogleProvider,
                                    configuration: Configuration): SocialProviderRegistry = {
    SocialProviderRegistry(Seq(
      configuration.getOptional[String]("silhouette.github.clientID").map(_ => gitHubProvider),
      configuration.getOptional[String]("silhouette.google.clientID").map(_ => googleProvider)
    ).filter(_.isDefined).map(_.get))
  }

  @Provides
  def provideHTTPLayer(client: WSClient)(implicit ec: ExecutionContext): HTTPLayer = new PlayHTTPLayer(client)

  @Provides
  @Named("social-state-signer")
  def provideSocialStateSigner(configuration: Configuration): Signer = {
    val config = JcaSignerSettings(
      key = configuration.get[String]("silhouette.socialStateHandler.signer.key")
    )

    new JcaSigner(config)
  }

  @Provides
  @Named("csrf-state-item-signer")
  def provideCSRFStateItemSigner(configuration: Configuration): Signer = {
    val config = JcaSignerSettings(configuration.get[String]("silhouette.csrfStateItemHandler.signer.key"))

    new JcaSigner(config)
  }

  @Provides
  def provideCsrfStateItemHandler(
                                   idGenerator: IDGenerator,
                                   @Named("csrf-state-item-signer") signer: Signer,
                                   configuration: Configuration): CsrfStateItemHandler = {
    val settings = CsrfStateSettings(
      cookieName = configuration.get[String]("silhouette.csrfStateItemHandler.cookieName"),
      cookiePath = configuration.get[String]("silhouette.csrfStateItemHandler.cookiePath"),
      secureCookie = configuration.get[Boolean]("silhouette.csrfStateItemHandler.secureCookie"),
      httpOnlyCookie = configuration.get[Boolean]("silhouette.csrfStateItemHandler.httpOnlyCookie"),
      expirationTime = configuration.get[FiniteDuration]("silhouette.csrfStateItemHandler.expirationTime"),
    )

    new CsrfStateItemHandler(settings, idGenerator, signer)
  }

  @Provides
  def provideSocialStateHandler(@Named("social-state-signer") signer: Signer,
                                csrfStateItemHandler: CsrfStateItemHandler): DefaultSocialStateHandler = {
    new DefaultSocialStateHandler(Set(csrfStateItemHandler), signer)
  }

  @Provides
  def provideGithubProvider(httpLayer: HTTPLayer,
                            socialStateHandler: DefaultSocialStateHandler,
                            configuration: Configuration): GitHubProvider = {

    val config = OAuth2Settings(
      authorizationURL = configuration.getOptional[String]("silhouette.github.authorizationURL"),
      accessTokenURL = configuration.get[String]("silhouette.github.accessTokenURL"),
      redirectURL = configuration.getOptional[String]("silhouette.github.redirectURL"),
      clientID = configuration.getOptional[String]("silhouette.github.clientID").getOrElse("?"),
      clientSecret = configuration.getOptional[String]("silhouette.github.clientSecret").getOrElse("?"),
    )

    new GitHubProvider(httpLayer, socialStateHandler, config)
  }

  @Provides
  def provideGoogleProvider(httpLayer: HTTPLayer,
                            socialStateHandler: DefaultSocialStateHandler,
                            configuration: Configuration): GoogleProvider = {

    val config = OAuth2Settings(
      authorizationURL = configuration.getOptional[String]("silhouette.google.authorizationURL"),
      accessTokenURL = configuration.get[String]("silhouette.google.accessTokenURL"),
      redirectURL = configuration.getOptional[String]("silhouette.google.redirectURL"),
      clientID = configuration.getOptional[String]("silhouette.google.clientID").getOrElse("?"),
      clientSecret = configuration.getOptional[String]("silhouette.google.clientSecret").getOrElse("?"),
      scope = configuration.getOptional[String]("silhouette.google.scope")
    )

    new GoogleProvider(httpLayer, socialStateHandler, config)
  }
}
