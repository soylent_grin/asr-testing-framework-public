package modules

import actors.{AsrExecutorActor, AsrExecutorActorFactory}
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.libs.akka.AkkaGuiceSupport
import repositories._
import repositories.interpreters.memory._
import services.{UserSyncService, UserSyncServiceImpl}
import services.python.{MorphoPythonService, MorphoPythonServiceImpl}

class Module extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  override def configure(): Unit = {

    bindActorFactory(classOf[AsrExecutorActor], classOf[AsrExecutorActorFactory])

    bind[SoundbankRepository].to(classOf[InMemorySoundBankRepository]).asEagerSingleton()
    bind[NoiseRepository].to(classOf[InMemoryNoiseRepository]).asEagerSingleton()
    bind[AsrJobLogRepository].to(classOf[InMemoryAsrJobLogRepository]).asEagerSingleton()
    bind[AsrModelRepository].to(classOf[InMemoryAsrModelRepository]).asEagerSingleton()
    bind[MorphoPythonService].to(classOf[MorphoPythonServiceImpl]).asEagerSingleton()
  }
}
