package modules

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.libs.akka.AkkaGuiceSupport
import repositories.SoundbankRepository
import repositories.interpreters.memory.InMemorySoundBankTestRepository

class TestModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind[SoundbankRepository].to(classOf[InMemorySoundBankTestRepository]).asEagerSingleton()
  }
}
