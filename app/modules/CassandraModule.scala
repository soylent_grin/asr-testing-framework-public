package modules

import com.google.inject.AbstractModule
import com.outworkers.phantom.connectors.CassandraConnection
import net.codingwell.scalaguice.ScalaModule
import play.libs.akka.AkkaGuiceSupport
import repositories._
import repositories.interpreters.cassandra._

class CassandraModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind[CassandraConnection].toProvider[CassandraConnectionProvider].asEagerSingleton()
    bind[AsrJobRepository].to(classOf[CassandraAsrJobRepository]).asEagerSingleton()
    bind[AsrStepResultRepository].to(classOf[CassandraAsrStepResultRepository]).asEagerSingleton()
    bind[AsrStepRepository].to(classOf[CassandraAsrStepRepository]).asEagerSingleton()
    bind[UserRepository].to(classOf[CassandraUserRepository]).asEagerSingleton()
  }
}
