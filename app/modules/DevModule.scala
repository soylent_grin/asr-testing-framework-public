package modules

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.libs.akka.AkkaGuiceSupport
import services.python.{PythonExecutionService, PythonExecutionServiceDev}

class DevModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind[PythonExecutionService].to(classOf[PythonExecutionServiceDev]).asEagerSingleton()
  }
}
