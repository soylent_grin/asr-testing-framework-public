package services

import actors.AsrExecutorActorFactory
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import beans.AsrJobBean
import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import dtos.AsrJobDto
import enums.AsrJobActorAcquireStrategy
import enums.AsrJobActorAcquireStrategy.AsrJobActorAcquireStrategy
import play.api.Logger

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[AsrExecutorActorFactoryServiceImpl])
trait AsrExecutorActorFactoryService {

  def acquireActor(job: AsrJobDto)(implicit bean: AsrJobBean): ActorRef

}

@Singleton
class AsrExecutorActorFactoryServiceImpl @Inject()(config: Config,
                                                   system: ActorSystem,
                                                   asrExecutorActorFactory: AsrExecutorActorFactory,
                                                   implicit val ec: ExecutionContext)
  extends AsrExecutorActorFactoryService {

  private implicit val logger = Logger(getClass)

  private implicit val acquireStrategy: AsrJobActorAcquireStrategy = AsrJobActorAcquireStrategy.getForName(
    config.getString("asr.jobActorAcquireStrategy")
  )

  logger.debug(s"using acquire strategy: $acquireStrategy")

  private val actors: TrieMap[String, ActorRef] = TrieMap()

  def injectedChild(create: => Actor, name: String, props: Props => Props = identity): ActorRef = {
    system.actorOf(props(Props(create)), name)
  }

  override def acquireActor(job: AsrJobDto)(implicit bean: AsrJobBean): ActorRef = {
    val id = AsrJobActorAcquireStrategy.jobToId(job)
    actors.get(id) match {
      case Some(actorRef) =>
        actorRef
      case _ =>
        val newActor = injectedChild(asrExecutorActorFactory.apply(bean), id)
        actors(id) = newActor
        newActor
    }
  }

}
