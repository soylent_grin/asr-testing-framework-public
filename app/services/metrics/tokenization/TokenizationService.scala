package services.metrics.tokenization

import com.google.inject.{ImplementedBy, Inject}
import com.typesafe.config.Config
import play.inject.Injector
import services.metrics.MetricsLogger
import utils.assets.FileUtils.makeMapping
import utils.metrics.tokenization.Regexp._
import utils.metrics.tokenization.Tagged

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.util.Try

@ImplementedBy(classOf[TokenizationServiceImpl])
trait TokenizationService {
  def tokenizeToPhonemes(logger: MetricsLogger)(ref: String): Array[String]
  def tokenizeToChars(logger: MetricsLogger)(ref: String): Array[String]
  def tokenizeWithTags(logger: MetricsLogger)(ref: String): (ListBuffer[String], ListBuffer[Int], ListBuffer[Set[String]], ListBuffer[(Int, String)])
}

class TokenizationServiceImpl @Inject()(injector: Injector,
                                        config: Config) extends TokenizationService {

  private implicit val defaultMetricsLogger: MetricsLogger = MetricsLogger()
  implicit private val delimiter: String = config.getString("metric.phonemesMappingDelimiter")

  private val tokenizer = config.getString("metric.tokenization.regexp.advancedComplexName")
  private val complexNameSeparator = config.getString("metric.tokenization.regexp.complexNameSeparator")
  private val garbageTags = Try(config.getStringList("asr.tags.garbage.list").asScala.toList)
  private val garbageTagsWithValues = config.getStringList("asr.tags.garbage.with.value").asScala.toList

  private var dicMap: Option[Map[String, List[String]]] = None

  def getDicMap(implicit metricsLogger: MetricsLogger): Map[String, List[String]] = {
    if (dicMap.isEmpty){
      dicMap = Some(makeMapping[List[String]](
        path = config.getString("metric.phonemesDicPath"),
        extractReplacingValue = phonemes => phonemes.slice(1, phonemes.size),
        composeDefault = (token, result) => tokenizeToChars(metricsLogger)(token).map(token =>
          if (result.contains(token)) result(token).head else token).toList
      ))
    }
    dicMap.get
  }

  // if there is no such word in dict, then split word into chars and find phonemes of each char separately
  // if even character is not presented in dict, then take character itself as a phoneme

  def tokenizeToPhonemes(logger: MetricsLogger)(ref: String): Array[String] = {
    getDicMap(logger)(ref).toArray
  }

  def tokenizeToChars(logger: MetricsLogger)(ref: String): Array[String] = {
    tokenizeByRegexp(ref, config.getString("metric.tokenization.regexp.char"))
  }

  def tokenizeWithTags(logger: MetricsLogger)(ref: String): (ListBuffer[String], ListBuffer[Int], ListBuffer[Set[String]], ListBuffer[(Int, String)]) =
    Tagged.tokenize(tokenizer, complexNameSeparator, garbageTags.getOrElse(List[String]()), garbageTagsWithValues)(ref)(logger)
}
