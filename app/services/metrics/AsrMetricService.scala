package services.metrics

import java.io.File

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import enums.Metric
import factories.TokenSequenceFactory
import models._
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import play.api.Logger
import play.inject.Injector
import services.metrics.averaging.AverageMetricService
import services.metrics.calculation.MetricsCalculationProxyService
import services.metrics.morpho.MorphoInfoExtractionService
import utils.Rounders._
import utils.metrics.Converter
import utils.metrics.Summarizer.summarize
import utils.metrics.LoggingUtils._
import utils.metrics.tokens.TokenSequenceUtils.extractWords

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[AsrMetricServiceImpl])
trait AsrMetricService {
  @deprecated("Obsolete version", "for a very long time")
  def getMetrics(pairs: Seq[(SoundbankSample, List[String])],
                 jobDurationInSeconds: Long): Future[Map[String, String]]

  def getMetricsFinal(pairs: Seq[(SoundbankSample, List[String])],
                      jobDurationInSeconds: Long,
                      input: AsrMetricInput): Future[AsrMetricOutput]
  def getLog: File
}

@Singleton
class AsrMetricServiceImpl @Inject()(injector: Injector,
                                     morphoInfoExtractor: MorphoInfoExtractionService,
                                     tokenSequenceFactory: TokenSequenceFactory,
                                     metricsCalculationHubService: MetricsCalculationProxyService,
                                     averageMetricService: AverageMetricService,
                                     config: Config,
                                     implicit val ec: ExecutionContext) extends AsrMetricService {

  private implicit val logger: MetricsLogger = MetricsLogger()

  override def getLog: File = logger.getLogFile

  def getMetrics(pairs: Seq[(SoundbankSample, List[String])],
                 jobDurationInSeconds: Long): Future[Map[String, String]] = Future {
    Map[String, String]()
  }

  // get metrics for concrete tokenSequence having concrete morpho info
  def getMetricsIndividual(metrics: Seq[Metric.Value],
                           tags: Option[List[String]],
                           tokenSequence: TokenSequence,
                           morphoInfo: Option[Map[String, MorphoInfoChunk]]): Map[Metric.Value, Option[_]] = {
    logger.debug(tokenSequence.id.formatted(config.getString("metric.logging.header.metrics")))
    val result = mutable.Map[Metric.Value, Option[_]]()
    for (metric <- metrics) {
      try {
        val metricValue = ceilOptional(metricsCalculationHubService.calculateMetric(metric, tokenSequence, morphoInfo, tags), config.getInt("metric.decimalPlaces"))
        result(metric) = metricValue
        logMetric(metric, tags, metricValue.getOrElse(config.getString("metric.noneReplacement")),
          config.getInt("metric.decimalPlaces"),
          config.getBoolean("metric.rightPadResultsWithZeros"))(logger, config)
      } catch {
        case error: Throwable =>
          logError(error, s"Cannot calculate metric $metric on sample ${tokenSequence.id} ${if (tags.nonEmpty) f"and tags ${tags.mkString(", ")}"}: $error. Skipping...")
      }
    }
    result.toMap
  }

  def getMetricsFinal(pairs: Seq[(SoundbankSample, List[String])],
                      jobDurationInSeconds: Long,
                      input: AsrMetricInput): Future[AsrMetricOutput] = Future {

    implicit val tokenSequences: Seq[TokenSequence] = tokenSequenceFactory.make(pairs)
    val globalMetrics = input.metrics.map(item => Metric.fromString(item).get)
    val localMetrics = for (tagGroup <- input.tagGroups) yield (tagGroup.tags, tagGroup.metrics.map(item => Metric.fromString(item).get))
    val cumulativeMetrics = Metric.cumulativeMetrics()
    val words = extractWords

    // Extract morpho info
    val morphoInfo = morphoInfoExtractor.getMorphoInfo(words, globalMetrics, localMetrics)
    logMorphoInfo(morphoInfo)(logger, config)

    // Calculate metrics
    for (tokenSequence <- tokenSequences){
      // Without tags
      logger.debug(tokenSequence.id.formatted(tokenSequence.id.formatted(config.getString("metric.logging.header.calculatingMetricsWithoutTags"))))
      logAlignment(tokenSequence, f"${tokenSequence.id} without tags")(logger, config)
      tokenSequence.metrics.result = getMetricsIndividual(
        globalMetrics.filter(!cumulativeMetrics.contains(_)),
        tags = None,
        tokenSequence,
        morphoInfo
      )

      for ((tags, metrics) <- localMetrics if tags.nonEmpty && metrics.nonEmpty){
        // With tags
        logger.debug(config.getString("metric.logging.header.filteringByTags").format(tokenSequence.id, tags.mkString(", ")))
        val taggedTokenSequence = tokenSequence.filterAndLog(
          _.filterByPresentedTags(tags.toSet),
          logger.debug(_: String)
        )
        logAlignment(taggedTokenSequence, config.getString("metric.logging.header.withTags").format(tokenSequence.id, tags.mkString(", ")))(logger, config)
        tokenSequence.metrics.byTags :+= AsrMetricOutputTagsResultAny(tags,
          getMetricsIndividual(
            metrics.filter(!cumulativeMetrics.contains(_)),
            tags = Some(tags),
            taggedTokenSequence,
            morphoInfo
          ))
        tokenSequence.pullWeights(taggedTokenSequence, tags)
      }
    }

    // Calculate average
    logger.debug(tokenSequences.map(_.id).mkString(", ").formatted(config.getString("metric.logging.header.average")))
    val asrMetricOutput = Converter.tokenSequencesToAsrMetricOutput(
      tokenSequences,
      averageMetricService.getAverageOrEmpty(tokenSequences, globalMetrics, localMetrics, jobDurationInSeconds),
      config.getInt("metric.decimalPlaces"),
      config.getBoolean("metric.rightPadResultsWithZeros"),
      config.getString("metric.noneReplacement"))

    // Summarize and return result
    logger.debug(summarize(asrMetricOutput))
    asrMetricOutput
  }
}
