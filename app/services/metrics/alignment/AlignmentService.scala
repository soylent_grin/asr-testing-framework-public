package services.metrics.alignment

import com.google.inject.ImplementedBy
import enums.WerOperation
import utils.metrics.alignment.LevenshteinDistance

@ImplementedBy(classOf[AlignmentServiceImpl])
trait AlignmentService {
  def align(referenceTokens: Seq[String], hypothesisTokens: Seq[String]): List[(Int, Int, WerOperation.Value)]
  def align(referenceToken: String, hypothesisToken: String): WerOperation.Value
}

class AlignmentServiceImpl extends AlignmentService {
  private def areEquals(refToken: String, hypToken: String): Boolean = refToken.equalsIgnoreCase(hypToken)

  def align(referenceTokens: Seq[String], hypothesisTokens: Seq[String]): List[(Int, Int, WerOperation.Value)]
  = LevenshteinDistance.align(referenceTokens, hypothesisTokens, areEquals)

  def align(referenceToken: String, hypothesisToken: String): WerOperation.Value =
    if (areEquals(referenceToken, hypothesisToken)) {
      WerOperation.OK
    } else {
      WerOperation.SUB
    }
}
