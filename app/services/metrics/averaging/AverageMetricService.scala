package services.metrics.averaging

import com.google.inject.{ImplementedBy, Inject}
import com.typesafe.config.Config
import enums.Metric
import models.metrics.tokens.TokenSequence
import models.{AsrMetricOutputResultAny, AsrMetricOutputTagsResultAny}
import play.api.Logger
import services.metrics.MetricsLogger
import services.metrics.calculation.{MetricsCalculationProxyService, MetricsCalculationService}
import utils.Rounders.ceilOptional
import utils.metrics.LoggingUtils.{logError, logMetricValues}
import utils.metrics.averaging.Calculators.{addWeightedScores, divideWeightedScores, getWeightedScoreForAny}
import utils.metrics.averaging.TokenSequenceCollectionHandlers.getSumOfWeights

import scala.collection.mutable

@ImplementedBy(classOf[AverageMetricServiceImpl])
trait AverageMetricService {
  def getAverageOrEmpty(tokenSequences: Seq[TokenSequence],
                        globalMetrics: Seq[Metric.Value],
                        localMetrics: Seq[(Seq[String], Seq[Metric.Value])],
                        jobDurationInMilliseconds: Long)
                       (implicit logger: MetricsLogger): AsrMetricOutputResultAny
}

class AverageMetricServiceImpl @Inject()(metricsCalculationService: MetricsCalculationService,
                                         metricsCalculationProxyService: MetricsCalculationProxyService,
                                         implicit val config: Config) extends AverageMetricService {

  def getCalculatedMetrics(tokenSequence: TokenSequence, tags: Option[Seq[String]]): Map[Metric.Value, Option[_]] =
    if (tags.isEmpty){
      tokenSequence.metrics.result
    } else {
      tokenSequence.metrics.byTags.filter(asrMetricOutputTagsResultAny => asrMetricOutputTagsResultAny.tags == tags.get) match {
        case filteredTokenSequence if filteredTokenSequence.nonEmpty => filteredTokenSequence.head.result
        case _ => Map[Metric.Value, Option[_]]()
      }
    }

  def performSumAtomicStep(result: mutable.Map[Metric.Value, Option[_]], metric: Metric.Value, weight: Any, added: Any): Unit = {
    if (result contains metric){
      result(metric) = Some(addWeightedScores(result(metric).get, getWeightedScoreForAny(metric, config.getDouble("metric.defaultMappedWeight"))(weight, added)))
    } else {
      result(metric) = Some(getWeightedScoreForAny(metric, config.getDouble("metric.defaultMappedWeight"))(weight, added))
    }
  }

  def performSumStep(result: mutable.Map[Metric.Value, Option[_]],
                     metrics: Seq[Metric.Value],
                     metricsValues: Seq[(TokenSequence, Map[Metric.Value, Option[_]])],
                     tags: Option[List[String]])
                    (implicit logger: MetricsLogger): Unit =
    for (metric <- metrics.filter(!Metric.cumulativeMetrics().contains(_))) {
      for ((tokenSequence, metrics) <- metricsValues if metrics.contains(metric) && metrics(metric).isDefined){
        val weight = tokenSequence.weight(metric, default = Some(0.0), tags = tags)
        logger.debug(s"Taking $metric = ${metrics(metric).get} with weight $weight for ${tokenSequence.id}")
        performSumAtomicStep(result, metric, weight, metrics(metric).get)
      }
    }

  def performDivAtomicStep(result: mutable.Map[Metric.Value, Option[_]], metric: Metric.Value, divisor: Any): Any = divisor match {
    case 0 => None
    case d => result(metric) = divideAndCeil(result(metric).get, d)
  }

  def performDivStep(result: mutable.Map[Metric.Value, Option[_]],
                     metrics: Seq[Metric.Value],
                     metricsValues: Seq[(TokenSequence, Map[Metric.Value, Option[_]])],
                     tags: Option[List[String]],
                     tokenSequences: Seq[TokenSequence],
                     calculateCumulativeMetric: Metric.Value => Option[_])
                    (implicit logger: MetricsLogger): Unit =
    for (metric <- metrics){
      if (result contains metric){ // if weighted sum of individual values of the metric (per sample) has been calculated
        if (Metric.unweightedMetrics contains metric) { // if this metric is simply weighted by number of samples
          performDivAtomicStep(result, metric, metricsValues.size)
        } else { // if this metric is weighted in more complex way (say, via length of reference transcription or using number of words which have some value of some morpho feature)
          logger.debug(f"Final step of calculating average $metric - dividing ${result(metric).get} by ${
            tokenSequences.map(tokenSequence => tokenSequence.weight(metric, default = Some(0.0), tags = tags)).mkString(" + ")}")
          performDivAtomicStep(result, metric, getSumOfWeights(tokenSequences, metric, tags))
        }
      } else if (Metric.cumulativeMetrics contains metric) { // if it doesn't make sense to calculate individual metric values (per sample) and only needed to consider the whole collection
        if (tags.isEmpty || tags.get.isEmpty) {
          result(metric) = calculateAndCeil(calculateCumulativeMetric(metric))
        }
      } else {
        result(metric) = None
      }
    }

  def divideAndCeil(divided: Any, divisor: Any): Option[_] =
    ceilOptional(Some(divideWeightedScores(config.getDouble("metric.defaultMappedDivisor"))(divided, divisor)), config.getInt("metric.decimalPlaces"))

  def calculateAndCeil(calculate: => Option[_]): Option[_] = {
    ceilOptional(calculate, config.getInt("metric.decimalPlaces"))
  }

  def averageMetricSet(tokenSequences: Seq[TokenSequence], tags: Option[List[String]], metrics: Seq[Metric.Value],
                       jobDurationInMilliseconds: Long)
                      (implicit logger: MetricsLogger): Map[Metric.Value, Option[_]] ={

    val result = mutable.Map[Metric.Value, Option[_]]()
    val metricsValues = tokenSequences.map(tokenSequence => (tokenSequence, getCalculatedMetrics(tokenSequence, tags)))

    // Sum up all values for each metric multiplying them by corresponding reference length
    performSumStep(result, metrics, metricsValues, tags)
    logMetricValues(result, tags, "Got weighted metric scores sums: ", config.getString("metric.noneReplacement"))(logger, config)

    // Divide results of sum by total ref length
    performDivStep(result, metrics, metricsValues, tags, tokenSequences, metric => metricsCalculationProxyService.calculateCumulativeMetric(metric, tokenSequences, jobDurationInMilliseconds, logger))
    logMetricValues(result, tags, "Weighted metric scores sums after division: ", config.getString("metric.noneReplacement"))(logger, config)

    result.toMap
  }

  def averageMetrics(tokenSequences: Seq[TokenSequence],
                     globalMetrics: Seq[Metric.Value],
                     localMetrics: Seq[(Seq[String], Seq[Metric.Value])],
                     jobDurationInMilliseconds: Long)
                    (implicit logger: MetricsLogger): AsrMetricOutputResultAny = {
    AsrMetricOutputResultAny(
      averageMetricSet(for (tokenSequence <- tokenSequences)
        yield tokenSequence, None, globalMetrics.filter(metric => !Metric.unaveragedMetrics().contains(metric)), jobDurationInMilliseconds),
      for ((tags, metrics) <- localMetrics)
        yield AsrMetricOutputTagsResultAny(tags.toList,
          averageMetricSet(for (tokenSequence <- tokenSequences)
            yield tokenSequence, Some(tags.toList), metrics.filter(metric => !Metric.unaveragedMetrics().contains(metric)), jobDurationInMilliseconds)))
  }

  def getAverageOrEmpty(tokenSequences: Seq[TokenSequence],
                        globalMetrics: Seq[Metric.Value],
                        localMetrics: Seq[(Seq[String], Seq[Metric.Value])],
                        jobDurationInMilliseconds: Long)
                       (implicit logger: MetricsLogger): AsrMetricOutputResultAny = {
    try{
      averageMetrics(tokenSequences, globalMetrics, localMetrics, jobDurationInMilliseconds)
    } catch {
      case error: Throwable =>
        logError(error, f"Cannot average metrics because of error ${error.getMessage}. Returning empty average object...")
        AsrMetricOutputResultAny(Map[Metric.Value, Option[_]](), List[AsrMetricOutputTagsResultAny]())
    }
  }
}
