package services.metrics.calculation

import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.morpho.MorphoUtils.{areFeatureValuesEqual, extractMorphoFeatures, getMerMorphoFeatures, getMorphoWeights}

import scala.collection.immutable
import scala.math.min

trait InflerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateInfler(tokenSequence: TokenSequence,
                      packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                      calculateOnAllTokens: Boolean = false)(implicit logger: MetricsLogger): Option[List[Option[Double]]] = {
    packedMorphoInfo match {
      case None =>
        logger.debug("Morpho info is empty hence can't calculate IMERS")
        None
      case _ =>
        val morphoFeatures = getMerMorphoFeatures(injectedConfig)
        val morphoWeights = getMorphoWeights(injectedConfig).withDefault(_ => 1.0)
        val morphoInfo = packedMorphoInfo.get

        Some((for (item <- extractMorphoFeatures(tokenSequence, calculateOnAllTokens, morphoInfo) // collect list of pairs - morpho features in ref and hyp appropriately
        ) yield {
          if (item.isDefined){
            val (refFeatures, hypFeatures, token) = item.get
            logger.debug(s"Calculating imer on token ${token.value}|${token.hypvalue}")
            val refFeaturesUnwrapped = refFeatures.keys.toList // Map feature names to features (in enum)
            val relevantFeatures = morphoFeatures.filter(feature => refFeaturesUnwrapped contains feature) // take just those among required features, values for which are presented
            val errorRate = relevantFeatures.filter(feature => !areFeatureValuesEqual(hypFeatures, feature, refFeatures(feature))) // take just those features where error is occured (values in ref and hyp are not equal)
              .map(feature => morphoWeights(feature)).sum // finally, take their weights and sum
            val maxErrorRate = relevantFeatures.map(feature => morphoWeights(feature)).sum // also calculate max possible sum of weights for current token
            logger.debug(s"True error rate for token  = $errorRate / $maxErrorRate") // log result
            val divided = min(errorRate / maxErrorRate, 1.0)
            Some(if (divided.isNaN) 0.0 else divided)
          } else {
            None
          }
        }).toList)
    }
  }

  def calculateInflera(tokenSequence: TokenSequence,
                       packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                       calculateOnAllTokens: Boolean = false,
                       imers: Option[List[Option[Double]]] = None)(implicit logger: MetricsLogger): Option[Double] = {
    // just average value of imers
    var operativeImers = imers
    if (imers.isEmpty) {
      operativeImers = calculateInfler(tokenSequence, packedMorphoInfo, calculateOnAllTokens)
    }
    if (operativeImers.isDefined && operativeImers.get.exists(_.isDefined)) {
      logger.debug(s"Imer = ${operativeImers.get.filter(_.isDefined).map(_.get).sum} / ${operativeImers.get.length}")
      Some(operativeImers.get.filter(_.isDefined).map(_.get).sum / operativeImers.get.count(_.isDefined))
    } else {
      None
    }
  }
}
