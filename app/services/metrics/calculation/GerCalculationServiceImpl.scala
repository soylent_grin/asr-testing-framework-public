package services.metrics.calculation

import enums.Metric
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger

import scala.collection.JavaConverters._
import scala.collection.{immutable, mutable}
import scala.util.{Failure, Success, Try}

trait GerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateGer(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[immutable.Map[String, Double]] = {
    Try(injectedConfig.getStringList("asr.tags.garbage.list").asScala.toList) match {
      case Success(garbageTags) =>
        val result = mutable.Map[String, Double]()
        val wer = calculateWer(tokenSequence)(Metric.GER, logger)
        // Calculate wer
        if (wer.isDefined) {
          result("WER") = wer.get
        }

        // Calculate stats for garbage tags
        for (tag <- garbageTags) {
          val tagCount = tokenSequence.countGarbageTokens(tag)
          val refLength = tokenSequence.refLength
          result(f"$tag-count") = tagCount

          if (refLength > 0) {
            result(f"$tag-percentage") = tagCount / refLength * 100
          }
        }

        Some(result.toMap)
      case Failure(_) =>
        None
    }
  }
}
