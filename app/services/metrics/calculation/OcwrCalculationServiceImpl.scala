package services.metrics.calculation

import enums.Metric
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.LoggingUtils.logAlignment

import scala.collection.immutable

trait OcwrCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateOcwr(tokenSequence: TokenSequence,
                    morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]])(implicit logger: MetricsLogger): Option[Double] = {
    if (morphoInfo.isEmpty){
      logger.debug("MorphoInfo is empty hence can't calculate OER")
      return None
    }
    val filteredTokenSequence = tokenSequence.filterByOpenness(morphoInfo)
    logAlignment(filteredTokenSequence, "[OER ALIGNMENT]")(logger, injectedConfig)
    tokenSequence.setWeight(Metric.OCWR, filteredTokenSequence.refLength)
    calculatePrecision(filteredTokenSequence)(Metric.OCWR, logger)
  }
}
