package services.metrics.calculation

import enums.WerOperation
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.Converter
import utils.metrics.MetricUtils.tryChange

trait IwerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateIwer(tokenSequence: TokenSequence, packedAlpha: Option[Double],
                    window: Int = injectedConfig.getInt("metric.iwerWindowSize"))(implicit logger: MetricsLogger): Option[List[Option[Double]]] = {
    if (packedAlpha.isEmpty){
      logger.debug("Alpha is empty hence can't calculate IWERS")
      return None
    }
    val alpha = packedAlpha.get
    val result = Some(tokenSequence.tokens.zipWithIndex.filter(pair => pair._1.operation != WerOperation.INS).map(pair => {
      val score = Converter.boolToInt(pair._1.operation == WerOperation.DEL).toDouble +
        Converter.boolToInt(pair._1.operation == WerOperation.SUB).toDouble +
        alpha * (for (localWindow <- 1 to window) yield
          Converter.boolToInt(tokenSequence.tokens(
            tryChange(0, tokenSequence.length() - 1, pair._2, localWindow)).operation == WerOperation.INS) +
            Converter.boolToInt(tokenSequence.tokens(
              tryChange(0, tokenSequence.length() - 1, pair._2, -localWindow)).operation == WerOperation.INS)).sum.toDouble
      logger.debug(s"IWER for ${pair._1.value} = $score")
      Some(score)
    }))
    result
  }

  def calculateIwera(tokenSequence: TokenSequence, packedAlpha: Option[Double],
                     window: Int = injectedConfig.getInt("metric.iwerWindowSize"), iwers: Option[List[Option[Double]]] = None)(implicit logger: MetricsLogger): Option[Double] = {
    if (packedAlpha.isEmpty){
      logger.debug("Alpha is empty hence can't calculate IWER")
      return None
    }
    var operativeIwers = iwers
    if (iwers.isEmpty) {
      operativeIwers = calculateIwer(tokenSequence, packedAlpha, window)
    }
    if (operativeIwers.isDefined && operativeIwers.get.nonEmpty){
      Some(operativeIwers.get.filter(_.isDefined).map(_.get).sum / operativeIwers.get.count(_.isDefined))
    } else {
      None
    }
  }
}
