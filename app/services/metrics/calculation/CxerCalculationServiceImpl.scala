package services.metrics.calculation

import enums.{Metric, WerOperation}
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger

import scala.collection.mutable.ListBuffer

trait CxerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateCxer(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Double] = {
    val numberOfComplexNames = tokenSequence.tokens.last.complexId + 1
    val complexNameAccuracy = ListBuffer.fill[Int](numberOfComplexNames)(0)
    val complexNameSizes = ListBuffer.fill[Int](numberOfComplexNames)(0)
    for (token <- tokenSequence.tokens if token.complexId >= 0) {
      complexNameSizes(token.complexId) += 1
      if (token.operation == WerOperation.OK){
        complexNameAccuracy(token.complexId) += 1
      }
    }
    logger.debug(f"""Got complex names sizes: ${complexNameSizes.mkString(", ")}""")
    logger.debug(f"""Got complex names accuracy scores: ${complexNameAccuracy.mkString(", ")}""")
    val complexNamesPostProcessed =
      for ((complexNameError, complexNameSize) <- complexNameAccuracy.zip(complexNameSizes)
           if complexNameSize > 1) yield complexNameError.toDouble / complexNameSize
    logger.debug(f"""Post-processed complex names sizes and accuracy scores: ${complexNamesPostProcessed.mkString(", ")}""")
    if (complexNamesPostProcessed.nonEmpty){
      tokenSequence.setWeight(Metric.CXER, complexNamesPostProcessed.size)
      Some(1.0 - complexNamesPostProcessed.sum / complexNamesPostProcessed.size)
    } else {
      None
    }
  }
}
