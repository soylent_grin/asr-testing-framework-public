package services.metrics.calculation

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import enums.{Metric, MorphoFeature}
import factories.TokenSequenceFactory
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import play.api.Logger
import play.inject.Injector
import services.metrics.MetricsLogger
import services.metrics.alignment.AlignmentService
import utils.metrics.LoggingUtils.logRefTokens
import utils.metrics.calculation.{F1Score, Precision}

import scala.collection.immutable
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[MetricsCalculationServiceImpl])
trait MetricsCalculationService {
  def calculateGer(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[immutable.Map[String, Double]]
  def calculateWer(tokenSequence: TokenSequence)(implicit metric: Metric.Value, logger: MetricsLogger): Option[Double]
  def calculateOcwr(tokenSequence: TokenSequence,
                    morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]])(implicit logger: MetricsLogger): Option[Double]
  def calculateNcr(tokenSequence: TokenSequence,
                   morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]])(implicit logger: MetricsLogger): Option[Double]

  def calculateIns(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Int]
  def calculateDel(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Int]
  def calculateSub(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Int]
  def calculateIwer(tokenSequence: TokenSequence, packedAlpha: Option[Double],
                    window: Int = 2)(implicit logger: MetricsLogger): Option[List[Option[Double]]]

  def calculateIwera(tokenSequence: TokenSequence, packedAlpha: Option[Double],
                     window: Int = 2, iwers: Option[List[Option[Double]]] = None)(implicit logger: MetricsLogger): Option[Double]

  def calculateMer(tokenSequence: TokenSequence,
                   packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                   calculateOnAllTokens: Boolean = false)(implicit logger: MetricsLogger): Option[immutable.Map[MorphoFeature.Value, Double]]

  def calculateInfler(tokenSequence: TokenSequence,
                      packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                      calculateOnAllTokens: Boolean = false)(implicit logger: MetricsLogger): Option[immutable.List[Option[Double]]]

  def calculateInflera(tokenSequence: TokenSequence,
                       packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                       calculateOnAllTokens: Boolean = false,
                       imers: Option[List[Option[Double]]] = None)(implicit logger: MetricsLogger): Option[Double]

  def calculateMstat(tokenSequence: TokenSequence,
                     packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                    )(implicit logger: MetricsLogger): Option[immutable.Map[MorphoFeature.Value, immutable.Map[String, Int]]]

  def calculateWmer(tokenSequence: TokenSequence,
                    packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                    calculateOnAllTokens: Boolean = false)(implicit logger: MetricsLogger): Option[Double]

  def calculateLer(tokenSequence: TokenSequence,
                   packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                   lemmasSimilarWeight: Double = ???,
                   lemmasDifferentWeight: Double = ???,
                   calculateWeight: Seq[_] => Double = lemmaEqualityForSubbedWords => lemmaEqualityForSubbedWords.size,
                   metric: Metric.Value = Metric.LER)(implicit logger: MetricsLogger): Option[Double]

  def calculateRler(tokenSequence: TokenSequence,
                    packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                    lemmasSimilarWeight: Double = ???,
                    lemmasDifferentWeight: Double = ???)(implicit logger: MetricsLogger): Option[Double]

  def calculateHes(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Double]

  def calculateRer(tokenSequence: TokenSequence, repairIndices: Option[List[Int]])(implicit logger: MetricsLogger): Option[Double]
  def calculateSubtokenErrorRate(tokenSequence: TokenSequence, tokenize: String => Array[String], metric: Metric.Value)(implicit logger: MetricsLogger): Option[Double]

  def calculateCxer(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Double]

  def calculateCer(tokenSequence: TokenSequence, conceptsIndices: Option[List[List[Int]]],
                   bio: Option[List[(String, String)]] = None)(implicit logger: MetricsLogger): Option[Double]

  def calculateCumulativeSpeedFactor(jobDurationInSeconds: Long, cumulativeDuration: Double)(implicit logger: MetricsLogger): Option[Double]
}

@Singleton
class MetricsCalculationServiceImpl @Inject()(injector: Injector,
                                              config: Config,
                                              alignmentService: AlignmentService,
                                              tokenSequenceFactory: TokenSequenceFactory,
                                              implicit val ec: ExecutionContext) extends MetricsCalculationService
  with WerCalculationServiceImpl
  with IwerCalculationServiceImpl
  with CxerCalculationServiceImpl
  with CerCalculationServiceImpl
  with SfCalculationServiceImpl
  with MerCalculationServiceImpl
  with GerCalculationServiceImpl
  with MstatCalculationServiceImpl
  with OcwrCalculationServiceImpl
  with NcrCalculationServiceImpl
  with RerCalculationServiceImpl
  with WmerCalculationServiceImpl
  with InflerCalculationServiceImpl
  with SubtokenErrorRateCalculationServiceImpl
  with HesCalculationServiceImpl
  with LerCalculationServiceImpl {

  val injectedConfig: Config = config
  val injectedAlignmentService: AlignmentService = alignmentService
  val injectedTokenSequenceFactory: TokenSequenceFactory = tokenSequenceFactory

  def calculatePrecision(tokenSequence: TokenSequence)(implicit metric: Metric.Value, logger: MetricsLogger): Option[Double] =
    Precision.calculate(tokenSequence.refLength, tokenSequence.countNotOk())(logRefTokens(tokenSequence)(logger, injectedConfig, metric))

  def calculateF1Score(tokenSequence: TokenSequence, accuracy:Int = 3)(implicit logger: MetricsLogger): Option[Double] =
    F1Score.calculate(tokenSequence.refLength, tokenSequence.hypLength, tokenSequence.countOk())
}
