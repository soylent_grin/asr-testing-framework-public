package services.metrics.calculation

import enums.Metric
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.LoggingUtils.logAlignment

import scala.collection.immutable

trait NcrCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateNcr(tokenSequence: TokenSequence,
                   morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]])(implicit logger: MetricsLogger): Option[Double] = {
    if (morphoInfo.isEmpty){
      logger.debug("MorphoInfo is empty hence can't calculate NER")
      return None
    }
    val filteredTokenSequence = tokenSequence.filterByNumeric(morphoInfo)
    logAlignment(filteredTokenSequence, "[NER ALIGNMENT]")(logger, injectedConfig)
    tokenSequence.setWeight(Metric.NCR, filteredTokenSequence.refLength)
    calculatePrecision(filteredTokenSequence)(Metric.NCR, logger)
  }
}
