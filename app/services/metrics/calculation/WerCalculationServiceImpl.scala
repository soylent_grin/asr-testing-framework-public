package services.metrics.calculation

import enums.Metric
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.LoggingUtils.logRefTokens
import utils.metrics.calculation.ErrorRate

trait WerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateWer(tokenSequence: TokenSequence)(implicit metric: Metric.Value, logger: MetricsLogger): Option[Double] =
    ErrorRate.calculate(tokenSequence.refLength, tokenSequence.countNotOk())(logRefTokens(tokenSequence)(logger, injectedConfig, metric))

  def calculateIns(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Int] = {
    Some(tokenSequence.countIns())
  }

  def calculateDel(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Int] = {
    Some(tokenSequence.countDel())
  }

  def calculateSub(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Int] = {
    Some(tokenSequence.countSub())
  }
}
