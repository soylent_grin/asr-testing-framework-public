package services.metrics.calculation

import services.metrics.MetricsLogger

trait SfCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateCumulativeSpeedFactor(jobDurationInSeconds: Long, sampleCollectionDurationInSeconds: Double)(implicit logger: MetricsLogger): Option[Double] = {
    logger.debug(f"True SpeedFactor = $jobDurationInSeconds / $sampleCollectionDurationInSeconds")
    var result: Option[Double] = None
    if (sampleCollectionDurationInSeconds > 0){
      result = Option[Double](jobDurationInSeconds / sampleCollectionDurationInSeconds)
    }
    result
  }
}
