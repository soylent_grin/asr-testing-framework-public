package services.metrics.calculation

import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.morpho.MorphoUtils.getMorphoWeights

import scala.collection.immutable

trait WmerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateWmer(tokenSequence: TokenSequence,
                    packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                    calculateOnAllTOkens: Boolean = false)(implicit logger: MetricsLogger): Option[Double] = {
    val morphoFeatures = getMorphoWeights(injectedConfig)
    val merResult = calculateMer(tokenSequence, packedMorphoInfo, calculateOnAllTOkens)

    var squashedResult = .0
    var sumOfWeights = 0.0
    if (merResult.isDefined){
      val unpackedResult = merResult.get
      for (morphoFeatureName <- unpackedResult.keys){
        val morphoFeature = morphoFeatureName
        val weight = morphoFeatures.getOrElse(morphoFeature, 1.0)
        squashedResult += unpackedResult(morphoFeatureName) * weight
        sumOfWeights += weight
        logger.debug(f"$morphoFeatureName = ${unpackedResult(morphoFeatureName)} * $weight")
      }
    }

    logger.debug(f"Squashed result without weighting = $squashedResult")
    logger.debug(f"Weight = $sumOfWeights")

    if (merResult.isDefined && merResult.get.values.nonEmpty){
      Some(squashedResult / sumOfWeights)
    } else {
      None
    }
  }
}
