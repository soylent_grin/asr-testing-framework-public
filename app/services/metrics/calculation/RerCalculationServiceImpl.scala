package services.metrics.calculation

import enums.Metric
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger

trait RerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateRer(tokenSequence: TokenSequence, repairIndices: Option[List[Int]])(implicit logger: MetricsLogger): Option[Double] = {
    repairIndices match {
      case None =>
        logger.debug("Speech repairs indices are empty hence can't calculate REP")
        None
      case _ =>
        tokenSequence.setWeight(Metric.RER, repairIndices.get.size)
        calculateCer(tokenSequence, Some(repairIndices.get.map(index => List[Int](index))))
    }
  }
}
