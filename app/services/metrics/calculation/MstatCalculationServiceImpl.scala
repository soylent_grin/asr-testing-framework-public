package services.metrics.calculation

import enums.MorphoFeature
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.morpho.MorphoUtils.{getMstatMorphoFeatures, isMorphoFeatureRelevant, mergeIndividualMstats}

import scala.collection.immutable

trait MstatCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateMstat(tokenSequence: TokenSequence,
                     packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]])(implicit logger: MetricsLogger): Option[immutable.Map[MorphoFeature.Value, immutable.Map[String, Int]]] = {
    packedMorphoInfo match {
      case None =>
        logger.debug("Morpho info is empty hence can't calculate MSTAT")
        None
      case _ =>
        val morphoFeatures: List[MorphoFeature.Value] = getMstatMorphoFeatures(injectedConfig)
        val morphoInfo = packedMorphoInfo.get

        Some(tokenSequence.tokens // take all presented tokens
          .filter(morphoInfo contains _.value) // throw out these for which there is no morphoInfo associated with token from reference transcription
          .map(token => morphoInfo(token.value).features) // get morphoFeatures for survived items
          .map(features => features.filter(pair => isMorphoFeatureRelevant(morphoFeatures, pair._1))) // throw out excessive morphoFeatures
          .foldLeft(Map[MorphoFeature.Value, Map[String, Int]]())(mergeIndividualMstats)) // merge calculated results (just count number of words having concrete values for concrete morpho features)
    }
  }
}
