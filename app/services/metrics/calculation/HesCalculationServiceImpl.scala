package services.metrics.calculation

import enums.Metric
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.LoggingUtils.logAlignment

import scala.collection.JavaConverters._

trait HesCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateHes(tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Double] = {
    val filteredTokenSequence = tokenSequence.filterByValues(
      injectedConfig.getStringList("metric.hezitationList").asScala.toList)
    logAlignment(filteredTokenSequence, "[HESER ALIGNMENT]")(logger, injectedConfig)
    tokenSequence.setWeight(Metric.HES, filteredTokenSequence.refLength)
    calculateF1Score(filteredTokenSequence)
  }
}
