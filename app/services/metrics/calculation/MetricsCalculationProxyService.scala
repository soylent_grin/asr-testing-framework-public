package services.metrics.calculation

import com.google.inject.{ImplementedBy, Inject}
import com.typesafe.config.Config
import enums.Metric
import enums.Metric.{CER, CHER, CXER, DEL, GER, HES, IMER, IMERA, INFLER, INFLERA, INS, IWER, IWERA, LER, LMER, MER, MSTAT, NCR, OCWR, PHER, RER, RLER, SF, SUB, WER, WLMER, WMER}
import exceptions.DicFileDoesNotExistException
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import play.api.Logger
import services.metrics.MetricsLogger
import services.metrics.tokenization.TokenizationService

import scala.collection.JavaConverters._
import scala.collection.immutable
import scala.util.Try

@ImplementedBy(classOf[MetricsCalculationHubServiceImpl])
trait MetricsCalculationProxyService {
  def calculateMetric(metric: Metric.Value,
                      externalTokenSequence: TokenSequence,
                      morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                      tags: Option[List[String]])(implicit logger: MetricsLogger): Option[_]

  def calculateCumulativeMetric: PartialFunction[(Metric.Value, Seq[TokenSequence], Long, MetricsLogger), Option[_]]
}

class MetricsCalculationHubServiceImpl @Inject()(metricsCalculationService: MetricsCalculationService,
                                                 simpleTokenizer: TokenizationService,
                                                 config: Config) extends MetricsCalculationProxyService {

  def calculateMorphoMetric: PartialFunction[(Metric.Value, TokenSequence, Option[immutable.Map[String, MorphoInfoChunk]], MetricsLogger), Option[_]] = {
    case (INFLERA, tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateInflera(tokenSequence, morphoInfo)(logger)
    case (INFLER,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateInfler(tokenSequence, morphoInfo)(logger)
    case (IMER,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateInfler(tokenSequence, morphoInfo, calculateOnAllTokens = true)(logger)
    case (IMERA,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateInflera(tokenSequence, morphoInfo, calculateOnAllTokens = true)(logger)
    case (MER,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateMer(tokenSequence, morphoInfo, calculateOnAllTokens = true)(logger)
    case (LMER,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateMer(tokenSequence, morphoInfo)(logger)
    case (MSTAT,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateMstat(tokenSequence, morphoInfo)(logger)
    case (OCWR,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateOcwr(tokenSequence, morphoInfo)(logger)
    case (NCR,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateNcr(tokenSequence, morphoInfo)(logger)
    case (WMER,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateWmer(tokenSequence, morphoInfo, calculateOnAllTokens = true)(logger)
    case (WLMER,  tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateWmer(tokenSequence, morphoInfo)(logger)
    case (LER, tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateLer(tokenSequence, morphoInfo)(logger)
    case (RLER, tokenSequence, morphoInfo, logger) => metricsCalculationService.calculateRler(tokenSequence, morphoInfo)(logger)
  }

  def calculateRegularMetric: PartialFunction[(Metric.Value, TokenSequence, Option[immutable.Map[String, MorphoInfoChunk]], MetricsLogger), Option[_]] = {
    case (WER, tokenSequence, _, logger) => metricsCalculationService.calculateWer(tokenSequence)(WER, logger)
    case (INS, tokenSequence, _, logger) => metricsCalculationService.calculateIns(tokenSequence)(logger)
    case (DEL, tokenSequence, _, logger) => metricsCalculationService.calculateDel(tokenSequence)(logger)
    case (SUB, tokenSequence, _, logger) => metricsCalculationService.calculateSub(tokenSequence)(logger)
    case (IWER, tokenSequence, _, logger) => metricsCalculationService.calculateIwer(tokenSequence, tokenSequence.meta.alpha)(logger)
    case (IWERA, tokenSequence, _, logger) => metricsCalculationService.calculateIwera(tokenSequence, tokenSequence.meta.alpha)(logger)
    case (HES, tokenSequence, _, logger) => metricsCalculationService.calculateHes(tokenSequence)(logger)
    case (RER, tokenSequence, _, logger) => metricsCalculationService.calculateRer(tokenSequence, tokenSequence.meta.speechRepairsIndices)(logger)
    case (CHER, tokenSequence, _, logger) => metricsCalculationService.calculateSubtokenErrorRate(tokenSequence, simpleTokenizer.tokenizeToChars(logger), CHER)(logger)
    case (PHER, tokenSequence, _, logger) => metricsCalculationService.calculateSubtokenErrorRate(tokenSequence, simpleTokenizer.tokenizeToPhonemes(logger), PHER)(logger)
    case (CXER, tokenSequence, _, logger) => metricsCalculationService.calculateCxer(tokenSequence)(logger)
    case (CER, tokenSequence, _, logger) => metricsCalculationService.calculateCer(tokenSequence, tokenSequence.meta.conceptsIndices, tokenSequence.meta.bio)(logger)
    case (GER, tokenSequence, _, logger) => metricsCalculationService.calculateGer(tokenSequence)(logger)
  }

  def calculateCumulativeMetric: PartialFunction[(Metric.Value, Seq[TokenSequence], Long, MetricsLogger), Option[_]] = {
      case (SF, tokenSequences, jobDurationInSeconds, logger) =>
        logger.debug(f"Token sequences durations: ${tokenSequences.map(tokenSequence => tokenSequence.duration)}")
        metricsCalculationService.calculateCumulativeSpeedFactor(jobDurationInSeconds,
          tokenSequences.map(tokenSequence => tokenSequence.duration).sum)(logger)
      case _ => None
  }

  def defaultMetricValue: PartialFunction[(Metric.Value, TokenSequence, Option[immutable.Map[String, MorphoInfoChunk]], MetricsLogger), Option[_]] = {
    case (_, _, _, _) => None
  }

  def calculateMetric(metric: Metric.Value, externalTokenSequence: TokenSequence,
                      morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                      tags: Option[List[String]])(implicit logger: MetricsLogger): Option[_] = {
    var tokenSequence = externalTokenSequence
    val garbageTags = Try(config.getStringList("asr.tags.garbage.list").asScala.toSet).getOrElse(Set[String]())
    // Delete garbage if required
    val refLengthBeforeDeletion = tokenSequence.refLength
    if ((metric != GER) && (refLengthBeforeDeletion > 0)){
      logger.debug(s"Filtering for garbage model by tags ${garbageTags.mkString(", ")}")
      tokenSequence = externalTokenSequence.filterAndLog(
        _.removeGarbageInserts(garbageTags).filterByNotPresentedTags(garbageTags),
        logger.debug(_: String)
      )
      externalTokenSequence.setWeight(Metric.GER, externalTokenSequence.refLength)
      externalTokenSequence.refLength = tokenSequence.refLength
    }

    try {
      (tokenSequence.refLength, tokenSequence.refLengthWithGarbage, metric == GER) match {
        case (0, _, false) | (_, 0, true) => None // if reflength encountering or not encountering garbage is 0 (depending on current metric)
        case _ => (calculateRegularMetric orElse calculateMorphoMetric orElse defaultMetricValue)(metric, tokenSequence, morphoInfo, logger)
      }
    } catch {
      case _: DicFileDoesNotExistException =>
        logger.error(f"Dic file does not exist, hence can't calculate ${metric}")
        None
    }
  }
}
