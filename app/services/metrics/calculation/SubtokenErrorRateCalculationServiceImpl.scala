package services.metrics.calculation

import enums.{Metric, WerOperation}
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.LoggingUtils.logAlignment
import utils.metrics.calculation.ErrorRate.calculate

trait SubtokenErrorRateCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def splitTokensSimple(tokens: List[String], tokenize: String => Array[String])(implicit logger: MetricsLogger): List[String] = {
    (for (token <- tokens) yield tokenize(token)).flatten
  }

  // Pher, Cher
  def calculateSubtokenErrorRate(tokenSequence: TokenSequence, tokenize: String => Array[String], metric: Metric.Value)(implicit logger: MetricsLogger): Option[Double] = {
    // logger.debug("[SUBTOKEN ALIGNMENT]")
    val ref = splitTokensSimple(tokenSequence.tokens.filter(token => token.operation != WerOperation.INS).
      map(token => token.value), tokenize)
    // logger.debug(f"Ref: $ref")
    val hyp = splitTokensSimple(tokenSequence.tokens.filter(token => token.operation != WerOperation.DEL).
      map(token => token.hypvalue), tokenize)
    // logger.debug(f"Hyp: $hyp")
    val alignment = injectedAlignmentService.align(ref, hyp)
    val operations = alignment.map(triplet => triplet._3).groupBy(identity).mapValues(_.size)
    val errors = operations.getOrElse(WerOperation.INS, 0) +
      operations.getOrElse(WerOperation.DEL, 0) + operations.getOrElse(WerOperation.SUB, 0)
    val refLength = operations.getOrElse(WerOperation.OK, 0) +
      operations.getOrElse(WerOperation.DEL, 0) + operations.getOrElse(WerOperation.SUB, 0)
    // logAlignment(alignment, ref, hyp)
    if (refLength > 0){
      tokenSequence.setWeight(metric, refLength)
      calculate(refLength, errors)(logAlignment(alignment, ref, hyp, injectedConfig.getString("metric.noneReplacement"))(logger, injectedConfig))
    } else {
      None
    }
  }
}
