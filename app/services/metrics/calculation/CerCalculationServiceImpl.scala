package services.metrics.calculation

import enums.{Metric, WerOperation}
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.Converter.bioToConceptsIndices

trait CerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def getConceptAccuracy(conceptIndices: List[Int], tokenSequence: TokenSequence)(implicit logger: MetricsLogger): Option[Double] = {
    var totalWords = 0
    var correctWords = 0
    logger.debug(s"Evaluating concept with indices $conceptIndices")
    for (token <- tokenSequence.tokens if conceptIndices.contains(token.complexId)) {
      logger.debug(f"${token.value}|${token.operation}")
      totalWords += 1
      if (token.operation == WerOperation.OK){
        correctWords += 1
      }
    }
    if (totalWords > 0){
      val accuracy = correctWords.toDouble / totalWords
      logger.debug(f"So, accuracy is $accuracy")
      Some(accuracy)
    } else {
      None
    }
  }

  def calculateCer(tokenSequence: TokenSequence, conceptsIndices: Option[List[List[Int]]],
                   bio: Option[List[(String, String)]] = None)(implicit logger: MetricsLogger): Option[Double] = {
    var operativeConceptsIndices = conceptsIndices
    if (conceptsIndices.isEmpty && bio.isDefined){
      operativeConceptsIndices = Some(bioToConceptsIndices(bio.get))
    }
    if (operativeConceptsIndices.isDefined){
      val conceptsAccuracy = for (accuracy <- operativeConceptsIndices.get.map(
        conceptIndices => getConceptAccuracy(conceptIndices, tokenSequence)) if accuracy.isDefined)
        yield accuracy.get
      if (conceptsAccuracy.nonEmpty){
        tokenSequence.setWeight(Metric.CER, conceptsAccuracy.size)
        logger.debug(s"Result concepts accuracies = ${conceptsAccuracy.mkString(", ")}")
        Some(1.0 - conceptsAccuracy.sum / conceptsAccuracy.size)
      } else {
        logger.debug("Neither concepts indices nor concepts bio is defined hence can't calculate CER")
        None
      }
    } else {
      None
    }
  }
}
