package services.metrics.calculation

import enums.{Metric, WerOperation}
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.{TokenSequence, TokenSequenceMeta}
import services.metrics.MetricsLogger
import utils.metrics.LoggingUtils.logRefTokens
import utils.metrics.calculation.ErrorRate
import utils.metrics.morpho.MorphoUtils.areLemmasEqual

import scala.collection.immutable
import scala.collection.mutable.ListBuffer

trait LerCalculationServiceImpl {
  self: MetricsCalculationServiceImpl =>

  def calculateLer(tokenSequence: TokenSequence,
                   packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                   lemmasSimilarWeight: Double = injectedConfig.getDouble("metric.lmer.lemmasSimilarWeight"),
                   lemmasDifferentWeight: Double = injectedConfig.getDouble("metric.lmer.lemmasDifferentWeight"),
                   calculateWeight: Seq[_] => Double = lemmaEqualityForSubbedWords => lemmaEqualityForSubbedWords.size,
                   metric: Metric.Value = Metric.LER)(implicit logger: MetricsLogger): Option[Double] = {
    packedMorphoInfo match {
      case None =>
        logger.debug(f"Morpho info is empty hence can't calculate $metric")
        None
      case _ =>
        val morphoInfo = packedMorphoInfo.get
        val lemmaEqualityForSubbedWords = tokenSequence.tokens.filter(_.operation == WerOperation.SUB).map(areLemmasEqual(_, morphoInfo))
        logger.debug(f"Found ${lemmaEqualityForSubbedWords.count(_ == true)} tokens with same lemma (taking with weight ${lemmasSimilarWeight})")
        logger.debug(f"Found ${lemmaEqualityForSubbedWords.count(_ == false)} tokens with different lemma (taking with weight ${lemmasDifferentWeight})")
        val weightedSub = lemmaEqualityForSubbedWords.count(_ == true) * lemmasSimilarWeight + lemmaEqualityForSubbedWords.count(_ == false) * lemmasDifferentWeight
        logger.debug(f"Errors: ${tokenSequence.countIns()} (ins) + ${tokenSequence.countDel()} (del) + ${weightedSub} (weighted sub)")
        ErrorRate.calculate(tokenSequence.refLength, tokenSequence.countIns() + tokenSequence.countDel() + weightedSub)(logRefTokens(tokenSequence)(logger, injectedConfig, metric))
    }
  }

  def calculateRler(tokenSequence: TokenSequence,
                    packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                    lemmasSimilarWeight: Double = injectedConfig.getDouble("metric.lmer.lemmasSimilarWeight"),
                    lemmasDifferentWeight: Double = injectedConfig.getDouble("metric.lmer.lemmasDifferentWeight"))(implicit logger: MetricsLogger): Option[Double] = {
    packedMorphoInfo match {
      case None =>
        logger.debug(f"Morpho info is empty hence can't calculate FLER")
        None
      case _ =>
        val morphoInfo = packedMorphoInfo.get
        val lemmatizedTokenSequence = injectedTokenSequenceFactory.make(
          tokenSequence.tokens.filter(_.operation != WerOperation.INS).map(token => (morphoInfo(token.value).lemma, token.complexId, token.tags)),
          tokenSequence.tokens.filter(_.operation != WerOperation.DEL).map(token => morphoInfo(token.hypvalue).lemma),
          tokenSequence.meta.garbageIndices.to[ListBuffer],
          (garbageIndices: ListBuffer[(Int, String)]) => new TokenSequenceMeta(tokenSequence, garbageIndices.toList),
          tokenSequence.id,
          tokenSequence.duration)

        val differentSub = lemmatizedTokenSequence.countSub()
        logger.debug(f"Found $differentSub SUBs in lemmatized tokenSequence (taking with weight $lemmasDifferentWeight)")
        val similarSub = Math.abs(tokenSequence.countSub() - differentSub)
        logger.debug(f"Found $similarSub SUBs in not-lemmatized tokenSequence (taking with weight $lemmasSimilarWeight)")
        val weightedSub = similarSub * lemmasSimilarWeight + differentSub * lemmasDifferentWeight
        logger.debug(f"Weighted sub = $weightedSub")

        logRefTokens(lemmatizedTokenSequence)(logger, injectedConfig, Metric.RLER)
        ErrorRate.calculate(tokenSequence.refLength, tokenSequence.countIns() + tokenSequence.countDel() + weightedSub)(logRefTokens(tokenSequence)(logger, injectedConfig, Metric.RLER))
    }
  }
}
