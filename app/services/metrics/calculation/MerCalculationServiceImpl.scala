package services.metrics.calculation

import enums.MorphoFeature
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import services.metrics.MetricsLogger
import utils.metrics.morpho.MorphoUtils._

import scala.collection.immutable
import scala.math.min

trait MerCalculationServiceImpl  {

  self: MetricsCalculationServiceImpl =>

  def calculateMer(tokenSequence: TokenSequence,
                   packedMorphoInfo: Option[immutable.Map[String, MorphoInfoChunk]],
                   calculateOnAllTokens: Boolean = false)(implicit logger: MetricsLogger): Option[Map[MorphoFeature.Value, Double]] = {
    packedMorphoInfo match {
      case None =>
        logger.debug("Morpho info is empty hence can't calculate MER")
        None
      case _ =>
        // init
        val morphoFeatures = getMerMorphoFeatures(injectedConfig)
        val morphoInfo = packedMorphoInfo.get

        // find morphological errors for each relevant feature per token
        val filteredTokens = tokenSequence.tokens
          .filter(token => calculateOnAllTokens || areLemmasEqual(token, morphoInfo)) // take only relevant tokens

        val morphoFeaturesErrorFlags = filteredTokens
          .map(token => (getFeatures(token.value, morphoInfo), getFeatures(token.hypvalue, morphoInfo))) // extract morpho features
          .map(pair => (mergeMorphoFeaturesLists(pair._1, morphoFeatures), pair._2)) // delete unnecessary features
          .map(pair => compareFeatures(pair._1, pair._2)) // compare ref and hyp features

        // collect results and get number of errors and number of relevant words per feature
        val morphoFeaturesErrorsCounts = morphoMapReduce(morphoFeaturesErrorFlags, featureValuesEqual => if (featureValuesEqual) 0.0 else 1.0) // number of errors per feature
      val morphoFeaturesWordsCounts = morphoMapReduce(morphoFeaturesErrorFlags, _ => 1.0) // number of words per feature

        // Write individual weight for each morpho feature
        setMerWeights(tokenSequence, morphoFeaturesWordsCounts, calculateOnAllTokens)

        // perform division and log results
        val result = morphoFeaturesErrorsCounts.map(pair => pair._1 -> {
          val morphoFeaturesErrorsCount = morphoFeaturesErrorsCounts(pair._1)
          val morphoFeaturesWordsCount = morphoFeaturesWordsCounts(pair._1)
          logger.debug(f"Feature ${pair._1} recognized with error $morphoFeaturesErrorsCount / $morphoFeaturesWordsCount")

          val divided = min(morphoFeaturesErrorsCount / morphoFeaturesWordsCount, 1.0)
          if (divided.isNaN) None else Some(divided)
        }).filter(pair => pair._2.isDefined).mapValues(_.get)

        // extend result
        Some(result ++ morphoFeatures.filter(!result.contains(_)).map(feature => (feature, 0.0)).toMap)
    }
  }
}
