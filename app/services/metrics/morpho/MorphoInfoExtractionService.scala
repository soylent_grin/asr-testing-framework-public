package services.metrics.morpho

import com.google.inject.{ImplementedBy, Inject}
import com.typesafe.config.Config
import enums.{Metric, MorphoFeature}
import json.AsrJsonBindings._
import models.metrics.morpho.MorphoInfoChunk
import play.api.libs.json._
import play.inject.Injector
import services.metrics.MetricsLogger
import services.python.MorphoPythonService
import utils.collections.ListsUtils.getListsUnion
import utils.metrics.Converter.stringListToMorphoFeatureList
import utils.metrics.LoggingUtils.logError
import utils.metrics.MetricUtils.shouldGetMorphoInfo

import scala.collection.JavaConverters._

@ImplementedBy(classOf[MorphoInfoExtractionServiceImpl])
trait MorphoInfoExtractionService {
  def getMorphoInfo(words: Set[String],
                    globalMetrics: List[Metric.Value],
                    localMetrics: List[(List[String], List[Metric.Value])])(implicit logger: MetricsLogger): Option[Map[String, MorphoInfoChunk]]
}

class MorphoInfoExtractionServiceImpl @Inject()(injector: Injector,
                                                config: Config,
                                                morphoService: MorphoPythonService) extends MorphoInfoExtractionService {

  //
  // Run morpho features extraction process for the given features
  //
  def extractMorphoInfo(words: Seq[String], features: List[MorphoFeature.Value])(implicit logger: MetricsLogger):
  Option[Map[String, MorphoInfoChunk]] = {
    var result: Option[JsValue] = None
    if (words.nonEmpty && features.nonEmpty) {
      try {
        result = Option[JsValue](morphoService.getMorphoInfo(words, for (feature <- features) yield feature.toString))
      } catch {
        case error: Throwable => logError(error, f"Error getting morpho info $error")
      }
      if (result.isDefined) {
        Some(result.get.as[Map[String, MorphoInfoChunk]])
      } else {
        None
      }
    } else {
      None
    }
  }

  //
  // With given metrics decide does it worth it to extract morpho features
  //
  def getMorphoInfo(words: Set[String],
                    globalMetrics: List[Metric.Value],
                    localMetrics: List[(List[String], List[Metric.Value])])(implicit logger: MetricsLogger): Option[Map[String, MorphoInfoChunk]] = {
    if (config.getBoolean("metric.enableMorphoFeaturesExtraction")) {
      val morphoFeatures = getListsUnion(
        stringListToMorphoFeatureList(config.getStringList("metric.mstatMorphoFeatureList").asScala.toList),
        stringListToMorphoFeatureList(config.getStringList("metric.merMorphoFeatureList").asScala.toList),
        stringListToMorphoFeatureList(config.getStringList("metric.otherMorphoFeatureList").asScala.toList))

      if (shouldGetMorphoInfo(morphoFeatures, globalMetrics, localMetrics)) {
        extractMorphoInfo(words.toList, morphoFeatures)
      } else { // if shouldn't extract morpho info
        None
      }
    } else { // if morpho info extraction is disabled in config
      None
    }
  }
}
