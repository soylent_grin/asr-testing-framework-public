package services.metrics

import java.io.File
import java.nio.charset.StandardCharsets
import java.util
import java.util.concurrent.LinkedBlockingQueue
import java.util.{Date, UUID}

import com.codahale.metrics.Slf4jReporter.LoggingLevel
import org.apache.commons.io.FileUtils
import org.slf4j
import play.api.{Logger, MarkerContext}
import utils.DateTimeUtils

import scala.collection.JavaConverters._

class MetricsLogger(logger: slf4j.Logger) extends Logger(logger: slf4j.Logger) {

  override def debug(message: => String)(implicit mc: MarkerContext): Unit = _log(LoggingLevel.DEBUG, message)

  override def info(message: => String)(implicit mc: MarkerContext): Unit = _log(LoggingLevel.INFO, message)

  override def warn(message: => String)(implicit mc: MarkerContext): Unit = _log(LoggingLevel.WARN, message)

  override def error(message: => String)(implicit mc: MarkerContext): Unit = _log(LoggingLevel.ERROR, message)

  //

  private var logBuffer = new LinkedBlockingQueue[String]()
  private val logFile = File.createTempFile(getClass.getName,UUID.randomUUID().toString)

  private lazy val _logger = Logger(getClass)

  private val bufferMaxSize = 1000

  private def _log(level: LoggingLevel, string: String) = {
    level match {
      case LoggingLevel.INFO =>
        _logger.info(string)
      case LoggingLevel.WARN =>
        _logger.warn(string)
      case LoggingLevel.ERROR =>
        _logger.error(string)
      case _: LoggingLevel =>
        _logger.debug(string)
    }
    logBuffer.offer(formatString(string, level))
    if (logBuffer.size() > bufferMaxSize) {
      flushLog()
    }
  }

  def getLogFile: File = {
    if (logBuffer.size() > 0) {
      flushLog()
    }
    logFile
  }

  private def flushLog() = this.synchronized {
    try {
      val buffer = new util.ArrayList[String]()
      logBuffer.drainTo(buffer)
      FileUtils.writeStringToFile(logFile, buffer.asScala.mkString("\n"), StandardCharsets.UTF_8, true)
    } catch {
      case t: Throwable =>
        logger.error(s"failed to write log file; error is: ", t)
    }
  }

  private def formatString(str: String, level: LoggingLevel): String = {
    s"[${DateTimeUtils.sdf.format(new Date())}] ${level.toString} $str"
  }

}

object MetricsLogger {

  def apply(): MetricsLogger = new MetricsLogger(Logger(getClass).logger)

}