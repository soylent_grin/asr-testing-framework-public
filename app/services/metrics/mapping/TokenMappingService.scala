package services.metrics.mapping

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import play.inject.Injector
import utils.assets.FileUtils.makeMapping

@ImplementedBy(classOf[TokenMappingServiceImpl])
trait TokenMappingService {
  def map(value: String): String
}
@Singleton
class TokenMappingServiceImpl @Inject()(injector: Injector,
                                        config: Config) extends TokenMappingService {
  implicit private val delimiter: String =
    config.getString("metric.tokensMappingDelimiter")

  private lazy val tokenValueMapping = makeMapping[String](
    path = config.getString("metric.tokensMappingPath"),
    extractReplacingValue = chunkSequence => chunkSequence(1),
    composeDefault = (token, _) => token
  )

  def map(value: String): String = tokenValueMapping(value)
}
