package services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import dtos.AppConfigDto

import scala.util.Try


@ImplementedBy(classOf[AppConfigServiceImpl])
trait AppConfigService {
  def getConfig(): AppConfigDto
}

@Singleton
class AppConfigServiceImpl @Inject()(config: Config) extends AppConfigService {

  override def getConfig(): AppConfigDto = {
    AppConfigDto(
      Try(config.getString("silhouette.github.clientSecret")).toOption.isDefined,
      Try(config.getString("silhouette.google.clientSecret")).toOption.isDefined,
      Try(config.getInt("appConfig.maxJobsPerUser")).getOrElse(Int.MaxValue),
      Try(config.getInt("appConfig.maxPrivateFoldersPerUser")).getOrElse(Int.MaxValue),
      Try(config.getInt("appConfig.maxSamplesPerJob")).getOrElse(Int.MaxValue),
    )
  }

}
