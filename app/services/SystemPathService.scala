package services

import java.io.File
import java.lang.reflect.Modifier
import java.util.UUID

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.{Config, ConfigException}
import dtos.{AugmentationParamsDto, AugmentationScenarioHolderDto, ParamDto, ParamOption}
import extensions.augmentation.{AugmentationScenario, AugmentationScenarioInfo, CSoundAugmentationScenario}
import models.SoundbankSample
import org.reflections.Reflections
import play.api.Logger
import play.api.libs.json.{JsObject, Json}
import play.inject.Injector
import repositories.NoiseRepository

import sys.process._
import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process.{Process, ProcessLogger}


@ImplementedBy(classOf[SystemPathServiceImpl])
trait SystemPathService {
  def getRoot(): String
}

@Singleton
class SystemPathServiceImpl @Inject()() extends SystemPathService {

  private implicit lazy val logger = Logger(getClass)

  override def getRoot(): String = {
    ???
  }

}
