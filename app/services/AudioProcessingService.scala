package services

import java.io.File
import java.lang.reflect.Modifier
import java.util.UUID

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.{Config, ConfigException}
import dtos.{AugmentationParamsDto, AugmentationScenarioHolderDto, ParamDto, ParamOption}
import extensions.augmentation.{AugmentationScenario, AugmentationScenarioInfo, CSoundAugmentationScenario}
import models.SoundbankSample
import org.reflections.Reflections
import play.api.Logger
import play.api.libs.json.{JsObject, Json}
import play.inject.Injector
import repositories.NoiseRepository

import sys.process._
import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process.{Process, ProcessLogger}


@ImplementedBy(classOf[AudioProcessingServiceImpl])
trait AudioProcessingService {
  def getScenarios(): List[String]
  def getAugmentationParams(scenarioKey: String): Future[List[ParamDto]]
  def process(samples: Seq[SoundbankSample], scenario: String, params: AugmentationParamsDto): Unit
  def normalizeSampleRate(sample: File, desiredSampleRate: Float): File
  def convertToWave(sample: File): File
}

@Singleton
class AudioProcessingServiceImpl @Inject()(injector: Injector,
                                         config: Config,
                                         noiseRepository: NoiseRepository,
                                         implicit val ec: ExecutionContext) extends AudioProcessingService {

  private implicit lazy val logger = Logger(getClass)

  private val reflections = new Reflections("extensions.augmentation")

  private val scenarios = mutable.Map[String, AugmentationScenarioHolderDto]()

  private val processLogger = ProcessLogger(
    (o: String) => {
      logger.debug(o)
    },
    (e: String) => {
      logger.warn(e)
    }
  )

  init()

  private def init(): Unit = {
    logger.info(s"initing; reading list of augmentation scenarios")
    reflections.getSubTypesOf(classOf[AugmentationScenario]).asScala.map(executorClass => {
      (executorClass, executorClass.getAnnotation(classOf[AugmentationScenarioInfo]))
    }).filter(pair => {
      pair._2 != null && !Modifier.isAbstract(pair._1.getModifiers)
    }).toList.foreach(pair => {
      logger.debug(s"found new CSound scenario of class ${pair._1.getName}")
      scenarios(pair._2.key) = AugmentationScenarioHolderDto(
        pair._2.title,
        pair._2.key,
        injector.instanceOf(pair._1)
      )
    })

    logger.info(s"done; found ${scenarios.size} scenarios")
  }

  override def getAugmentationParams(scenarioKey: String): Future[List[ParamDto]] = {
    try {
      val params = config.getConfigList(s"augmentation.params.$scenarioKey")
      Future.successful(
        ParamDto.fromConfig(params.asScala) ++ getNoiseParams()
      )
    } catch {
      case t: ConfigException.Missing =>
        logger.debug(s"not found params for $scenarioKey: ${t.getMessage}")
        Future.successful(Nil)
      case t: Throwable =>
        logger.error(s"unexpected error while parsing params for $scenarioKey: ", t)
        Future.successful(Nil)
    }
  }

  override def getScenarios(): List[String] = {
    scenarios.keys.toList
  }

  override def process(samples: Seq[SoundbankSample], scenario: String, params: AugmentationParamsDto): Unit = {
    logger.trace(s"processing ${samples.size} samples with scenario $scenario")

    scenarios.get(scenario) match {
      case Some(holder) =>
        holder.instance.run(samples, params.params.getOrElse(Json.obj()))
      case _ =>
        throw new IllegalArgumentException(s"not found scenario for key = $scenario")
    }

  }

  // sox input.mp3 -r 8000 output.wav
  override def normalizeSampleRate(sample: File, desiredSampleRate: Float): File = {
    logger.info(s"converting file ${sample.getAbsolutePath} to sample rate $desiredSampleRate")

    val outputFile = File.createTempFile(getClass.getName, UUID.randomUUID().toString + ".wav")

    logger.debug(s"prepared tmp file: ${outputFile.getAbsolutePath}")

    val cmd = Seq[String]("sox", sample.getAbsolutePath, "-r", desiredSampleRate.toLong.toString, outputFile.getAbsolutePath)

    logger.debug(s"running cmd: ${cmd.mkString(" ")}")

    val process = Process(cmd).run(processLogger)
    val exitValue = process.exitValue()

    if (exitValue != 0) {
      throw new IllegalStateException(s"non-zero exit code by sox: $exitValue; maybe you do not have ffmpeg installed locally?")
    }

    outputFile
  }

  // ---

  private def getNoiseParams(): Seq[ParamDto] = {
    val groups = noiseRepository.getNoiseGroups
    if (groups.isEmpty) {
      Nil
    } else {
      Seq(
        ParamDto(
          "backgroundNoiseGroup",
          "select",
          None,
          Some("Background noise group"),
          None,
          groups.map(g => ParamOption(
            g.key,
            s"${g.key}, ${g.samples.size}"
          )).toList
        ),
        ParamDto(
          "pauseNoiseGroup",
          "select",
          None,
          Some("Pause noise group"),
          None,
          groups.map(g => ParamOption(
            g.key,
            s"${g.key}, ${g.samples.size}"
          )).toList
        )
      )
    }
  }

  override def convertToWave(sample: File): File = {
    // ffmpeg -i "first" -f wav -bitexact -acodec pcm_s16le -ar 22050 -ac 1 "ffmpeg.wav"
    logger.info(s"converting file ${sample.getAbsolutePath} to waveform")

    val outputFile = File.createTempFile(getClass.getName, UUID.randomUUID().toString + ".wav")

    logger.debug(s"prepared tmp file: ${outputFile.getAbsolutePath}")

    val cmd = Seq[String](
      "ffmpeg",
      "-y",
      "-i",
      sample.getAbsolutePath,
      "-f",
      "wav",
      "-bitexact",
      "-acodec",
      "pcm_s16le",
      "-ar",
      "22050",
      "-ac",
      "1",
      outputFile.getAbsolutePath
    )

    logger.debug(s"running cmd: ${cmd.mkString(" ")}")

    val process = Process(cmd).run(processLogger)
    val exitValue = process.exitValue()

    if (exitValue != 0) {
      throw new IllegalStateException(s"non-zero exit code by ffmpeg: $exitValue; unable to convert to wave")
    }

    logger.debug(s"deleting original file")
    sample.delete()

    outputFile
  }

}
