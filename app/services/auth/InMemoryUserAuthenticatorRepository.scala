package services.auth

import com.mohiva.play.silhouette.api.repositories.AuthenticatorRepository
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import javax.inject.{Inject, Singleton}
import org.joda.time.DateTime
import play.api.{Configuration, Logger}

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class InMemoryUserAuthenticatorRepository @Inject()(configuration: Configuration)
                                                   (implicit ec: ExecutionContext)
  extends AuthenticatorRepository[CookieAuthenticator] {

  private val idToAuthenticator = mutable.Map[String, CookieAuthenticator]()

  private val logger = Logger(getClass)

  private implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)

  override def find(id: String): Future[Option[CookieAuthenticator]] = Future {
    idToAuthenticator.synchronized {
      idToAuthenticator.get(id)
    }
  }

  override def add(authenticator: CookieAuthenticator): Future[CookieAuthenticator] = Future {
    idToAuthenticator.synchronized {
      // register new session authenticator
      idToAuthenticator += (authenticator.id -> authenticator)
      authenticator
    }
  }

  override def update(authenticator: CookieAuthenticator): Future[CookieAuthenticator] = Future {
    idToAuthenticator.synchronized {
      idToAuthenticator += (authenticator.id -> authenticator)
      authenticator
    }
  }

  override def remove(id: String): Future[Unit] = Future {
    idToAuthenticator.synchronized {
      idToAuthenticator.get(id) match {
        case Some(_) =>
          idToAuthenticator -= id
        case _ =>
      }
    }
  }
}
