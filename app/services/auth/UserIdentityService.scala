package services.auth

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import javax.inject.Inject
import models.UserIdentity
import repositories.UserRepository
import utils.UserUtils

import scala.concurrent.{ExecutionContext, Future}

class UserIdentityService @Inject()(userRepository: UserRepository)
                                   (implicit ec: ExecutionContext) extends IdentityService[UserIdentity] {

  override def retrieve(loginInfo: LoginInfo): Future[Option[UserIdentity]] = {
    userRepository.find(UserUtils.makeUserId(loginInfo)).map(res => {
      res.map(user => {
        UserIdentity(user)
      })
    })
  }
}