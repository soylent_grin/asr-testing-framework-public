package services.auth

import com.google.inject.{ImplementedBy, Inject}
import com.mohiva.play.silhouette.api.util.Credentials
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import javax.inject.Singleton
import models.User
import repositories.UserRepository
import utils.UserUtils

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[PasswordAuthServiceImpl])
trait PasswordAuthService {

  def credentials(username: String, password: String): Future[AuthenticateResult]

}

@Singleton
class PasswordAuthServiceImpl @Inject()(userRepository: UserRepository,
                                        userIdentityService: UserIdentityService,
                                        credentialsProvider: CredentialsProvider)
                                       (implicit ec: ExecutionContext) extends PasswordAuthService {

  override def credentials(username: String, password: String): Future[AuthenticateResult] = {
    credentialsProvider.authenticate(
      Credentials(username, password)
    ).flatMap { loginInfo =>
      userIdentityService.retrieve(loginInfo).map {
        case Some(identity) =>
          Success(identity.user)
        case None =>
          UserNotFound
      }
    }
  }

}

sealed trait AuthenticateResult
case class Success(user: User) extends AuthenticateResult
case class InvalidPassword(attemptsAllowed: Int) extends AuthenticateResult
object NonActivatedUserEmail extends AuthenticateResult
object UserNotFound extends AuthenticateResult