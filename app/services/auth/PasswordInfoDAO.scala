package services.auth

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import javax.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag

@Singleton
class PasswordInfoDAO @Inject()(implicit ec: ExecutionContext)
  extends DelegableAuthInfoDAO[PasswordInfo] {

  override val classTag: ClassTag[PasswordInfo] = ClassTag(classOf[PasswordInfo])

  override def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] = {
    Future.successful(None)
  }

  override def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = ???

  override def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = ???

  override def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = ???

  override def remove(loginInfo: LoginInfo): Future[Unit] = ???
}
