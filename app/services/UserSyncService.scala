package services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import com.typesafe.config.Config
import models.User
import play.api.Logger
import repositories.UserRepository
import utils.UserUtils

import scala.jdk.CollectionConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait UserSyncService {}

// we'll bind this eagerly in module
class UserSyncServiceImpl @Inject()(config: Config,
                                    userRepository: UserRepository,
                                    passwordHasherRegistry: PasswordHasherRegistry,
                                    authInfoRepository: AuthInfoRepository)
                                   (implicit ec: ExecutionContext) extends UserSyncService {

  private implicit lazy val logger = Logger(getClass)

  init()

  private def addAuthInfoForBuiltInUser(user: User): Future[_] = {
    logger.debug(s"adding user ${user.id} to auth repo")
    val loginInfo = LoginInfo(CredentialsProvider.ID, user.name)
    val authInfo = passwordHasherRegistry.current.hash(user.password.get)
    authInfoRepository.add(loginInfo, authInfo)
  }

  private def init() = {
    logger.info(s"initing; ensuring built-in users in repository")
    try {
      val rawUsers = Try(config.getConfigList("builtInUsers").asScala).toOption.getOrElse(Nil)
      rawUsers.map(c => {
        val user = User(c)
        val patchedUser = user.copy(
          id = UserUtils.makeCredentialsUserId(user.id)
        )
        userRepository.upsert(patchedUser).map(_ => {
          addAuthInfoForBuiltInUser(patchedUser)
        })
      })
      logger.debug(s"done parsing list of built-in users (${rawUsers.size})")
    } catch {
      case t: Throwable =>
        logger.error(s"failed to read list of built-in users; error is: ", t)
    }
  }

}
