package services

import java.lang.reflect.Modifier
import java.nio.file.Path
import java.util.UUID

import akka.http.scaladsl.model.DateTime
import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import dtos.{AsrExecutionResultDto, AsrExecutorCapabilities, AsrExecutorInfo, AsrExecutorInfoDto}
import enums.{AsrExecutionStatus, AsrJobStepInputType, AsrStepType}
import extensions.executors.AsrExecutor
import models._
import org.reflections.Reflections
import play.api.Logger
import play.inject.Injector
import repositories.{AsrModelRepository, AsrStepRepository, AsrStepResultRepository, SoundbankRepository}
import utils.{AsrStepExecutionException, AsrStepTerminationException}

import scala.collection.JavaConverters._
import scala.collection.concurrent.TrieMap
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

@ImplementedBy(classOf[AsrExecutorServiceImpl])
trait AsrExecutorService {
  def getList: List[AsrExecutorInfoDto]

  def execute(asrKey: String, step: AsrStep)
             (implicit ec: ExecutionContext): Future[AsrExecutionResultDto]

  def terminate(jobId: UUID): Future[_]

}

@Singleton
class AsrExecutorServiceImpl @Inject()(injector: Injector,
                                       config: Config,
                                       asrStepSampleProviderService: AsrStepSampleProviderService,
                                       soundbankRepository: SoundbankRepository,
                                       asrStepRepository: AsrStepRepository,
                                       asrStepResultRepository: AsrStepResultRepository,
                                       asrModelRepository: AsrModelRepository) extends AsrExecutorService {

  private lazy val logger = Logger(getClass)

  private val reflections = new Reflections("extensions")

  private val asrExecutors = mutable.Map[String, AsrExecutorInfo]()

  private val jobToExecutor = TrieMap[UUID, AsrExecutor]()

  private val disabledExecutors = Try(config.getStringList("asr.disabled")).map(_.asScala).getOrElse(Nil)

  init()

  // -------------------------------------------------------------------------------------------------------------------

  override def getList: List[AsrExecutorInfoDto] = {
    asrExecutors.toList.map(_._2).sortWith((a, b) => {
      a.title < b.title
    }).map(AsrExecutorInfoDto.fromModel)
  }

  override def execute(asrKey: String, step: AsrStep)
                      (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = {
    logger.info(s"trying to launch step $step on ASR $asrKey")
    asrExecutors.get(asrKey) match {
      case Some(e) =>
        val instance = injector.instanceOf(e.executorClass)
        logger.debug(s"found instance for executor $asrKey; preparing samples")
        val samples = Await.result(
          asrStepSampleProviderService.getSamples(step),
          Duration.Inf
        )
        logger.debug(s"done; ready to launch")
        var future = step.stepType match {
          case AsrStepType.`train` =>
            instance.runTrainAsr(
              samples,
              step.getAsrConfig
            )
          case AsrStepType.`test` =>
            getAsrModel(step, asrKey).flatMap(path => {
              instance.runTestAsr(
                path,
                samples,
                step.getAsrConfig
              )
            })
          case _ =>
            throw new IllegalArgumentException(s"unknown step type: ${step.stepType}; wtf?")
        }
        future = future.map(result => {
          logger.info(s"successfully finished ASR execution for step ${step.id}")
          jobToExecutor.remove(step.jobId.get)
          result
        }) recover {
          case _: AsrStepTerminationException =>
            throw AsrStepTerminationException(Some(instance.getLogFile))
          case t: Throwable =>
            jobToExecutor.remove(step.jobId.get)
            logger.error(s"error while executing ASR step: ", t)
            throw AsrStepExecutionException(instance.getLogFile)
        }
        jobToExecutor(step.jobId.get) = instance
        future
      case _ =>
        throw new IllegalArgumentException(s"not found ASR executor for key = $asrKey")
    }
  }

  override def terminate(jobId: UUID): Future[_] = {
    logger.debug(s"asked to terminate execution for job $jobId")
    jobToExecutor.get(jobId) match {
      case Some(instance) =>
        logger.debug(s"found currently processing job ${instance.hashCode()}; terminating")
        instance.terminate
      case _ =>
        logger.error(s"not found currently active job for step $jobId; possible race run?")
        Future.successful(None)
    }
  }

  // -------------------------------------------------------------------------------------------------------------------

  private def init(): Unit = {
    logger.info(s"initing; reading list of ASR executors; disabled executors are: $disabledExecutors")
    reflections.getSubTypesOf(classOf[AsrExecutor]).asScala.map(executorClass => {
      (executorClass, executorClass.getAnnotation(classOf[extensions.executors.AsrExecutorInfo]))
    }).filter(pair => {
      pair._2 != null && !Modifier.isAbstract(pair._1.getModifiers) && !disabledExecutors.contains(pair._2.key())
    }).toList.foreach(pair => {
      logger.debug(s"found new active ASR executor of class ${pair._1.getName}")
      asrExecutors(pair._2.key) = AsrExecutorInfo(
        pair._2.title,
        pair._2.key,
        AsrExecutorCapabilities(pair._2.canTrain),
        pair._1
      )
    })
    logger.info(s"done; found ${asrExecutors.size} ASR executors")
  }

  private def getAsrModel(step: AsrStep, asrKey: String)(implicit ec: ExecutionContext): Future[Option[Path]] = {
    logger.debug(s"finding model for step ${step.id}")
    try {
      step.inputType.getOrElse(AsrJobStepInputType.default) match {
        case AsrJobStepInputType.`default` =>
          logger.debug(s"using default model ${step.defaultModelName} for step ${step.id}")
          asrModelRepository.getDefaultModels(asrKey).map(pairs => {
            if (pairs.size == 1) { // for backward compatible
              Some(pairs.head._2)
            } else if (pairs.isEmpty) {
              None
            } else {
              if (step.defaultModelName.isDefined) {
                pairs.find(_._1.startsWith(step.defaultModelName.get)) match {
                  case Some(p) =>
                    Some(p._2)
                  case _ =>
                    throw new IllegalArgumentException(s"not found default model for name ${step.defaultModelName.get}")
                }
              } else {
                throw new IllegalArgumentException(s"there are multiple default models for $asrKey, but in step " +
                  s"${step.id} there is no specified defaultModelName field; this is unexpected")
              }
            }
          })
        case AsrJobStepInputType.`current` =>
          logger.debug(s"using model from current (${step.jobId}) job for step ${step.id}")
          asrStepRepository.findForJob(step.jobId.get).flatMap(steps => {
            steps.find(s => s.stepType == AsrStepType.train && s.getStatus == AsrExecutionStatus.success) match {
              case Some(s) =>
                asrModelRepository.getModels(asrKey, s.id.get) map {
                  case Some(path) =>
                    logger.debug(s"found model path $path for step ${step.id}")
                    Some(path)
                  case _ =>
                    throw new IllegalStateException(s"not found model for step ${s.id}; possible race run?")
                }
              case _ =>
                Future.failed(new IllegalStateException(s"somehow we are 1) running test step 2) using current models and" +
                  s" 3) there are no previous succeeded train steps. wth?"))
            }
          })
        case AsrJobStepInputType.`previous` =>
          logger.debug(s"using model from job ${step.inputJob} for step ${step.id}")
          step.inputJob match {
            case Some(jId) =>
              asrStepRepository.findForJob(jId).flatMap(steps => {
                steps.find(s => s.stepType == AsrStepType.train && s.getStatus == AsrExecutionStatus.success) match {
                  case Some(s) =>
                    asrModelRepository.getModels(asrKey, s.id.get) map {
                      case Some(path) =>
                        logger.debug(s"found model path $path for step ${step.id}")
                        Some(path)
                      case _ =>
                        throw new IllegalStateException(s"not found model for step ${s.id}; wtf?")
                    }
                  case _ =>
                    Future.failed(new IllegalStateException(s"somehow we are 1) running test step 2) using previous models and" +
                      s" 3) there are no previous succeeded train steps. wth?"))
                }
              })
            case _ =>
              Future.failed(new IllegalStateException(s"this is test step with models from previous job, " +
                s"but prev job id is null. wth?"))
          }
      }
    } catch {
      case t: Throwable =>
        logger.warn(s"error while getting default model (${t.getMessage}), considering it as None")
        Future.successful(None)
    }
  }

}
