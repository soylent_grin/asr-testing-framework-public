package services

import java.util.concurrent.ConcurrentHashMap

import actors.WebSocketMessage
import akka.actor.ActorRef
import com.google.inject.{ImplementedBy, Singleton}
import play.api.Logger
import play.api.libs.json.{Json, Writes}

@ImplementedBy(classOf[MessageBrokerServiceImpl])
trait MessageBrokerService {

  def registerActor(actorRef: ActorRef): Unit

  def unregisterActor(actorRef: ActorRef): Unit

  def notify[T](topic: String, message: T)(implicit writes: Writes[T]): Unit

}

@Singleton
class MessageBrokerServiceImpl extends MessageBrokerService {

  private val actors: ConcurrentHashMap[Int, ActorRef] = new ConcurrentHashMap[Int, ActorRef]()
  private val logger = Logger(getClass)

  def registerActor(actorRef: ActorRef): Unit = {
    logger.debug(s"registering new actor ${actorRef.hashCode()}")
    actors.put(actorRef.hashCode, actorRef)
  }

  def unregisterActor(actorRef: ActorRef): Unit = {
    logger.debug(s"unregistering actor ${actorRef.hashCode()}")
    actors.remove(actorRef.hashCode)
  }

  override def notify[T](topic: String, message: T)(implicit writes: Writes[T]): Unit = {
    val iterator = actors.values().iterator()
    while (iterator.hasNext) {
      val current = iterator.next()
      current ! WebSocketMessage(topic, Json.toJson(message))
    }
  }

}
