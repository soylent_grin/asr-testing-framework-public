package services.python

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import play.api.Logger
import play.inject.Injector
import utils.metrics.LoggingUtils._

@ImplementedBy(classOf[PythonServiceImpl])
trait PythonService {
  def executeScript(command: Seq[String], interpreter: String = ???, verbose: Boolean = true): String
  def executeAndReturnExitCode(command: Seq[String], interpreter: String = ???): Int
}

@Singleton
class PythonServiceImpl @Inject()(injector: Injector,
                                  config:Config,
                                  pythonExecutionService: PythonExecutionService) extends PythonService {

  private lazy implicit val logger: Logger = Logger(getClass)

  override def executeScript(command: Seq[String], interpreter: String = config.getString("pythonPaths.linux.interpreter"), verbose: Boolean = true): String =
    logProcessStreams(pythonExecutionService.run(command, interpreter), command.mkString(" "))(logger, config)._1

  override def executeAndReturnExitCode(command: Seq[String], interpreter: String = config.getString("pythonPaths.linux.interpreter")): Int = {
    val p = pythonExecutionService.run(command, interpreter)
    logProcessStreams(p, command.mkString(" "))(logger, config)
    p.waitFor()
    p.exitValue()
  }
}