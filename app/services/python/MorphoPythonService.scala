package services.python

import com.google.inject.{Inject, Singleton}
import com.typesafe.config.Config
import play.api.libs.json.{JsValue, Json}
import play.inject.Injector
import utils.assets.FileUtils.handleThroughTmpFile

trait MorphoPythonService {
  def getMorphoInfo(words: Seq[String], features: List[String]): JsValue
}

@Singleton
class MorphoPythonServiceImpl @Inject()(injector: Injector,
                                        pythonService: PythonService,
                                        config: Config) extends MorphoPythonService {
  def getMorphoInfo(words: Seq[String], features: List[String]): JsValue =
    handleThroughTmpFile(words,
      Some(config.getString("tmpFileOptions.wordsForMorphoFeaturesExtraction.name.prefix")),
      Some(config.getString("tmpFileOptions.wordsForMorphoFeaturesExtraction.name.suffix")),
      config.getBoolean("tmpFileOptions.wordsForMorphoFeaturesExtraction.dispose")) {
      tmpFile => Json.parse( // parse json
        pythonService.executeScript( // made as a result of python script execution
          Seq(config.getString("metric.morphoFeaturesExtractorScriptPath"), // path to script which extracts morpho features
          tmpFile.toPath.toString, // path to tmp files containing written words
          features.mkString(" "))
        )
      )
    }
}
