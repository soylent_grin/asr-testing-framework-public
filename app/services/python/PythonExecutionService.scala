package services.python

import com.google.inject.Inject
import com.typesafe.config.Config
import play.api.Logger
import play.inject.Injector

trait PythonExecutionService {
  def run(cmd: Seq[String], interpreter: String = ???): Process
}

class PythonExecutionServiceImpl @Inject()(injector: Injector,
                                           config: Config) extends PythonExecutionService {
  private val logger: Logger = Logger(getClass)
  def run(cmd: Seq[String], interpreter: String = config.getString("pythonPaths.linux.interpreter")): Process = {
    logger.debug(f"Using interpreter $interpreter")
    Runtime.getRuntime.exec((Seq(interpreter) ++ cmd).toArray)
  }
}

class PythonExecutionServiceDev @Inject()(injector: Injector,
                                          config: Config) extends PythonExecutionService {
  private val logger: Logger = Logger(getClass)
  def run(cmd: Seq[String], interpreter: String = config.getString("pythonPaths.windows.interpreter")): Process = {
    logger.debug(f"Using interpreter $interpreter")
    Runtime.getRuntime.exec(
      (Seq(interpreter) ++ cmd).toArray,
      Array(
        f"PYTHONPATH=${config.getString("pythonPaths.windows.pythonPath")}",
        f"PATH=${config.getString("pythonPaths.windows.path")}",
        f"APPDATA=${config.getString("pythonPaths.windows.appData}")}"))
  }
}
