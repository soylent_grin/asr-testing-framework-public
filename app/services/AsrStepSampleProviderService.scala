package services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import enums.{AsrStepSamplesType, AsrStepType}
import models.{AsrStep, SoundbankSample}
import play.api.Logger
import repositories.{AsrStepRepository, AsrStepResultRepository, SoundbankRepository}

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[AsrStepSampleProviderServiceImpl])
trait AsrStepSampleProviderService {

  def getSamples(step: AsrStep): Future[Seq[SoundbankSample]]

}

@Singleton
class AsrStepSampleProviderServiceImpl @Inject()(stepRepository: AsrStepRepository,
                                                 soundbankRepository: SoundbankRepository,
                                                 stepResultRepository: AsrStepResultRepository,
                                                 implicit val ec: ExecutionContext)
  extends AsrStepSampleProviderService {

  private implicit val logger = Logger(getClass)

  override def getSamples(step: AsrStep): Future[Seq[SoundbankSample]] = {
    logger.debug(s"trying to provide samples for step ${step.id.get}")
    val sampleIdsFuture = step.samples.`type` match {
      case AsrStepSamplesType.previous =>
        Future.successful(step.samples.list)
      case _ =>
        for {
          datasetStep <- stepRepository.findForJob(step.jobId.get).map(steps => {
            steps.find(s => {
              s.stepType == AsrStepType.dataset
            }) match {
              case Some(s) =>
                s
              case _ =>
                throw new IllegalStateException(s"asked to find samples, produced by current job, but no step with" +
                  s" `dataset` type was found; step is $step")
            }
          })
          sampleIds <- step.samples.`type` match {
            case AsrStepSamplesType.current =>
              stepResultRepository.findForStep(datasetStep.id.get) map {
                case Some(r) =>
                  (r.result \ "samples").asOpt[List[String]] match {
                    case Some(samples) =>
                      samples
                    case _ =>
                      throw new IllegalStateException(s"unable to parse sample list from step result: $r")
                  }
                case _ =>
                  throw new IllegalStateException(s"not found step result for id = ${datasetStep.id.get}; unable to get samples")
              }
            case _ =>
              Future.successful(step.samples.list)
          }
        } yield sampleIds
    }

    sampleIdsFuture.flatMap(sampleIds => {
      soundbankRepository.findSamples(sampleIds)
    })
  }



}
