package controllers.auth

import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.services.AuthenticatorResult
import com.mohiva.play.silhouette.api.{LoginEvent, LoginInfo, Silhouette}
import com.mohiva.play.silhouette.impl.providers.{CommonSocialProfileBuilder, CredentialsProvider, SocialProvider, SocialProviderRegistry}
import com.typesafe.config.Config
import dtos.SignInDto
import javax.inject.Inject
import models.User
import models.auth.SessionEnv
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._
import repositories.UserRepository
import services.auth.{PasswordAuthService, Success}

import scala.concurrent.{ExecutionContext, Future}

class PasswordAuthController @Inject()(silhouette: Silhouette[SessionEnv],
                                       passwordAuthService: PasswordAuthService,
                                       userRepository: UserRepository,
                                       implicit val config: Config)
                                      (implicit val ec: ExecutionContext) extends InjectedController {

  private val logger = Logger(getClass)

  import json.UserJsonBinding._

  def signIn: Action[AnyContent] = silhouette.UnsecuredAction.async { implicit request: Request[AnyContent] =>
    request.body.asJson match {
      case Some(json) =>
        json.validate[SignInDto].fold(
          errors => {
            Future.successful(BadRequest(errors.toString()))
          },
          form => {
            passwordAuthService.credentials(form.username, form.password).flatMap {
              case Success(user) =>
                val loginInfo = LoginInfo(CredentialsProvider.ID, user.name)
                authenticateUser(user, loginInfo)
              case _ =>
                Future.successful(Forbidden("invalid username / login"))
            }
          }
        )
      case _ =>
        Future.successful(BadRequest("not found body in JSON format"))
    }
  }

  protected def authenticateUser(user: User, loginInfo: LoginInfo)(implicit request: Request[_]): Future[AuthenticatorResult] = {
    silhouette.env.authenticatorService.create(loginInfo).flatMap { authenticator =>
      silhouette.env.eventBus.publish(LoginEvent(user, request))
      silhouette.env.authenticatorService.init(authenticator).flatMap { token =>
        silhouette.env.authenticatorService.embed(token, Ok("you are welcome"))
      }
    }
  }

}
