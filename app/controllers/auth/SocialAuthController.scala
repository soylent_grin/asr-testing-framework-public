package controllers.auth

import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.{LoginEvent, Silhouette}
import com.mohiva.play.silhouette.impl.providers.{CommonSocialProfileBuilder, SocialProvider, SocialProviderRegistry}
import com.typesafe.config.Config
import javax.inject.Inject
import models.User
import models.auth.SessionEnv
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._
import repositories.UserRepository

import scala.concurrent.{ExecutionContext, Future}

class SocialAuthController @Inject()(silhouette: Silhouette[SessionEnv],
                                     userRepository: UserRepository,
                                     socialProviderRegistry: SocialProviderRegistry,
                                     implicit val config: Config)
                                    (implicit val ec: ExecutionContext) extends InjectedController {

  private val logger = Logger(getClass)

  import json.UserJsonBinding._

  def authenticate(provider: String): Action[AnyContent] = silhouette.UnsecuredAction.async { implicit request: Request[AnyContent] =>
    (socialProviderRegistry.get[SocialProvider](provider) match {
      case Some(p: SocialProvider with CommonSocialProfileBuilder) =>
        p.authenticate().flatMap {
          case Left(result) => Future.successful(result)
          case Right(authInfo) => for {
            profile <- p.retrieveProfile(authInfo)
            rawUser = User(profile)
            user <- userRepository.find(rawUser.id).flatMap {
              case Some(_) =>
                logger.debug(s"user ${rawUser.id} already exist")
                Future.successful(rawUser)
              case _ =>
                logger.debug(s"user ${rawUser.id} does not exist; creating...")
                userRepository.upsert(rawUser)
            }
            authenticator <- silhouette.env.authenticatorService.create(profile.loginInfo)
            value <- silhouette.env.authenticatorService.init(authenticator)
            res <- silhouette.env.authenticatorService.embed(value, Redirect("/"))
          } yield {
            silhouette.env.eventBus.publish(LoginEvent(user, request))
            res
          }
        }
      case _ => Future.failed(new ProviderException(s"Cannot authenticate with unexpected social provider"))
    }).recover {
      case e: ProviderException =>
        logger.error("Unexpected provider error", e)
        BadRequest("UNEXPECTED_PROVIDER")
      case t: Throwable =>
        logger.error(s"unexpected exception: ", t)
        BadRequest(s"unexpected exception")
    }
  }

  def getUser(): Action[AnyContent] = silhouette.UserAwareAction { implicit request =>
    request.identity match {
      case Some(identity) =>
        Ok(
          Json.toJson(
            identity.user
          )
        )
      case None =>
        Unauthorized
    }
  }


  def signOut(): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    silhouette.env.authenticatorService.discard(request.authenticator, Redirect("/"))
  }

}
