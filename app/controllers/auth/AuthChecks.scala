package controllers.auth

import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import com.mohiva.play.silhouette.api.exceptions.NotAuthenticatedException
import enums.UserRole
import enums.UserRole.UserRole
import javax.inject.Inject
import models.User
import models.auth.SessionEnv
import play.api.mvc.{Action, AnyContent, Result}
import utils.UserContext

import scala.concurrent.Future

trait AuthChecks {

  protected[this] var silhouette: Silhouette[SessionEnv] = _

  @Inject
  def setSilhouette(silhouette: Silhouette[SessionEnv]): Unit = {
    this.silhouette = silhouette
  }

  def withUserContext(role: Seq[UserRole.Value] = Nil)
                     (block: SecuredRequest[SessionEnv, AnyContent] => UserContext => Result): Action[AnyContent] = {
    silhouette.SecuredAction { implicit request =>
      implicit val userContext: UserContext = UserContext(request.identity.user)
      checkRole(role, userContext.user)
      block(request)(userContext)
    }
  }

  def withUserContextAsync(role: Seq[UserRole.Value] = Nil)
                          (block: SecuredRequest[SessionEnv, AnyContent] => UserContext => Future[Result]): Action[AnyContent] = {
    silhouette.SecuredAction.async { implicit request =>
      implicit val userContext: UserContext = UserContext(request.identity.user)
      checkRole(role, userContext.user)
      block(request)(userContext)
    }
  }

  private def checkRole(role: Seq[UserRole.Value], user: User) = {
    if (role.nonEmpty && !role.contains(user.role)) {
      throw new NotAuthenticatedException(s"user ${user.id} is not permitted to perform action")
    }
  }

}
