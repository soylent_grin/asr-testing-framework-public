package controllers

import actors.WebSocketActor
import akka.actor.ActorSystem
import akka.stream.Materializer
import com.google.inject.Inject
import com.mohiva.play.silhouette.api.{HandlerResult, Silhouette}
import models.auth.SessionEnv
import play.api.libs.streams.ActorFlow
import play.api.mvc.{AbstractController, AnyContentAsEmpty, ControllerComponents, Request, WebSocket}
import services.MessageBrokerService

import scala.concurrent.{ExecutionContext, Future}

class WebSocketController @Inject()(cc: ControllerComponents,
                                    silhouette: Silhouette[SessionEnv],
                                    messageService: MessageBrokerService)
                                   (implicit system: ActorSystem,
                                    mat: Materializer,
                                    ec: ExecutionContext) extends AbstractController(cc) {

  def accept: WebSocket = WebSocket.acceptOrResult[String, String] { request =>
    implicit val req = Request(request, AnyContentAsEmpty)
    silhouette.SecuredRequestHandler { securedRequest =>
      Future.successful(HandlerResult(Ok, Some(securedRequest.identity)))
    }.map {
      case HandlerResult(_, Some(_)) => Right(
        ActorFlow.actorRef { out =>
          WebSocketActor.props(out, messageService)
        }
      )
      case HandlerResult(r, None) => Left(r)
    }

  }
}
