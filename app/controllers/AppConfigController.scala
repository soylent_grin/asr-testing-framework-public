package controllers

import beans.{AsrBean, MetricBean}
import com.mohiva.play.silhouette.api.Silhouette
import com.typesafe.config.Config
import controllers.auth.AuthChecks
import dtos.AsrParamListDto
import enums.Metric
import javax.inject._
import models.auth.SessionEnv
import play.api.libs.json.Json
import play.api.mvc._
import repositories.AsrModelRepository
import services.{AppConfigService, AsrExecutorService}

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

@Singleton
class AppConfigController @Inject()(cc: ControllerComponents,
                                    appConfigService: AppConfigService,
                                    implicit val ec: ExecutionContext) extends AbstractController(cc) with AuthChecks {

  import json.AsrJsonBindings._

  def getConfig: Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      Ok(
        Json.toJson(
          appConfigService.getConfig()
        )
      )
  }

}
