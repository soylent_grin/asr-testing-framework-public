package controllers

import beans.{AsrBean, MetricBean}
import com.mohiva.play.silhouette.api.Silhouette
import com.typesafe.config.Config
import controllers.auth.AuthChecks
import dtos.AsrParamListDto
import enums.Metric
import javax.inject._
import models.auth.SessionEnv
import play.api.libs.json.Json
import play.api.mvc._
import repositories.AsrModelRepository
import services.AsrExecutorService

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

@Singleton
class AsrController @Inject()(cc: ControllerComponents,
                              asrBean: AsrBean,
                              asrExecutorService: AsrExecutorService,
                              asrModelRepository: AsrModelRepository,
                              metricBean: MetricBean,
                              val config: Config,
                              implicit val ec: ExecutionContext) extends AbstractController(cc) with AuthChecks {

  import json.AsrJsonBindings._

  def getExecutors(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      asrBean.getAllParamTypes.map(types => {
        Ok(
          Json.toJson(
            asrExecutorService.getList.map(e => {
              e.copy(
                params = types.getOrElse(e.key, Nil)
              )
            })
          )
        )
      })
  }

  def getDefaultModels(key: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrModelRepository.getDefaultModels(key).map(res => {
      Ok(
        Json.toJson(
          res.map(r => Json.obj("name" -> r._1))
        )
      )
    })
  }

  def getParams(key: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrBean.getParamTypes(key).map(res => {
      Ok(
        Json.toJson(
          AsrParamListDto.fromList(res)
        )
      )
    })
  }

  def getMetrics(): Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      metricBean.getAvailableMetrics.map(metrics => {
        Ok(
          Json.toJson(
            metrics.sortBy(_.id).map(m => {
              Json.obj(
                "key" -> Json.toJson(m),
                "title" -> Metric.getTitle(m),
                "description" -> Metric.getDescription(m)
              )
            })
          )
        )
      })
  }

  def getTags(): Action[AnyContent] = withUserContext() {
    implicit request: Request[AnyContent] => implicit userContext =>
    Ok(
      Json.toJson(
        config.getStringList("asr.tags.list").asScala
      )
    )
  }

}
