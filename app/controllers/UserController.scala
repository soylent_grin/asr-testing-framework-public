package controllers

import beans.UserBean
import com.typesafe.config.Config
import controllers.auth.AuthChecks
import enums.UserRole
import javax.inject.{Inject, Singleton}
import models.User
import play.api.libs.json.Json
import play.api.mvc.{Action, _}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserController @Inject()(cc: ControllerComponents,
                               userBean: UserBean,
                               val config: Config,
                               implicit val ec: ExecutionContext) extends AbstractController(cc) with AuthChecks {

  import json.UserJsonBinding._

  private val allowedRoles = Seq(UserRole.admin)

  def findAll(): Action[AnyContent] = withUserContextAsync(allowedRoles) {
    implicit request: Request[AnyContent] => implicit userContext =>
      userBean.findAllUsers.map(res => {
        Ok(
          Json.toJson(
            res
          )
        )
      })
  }

  def updateUser(userId: String): Action[AnyContent] = withUserContextAsync(allowedRoles) {
    implicit request: Request[AnyContent] => implicit userContext =>
      request.body.asJson match {
        case Some(json) =>
          json.validate[User].fold(
            errors => {
              Future.successful(BadRequest(errors.toString()))
            },
            user => {
              userBean.updateUser(user).map(_ => {
                Ok("thank you")
              })
            }
          )
        case _ =>
          Future.successful(BadRequest("not found body in JSON format"))
      }
  }

}
