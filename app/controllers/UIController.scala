package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.Logger

@Singleton
class UIController @Inject()(cc: ControllerComponents, assets: Assets) extends AbstractController(cc) {

  private lazy val logger = Logger(getClass)

  def index(path: String): Action[AnyContent] = {
    logger.info(s"serving: $path")
    assets.at("index.html")
  }
}
