package controllers

import beans.SoundbankBean
import com.mohiva.play.silhouette.api.Silhouette
import com.typesafe.config.Config
import controllers.auth.AuthChecks
import dtos.{AugmentationParamsDto, RecordSampleDto}
import javax.inject.{Inject, Singleton}
import models.auth.SessionEnv
import org.apache.commons.io.FilenameUtils
import play.api.libs.json.Json
import play.api.mvc.{Action, _}
import services.{AppConfigService, AudioProcessingService}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SoundbankController @Inject()(cc: ControllerComponents,
                                    soundbankBean: SoundbankBean,
                                    audioProcessingService: AudioProcessingService,
                                    val config: Config,
                                    implicit val ec: ExecutionContext) extends AbstractController(cc) with AuthChecks {

  import json.AsrJsonBindings._

  def getFolderList(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    soundbankBean.getFolderList.map(res => {
      Ok(
        Json.toJson(
          res
        )
      )
    })
  }

  def getSample(id: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      soundbankBean.getSample(id: String).map(res => {
        Ok(
          Json.toJson(
            res
          )
        )
      })
  }

  def getSamples(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      request.body.asJson match {
        case Some(json) =>
          json.validate[List[String]].fold(
            errors => {
              Future.successful(BadRequest(errors.toString()))
            },
            dto => {
              soundbankBean.getSamples(dto).map(res => {
                Ok(
                  Json.toJson(res)
                )
              })
            }
          )
        case _ =>
          Future.successful(BadRequest("not found body in JSON format"))
      }
  }

  def getAugmentationScenarios(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    Future.successful(
      Ok(
        Json.toJson(
          audioProcessingService.getScenarios()
        )
      )
    )
  }

  def getAugmentationScenarioParams(key: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    audioProcessingService.getAugmentationParams(key).map(res => {
      Ok(
        Json.toJson(
          res
        )
      )
    })
  }

  def augmentate(folderKey: String, scenarioKey: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    request.body.asJson match {
      case Some(json) =>
        json.validate[AugmentationParamsDto].fold(
          errors => {
            Future.successful(BadRequest(errors.toString()))
          },
          dto => {
            soundbankBean.processFolder(folderKey, scenarioKey, dto).map(res => {
              Ok(
                Json.toJson(
                  res
                )
              )
            }) recover {
              case t: Exception =>
                BadRequest(t.getMessage)
              case t: Throwable =>
                InternalServerError(t.getMessage)
            }
          }
        )
      case _ =>
        Future.successful(BadRequest("not found body in JSON format"))
    }
  }

  def getFolderSamples(key: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    soundbankBean.getFolderSamples(key).map(res => {
      Ok(
        Json.toJson(
          res
        )
      )
    })
  }

  def load(folder: String, key: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    soundbankBean.getSample(folder, key) map {
      case Some(s) =>
        Ok.sendFile(s.audioFile)
      case _ =>
        NotFound(s"not found sample for folder $folder and key $key")
    }
  }

  def loadBySampleId(sampleId: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      soundbankBean.getSample(sampleId) map {
        case Some(s) =>
          Ok.sendFile(s.audioFile)
        case _ =>
          NotFound(s"not found sample for id = $sampleId")
      }
  }

  def uploadFolder(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      request.body.asMultipartFormData match {
        case Some(mfd) =>
          mfd.files.headOption match {
            case Some(f) =>
              if (!f.filename.endsWith(".zip")) {
                Future.successful(BadRequest(s"file mist be ZIP archive"))
              } else {
                soundbankBean.uploadFolder(f.ref, FilenameUtils.getBaseName(f.filename)).map(newFolder => {
                  Ok(
                    Json.toJson(
                      newFolder
                    )
                  )
                }) recover {
                  case t: Exception =>
                    BadRequest(t.getMessage)
                  case t: Throwable =>
                    InternalServerError(s"unexpected error: ${t.getMessage}")
                }
              }
            case _ =>
              Future.successful(BadRequest(s"request must contain file"))
          }
        case _ =>
          Future.successful(BadRequest(s"request must be multipart/form-data"))
      }
  }

  def addFolder(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      request.body.asJson match {
        case Some(json) =>
          soundbankBean.registerEmptyFolder(
            (json \ "key").as[String]
          ).map(newFolder => {
            Ok(
              Json.toJson(
                newFolder
              )
            )
          }) recover {
            case t: Exception =>
              BadRequest(t.getMessage)
            case t: Throwable =>
              InternalServerError(s"unexpected error: ${t.getMessage}")
          }
        case _ =>
          Future.successful(BadRequest(s"request must be multipart/form-data"))
      }
  }

  def recordSample(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      request.body.asMultipartFormData match {
        case Some(mfd) =>
          val folder = mfd.dataParts("folder").head
          val name = mfd.dataParts("name").head
          val transcription = mfd.dataParts("transcription").head
          val meta = mfd.dataParts("meta").headOption.map(str => {
            Json.parse(str)
          })
          val audio = mfd.files.head.ref
          soundbankBean.registerRecordedSample(
            RecordSampleDto(
              folder,
              name,
              audio,
              transcription,
              meta
            )
          ).map(newFolder => {
            Ok(
              Json.toJson(
                newFolder
              )
            )
          }) recover {
            case t: Exception =>
              BadRequest(t.getMessage)
            case t: Throwable =>
              InternalServerError(s"unexpected error: ${t.getMessage}")
          }
        case _ =>
          Future.successful(BadRequest(s"request must be multipart/form-data"))
      }
  }

  def removeFolder(key: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      soundbankBean.removeFolder(key).map(_ => {
        Ok("thank you")
      })
  }

}
