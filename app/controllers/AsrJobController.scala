package controllers

import java.util.UUID

import akka.stream.scaladsl.FileIO
import beans.{AsrJobBean, AsrStepLogBean}
import com.mohiva.play.silhouette.api.Silhouette
import com.typesafe.config.Config
import controllers.auth.AuthChecks
import dtos.AsrJobDto
import javax.inject._
import models.auth.SessionEnv
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AsrJobController @Inject()(cc: ControllerComponents,
                                 asrJobBean: AsrJobBean,
                                 asrStepLogBean: AsrStepLogBean,
                                 val config: Config,
                                 implicit val ec: ExecutionContext) extends AbstractController(cc) with AuthChecks {

  import json.AsrJsonBindings._

  def getJobs(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrJobBean.getJobDtos.map(res => {
      Ok(
        Json.toJson(res)
      )
    })
  }
  def getStepResult(id: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrJobBean.getStepResult(UUID.fromString(id)) map {
      case Some(j) =>
        Ok(Json.toJson(j))
      case _ =>
        NotFound(s"unable to find job result for job $id")
    }
  }
  def getStepLog(stepId: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrStepLogBean.getLog(UUID.fromString(stepId)) map {
      case Some(f) =>
        Ok.chunked(FileIO.fromPath(f.toPath))
      // Ok(scala.io.Source.fromFile(f.getAbsolutePath).getLines.mkString("\n"))
      case _ =>
        NotFound(s"unable to find train job log for step $stepId")
    }
  }
  def createJob(): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    request.body.asJson match {
      case Some(json) =>
        json.validate[AsrJobDto].fold(
          errors => {
            Future.successful(BadRequest(errors.toString()))
          },
          job => {
            asrJobBean.createJob(job).map(res => {
              Ok(Json.toJson(res))
            })
          }
        )
      case _ =>
        Future.successful(BadRequest("not found body in JSON format"))
    }
  }
  def updateLabel(id: String, label: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
      asrJobBean.updateJobLabel(UUID.fromString(id), label).map(_ => {
        Ok("thank you")
      })
  }
  def launchJob(id: String): Action[AnyContent] = withUserContext() {
    implicit request: Request[AnyContent] => implicit userContext =>
      asrJobBean.launchJob(UUID.fromString(id))
      Ok("thank you")
  }

  def terminate(id: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrJobBean.terminateJob(UUID.fromString(id)).map(_ => {
      Ok("thank you")
    })
  }

  def removeJob(id: String): Action[AnyContent] = withUserContextAsync() {
    implicit request: Request[AnyContent] => implicit userContext =>
    asrJobBean.removeJob(UUID.fromString(id)) map { _ =>
      Ok("thank you")
    }
  }

}
