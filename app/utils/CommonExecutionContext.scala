package utils

import java.util.concurrent.{Future, Executors}

import play.api.Logger

import scala.concurrent.ExecutionContext

// TODO: use me in ASR executions
class CommonExecutionContext extends ExecutionContext {
  private val logger = Logger(getClass)

  private val threadPool = Executors.newCachedThreadPool()

  override def execute(runnable: Runnable): Unit = {
    threadPool.submit(runnable)
  }

  def submit(runnable: Runnable): Future[_] = {
    threadPool.submit(runnable)
  }

  override def reportFailure(cause: Throwable): Unit = {
    logger.error("something has failed:", cause)
  }
}

object CommonExecutionContext extends CommonExecutionContext {}