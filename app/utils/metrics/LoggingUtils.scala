package utils.metrics

import java.io.{BufferedReader, InputStreamReader}

import com.typesafe.config.Config
import enums.{Metric, WerOperation}
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.TokenSequence
import play.api.Logger
import utils.BufferedReaderUtils.read

import scala.collection.mutable

object LoggingUtils {

  def logMetric(metric: Metric.Value, tags: Option[List[String]], value: Any, accuracy: Int, enablePreciseConversion: Boolean)(implicit logger: Logger, config: Config): Unit = {
    val chunkId = if (tags.nonEmpty) f"""with tags ${tags.get.mkString(", ")}""" else "without tags"
    logger.debug(f"""$metric $chunkId = ${Converter.metricValueToString(value, accuracy, enablePreciseConversion, config.getString("metric.noneReplacement"))}""")
  }

  def logAlignment(tokenSequence: TokenSequence, header: String)(implicit logger: Logger, config: Config): Unit = {
    logger.debug(header.formatted(config.getString("metric.logging.header.alignment")))
    def logRow(value: String, hypvalue: String, operation: String, id: String, complexId: String, tags: String): Unit =
      logger.debug(value.formatted(s"%-${config.getInt("metric.logging.fieldWidth.value")}s") +
      "\t" + hypvalue.formatted(s"%-${config.getInt("metric.logging.fieldWidth.hypvalue")}s") +
      "\t" + operation.formatted(s"%-${config.getInt("metric.logging.fieldWidth.operation")}s") +
      "\t" + id.formatted(s"%-${config.getInt("metric.logging.fieldWidth.id")}s") +
      "\t" + complexId.formatted(s"%-${config.getInt("metric.logging.fieldWidth.complexId")}s") +
      f"\t$tags")
    logRow("ref_token", "hyp_token", "operation", "id", "complex_id", "tags")
    for (token <- tokenSequence.tokens) {
      logRow(token.value, token.hypvalue, token.operation.toString, token.id.toString, token.complexId.toString,
        token.tags match {
          case s if s.nonEmpty => s.mkString(", ")
          case _ => "-"
        })
    }
  }

  def logAlignment(alignment: Seq[(Int, Int, WerOperation.Value)], ref: Seq[String], hyp: Seq[String], none: String)(implicit logger: Logger, config: Config): Unit = {
    for ((refId, hypId, operation) <- alignment) {
      val refToken = if ((operation != WerOperation.INS) && (refId < ref.length)) ref(refId) else none
      val hypToken = if ((operation != WerOperation.DEL) && (hypId < hyp.length)) hyp(hypId) else none
      logger.debug(refToken.formatted(s"%-${config.getInt("metric.logging.fieldWidth.value")}s") +
        "\t" + hypToken.formatted(s"%-${config.getInt("metric.logging.fieldWidth.hypvalue")}s") +
        "\t" + operation.formatted(s"%-${config.getInt("metric.logging.fieldWidth.operation")}s"))
    }
  }

  def logMorphoInfo(morphoInfo: Option[Map[String, MorphoInfoChunk]])(implicit logger: Logger, config: Config): Unit = {
    logger.debug(config.getString("metric.logging.header.morphoInfo"))
    def logRow(token: String, lemma: String, features: String): Unit = logger.debug(
      token.formatted(s"%-${config.getInt("metric.logging.fieldWidth.token")}s") +
        "\t" + lemma.formatted(s"%-${config.getInt("metric.logging.fieldWidth.lemma")}s") +
        f"\t$features")
    logRow("token", "lemma", "features")
    if (morphoInfo.nonEmpty) {
      morphoInfo.get.foreach({
        case (token: String, chunk: MorphoInfoChunk) => logRow(token, chunk.lemma,
          Converter.metricValueToString(chunk.features,
            config.getInt("metric.decimalPlaces"),
            config.getBoolean("metric.rightPadResultsWithZeros"),
            config.getString("metric.noneReplacement")))
      })
    } else {
      logger.debug(config.getString("metric.logging.emptyReplacement.morphoInfo"))
    }
  }

  def logStdStream(header: String, content: String)(implicit logger: Logger): Unit ={
    logger.debug(header)
    logger.debug(content)
  }

  def logRefTokens(tokenSequence: TokenSequence)(implicit logger: Logger, config: Config, metric: Metric.Value): Unit = {
    logger.debug(s"${tokenSequence.id.formatted(config.getString("metric.logging.header.refTokens"))}\t$metric\n${
      tokenSequence.tokens.map(
        token =>
          token.value.formatted(s"%${config.getString("metric.logging.fieldWidth.value")}s") +
          token.hypvalue.formatted(s"%${config.getInt("metric.logging.fieldWidth.hypvalue")}s") +
          token.operation.formatted(s"%${config.getInt("metric.logging.fieldWidth.operation")}s")
      ).mkString("\n")
    }")
  }

  def logStdout(content: String, description: String)(implicit logger: Logger, config: Config): Unit =
    logStdStream(description.formatted(config.getString("metric.logging.header.stdout")), content)
  def logStderr(content: String, description: String)(implicit logger: Logger, config: Config): Unit =
    logStdStream(description.formatted(config.getString("metric.logging.header.stderr")), content)

  def logProcessStreams(p: Process, description: String)(implicit logger: Logger, config: Config): (String, String) = {
    val result = read(new BufferedReader(new InputStreamReader(p.getInputStream)))
    val errors = read(new BufferedReader(new InputStreamReader(p.getErrorStream)))

    logStdout(result, description)
    logStderr(errors, description)

    (result, errors)
  }

  def logError(error: Throwable, message: String)(implicit logger: Logger): Unit = {
    logger.error(message)
    logger.error(error.getStackTrace.mkString("\n"))
  }

  def logMetricValues(mapping: mutable.Map[Metric.Value, Option[_]], header: String)(implicit logger: Logger, config: Config): Unit = {
    logger.debug(header)
    mapping.foreach({
      case (metric, value) => logger.debug(metric.formatted(s"%-${config.getInt("metric.logging.fieldWidth.metric")}s") +
        "\t" + value.formatted(s"%-${config.getInt("metric.logging.fieldWidth.metricValue")}s"))
    })
  }

  def logMetricValues(mapping: mutable.Map[Metric.Value, Option[_]], tags: Option[List[String]], header: String, none: String)(implicit logger: Logger, config: Config): Unit = {
    logger.debug(header)
    for (metric <- mapping.keys) {
      logMetric(metric, tags, mapping(metric).getOrElse(none),
        config.getInt("metric.decimalPlaces"),
        config.getBoolean("metric.rightPadResultsWithZeros"))
    }
  }
}
