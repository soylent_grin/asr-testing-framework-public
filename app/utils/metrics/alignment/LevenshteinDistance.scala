package utils.metrics.alignment

import enums.WerOperation
import enums.WerOperation.{DEL, INS, OK, SUB}
import utils.metrics.calculation.LevenshteinDistance.makeBacktraceMatrix

object LevenshteinDistance {
  def align(referenceTokens: Seq[String], hypothesisTokens: Seq[String], areEquals: (String, String) => Boolean): List[(Int, Int, WerOperation.Value)] = {
    var ref_index = referenceTokens.length
    var hyp_index = hypothesisTokens.length

    val backtrace: Array[Array[WerOperation.Value]] = makeBacktraceMatrix(referenceTokens.toArray, hypothesisTokens.toArray, areEquals)
    var operation: WerOperation.Value = OK
    var result = List[(Int, Int, WerOperation.Value)]()

    while ((ref_index > 0) || (hyp_index > 0)){
      if (backtrace(ref_index)(hyp_index) == OK){
        operation = OK
        ref_index -= 1
        hyp_index -= 1
      } else if (backtrace(ref_index)(hyp_index) == SUB){
        operation = SUB
        ref_index -= 1
        hyp_index -= 1
      } else if (backtrace(ref_index)(hyp_index) == INS){
        operation = INS
        hyp_index -= 1
      } else if (backtrace(ref_index)(hyp_index) == DEL){
        operation = DEL
        ref_index -= 1
      }

      result :+= (ref_index, hyp_index, operation)
    }

    result.reverse
  }
}
