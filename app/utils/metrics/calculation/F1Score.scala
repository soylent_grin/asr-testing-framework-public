package utils.metrics.calculation

import play.api.Logger

object F1Score {
  def calculate(refSize: Int, hypSize: Int, numberOfOk: Int)(implicit logger: Logger): Option[Double] = {
    var result: Option[Double] = None
    if ((hypSize > 0) && (refSize > 0)){
      val precision = numberOfOk.toDouble / hypSize
      logger.debug(f"Precision = $precision")
      val recall = numberOfOk.toDouble / refSize
      logger.debug(f"Recall = $recall")
      if (recall + precision > 0) {
        result = Some(2 * precision * recall / (precision + recall))
      } else {
        result = Some(0.0)
      }
    }
    result
  }
}
