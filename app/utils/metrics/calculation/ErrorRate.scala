package utils.metrics.calculation

import play.api.Logger

import scala.math.min

object ErrorRate {
  def calculate(refLength: Int, numberOfNotOk: => Double)(log: => Unit)(implicit logger: Logger): Option[Double] = {
    if (refLength != 0){
      log
      logger.debug(s"Error rate = ${numberOfNotOk} / $refLength")
      Some(min(numberOfNotOk.toDouble / refLength, 1.0))
    } else {
      None
    }
  }
}
