package utils.metrics.calculation

import enums.WerOperation
import enums.WerOperation._

import scala.math.min

object LevenshteinDistance {

  def makeBacktraceMatrix(seq1: Array[String], seq2: Array[String], areEquals: (String, String) => Boolean): Array[Array[WerOperation.Value]] = {
    val size_x = seq1.length + 1
    val size_y = seq2.length + 1

    val matrix = Array.ofDim[Int](size_x, size_y)
    val backtrace = Array.ofDim[WerOperation.Value](size_x, size_y)

    for (x <- 0 until size_x) {
      matrix(x)(0) = x
      backtrace(x)(0) = DEL
    }
    for (y <- 0 until size_y) {
      matrix(0)(y) = y
      backtrace(0)(y) = INS
    }

    for (x <- 1 until size_x) {
      for (y <- 1 until size_y) {
        if (areEquals(seq1(x-1), (seq2(y-1)))) {
          matrix (x)(y) = min(min(
            matrix(x-1)(y) + 1,
            matrix(x-1)(y-1)),
            matrix(x)(y-1) + 1)
          backtrace(x)(y) = OK
        } else {
          val deletion_penalty = matrix(x - 1)(y) + 1
          val insertion_penalty = matrix(x)(y - 1) + 1
          val substitution_penalty = matrix(x - 1)(y - 1) + 1

          matrix(x)(y) = min(min(
            matrix(x - 1)(y) + 1,
            matrix(x - 1)(y - 1) + 1),
            matrix(x)(y - 1) + 1
          )

          if (deletion_penalty == matrix(x)(y)){
            backtrace(x)(y) = DEL
          } else if (insertion_penalty == matrix(x)(y)) {
            backtrace(x)(y) = INS
          } else if (substitution_penalty == matrix(x)(y)) {
            backtrace(x)(y) = SUB
          }
        }
      }
    }
    backtrace
  }
}
