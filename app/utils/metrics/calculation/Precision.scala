package utils.metrics.calculation

import play.api.Logger

object Precision {
  def calculate(refLength: Int, numberOfNotOk: Int)(log: => Unit)(implicit logger: Logger): Option[Double] = {
    val er = ErrorRate.calculate(refLength, numberOfNotOk)(log)
    if (er.isDefined){
      Some(1.0 - er.get)
    } else {
      None
    }
  }
}
