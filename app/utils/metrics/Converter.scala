package utils.metrics

import com.typesafe.config.ConfigObject
import enums.{Metric, MorphoFeature}
import models._
import models.metrics.tokens.TokenSequence
import play.api.libs.json.{JsValue, Json}

import scala.collection.mutable.ListBuffer
import scala.collection.{immutable, mutable}

object Converter {
  def boolToInt(value: Boolean): Int = {
    if (value) 1 else 0
  }

  def ListOfPlainMapsToListOfTuples[A, B](plainMaps: List[Map[A, B]]): List[(A, B)] =
    for (plainMap <- plainMaps) yield (plainMap.keys.head, plainMap.values.head)

  def ccToMap(cc: AnyRef): immutable.Map[String, Any] =
    (immutable.Map[String, Any]() /: cc.getClass.getDeclaredFields) {
      (a, f) =>
        f.setAccessible(true)
        a + (f.getName -> f.get(cc))
    }

  def mapToAsrOutput(extendedResult: Map[_, _], allKey: String, defaultMetricValue: String = "=", accuracy: Int, enablePreciseConversion: Boolean, none: String): AsrMetricOutputResult = {
    AsrMetricOutputResult(
      extendedResult.asInstanceOf[immutable.Map[Any, immutable.Map[Metric.Value, Option[Double]]]](allKey) map {
        case (key, value) => (key.toString, Converter.metricValueToString(value.getOrElse(defaultMetricValue), accuracy, enablePreciseConversion, none))
      },
      (extendedResult.asInstanceOf[immutable.Map[Any, immutable.Map[Metric.Value, Option[Double]]]].filter(pair => pair._1 != allKey) map {
        case (key, value) => AsrMetricOutputTagsResult(key.asInstanceOf[List[String]], value map {
          case (metricKey, metricValue) => (metricKey.toString, Converter.metricValueToString(metricValue.getOrElse(defaultMetricValue), accuracy, enablePreciseConversion, none))
        })
      }).toList
    )
  }

  def asrMetricOuptutResultAnyToAsrMetricOuptutResult(asrMetricOutputAny: AsrMetricOutputAny): AsrMetricOutput = {
    AsrMetricOutput(List[AsrMetricOutputResult](),
      List[(String, AsrMetricOutputResult)]())
  }

  def pairsToRefsHyps(pairs: Seq[(SoundbankSample, List[String])]): (List[String], List[String]) = {
    val refs = for (pair <- pairs) yield pair._1.transcription.mkString(" ")
    val hyps = for (pair <- pairs) yield pair._2.mkString(" ")

    (refs.toList, hyps.toList)
  }

  def stringListToMorphoFeatureList(morphoFeatureNames: List[String]): List[MorphoFeature.Value] = {
    for (morphoFeatureName <- morphoFeatureNames) yield MorphoFeature.fromString(morphoFeatureName)
  }

  def configObjectToMorphoFeatureMap(configObject: ConfigObject): immutable.Map[MorphoFeature.Value, Double] = {
    var result = mutable.Map[MorphoFeature.Value, Double]()
    configObject.keySet.forEach(key => {
      result += (MorphoFeature.fromString(key) -> configObject.get(key).unwrapped.asInstanceOf[Number].doubleValue())
    })
    result.toMap
  }

  private def escapeString(s: String): String = {
    s.replaceAll("\"" , "\\\\\"")
  }

  private def escapeAny(value: Any): String = {
    escapeString(value match {
      case _: String => escapeString(value.asInstanceOf[String])
      case _: Seq[_] => escapeString(value.asInstanceOf[Seq[_]].map(item => item.toString).mkString(" "))
      case _: Metric.Value => escapeString(value.asInstanceOf[Metric.Value].toString)
      case _ => escapeString(value.toString)
    })
  }

  def stringifyDouble(num: Double, accuracy: Int, enablePreciseConversion: Boolean): String =
    if (enablePreciseConversion){
      f"%%.${accuracy}f".format(num).replace(",", ".")
    } else {
      num.toString
    }


  def metricValueToString(value: Any, accuracy: Int, enablePreciseConversion: Boolean, none: String): String = {
    value match {
      case a: String => a
      case a: Double => stringifyDouble(a, accuracy, enablePreciseConversion)
      case a: Number => a.toString
      case a => anyToJsonString(a, accuracy, enablePreciseConversion, none)
    }
  }

  def metricValueToString(value: Option[_], accuracy: Int, enablePreciseConversion: Boolean, none: String): String =
    value match {
      case None => none
      case a => metricValueToString(a.get, accuracy, enablePreciseConversion, none)
    }

  def anyToJsonString(value: Any, accuracy: Int, enablePreciseConversion: Boolean, none: String): String = {
    var json = new ListBuffer[String]()
    value match {
      case m: Map[_,_] => {
        for ( (k,v) <- m ) {
          var key = escapeAny(k)
          v match {
            case a: Map[_,_] => json += "\"" + key + "\":" + anyToJsonString(a, accuracy, enablePreciseConversion, none)
            case a: List[_] => json += "\"" + key + "\":" + anyToJsonString(a, accuracy, enablePreciseConversion, none)
            case a: Int => json += "\"" + key + "\":" + a
            case a: Boolean => json += "\"" + key + "\":" + a
            case a: String => json += "\"" + key + "\":\"" + escapeString(a) + "\""
            case a: Double => json += "\"" + key + "\":" + stringifyDouble(a, accuracy, enablePreciseConversion)
            case a: Option[_] => json += "\"" + key + "\":" + {
              if (a.isDefined) {
                a.get match {
                  case b: Map[_,_] => anyToJsonString(b, accuracy, enablePreciseConversion, none)
                  case b: List[_] => anyToJsonString(b, accuracy, enablePreciseConversion, none)
                  case b => b.toString
                }
              } else {
                none
              }
            }
            case _ => none;
          }
        }
      }
      case m: List[_] => {
        var list = new ListBuffer[String]()
        for ( el <- m ) {
          el match {
            case a: Map[_,_] => list += anyToJsonString(a, accuracy, enablePreciseConversion, none)
            case a: List[_] => list += anyToJsonString(a, accuracy, enablePreciseConversion, none)
            case a: Int => list += a.toString
            case a: Boolean => list += a.toString
            case a: String => list += "\"" + escapeString(a) + "\""
            case a: Double => list += stringifyDouble(a, accuracy, enablePreciseConversion)
            case a: Option[Double] => {
              list += {
                if (a.isDefined) {
                a.get match {
                  case b => stringifyDouble(b, accuracy, enablePreciseConversion)
                }
              } else {
                none
              }
            }}
            case _ => none;
          }
        }
        return "[" + list.mkString(",") + "]"
      }
      case _ => ;
    }
    "{" + json.mkString(",") + "}"
  }

  def anyToJsValue(value: Any, accuracy: Int, enablePreciseConversion: Boolean, none: String): JsValue = {
    val jsonString = anyToJsonString(value, accuracy, enablePreciseConversion, none)
    Json.parse(jsonString)
  }

  def bioToConceptsIndices(bio: List[(String, String)]): List[List[Int]] ={
    val tags = mutable.Map[String, List[Int]]()
    var i = 0
    for (pair <- bio){
      if (pair._2 != "O") {
        val key = pair._2.split("-")(1)
        if (tags contains key){
          tags(key) = i :: tags(key)
        } else {
          tags(key) = List[Int](i)
        }
      }
      i += 1
    }
    tags.values.toList
  }

  def stringifyMetricsMap(metrics: Map[Metric.Value, Option[_]], accuracy: Int, enablePreciseConversion: Boolean, none: String): Map[String, String] = {
    (for ((metric, value) <- metrics) yield (metric.toString, metricValueToString(value, accuracy, enablePreciseConversion, none))).toMap
  }

  def asrMetricOutputTagsResultAnyToAsrMetricOutputTagsResult(asrMetricOutputTagsResultAny: AsrMetricOutputTagsResultAny, accuracy: Int, enablePreciseConversion: Boolean, none: String):
  AsrMetricOutputTagsResult = {
    AsrMetricOutputTagsResult(asrMetricOutputTagsResultAny.tags, stringifyMetricsMap(asrMetricOutputTagsResultAny.result, accuracy, enablePreciseConversion, none))
  }

  def asrMetricOutputResultAnyToAsrMetricOutputResult(asrMetricOutputResultAny: AsrMetricOutputResultAny, accuracy: Int, enablePreciseConversion: Boolean, none: String):
  AsrMetricOutputResult = {
    AsrMetricOutputResult(stringifyMetricsMap(asrMetricOutputResultAny.result, accuracy, enablePreciseConversion, none),
      for (asrMetricOutputTagResultAny <- asrMetricOutputResultAny.byTags)
        yield asrMetricOutputTagsResultAnyToAsrMetricOutputTagsResult(asrMetricOutputTagResultAny, accuracy, enablePreciseConversion, none))
  }

  def tokenSequencesToAsrMetricOutput(tokenSequences: Seq[TokenSequence],
                                      average: AsrMetricOutputResultAny,
                                      accuracy: Int,
                                      enablePreciseConversion: Boolean,
                                      none: String): AsrMetricOutput = {
    AsrMetricOutput(List[AsrMetricOutputResult](asrMetricOutputResultAnyToAsrMetricOutputResult(average, accuracy, enablePreciseConversion, none)),
      for (tokenSequence <- tokenSequences)
        yield (tokenSequence.id, asrMetricOutputResultAnyToAsrMetricOutputResult(tokenSequence.metrics, accuracy, enablePreciseConversion, none)))
  }
}
