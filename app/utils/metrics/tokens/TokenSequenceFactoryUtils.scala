package utils.metrics.tokens

import enums.WerOperation
import models.metrics.Mut
import models.metrics.tokens.Token

import scala.collection.mutable.ListBuffer

object TokenSequenceFactoryUtils {
  def shiftNrzb(nrzb: Mut[ListBuffer[(Int, String)]], id: Int): Unit = {
    var i = nrzb.value.size - 1
    while ((i >= 0) && (nrzb.value(i)._1 >= id)) {
      nrzb.value(i) = (nrzb.value(i)._1 + 1, nrzb.value(i)._2)
      i -= 1
    }
  }

  def makeTokens(alignment: List[(Int, Int, WerOperation.Value)],
                 referenceTokens: List[(String, Int, Set[String])],
                 hypothesisWords: List[String],
                 nrzb: Option[Mut[ListBuffer[(Int, String)]]] = None): Seq[Token] = {
    val emptySet = Set[String]()
    for(((ref_index, hyp_index, operation), id) <- alignment.zipWithIndex)
      yield new Token(
        if (operation != WerOperation.INS) {
          referenceTokens(ref_index)._1
        } else {
          // If it is insert, then all indices of nrzb tokens going after current token must be increased by one
          // because they reflect position in the reference transcription
          if (nrzb.isDefined){
            shiftNrzb(nrzb.get, id)
          }
          "*" * hypothesisWords(hyp_index).length
        },
        if (operation != WerOperation.DEL) {
          hypothesisWords(hyp_index)
        } else {
          "*" * referenceTokens(ref_index)._1.length
        },
        id,
        if (operation != WerOperation.INS) {
          referenceTokens(ref_index)._2
        } else {
          -1
        },
        if ((ref_index < referenceTokens.length) && (ref_index >= 0)) {
          referenceTokens(ref_index)._3
        } else {
          emptySet
        },
        operation)
  }
}
