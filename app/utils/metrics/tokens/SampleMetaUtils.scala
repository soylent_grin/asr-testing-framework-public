package utils.metrics.tokens

import models.SoundbankSample

object SampleMetaUtils {
  def getMetaProperty(sample: SoundbankSample, propertyName: String): Option[String] = {
    val refFragments: Option[List[String]] = (sample.meta \ propertyName).asOpt[List[String]]
    var ref: Option[String] = None
    if (refFragments.isDefined){
      ref = Option[String](refFragments.get.mkString(" ").trim)
    }
    ref
  }

  def getRef(sample: SoundbankSample): Option[String] = {
    var ref = getMetaProperty(sample, "ref")
    if (ref.isEmpty){
      ref = Option[String](sample.transcription.mkString(" ").trim)
    }
    ref
  }

  def getTaggedRef(sample: SoundbankSample): Option[String] = {
    getMetaProperty(sample, "tagged-ref")
  }
}
