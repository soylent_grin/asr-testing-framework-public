package utils.metrics.tokens

import services.metrics.mapping.TokenMappingService

object TokenUtils {
  def mapToken(token: String, tagsForMapping: Set[String], tagsPresented: Set[String])(implicit tokenMappingService: TokenMappingService): String =
    if (tagsForMapping.intersect(tagsPresented).nonEmpty || tagsForMapping.isEmpty) {
      tokenMappingService.map(token)
    } else {
      token
    }
}
