package utils.metrics.tokens

import models.metrics.tokens.TokenSequence

object TokenSequenceUtils {
  def oneOrMany[A, B](values: B, actionForOne: B => A, actionForMany: Seq[B] => A): A ={
    values match {
      case values: Seq[B @unchecked] => actionForMany(values)
      case _ => actionForOne(values)
    }
  }

  def extractWords(implicit tokenSequences: Seq[TokenSequence]): Set[String] =
    (for (tokenSequence <- tokenSequences) yield tokenSequence.values()).
      foldLeft(Set[String]())((tokenSequencesWords: Set[String], tokenSequenceWords: Set[String]) => tokenSequencesWords.union(tokenSequenceWords))
}
