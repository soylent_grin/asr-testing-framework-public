package utils.metrics.morpho

import com.typesafe.config.Config
import enums.{Metric, MorphoFeature}
import models.metrics.morpho.MorphoInfoChunk
import models.metrics.tokens.{Token, TokenSequence}
import utils.metrics.Converter.{configObjectToMorphoFeatureMap, stringListToMorphoFeatureList}
import scala.collection.JavaConverters._

object MorphoUtils {
  // Extract morpho features from the given word using provided morphoInfo
  def getFeatures(value: String, morphoInfo: Map[String, MorphoInfoChunk]): Map[MorphoFeature.Value, String] = {
    if (morphoInfo contains value){
      morphoInfo(value).features
    } else {
      Map[MorphoFeature.Value, String]()
    }
  }

  // Check are lemmas of value and hypvalue of a token are equal
  def areLemmasEqual(token: Token, morphoInfo: Map[String, MorphoInfoChunk]): Boolean = {
    (morphoInfo contains token.value) && (morphoInfo contains token.hypvalue) &&
      (morphoInfo(token.value).lemma == morphoInfo(token.hypvalue).lemma)
  }

  def isMorphoFeatureRelevant(requiredMorphoFeatures: List[MorphoFeature.Value], feature: MorphoFeature.Value): Boolean =
    requiredMorphoFeatures.isEmpty || (requiredMorphoFeatures contains feature)


  // Filter out morpho features which are presented in morpho info about some reference token, but are not required
  def mergeMorphoFeaturesLists(presentedMorphoFeatures: Map[MorphoFeature.Value, String], requiredMorphoFeatures: List[MorphoFeature.Value]):
  Map[MorphoFeature.Value, String] =
    presentedMorphoFeatures.filter(pair => isMorphoFeatureRelevant(requiredMorphoFeatures, pair._1))

  def areFeatureValuesEqual(hypFeatures: Map[MorphoFeature.Value, String], featureName: MorphoFeature.Value, featureValue: String): Boolean =
    (hypFeatures contains featureName) && (featureValue == hypFeatures(featureName))

  // Check are features from the morpho info about some reference token have the same value in the corresponding hypothesis token
  def compareFeatures(refFeatures: Map[MorphoFeature.Value, String], hypFeatures: Map[MorphoFeature.Value, String]): Map[MorphoFeature.Value, Boolean] =
    refFeatures.map(pair => (pair._1, areFeatureValuesEqual(hypFeatures, pair._1, pair._2)))

  // Apply map reduce to list of sets of morpho Features
  def morphoMapReduce(featureSets: List[Map[MorphoFeature.Value, Boolean]], mapFunction: Boolean => Double): Map[MorphoFeature.Value, Double] = {
    // convert bool values to ints
    val mappedValues = featureSets.map(features => features.mapValues(mapFunction))
    if (mappedValues.isEmpty){
      Map[MorphoFeature.Value, Double]()
    } else {
      mappedValues.reduce((previousMorphoFeatureErrorCount, nextMorphoFeatureErrorCount) => { // calculate errors
        (for (feature <- previousMorphoFeatureErrorCount.keySet.union(nextMorphoFeatureErrorCount.keySet))
          yield (feature, previousMorphoFeatureErrorCount.getOrElse(feature, 0.0) + nextMorphoFeatureErrorCount.getOrElse(feature, 0.0))).toMap
      })
    }
  }

  def mergeIndividualMstats(collectedMstat: Map[MorphoFeature.Value, Map[String, Int]], nextIndividualMstat: Map[MorphoFeature.Value, String]):
  Map[MorphoFeature.Value, Map[String, Int]] = {
    val collectedMstatWithDefault = collectedMstat.mapValues(_.withDefault(_ => 0)).withDefault(_ => Map[String, Int]().withDefault(_ => 0))
    (for (feature <- nextIndividualMstat.keySet.union(collectedMstat.keySet))
      yield (feature, (
        for (featureValue <- collectedMstatWithDefault(feature).keySet.union(
          if (nextIndividualMstat contains feature) Set(nextIndividualMstat(feature)) else Set()))
          yield (featureValue,
            (if ((nextIndividualMstat contains feature) && (nextIndividualMstat(feature) == featureValue)) 1 else 0) +
              collectedMstatWithDefault(feature)(featureValue))).toMap)).toMap
  }

  // Extract morpho features from given token using provided morpho info
  def extractMorphoFeatures(tokenSequence: TokenSequence,
                            calculateOnAllTokens: Boolean,
                            morphoInfo: Map[String, MorphoInfoChunk]): Seq[Option[(Map[MorphoFeature.Value, String], Map[MorphoFeature.Value, String], Token)]] =
    for (token <- tokenSequence.tokens)
      yield {
        if (calculateOnAllTokens || areLemmasEqual(token, morphoInfo)){
          Some(getFeatures(token.value, morphoInfo), getFeatures(token.hypvalue, morphoInfo), token)
        } else {
          None
        }
      }

  def getMerMorphoFeatures(config: Config): List[MorphoFeature.Value] =
    stringListToMorphoFeatureList(config.getStringList("metric.merMorphoFeatureList").asScala.toList)

  def getMorphoWeights(config: Config): Map[MorphoFeature.Value, Double] =
    configObjectToMorphoFeatureMap(config.getObject("metric.morphoFeaturesWeights"))

  def getMstatMorphoFeatures(config: Config): List[MorphoFeature.Value] =
    stringListToMorphoFeatureList(config.getStringList("metric.mstatMorphoFeatureList").asScala.toList)

  def setLemmaWeightsIfRequired(tokenSequence: TokenSequence, weight: Int): Unit =
    Metric.metricsOnLemma().foreach(metric => {
      if (!tokenSequence.isCustomWeightDefined(metric)){
        tokenSequence.setWeight(metric, weight)
      }
    })

  def setMerWeights(tokenSequence: TokenSequence, weights: Map[MorphoFeature.Value, Double], calculateOnAllTokens: Boolean, tags: Option[List[String]] = None): Unit =
  // Stringify metric feature names for unified averaging
    tokenSequence.setWeight(
      if (calculateOnAllTokens) {
        Metric.MER
      } else {
        Metric.LMER
      }, weights.map(pair => (pair._1.toString, pair._2)), tags)
}
