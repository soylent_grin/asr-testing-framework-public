package utils.metrics

import enums.{Metric, MorphoFeature}
import utils.collections.ListsUtils.{areIntersected, getListsUnion}

object MetricUtils {
  def tryChange(min: Int, max: Int, value: Int, step: Int): Int = {
    val new_value = value + step
    if ((new_value < min) || (new_value > max)) value else new_value
  }

  def toZero(value: Any): Any = {
    value match {
      case _: Double => 0.0
      case a: Map[String @unchecked, _] => a.mapValues(_ => 0.0)
      case a: List[_] => a.map(_ => 0.0)
      case _ => value
    }
  }

  def shouldGetMorphoInfo(features: List[MorphoFeature.Value], globalMetrics: List[Metric.Value], localMetrics: List[(List[String], List[Metric.Value])]): Boolean = {
    val metrics = getListsUnion(globalMetrics, (for (metric <- localMetrics) yield metric._2).flatten)
    features.nonEmpty && areIntersected(metrics, Metric.morphoMetrics())
  }
}
