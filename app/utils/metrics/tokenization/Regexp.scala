package utils.metrics.tokenization

object Regexp {
  def tokenizeByRegexp(ref: String, regexp: String): Array[String] = {
    regexp.r.findAllIn(ref).toArray
  }
}
