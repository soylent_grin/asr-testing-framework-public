package utils.metrics.tokenization

import play.api.Logger

import scala.collection.mutable.{ListBuffer, Stack}

object Tagged {
  def tokenize(tokenizer: String, complexNameSeparator: String, garbageTags: Seq[String], garbageTagsWithValues: Seq[String])(ref: String)
              (implicit logger: Logger): (ListBuffer[String], ListBuffer[Int], ListBuffer[Set[String]], ListBuffer[(Int, String)]) = {
    var indexComplexName = 0
    var tokens = new ListBuffer[String]()
    var indexesComplex = new ListBuffer[Int]()
    var tags = new ListBuffer[List[String]]()
    var garbageList = scala.collection.mutable.Set[(Int, String)]()

    logger.debug(s"Tokenizing string '$ref'...")
    var openBrackets = Stack[Int]()
    var openTagsIndex = Stack[Int]()
    var openTags = Stack[String]()

    var value = ""
    for ((c, i) <- ref.view.zipWithIndex) {
      if ((c == '(')) { // добавляем открытие скобки
        if (!value.isEmpty) {
          if (openBrackets.isEmpty && openTagsIndex.isEmpty) {
            indexComplexName = addTokensOrGarbage(garbageTags, garbageTagsWithValues, complexNameSeparator, tokenizer)(value, openTags.toList, indexComplexName, tokens, indexesComplex, tags, garbageList)._1
          } else {
            //copy
            if (isTag(openBrackets, openTagsIndex)) { // если тэг
              val oldGarbageSize = garbageList.size
              val newTokGarb = addTokensOrGarbage(garbageTags, garbageTagsWithValues, complexNameSeparator, tokenizer)(value, openTags.toList, indexComplexName, tokens, indexesComplex, tags, garbageList)
              if (newTokGarb._1 > indexComplexName) {
                indexComplexName = newTokGarb._1
              } /* else if(!newTokGarb._2) {
                throw new IllegalStateException(s"Not correct tags transcription: tags without value or extra bracket with index $i")
              } */
            } else { // replace ??? or error?
              throw new IllegalStateException(s"Not correct tags transcription: extra bracket with index $i")
            }
          }
          value = ""
        }
        openBrackets.push(i)
      } else if (c == ':') { // гипотетически тэг
        if (!openBrackets.isEmpty) {
          if (value.trim.isEmpty) {
            throw new IllegalStateException(s"Not correct tags transcription: empty tag with index $i")
          } else {
            openTags.push(value.trim)
            openTagsIndex.push(openBrackets.pop())
            value = ""
          }
        }
      } else if (c == ')') { // закрытие тэга или замены
        if (isTag(openBrackets, openTagsIndex)) { // если тэг
          if (!openTagsIndex.isEmpty && !openTags.isEmpty) {
            if(!value.isEmpty) {
              indexComplexName = addTokensOrGarbage(garbageTags, garbageTagsWithValues, complexNameSeparator, tokenizer)(value, openTags.toList, indexComplexName, tokens, indexesComplex, tags, garbageList)._1
            }
            openTagsIndex.pop()
            openTags.pop()
          } else {
            throw new IllegalStateException(s"Not correct tags transcription: close bracket without tags (or bracket) in $i")
          }
        } else { // если замена
          openBrackets.pop()
          value = value.trim
          var garbageValue = false
          for(garbageTagWithValue <- garbageTagsWithValues) {
            if(value.matches(garbageTagWithValue)) {
              garbageList ++= Set((tokens.size - 1, garbageTagWithValue))
              garbageValue = true
            }
          }
          if (!garbageValue) {
            removeToken(tokens, indexesComplex, tags, garbageList)
            indexComplexName = addTokensOrGarbage(garbageTags, garbageTagsWithValues, complexNameSeparator, tokenizer)(value, openTags.toList, indexComplexName - 1, tokens, indexesComplex, tags, garbageList)._1
          }
        }
        value = ""
      } else {
        value += c
      }
    }
    if (!value.isEmpty) {
      if (openBrackets.isEmpty && openTagsIndex.isEmpty) {
        indexComplexName = addTokensOrGarbage(garbageTags, garbageTagsWithValues, complexNameSeparator, tokenizer)(value, openTags.toList, indexComplexName, tokens, indexesComplex, tags, garbageList)._1
      } else {
        if (openBrackets.isEmpty) {
          throw new IllegalStateException(s"Not correct tags transcription: open bracket without pair ${openTagsIndex.top}")
        } else {
          throw new IllegalStateException(s"Not correct tags transcription: open bracket without pair ${openBrackets.top}")
        }
      }
    }

    logger.debug(tokens.toString())
    logger.debug(indexesComplex.toString())
    logger.debug(tags.toString())
    logger.debug(garbageList.toString())

    (tokens, indexesComplex, tags.map(_.toSet), (ListBuffer.empty ++= garbageList).sorted)
  }

  def isTag(openBrackets: Stack[Int], openTagsIndex: Stack[Int]): Boolean = {
    var indexBracket = -1
    var indexTag = -1
    if (!openTagsIndex.isEmpty) {
      indexTag = openTagsIndex.top
    }
    if (!openBrackets.isEmpty) {
      indexBracket = openBrackets.top
    }
    indexBracket <= indexTag
  }

  def addTokensOrGarbage(garbageTags: Seq[String], garbageTagsWithValues: Seq[String], complexNameSeparator: String, tokenizer: String)
                        (value: String, tags: List[String], immutableIndexComplexName: Int, tokens: ListBuffer[String],
                         indexesComplex: ListBuffer[Int], tagsList: ListBuffer[List[String]],
                         garbageList: scala.collection.mutable.Set[(Int, String)]): (Int, Boolean) = {
    var tagsWithVal = new ListBuffer[String]()
    var newGarbageList = new ListBuffer[(Int, String)]
    for(garbageTag <- garbageTags) {
      if (tags.contains(garbageTag)) {
        if (!garbageList.contains((tokens.size - 1, garbageTag))) {
          if(garbageTagsWithValues.contains(garbageTag)) {
            tagsWithVal ++= List((garbageTag))
          } else {
            newGarbageList ++= Set((tokens.size - 1, garbageTag))
          }
        }
      }
    }
    if(!newGarbageList.isEmpty || !tagsWithVal.isEmpty) {
      if(!tagsWithVal.isEmpty && newGarbageList.isEmpty) {
        val res = addTokens(complexNameSeparator, tokenizer)(value, tags, immutableIndexComplexName, tokens, indexesComplex, tagsList)
        for (tagWithVal <- tagsWithVal) {
          garbageList ++= Set((tokens.size - 1, tagWithVal))
        }
        (res, true)
      } else {
        garbageList ++= newGarbageList
        (immutableIndexComplexName, true)
      }
    } else {
      (addTokens(complexNameSeparator, tokenizer)(value, tags, immutableIndexComplexName, tokens, indexesComplex, tagsList), false)
    }
  }

  def addTokens(complexNameSeparator: String, tokenizer: String)(value: String, tags: List[String], immutableIndexComplexName: Int, tokens: ListBuffer[String],
                indexesComplex: ListBuffer[Int], tagsList: ListBuffer[List[String]]): Int = {
    var resTokenize = tokenizeWithComplex(complexNameSeparator, tokenizer)(value, tags, immutableIndexComplexName)
    tokens ++= resTokenize._1
    indexesComplex ++= resTokenize._2
    tagsList ++= resTokenize._3
    resTokenize._4
  }

  def removeToken(tokens: ListBuffer[String], indexes: ListBuffer[Int], tags: ListBuffer[List[String]],
                  garbageList: scala.collection.mutable.Set[(Int, String)]): Unit = {
    if(indexes.size > 0) {
      val indexLast = indexes.last
      for(i <- indexes.size-1 to(0,-1) if indexes(i) >= indexLast ) {
        val removingGarbageList = garbageList.filter{case (index, _) => index == tags.size-1}
        if(!removingGarbageList.isEmpty) {
          if(tags.size > 1) {
            val newGarbIndexes = removingGarbageList.zip(tags(tags.size-2)).filter(x => x._1._2==x._2).map(x => (x._1._1 - 1, x._1._2))
            garbageList ++= newGarbIndexes
          }
          garbageList --= removingGarbageList
        }

        tokens.remove(tokens.size-1)
        indexes.remove(indexes.size-1)
        tags.remove(tags.size-1)
      }
    }
  }

  def tokenizeWithComplex(complexNameSeparator: String, tokenizer: String)(value: String, tags: List[String], immutableIndexComplexName: Int): (ListBuffer[String],
    ListBuffer[Int], ListBuffer[List[String]], Int) = {
    var indexComplexName = immutableIndexComplexName
    var tokens = new ListBuffer[String]()
    val indexesComplex = new ListBuffer[Int]()
    var tagsList = new ListBuffer[List[String]]()

    var newTokens = _tokenize(tokenizer)(value)

    for (token<-newTokens) {
      if(token(0) != null) {
        var words = token(0).split(complexNameSeparator)
        for(word <- words) {
          tokens += word
          indexesComplex += indexComplexName
          tagsList += tags
        }
        indexComplexName=indexComplexName + 1
      } else {
        tokens += token(1)
        indexesComplex += indexComplexName
        indexComplexName=indexComplexName + 1
        tagsList += tags
      }
    }
    (tokens, indexesComplex, tagsList, indexComplexName)
  }

  def _tokenize(tokenizer: String)(text: String): List[List[String]] = {
    (for (m <- tokenizer.r.findAllIn(text).matchData) yield (for (i <- 1 to m.groupCount) yield m.group(i)).toList).toList
  }
}
