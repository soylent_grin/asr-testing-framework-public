package utils.metrics

import models.AsrMetricOutput
import models.metrics.tokens.{Token, TokenSequence}

import scala.collection.mutable.ListBuffer

object Summarizer {
  def findTokenSequenceById(id: String)(implicit tokenSequences: Seq[TokenSequence]): Option[TokenSequence] = tokenSequences.filter(_.id == id) match {
    case a if a.nonEmpty => Some(a.head)
    case _ => None
  }

  def stringifyOrElse(tokenSequence: Option[TokenSequence], stringifyToken: Token => String, default: String = "-"): String =
    tokenSequence match {
      case ts if ts.isDefined => ts.get.tokens.map(stringifyToken).mkString(" ")
      case _ => default
    }

  def extractRef(tokenSequence: Option[TokenSequence]): String = stringifyOrElse(tokenSequence, token => token.value)
  def extractHyp(tokenSequence: Option[TokenSequence]): String = stringifyOrElse(tokenSequence, token => token.hypvalue)

  def summarize(asrMetricOutput: AsrMetricOutput)(implicit tokenSequences: Seq[TokenSequence]): String = {
    val result = ListBuffer[String]()
    result += "Without tags"
    //
    // output metrics without tags
    //

    // average
    result += "Overall"
    result += f"id\t\t\t${(for ((metric, _) <- asrMetricOutput.overall.head.result) yield metric).mkString("\t")}"
    result += f"overall\t\t\t${(for ((_, value) <- asrMetricOutput.overall.head.result) yield value).mkString("\t")}"


    // single
    result += "Single"
    if (asrMetricOutput.single.nonEmpty){
      val metrics = for ((metric, _) <- asrMetricOutput.single.head._2.result) yield metric
      result += f"id\treference\thypothesis\t${metrics.mkString("\t")}"
      for ((id, values) <- asrMetricOutput.single) {
        val currentTokenSequence = findTokenSequenceById(id)
        result += f"$id\t${extractRef(currentTokenSequence)}\t${extractHyp(currentTokenSequence)}\t${metrics.map(values.result.getOrElse(_, "-")).mkString("\t")}"
      }

    }

    //
    // output metrics with tags
    //
    result += "With tags"
    if (asrMetricOutput.single.nonEmpty){
      for (tags <- asrMetricOutput.single.head._2.byTags.map(item => item.tags)){
        result += f"Tags: ${tags.mkString(", ")}"
        // average
        result += "Overall"
        val overallValues = asrMetricOutput.overall.head.byTags.filter(result => result.tags == tags).head.result
        result += f"id\t\t\t${(for ((metric, _) <- overallValues) yield metric).mkString("\t")}"
        result += f"overall\t\t\t${(for ((_, value) <- overallValues) yield value).mkString("\t")}"

        // single
        result += "Single"
        val metrics = for ((metric, _) <- asrMetricOutput.single.head._2.byTags.head.result) yield metric
        result += f"id\treference\thypothesis\t${metrics.mkString("\t")}"
        for ((id, values) <- for(singleWithTags <- asrMetricOutput.single) yield (singleWithTags._1,
          singleWithTags._2.byTags.filter(item => item.tags == tags).head)){
          val currentTokenSequence = findTokenSequenceById(id)
          result += f"$id\t${extractRef(currentTokenSequence)}\t${extractHyp(currentTokenSequence)}\t${metrics.map(values.result.getOrElse(_, "-")).mkString("\t")}"
        }
      }
    }
    result.mkString("\n")
  }
}
