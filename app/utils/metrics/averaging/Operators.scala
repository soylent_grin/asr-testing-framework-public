package utils.metrics.averaging

object Operators {
  //
  // Operators implement low-level computations of intermediate values which are needed in process of averaging metrics
  //

  def operateWithNumericScores[A, B](x: A, y: B, op: (Double, Double) => Double)(implicit xConverter: Numeric[A], yConverter: Numeric[B]): Double = {
    val xDouble = xConverter.toDouble(x)
    val yDouble = yConverter.toDouble(y)

    if ((xDouble != Double.NaN) && (yDouble != Double.NaN)){
      op(xDouble, yDouble)
    } else {
      throw new IllegalStateException("Incorrect weighted scores for adding")
    }
  }

  def operateWithMapAndNumericScore[A, B](m: Map[B, Any], score: A, op: (Any, Any) => Any)(implicit scoreConverter: Numeric[A]): Map[B, Any] = {
    m.map({
      case (kn, vn) => kn -> op(vn, scoreConverter.toDouble(score))
    })
  }

  // Reverse order here means that first argument provided to the operator is the numeric score, and as a second - value from map
  def operateWithMapAndNumericScoreReverseOrder[A, B](m: Map[B, Any], score: A, op: (Any, Any) => Any)(implicit scoreConverter: Numeric[A]): Map[B, Any] = {
    m.map({
      case (kn, vn) => kn -> op(scoreConverter.toDouble(score), vn)
    })
  }

  def operateWithMaps[B](a: Map[B, Any], b: Map[B, Any], op: (Any, Any) => Any, default: Any): Map[B, Any] = {
    a.map({
      case (kn, vn) => kn -> op(vn, b.getOrElse(kn, default))
    })
  }

  def operateWithMapsUsingDefaultFunction[B](a: Map[B, Any], b: Map[B, Any], op: (Any, Any) => Any, default: Any => Any): Map[B, Any] = {
    a.map({
      case (kn, vn) => kn -> op(vn, b.getOrElse(kn, default(vn)))
    })
  }

  //
  // Higher-level operators for additional checks
  //

  def addNumericScores[A, B](x: A, y: B)(implicit xConverter: Numeric[A], yConverter: Numeric[B]): Double = operateWithNumericScores(x, y, _+_)
  def multiplyNumericScores[A, B](x: A, y: B)(implicit xConverter: Numeric[A], yConverter: Numeric[B]): Double = operateWithNumericScores(x, y, _*_)
  def divideNumericScores[A, B](x: A, y: B)(implicit xConverter: Numeric[A], yConverter: Numeric[B]): Double =
    y match {
      case 0 => throw new IllegalStateException("Can't divide numeric score by weight of 0")
      case _ => operateWithNumericScores(x, y, _/_)
    }
}