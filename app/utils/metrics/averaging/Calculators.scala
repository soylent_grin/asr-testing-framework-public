package utils.metrics.averaging

import enums.Metric
import utils.metrics.MetricUtils.toZero
import utils.metrics.averaging.Operators._

object Calculators {
  //
  // Calculators are working on top of Operators and provide direct implementations for stages of averaging without dealing with concrete numbers
  //

  def getWeightedScoreForAny(metric: Metric.Value, defaultMappedWeight: Double)(weight: Any, score: Any): Any = {
    if (Metric.unweightedMetrics() contains metric) {
      score
    } else {
      (score, weight) match {
        case (s: Double, w: Double) => multiplyNumericScores(s, w)
        case (s: Double, w: Int) => multiplyNumericScores(s, w)
        case (s: Int, w: Double) => multiplyNumericScores(s, w)
        case (s: Int, w: Int) => multiplyNumericScores(s, w)

        case (s: Map[String @unchecked, _], w: Double) => operateWithMapAndNumericScoreReverseOrder(s, w, getWeightedScoreForAny(metric, defaultMappedWeight))
        case (s: Map[String @unchecked, _], w: Int) => operateWithMapAndNumericScoreReverseOrder(s, w, getWeightedScoreForAny(metric, defaultMappedWeight))

        case (s: Map[String @unchecked, _], w: Map[String @unchecked, _]) => operateWithMaps(s, w, getWeightedScoreForAny(metric, defaultMappedWeight), defaultMappedWeight)

        case a => a
      }
    }
  }

  def addWeightedScores(sum: Any, value: Any): Any = {
    (sum, value) match {
      case (s: Double, v: Double) => addNumericScores(s, v)
      case (s: Double, v: Int) => addNumericScores(s, v)
      case (s: Int, v: Double) => addNumericScores(s, v)
      case (s: Int, v: Int) => addNumericScores(s, v)

      case (s: Map[String @unchecked, _], v: Map[String @unchecked, _]) => operateWithMapsUsingDefaultFunction(s, v, addWeightedScores, toZero)

      case _ => sum
    }
  }

  def divideMapByNumericScore[A, B](m: Map[B, Any], score: A, defaultMappedDivisor: Double)(implicit scoreConverter: Numeric[A]): Map[B, Any] =
    operateWithMapAndNumericScore(m, score, divideWeightedScores(defaultMappedDivisor))

  def divideWeightedScores(defaultMappedDivisor: Double)(divided: Any, divisor: Any): Any = {
    (divided, divisor) match {
      case (s: Double, v: Double) => divideNumericScores(s, v)
      case (s: Double, v: Int) => divideNumericScores(s, v)
      case (s: Int, v: Double) => divideNumericScores(s, v)
      case (s: Int, v: Int) => divideNumericScores(s, v)

      case (s: Map[String @unchecked, _], v: Map[String @unchecked, _]) => operateWithMaps(s, v, divideWeightedScores(defaultMappedDivisor), defaultMappedDivisor)
      case (s: Map[String @unchecked, _], v: Double) => divideMapByNumericScore(s, v, defaultMappedDivisor)
      case (s: Map[String @unchecked, _], v: Int) => divideMapByNumericScore(s, v, defaultMappedDivisor)

      case _ => divided
    }
  }
}
