package utils.metrics.averaging

import enums.Metric
import models.metrics.tokens.TokenSequence
import utils.collections.MapsUtils.sumMaps

import scala.util.Try

object TokenSequenceCollectionHandlers {
  def getSumOfWeights(tokenSequences: Seq[TokenSequence], metric: Metric.Value, tags: Option[List[String]]): Any = {
    // extract required weight for every tokenSequence which has that weight written
    val weights = tokenSequences
      .filter(tokenSequence => Try(tokenSequence.weight(metric, tags = tags)).isSuccess)
      .map(tokenSequence => tokenSequence.weight(metric, tags = tags))
    weights.head match {
      case _: Int => weights.asInstanceOf[Seq[Int]].sum
      case _: Double => weights.asInstanceOf[Seq[Double]].sum
      case _: Map[String @unchecked, Double @unchecked] => weights.asInstanceOf[Seq[Map[String, Double]]].foldLeft(Map[String, Double]())(sumMaps)
    }
  }
}
