package utils.assets

object ResourceUtils {
  def getResourceAbsolutePath(resourceRelativePath: String, callingClass: Class[_]): String = {
    val resource = callingClass.getResource(resourceRelativePath)
    if (resource == null){
      resourceRelativePath
    } else {
      resource.getPath
    }
  }
}
