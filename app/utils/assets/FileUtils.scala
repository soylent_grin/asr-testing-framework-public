package utils.assets

import java.io._
import java.nio.file.{Files, Paths}

import exceptions.DicFileDoesNotExistException
import play.api.Logger

import scala.collection.mutable.ListBuffer
import scala.collection.{immutable, mutable}
import scala.io.Source
import scala.util.control.NonFatal

object FileUtils {

  private val logger = Logger(getClass)

  def write(path: String, bytes: Array[Byte]) = {
    val bos = new BufferedOutputStream(new FileOutputStream(path))
    try {
      Stream.continually(bos.write(bytes))
    } catch {
      case t: Throwable =>
        logger.error(s"failed to write file: error is ${t.getMessage}")
    } finally {
      bos.close()
    }
  }

  def read(path: String): List[String] = {
    val buffer = ListBuffer[String]()
    for (line <- Source.fromFile(path).getLines) {
      buffer += line
    }
    buffer.toList
  }

  def copy(from: String, to: String)(implicit logger: Logger): Unit = {
    val inputChannel = new FileInputStream(from).getChannel
    val outputChannel = new FileOutputStream(to).getChannel
    outputChannel.transferFrom(inputChannel, 0, inputChannel.size)
    inputChannel.close()
  }

  def move(from: File, to: File): Unit = {
    if (from.isDirectory) {
      org.apache.commons.io.FileUtils.moveDirectory(from, to)
    } else {
      org.apache.commons.io.FileUtils.moveFile(from, to)
    }
  }

  def deleteRecursively(file: File): Unit = {
    if (file.isDirectory) {
      file.listFiles.foreach(deleteRecursively)
    }
    if (file.exists && !file.delete) {
      throw new Exception(s"Unable to delete ${file.getAbsolutePath}")
    }
  }
  /**
    * Pretty print the directory tree and its file names.
    *
    * @param folder
    * must be a folder.
    * @return
    */
  def printDirectoryTree(folder: File): String = {
    if (!folder.isDirectory) throw new IllegalArgumentException("folder is not a Directory")
    val indent = 0
    val sb = new StringBuilder
    printDirectoryTree(folder, indent, sb)
    print(sb.toString)
    sb.toString()
  }

  private def printDirectoryTree(folder: File, indent: Int, sb: StringBuilder): Unit = {
    if (!folder.isDirectory) throw new IllegalArgumentException("folder is not a Directory")
    sb.append(getIndentString(indent))
    sb.append("+--")
    sb.append(folder.getName)
    sb.append("/")
    sb.append("\n")
    for (file <- folder.listFiles) {
      if (file.isDirectory) printDirectoryTree(file, indent + 1, sb)
      else printFile(file, indent + 1, sb)
    }
  }

  private def printFile(file: File, indent: Int, sb: StringBuilder): Unit = {
    sb.append(getIndentString(indent))
    sb.append("+--")
    sb.append(file.getName)
    sb.append("\n")
  }

  private def getIndentString(indent: Int) = {
    val sb = new StringBuilder
    var i = 0
    while ( {
      i < indent
    }) {
      sb.append("|  ")

      {
        i += 1; i - 1
      }
    }
    sb.toString
  }

  def writeToTempFile(contents: Seq[String],
                      prefix: Option[String] = None,
                      suffix: Option[String] = None): File = {
    val tempFile = File.createTempFile(prefix.getOrElse("prefix-"), suffix.getOrElse("-suffix"))
    tempFile.deleteOnExit()

    new PrintWriter(tempFile, "utf-8") {
      try {
        for (contentPiece <- contents){
          write(contentPiece ++ "\n")
        }
      } finally {
        close()
      }
    }
    tempFile
  }

  def withResources[T <: AutoCloseable, V](r: => T)(f: T => V): V = {
    val resource: T = r
    require(resource != null, "resource is null")
    var exception: Option[Throwable] = None
    try {
      f(resource)
    } catch {
      case NonFatal(e) =>
        exception = Some(e)
        throw e
    } finally {
      closeAndAddSuppressed(exception, resource)
    }
  }

  private def closeAndAddSuppressed(e: Option[Throwable],
                                    resource: AutoCloseable): Unit = {
    if (e.isDefined) {
      try {
        resource.close()
      } catch {
        case NonFatal(suppressed) =>
          e.get.addSuppressed(suppressed)
      }
    } else {
      resource.close()
    }
  }

  def makeMapping[A](path: String, extractReplacingValue: List[String] => A,
                     composeDefault: (String, immutable.Map[String, A]) => A)
                    (implicit delimiter: String):
  immutable.Map[String, A] = {
    var result = mutable.Map[String, A]()

    if (Files.exists(Paths.get(path))) {
      withResources(Source.fromFile(path, "UTF-8")) {
        source => {
          for (line <- source.getLines()) {
            val splittedLine = line.split(" ").toList
            try {
              result += (splittedLine.head -> extractReplacingValue(splittedLine))
            } catch {
              case e: Throwable =>
                logger.trace(s"Can't make mapping for value '${splittedLine.head}'. Skipping...")
                logger.trace(e.getStackTrace.mkString("\n"))
            }
          }
        }
      }
    } else {
      throw DicFileDoesNotExistException()
    }
    logger.debug(s"Read mapping of ${result.size} entries")

    result.toMap.withDefault(token => composeDefault(token, result.toMap))
  }

  def handleThroughTmpFile[A](content: Seq[String], prefix: Option[String] = None, suffix: Option[String] = None, shouldBeDisposed: Boolean = true)(handle: File => A): A = {
    val tmpFile = writeToTempFile(content, prefix, suffix)
    try {
      handle(tmpFile)
    } finally {
      if (shouldBeDisposed){
        tmpFile.delete
      }
    }
  }
}
