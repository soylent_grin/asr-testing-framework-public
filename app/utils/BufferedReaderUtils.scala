package utils

import java.io.BufferedReader

object BufferedReaderUtils {
  def read(reader: BufferedReader): String =
    Stream.continually(reader.readLine).takeWhile(_ != null).mkString("\n")
}
