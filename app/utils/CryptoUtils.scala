package utils

import java.math.BigInteger
import java.security.MessageDigest

object CryptoUtils {

  private val d = MessageDigest.getInstance("MD5")

  def md5Hash(str: String): String = {
    val di = d.digest(str.getBytes)
    val bigInt = new BigInteger(1, di)
    bigInt.toString(16)
  }

}
