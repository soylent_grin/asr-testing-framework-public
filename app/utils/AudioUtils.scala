package utils

import java.io.{ByteArrayInputStream, DataOutputStream, File, FileInputStream, FileOutputStream, InputStream, OutputStream}

import sys.process._
import dtos.AudioInfoDto
import javax.sound.sampled._
import play.api.Logger
import javax.sound.sampled.AudioFileFormat
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import java.nio.file.Files

import cats.instances.int
import javax.sound.sampled
import org.apache.commons.io.{FileUtils, IOUtils}

object AudioUtils {

  val logger = Logger(getClass)

  // sox input.wav -n stat  2>&1 | sed -n 's#^Max    amplitude:[^0-9]*\([0-9.]*\)$#\1#p'
  def getMaxAmplitude(sample: File)(implicit logger: Logger): String = {
    val cmd = Seq(
      "sox",
      sample.getAbsolutePath,
      "-n",
      "stat",
      "2>&1"
    )
    logger.trace(s"running command: $cmd")
    val result = Seq(
      "bash",
      "-c",
      cmd.mkString(" ")
    ).!!
    val prefix = "Maximum amplitude:"
    val a = result.split("\n").filter(_.startsWith(prefix)).map(_.substring(prefix.length).trim)
    a.head
  }

  def getAudioInfo(file: File): AudioInfoDto = {
    val audioInputStream = AudioSystem.getAudioInputStream(file)
    val format = audioInputStream.getFormat
    val audioFileLength = file.length
    val frameSize = format.getFrameSize
    val frameRate = format.getFrameRate
    AudioInfoDto(
      audioFileLength / (frameSize * frameRate),
      format.getSampleRate
    )
  }

  def rawPcmToRiff(pcmFile: File): File = {
    import java.io.FileInputStream
    import java.io.FileOutputStream
    import java.io.OutputStream
    val inputSize = pcmFile.length.asInstanceOf[Int]
    val output = Files.createTempFile("audio", "record").toFile
    output.createNewFile()

    val sampleRate = 48000
    val bitsPerSample = 16
    val channelCount = 2

    try {
      val encoded = new FileOutputStream(output)
      // WAVE RIFF header
      writeToOutput(encoded, "RIFF") // chunk id

      writeToOutput(encoded, 36 + inputSize) // chunk size

      writeToOutput(encoded, "WAVE") // format

      // SUB CHUNK 1 (FORMAT)
      writeToOutput(encoded, "fmt ") // subchunk 1 id

      writeToOutput(encoded, 16) // subchunk 1 size

      writeToOutput(encoded, 1.toShort) // audio format (1 = PCM)

      writeToOutput(encoded, channelCount.asInstanceOf[Short]) // number of channelCount

      writeToOutput(encoded, sampleRate) // sample rate

      writeToOutput(encoded, sampleRate * channelCount * bitsPerSample / 8) // byte rate

      writeToOutput(encoded, (channelCount * bitsPerSample / 8).asInstanceOf[Short]) // block align

      writeToOutput(encoded, bitsPerSample.asInstanceOf[Short]) // bits per sample

      // SUB CHUNK 2 (AUDIO DATA)
      writeToOutput(encoded, "data") // subchunk 2 id

      writeToOutput(encoded, inputSize) // subchunk 2 size

      IOUtils.copy(new FileInputStream(pcmFile), encoded)
      output
    } catch {
      case e: Exception =>
        throw e
    }
  }

  import java.io.IOException

  private val TRANSFER_BUFFER_SIZE = 10 * 1024

  /**
    * Writes string in big endian form to an output stream
    *
    * @param output stream
    * @param data   string
    * @throws IOException
    */
  @throws[IOException]
  def writeToOutput(output: OutputStream, data: String): Unit = {
    var i = 0
    while ({
      i < data.length
    }) {
      output.write(data.charAt(i))
      i += 1
    }
  }

  @throws[IOException]
  def writeToOutput(output: OutputStream, data: Int): Unit = {
    output.write(data >> 0)
    output.write(data >> 8)
    output.write(data >> 16)
    output.write(data >> 24)
  }

  @throws[IOException]
  def writeToOutput(output: OutputStream, data: Short): Unit = {
    output.write(data >> 0)
    output.write(data >> 8)
  }

  @throws[IOException]
  def copy(source: InputStream, output: OutputStream): Long = copy(source, output, TRANSFER_BUFFER_SIZE)

  @throws[IOException]
  def copy(source: InputStream, output: OutputStream, bufferSize: Int): Long = {
    var read = 0L
    val buffer = new Array[Byte](bufferSize)
    var n = 0
    while ( {
      (n = source.read(buffer)) != -1
    }) {
      output.write(buffer, 0, n)

      read += n
    }
    read
  }

}
