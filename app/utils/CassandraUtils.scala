package utils

import com.outworkers.phantom.builder.primitives.Primitive
import play.api.libs.json.{JsValue, Json}

object CassandraUtils {

  implicit val jsonPrimitive: Primitive[JsValue] = {
    Primitive.json[JsValue](_.toString())(Json.parse)
  }

}
