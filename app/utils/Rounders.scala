package utils

import scala.collection.immutable

object Rounders {
  def ceil(map: immutable.Map[_, _], accuracy: Int): immutable.Map[String, _] = {
    map.map{
      case (k, v) => k.toString -> {
        v match {
          case a: Double => ceil(a, accuracy)
          case a: immutable.Map[_, _] => ceil(a, accuracy)
          case a: List[Option[Double] @unchecked] => ceil(a, accuracy)
          case a => a
        }
      }
    }
  }

  def ceilOptional(number: Option[_], accuracy: Int): Option[_] = {
    if (number.isDefined){
      Some(number.get match {
        case value: Double => ceil(value, accuracy)
        case value: Map[String @unchecked, _] => ceil(value, accuracy)
        case value: List[Option[Double] @unchecked] => ceil(value, accuracy)
        case _ => number.get
      })
    } else {
      number
    }
  }

  //
  // Basic ceils
  //

  def ceil(list: List[Option[Double]], accuracy: Int): List[Option[_]] = {
    list.map(item => {
      if (item.isDefined){
        Some(ceil(item.get, accuracy))
      } else {
        None
      }
    })
  }

  def ceil(number: Double, accuracy: Int): Double = {
    if (number.isNaN) {
      number
    } else {
      BigDecimal(number).setScale(accuracy, BigDecimal.RoundingMode.HALF_UP).toDouble
    }
  }
}
