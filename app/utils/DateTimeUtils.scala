package utils

import java.text.SimpleDateFormat

object DateTimeUtils {
  val sdf: SimpleDateFormat = {
    sys.props("os.name").toLowerCase match {
      case x if x contains "windows" => new SimpleDateFormat("dd_M_yyyy___hh_mm_ss")
      case _ => new SimpleDateFormat("dd-M-yyyy hh:mm:ss")
    }
  }
  val oneMoreSdf = new SimpleDateFormat("dd-M-yyyy_hh:mm:ss")
}
