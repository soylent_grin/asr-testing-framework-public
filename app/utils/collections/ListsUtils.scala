package utils.collections

object ListsUtils {
  def getListsIntersection[A](firstList: List[A], secondList: List[A]): List[A] = {
    firstList.toSet.intersect(secondList.toSet).toList
  }

  def getListsUnion[A](lists: List[A]*): List[A] = lists.reduce((firstList, secondList) => firstList.toSet.union(secondList.toSet).toList)

  def isSublist[A](list: List[A], lists: List[List[A]]): Boolean ={
    var globalResult = false
    for (refList <- lists){
      var result = true
      for (item <- list){
        if (!(refList contains item)){
          result = false
        }
      }
      if (result){
        globalResult = true
      }
    }
    globalResult
  }

  def areIntersected[A](listOne: List[A], listTwo: List[A]): Boolean =
    if (listOne.isEmpty || listTwo.isEmpty) {
      false
    } else {
      listOne.map(item => listTwo contains item).reduceLeft(_ || _)
    }
}
