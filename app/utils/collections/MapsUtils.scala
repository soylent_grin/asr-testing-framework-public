package utils.collections

object MapsUtils {
  def sumMaps[K](mapOne: Map[K, Double], mapTwo: Map[K, Double]): Map[K, Double] = {
    for ((k, v) <- mapOne ++ mapTwo) yield k -> (if ((mapOne contains k) && (mapTwo contains k)) mapOne(k) + v else v)
  }
}
