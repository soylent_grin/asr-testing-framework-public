package utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
    public static File zipFiles(List<File> files, String filename) {
        File zipfile = new File(filename);
        // Create a buffer for reading the files
        byte[] buf = new byte[1024];
        try {
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
            // compress the files
            for(int i=0; i<files.size(); i++) {
                FileInputStream in = new FileInputStream(files.get(i).getCanonicalPath());
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(files.get(i).getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            }
            // complete the ZIP file
            out.close();
            return zipfile;
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void zipDirectory(Path source, OutputStream to) throws IOException {
        byte[] buffer = new byte[65536];

        try (ZipOutputStream zipStream = new ZipOutputStream(to)) {
            String sourceDirAbsolutePath = source.toAbsolutePath().toString();
            Files.walk(source)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        String relativePath = path.toAbsolutePath().toString()
                                .replace(sourceDirAbsolutePath, "")
                                .replace("\\", "/");
                        ZipEntry zipEntry = new ZipEntry(relativePath);
                        try {
                            zipStream.putNextEntry(zipEntry);
                            try (FileInputStream fis = new FileInputStream(path.toAbsolutePath().toFile())) {
                                int len = fis.read(buffer);
                                while (len > 0) {
                                    zipStream.write(buffer, 0, len);
                                    len = fis.read(buffer);
                                }
                            }
                            zipStream.closeEntry();
                        } catch (Exception e) {
                            System.err.println(e);
                        }
                    });
        }
    }

    public static void unzipDirectory(InputStream source, Path to) throws IOException {
        byte[] buffer = new byte[65536];

        ZipInputStream zis = new ZipInputStream(source);
        ZipEntry ze = zis.getNextEntry();
        while (ze != null) {
            if (!ze.isDirectory()) {
                String fileName = ze.getName();
                File newFile = new File(to.toAbsolutePath() + File.separator + fileName);
                new File(newFile.getParent()).mkdirs();
                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    int len = zis.read(buffer);
                    while (len > 0) {
                        fos.write(buffer, 0, len);
                        len = zis.read(buffer);
                    }
                }
            }
            ze = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }

    public static String readZipEntryText(InputStream zipStream, String entryPath) throws IOException {
        ZipInputStream zis = new ZipInputStream(zipStream);
        try {
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                if (!ze.isDirectory()) {
                    if (entryPath.equals(ze.getName())) {
                        StringBuilder out = new StringBuilder();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(zis));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            out.append(line);
                            out.append(System.lineSeparator());
                        }
                        return out.toString();
                    }
                }
                ze = zis.getNextEntry();
            }
        } finally {
            zis.closeEntry();
            zis.close();
        }
        return null;
    }
}
