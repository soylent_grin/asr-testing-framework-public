package utils

import com.mohiva.play.silhouette.api.LoginInfo
import models.User

object UserUtils {

  def makeUserId(loginInfo: LoginInfo): String = s"${loginInfo.providerID}-${loginInfo.providerKey}"

  def makeCredentialsUserId(username: String): String = s"credentials-$username"

}

case class UserContext(user: User)
