package utils

import java.io.File

case class AsrStepExecutionException(log: File) extends Exception

class AsrExecutionException extends Exception

case class AsrStepTerminationException(log: Option[File] = None) extends Exception

class AsrTerminationException extends Exception

