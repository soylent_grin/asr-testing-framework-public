package utils

import scala.concurrent.{ExecutionContext, Future}

object ScalaUtils {

  def chainFutures(list: List[() => Future[_]])
                  (implicit ec: ExecutionContext): Future[_] = {
    list match {
      case Nil => Future.successful(None)
      case x :: Nil =>
        x()
      case x :: xs => x() flatMap (_ =>  {
        chainFutures(xs)
      })
    }
  }

}
