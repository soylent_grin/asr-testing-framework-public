package extensions.executors

import java.nio.file.Path
import java.util.concurrent.atomic.AtomicBoolean

import dtos.AsrExecutionResultDto
import models.SoundbankSample
import play.api.Logger
import play.api.libs.json.{JsValue, Reads}
import play.api.libs.ws.{WSAuthScheme, WSClient, WSRequest, WSResponse}
import utils.{AsrStepTerminationException, ScalaUtils}

import scala.collection.concurrent.TrieMap
import scala.concurrent.{ExecutionContext, Future}

abstract class CloudAsrExecutor extends AsrExecutor {

  protected val ws: WSClient

  protected val isTerminated: AtomicBoolean = new AtomicBoolean(false)

  protected def getValueFromConfig[T](config: Map[String, JsValue],
                                      key: String)
                                     (implicit reads: Reads[T]): T = {
    config.get(key).flatMap(_.asOpt[T]).getOrElse(
      throwAndLogException[T](new IllegalArgumentException(s"not found $key"))
    )
  }

  protected def throwAndLogException[T](t: Throwable): T = {
    log(s"ERROR: ${t.getMessage}")
    throw t
  }

  protected def performRequest(sample: SoundbankSample,
                               config: Map[String, JsValue])
                              (implicit ec: ExecutionContext): Future[String]

  override def runTestAsr(pathToModel: Option[Path],
                          samples: Seq[SoundbankSample],
                          config: Map[String, JsValue])
                         (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = {
    val results = TrieMap[String, Option[String]]()
    val futureCreators = samples.map(s => {
      () => {
        if (isTerminated.get()) {
          log(s"job terminated; do nothing with sample ${s.id}")
          Future.successful(None)
        } else {
          log(s"processing sample ${s.id}")
          try {
            performRequest(s, config)
              .map(res => {
                log(s"received transcription for sample ${s.id}: $res")
                results.put(s.id.get, Some(res))
              })
              .recover {
                case t: Throwable =>
                  throwAndLogException(t)
              }
          } catch {
            case t: Throwable =>
              throwAndLogException(t)
          }
        }
      }
    })
    ScalaUtils.chainFutures(futureCreators.toList).map(_ => {
      if (isTerminated.get()) {
        log(s"all samples done, but job was terminated")
        throw AsrStepTerminationException()
      }
      log(s"all samples done")
      AsrExecutionResultDto(
        Map[String, String](),
        Some(
          results.filter(_._2.isDefined).map(r => {
            (r._1, r._2.get)
          }).toMap,
        ),
        Some(getLogFile)
      )
    })
  }

  override def terminate: Future[_] = {
    log(s"terminating current ASR; setting terminated flag to true")
    isTerminated.set(true)
    Future.successful(None)
  }

}
