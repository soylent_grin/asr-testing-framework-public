package extensions.executors

import java.io.File
import java.nio.file.{Files, Path}
import java.util.concurrent.atomic.AtomicBoolean

import com.google.inject.Inject
import com.typesafe.config.Config
import dtos.AsrExecutionResultDto
import models.SoundbankSample
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.json.{JsBoolean, JsNumber, JsValue}
import repositories.SoundbankRepository
import services.metrics.AsrMetricService
import utils.AsrStepTerminationException
import utils.assets.FileUtils

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import scala.sys.process.{Process, ProcessLogger}
import scala.util.Random

@AsrExecutorInfo(title = "Base example of ASR Executor", key = "reference", canTrain = true)
class ReferenceAsrExecutor @Inject()(metricService: AsrMetricService,
                                     soundbankRepository: SoundbankRepository,
                                     globalConfig: Config) extends AsrExecutor {

  private lazy val logger = Logger(getClass)

  private var process: Process = null

  private val terminated = new AtomicBoolean(false)

  private val processLogger = ProcessLogger(
    (o: String) => {
      log(o)
    },
    (e: String) => {
      log(e)
    }
  )

  override def runTestAsr(pathToModel: Option[Path],
                          samples: Seq[SoundbankSample],
                          config: Map[String, JsValue])
                         (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = {
    log(s"running test of reference ASR in Future on ${samples.size} samples; model is $pathToModel")
    Future {
      if (pathToModel.isDefined) {
        log(s"lets show content of model files...")
        listModelDirectory(pathToModel.get)
      }
      log(s"done; let's launch fake process")
      process = launchMockLongRunningProcess(config)
      if (config.get("shouldFail").flatMap(_.asOpt[Boolean]).getOrElse(false)) {
        throw new RuntimeException(s"this is mock exception to check job error status")
      }
      val exitValue = process.exitValue()
      handleExitCode(exitValue)
      logger.info(s"finally, deleting prepared dataset")
      // FileUtils.deleteRecursively(dataset)
      //
      // Now there are two options
      //
      // Take only valid samples (throws another error later in AsrExecutorActor):
      //
      //val samplesWithExplicitTranscription = samples.filter(s => (s.meta \ "transcription-from-asr").isDefined)
      //logger.info(f"Taking only samples with explicitly defined transcription in meta: ${
      //  samplesWithExplicitTranscription.map(sample => sample.id.get).mkString(", ")
      //}")
      //
      // Take all samples, if there are some invalid, throw an error
      //
      val invalidSamplesIndices = ListBuffer[String]()
      samples.foreach(sample => {
       if ((sample.meta \ "transcription-from-asr").isEmpty) {
         invalidSamplesIndices += sample.id.getOrElse("-")
       }
      })
      if (invalidSamplesIndices.nonEmpty){
        val message = f"Can't start metrics calculation because these samples have no field meta.transcription-from-asr: ${
          invalidSamplesIndices.mkString(", ")
        }"
        logger.error(message)
        throw new IllegalArgumentException(message)
      }
      getMockTestResult(samples, samples.map(s => (s.meta \ "transcription-from-asr").as[List[String]]))
    }
  }

  override def runTrainAsr(samples: Seq[SoundbankSample],
                           config: Map[String, JsValue])
                          (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = {
    log(s"running train of reference ASR in Future on ${samples.size} samples")
    Future {
      log(s"let's launch fake process")
      process = launchMockLongRunningProcess(config)
      val exitValue = process.exitValue()
      handleExitCode(exitValue)
      getMockTrainResult()
    }
  }

  override def terminate: Future[_] = {
    logger.info(s"terminating current ASR - stopping process")
    if (process != null) {
      terminated.set(true)
      process.destroy()
    }
    Future.successful(None)
  }

  // ---

  private def listModelDirectory(path: Path): Unit = {
    if (!Files.isDirectory(path)) {
      throw new IllegalArgumentException(s"model path is not a directory; wtf?")
    }
    path.toFile.listFiles.foreach(f => {
      log(s"file: ${f.getAbsolutePath}, content:")
      val source = Source.fromFile(f)
      try {
        source.getLines.foreach(l => {
          log(l)
        })
      } finally {
        source.close()
      }
    })
  }

  private def getMockTestResult(samples: Seq[SoundbankSample], transcriptions: Seq[List[String]]): AsrExecutionResultDto = {
    AsrExecutionResultDto(
      Map(),
      Some(
        samples.zipWithIndex.map(pair => {
          (pair._1.id.get, transcriptions(pair._2).mkString(" "))
        }).toMap,
      ),
      Some(getLogFile)
    )
  }
  private def getMockTrainResult(): AsrExecutionResultDto = {
    log(s"let's prepare some mock model files")
    val first = Files.createTempFile("referenceAsrMockTrainedModel", DateTime.now().getMillis.toString + ".bin")
    FileUtils.write(first.toAbsolutePath.toString, DateTime.now().toString().getBytes)
    val second = Files.createTempFile("referenceAsrMockTrainedModel", DateTime.now().getMillis.toString + ".data")
    FileUtils.write(second.toAbsolutePath.toString, DateTime.now().toString().getBytes)
    AsrExecutionResultDto(
      Map(),
      None,
      Some(getLogFile),
      Some(List(first, second))
    )
  }

  private def launchMockLongRunningProcess(config: Map[String, JsValue]): Process = {
    val shouldSimulateLongRunningProcess = true
    val a = config.get("duration")
    val b = a.flatMap(_.asOpt[Long])
    val duration = b.getOrElse(5)
    log(s"Running process for $duration seconds")
    val command = sys.props("os.name").toLowerCase match {
      case x if x contains "windows" => Seq(
        "wsl",
        "bash",
        "-c",
        if (shouldSimulateLongRunningProcess)
        "for i in 0 1 0 0; do echo '$(date)'; sleep 1; done" else "echo performing complex computations..."
      )
      case _ => Seq(
        "bash",
        "-c",
        if (shouldSimulateLongRunningProcess)
        s"for i in {1..$duration}; do    echo running...;    sleep 1; done" else "echo performing complex computations..."
      )
    }
    log(s"Executing command: $command")
    Process(command).run(processLogger)
  }

  private def handleExitCode(exitValue: Int): Unit = {
    if(exitValue != 0) {
      if (terminated.get()) {
        throw AsrStepTerminationException()
      } else {
        throw new IllegalStateException(s"process execution error")
      }
    }
  }

  private def prepareAndLogSamples(samples: Seq[SoundbankSample], datasetType: String): File = {
    logger.info(s"let's convert samples to $datasetType dataset format and log it...")
    val resultFolder = soundbankRepository.samplesToDataset(samples, datasetType)
    logger.info(s"done; listing result directory ${resultFolder.getAbsolutePath}...")
    FileUtils.printDirectoryTree(resultFolder)
    resultFolder
  }
}
