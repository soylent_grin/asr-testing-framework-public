
package extensions.executors

import java.nio.file.Files
import java.time.{Clock, Instant}
import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.grpc.internal.JavaUnaryRequestBuilder
import com.google.inject.Inject
import com.mohiva.play.silhouette.api.crypto.Base64
import com.typesafe.config.Config
import models.SoundbankSample
import pdi.jwt.{Jwt, JwtAlgorithm}
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSClient
import repositories.SoundbankRepository
import tinkoff.cloud.stt.v1._

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

@AsrExecutorInfo(title = "Tinkoff VoiceKit", key = "tinkoff")
class TinkoffAsrExecutor @Inject()(soundbankRepository: SoundbankRepository,
                                           val ws: WSClient,
                                            implicit val system: ActorSystem,
                                           config: Config) extends CloudAsrExecutor {

  private val TIMEOUT_SEC = 30

  implicit val clock: Clock = Clock.systemUTC

  private val host = "stt.tinkoff.ru"

  private val port = 443

  override protected def performRequest(sample: SoundbankSample,
                                        config: Map[String, JsValue])
                                       (implicit ec: ExecutionContext): Future[String] = {
    import akka.grpc.GrpcClientSettings
    log(s"creating gRPC client to host = $host, port = $port...")
    val clientSettings = GrpcClientSettings.connectToServiceAt(host, port)
    val client = SpeechToTextClient.create(clientSettings, system)
    log(s"done; calculating JWT token...")
    val token = getJwtToken(config)
    log(s"done; preparing audio and config...")
    val fileContentRaw = Files.readAllBytes(sample.audioFile.toPath)
    val audio: RecognitionAudio = RecognitionAudio.newBuilder()
      .setContent(com.google.protobuf.ByteString.copyFrom(fileContentRaw))
      .build()
    val recognitionConfig: RecognitionConfig = RecognitionConfig.newBuilder()
        .setEncoding(AudioEncoding.LINEAR16)
        .setSampleRateHertz(sample.sampleRate.toInt)
        .setMaxAlternatives(1)
        .setEnableAutomaticPunctuation(false)
        .setNumChannels(1)
        .build()
    log(s"done; building request...")
    val req = RecognizeRequest.newBuilder()
        .setAudio(audio)
        .setConfig(recognitionConfig)
        .build()
    val request = client.recognize()
    log(s"setting Bearer token header")
    val as = request.addHeader("authorization", s"Bearer $token").asInstanceOf[JavaUnaryRequestBuilder[RecognizeRequest, RecognizeResponse]]
    log(s"done; sending request...")
    val response = as.invoke(req).toCompletableFuture.get(TIMEOUT_SEC, TimeUnit.SECONDS)
    log(s"done; received result; parsing head transcription")
    val head = response.getResultsList.asScala.head
    val transcription = head.getAlternativesList.asScala.head.getTranscript
    log(s"confidence is ${head.getAlternativesList.asScala.head.getConfidence}")
    Future.successful(transcription)
  }

  private def getJwtToken(config: Map[String, JsValue]): String = {
    val apiKey = getValueFromConfig[String](config, "apiKey")
    val secretKey = getValueFromConfig[String](config, "secretKey")
    val header = Json.obj(
      "alg" -> "HS256",
      "typ" ->  "JWT",
      "kid" -> apiKey
    )
    val tokenId = UUID.randomUUID()
    val claim = Json.obj(
      "iss" -> "tinkoff_mobile_bank_api",
      "sub" -> "cvoaf",
      "aud" -> "tinkoff.cloud.stt",
      "exp" -> ((Instant.now().toEpochMilli / 1000) + 600),
      "iat" -> (Instant.now().toEpochMilli / 1000 - 600),
      "nbf" -> (Instant.now().toEpochMilli / 1000 - 600),
      "jti" -> tokenId,
      "sid" -> tokenId
    )

    /*

    val headerBytes = Base64.encode(header.toString())
    val claimBytes = Base64.encode(claim.toString())

    val data = s"${headerBytes.split("=").head}.${claimBytes.split("=").head}"

    import javax.crypto.Mac
    import javax.crypto.spec.SecretKeySpec
    val sha256_HMAC = Mac.getInstance("HmacSHA256")
    val secret_key = new SecretKeySpec(Base64.decode(secretKey).getBytes("UTF-8"), "HmacSHA256")
    sha256_HMAC.init(secret_key)

    import org.apache.commons.codec.binary.Base64
    val signed = Base64.encodeBase64String(sha256_HMAC.doFinal(data.getBytes("UTF-8")))
    val token = s"$data.${signed.split("=").head}"

    token

     */
    Jwt.encode(
      header.toString(),
      claim.toString(),
      Base64.decode(secretKey),
      JwtAlgorithm.HS256
    )
  }
}