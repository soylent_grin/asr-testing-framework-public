package extensions.executors

import com.google.inject.Inject
import com.neovisionaries.ws.client.{WebSocket, WebSocketAdapter, WebSocketException}
import com.speechpro.cloud.client.ApiResponse
import com.speechpro.cloud.client.model.{SessionDto, WebSocketServerConfiguration}
import com.typesafe.config.Config
import models.SoundbankSample
import play.api.libs.json.JsValue
import play.api.libs.ws.WSClient
import repositories.SoundbankRepository
import java.io.IOException
import java.nio.file.Files

import com.speechpro.cloud.client.{ApiClient, ApiException}
import com.speechpro.cloud.client.api.{RecognizeApi, SessionApi, WebSocketApi}
import com.speechpro.cloud.client.model.{StartSessionRequest, StreamRequestDto}

import scala.concurrent.{ExecutionContext, Future, Promise}

@AsrExecutorInfo(title = "Speech Technology Center", key = "stc")
class StcAsrExecutor @Inject()(soundbankRepository: SoundbankRepository,
                               val ws: WSClient,
                               config: Config) extends CloudAsrExecutor {

  private val timeout = 5000

  override protected def performRequest(sample: SoundbankSample, config: Map[String, JsValue])
                                       (implicit ec: ExecutionContext): Future[String] = {
    val promise: Promise[String] = Promise()
    val apiClient = new ApiClient
    val sessionApi = new SessionApi(apiClient)
    val model = config.get("model").flatMap(_.asOpt[String]).getOrElse(
      "FarField"
    )
    val username = config.get("username").flatMap(_.asOpt[String]).getOrElse(
      throwAndLogException(new RuntimeException(s"username is not provided"))
    )
    val password = config.get("password").flatMap(_.asOpt[String]).getOrElse(
      throwAndLogException(new RuntimeException(s"password is not provided"))
    )
    val domain = config.get("domain").flatMap(_.asOpt[Long]).getOrElse(
      throwAndLogException(new RuntimeException(s"domain is not provided"))
    )
    val mimeType = config.get("mime").flatMap(_.asOpt[String]).getOrElse(
      throwAndLogException(new RuntimeException(s"mime type is not provided"))
    )
    val startSessionRequest = new StartSessionRequest(
      username,
      password,
      domain
    )
    log(s"creating session")
    var sessionDto: SessionDto = null
    try sessionDto = sessionApi.startSession(startSessionRequest)
    catch {
      case e: ApiException =>
        throwAndLogException(e)
    }
    val sessionId = sessionDto.getSessionId
    log(s"session created: sessionId is $sessionId")
    val recognizeApi = new RecognizeApi
    var apiResponse: ApiResponse[WebSocketServerConfiguration] = null
    log(s"mime type: $mimeType")
    log(s"model: $model")
    val streamRequestDto = new StreamRequestDto(model, mimeType)
    log("starting recognition API")
    try {
      apiResponse = recognizeApi.startWithHttpInfo(sessionId, streamRequestDto)
    }
    catch {
      case e: ApiException =>
        throwAndLogException(e)
    }
    log("Web socket URL: " + apiResponse.getData)
    var fileContentRaw = new Array[Byte](0)
    try {
      fileContentRaw = Files.readAllBytes(sample.audioFile.toPath)
    }
    catch {
      case e: IOException =>
        throwAndLogException(e)
    }
    log(s"timeout: $timeout")
    // you should implement methods for different events
    var webSocketApi: WebSocketApi = null
    webSocketApi = new WebSocketApi(apiResponse.getData.getUrl, timeout, new WebSocketAdapter() {
      override def onTextMessage(websocket: WebSocket, message: String): Unit = {
        log(s"received message: $message")
        log(s"done; disconnecting from the client")
        webSocketApi.disconnect()
        promise.success(message)
      }
      override def onConnected(websocket: WebSocket,
                               headers: java.util.Map[String, java.util.List[String]]): Unit = {
        log(s"connected; headers are: $headers")
      }
      override def onError(websocket: WebSocket, cause: WebSocketException): Unit = {
        log(s"error: ${cause.getError}; disconnecting")
        webSocketApi.disconnect()
        promise.failure(cause)
      }
    })
    log(s"connecting to websocket API")
    webSocketApi.connect()
    log(s"sending bytes")
    webSocketApi.sendBytes(fileContentRaw)
    log(s"done; waiting for response...")
    promise.future
  }
}
