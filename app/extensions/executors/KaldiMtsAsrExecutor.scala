package extensions.executors

import java.io.File
import java.io.PrintWriter
import java.nio.file.{Files, Path}
import java.util.concurrent.atomic.AtomicBoolean

import scala.collection.JavaConverters._
import com.google.inject.Inject
import dtos.AsrExecutionResultDto
import models.SoundbankSample
import org.apache.commons.io.{FileUtils => fu}
import play.api.{Configuration, Logger}
import play.api.libs.json.JsValue
import repositories.SoundbankRepository
import utils.{AsrStepTerminationException, assets}

import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process.{Process, ProcessLogger}
import scala.collection.mutable.ListBuffer

@AsrExecutorInfo(title = "KALDI (MTS edition)", key = "kaldi-mts", canTrain = true)
class KaldiMtsAsrExecutor @Inject()(soundbankRepository: SoundbankRepository,
                                    configuration: Configuration,
                                    implicit val ec: ExecutionContext) extends AsrExecutor {

  private val resultTemplate: String = "Recognized text: "

  private var scriptTimeResult: String = _
  private var werResult: String = ""

  private var transcriptionResult = Seq[List[String]]()

  private var warningResult = ListBuffer[String]()
  private var errorResult = ListBuffer[String]()

  private var resFlag: Boolean = false
  private var werFlag: Boolean = false

  private var exitValue: Int = 0

  private lazy val logger = Logger(getClass)
  
  private val scriptBasePath = configuration.get[String]("asr.scriptPath")
  private val tempBasePath = "/cvoaf/temp/" // there is no way to use another path due to hardcoded scripts

  logger.info(s"using script base path = $scriptBasePath; temp base path = $tempBasePath")

  val exceptionWords = List(
    "!SIL", "<SPN>", "<NSN>",
    "<SPN_CLAC>", "<SPN_LAU>", "<SPN_COUGH>", "<SPN_BREATH>",
    "<NSN_SOUND>", "<NSN_NOISE>", "<NSN_MUSIC>", "<NSN_KEYBOARD>",
    "<UNK>", "ПДН", "ПНД", "ЛЕКСИКА", "НЕЦЕНЗУРНАЯ"
  )

  private var process: Process = _

  private val terminated = new AtomicBoolean(false)

  private val processLogger = ProcessLogger(
    (o: String) => {

      if (o.startsWith("Best IVector WER") && !werFlag) {
        werFlag = true
      } else if (
        !o.startsWith("Best IVector WER")
          && werFlag
          && !resFlag &&
          !o.startsWith(resultTemplate)
      ) {
        werResult += o + "\n"
      } else {
        werFlag = false
      }

      if (o.startsWith(resultTemplate) && !resFlag) {
        resFlag = true
      } else if (!o.startsWith(resultTemplate) && resFlag) {
        val res = List(o)
        transcriptionResult = transcriptionResult :+ res
      } else {
        resFlag = false
      }

      if (o.startsWith("Time elapsed: ")) {
        scriptTimeResult = o.substring("Time elapsed: ".length)
      }

      log(o)
    },
    (e: String) => {
      if (e.toLowerCase.contains("warning")) {
        warningResult += e
      }
      if (e.toLowerCase.contains("error") || e.toLowerCase.contains("can't")) {
        errorResult += e
      }
      log(e)
    }
  )

  override def runTestAsr(pathToModel: Option[Path],
                          samples: Seq[SoundbankSample],
                          config: Map[String, JsValue])
                         (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = {
    log(s"running test of reference ASR in Future on ${samples.size} samples; model is $pathToModel")

    val sortedSoundbankSamples = samples.sortBy(_.id.getOrElse(""))

    var expConfigMap: Map[String, String] = Map()
    var decodeConfigMap: Map[String, String] = Map()
    var decodeDnnConfigDnnMap: Map[String, String] = Map()
    var mfccConfigMap: Map[String, String] = Map()
    val dataset = prepareAndLogSoundbankSamples(
      samples,
      config.get("datasetType") match {
        case Some(json) =>
          json.as[String]
        case _ =>
          "voxforge-mts"
      }
    )
    val pathToDataset = dataset.getAbsolutePath
    var pathToDecoder: String = null

    for ((k, v) <- config) {
      if (k.startsWith("decode-config")) {
        decodeConfigMap += (k.replace("decode-config-", "") -> v.as[String])
      } else if (k.startsWith("decode-dnn-config")) {
        decodeDnnConfigDnnMap += (k.replace("decode-dnn-config-", "") -> v.as[String])
      } else if (k.startsWith("mfcc-config")) {
        mfccConfigMap += (k.replace("mfcc-config", "-") -> v.as[String])
      } else if (k.startsWith("exp-config")) {
        expConfigMap += (k.replace("exp-config-", "") -> v.toString())
      } else if (k.contains("njobs")) {
        if (v.as[Int] > samples.size) {
          expConfigMap += (k.replace("exp-config-", "") -> samples.size.toString)
        } else {
          expConfigMap += (k.replace("exp-config-", "") -> v.as[String])
        }
      }
    }

    val decoder = config("decoderType").as[String]

    if (decoder == "mts-ivector-decode") {
      pathToDecoder = s"$scriptBasePath/kaldi/mts-ivector-decode/ivector_decode.sh"

      if (pathToModel.isEmpty) {
        throw new IllegalArgumentException(s"not found model; unable to perform test")
      }
      expConfigMap += ("EXP_ROOT" -> pathToModel.get.toRealPath().toString)

      createDirectoriesIfNotExist(
        s"$tempBasePath/mts-ivector-decode/conf",
        s"$tempBasePath/mts-ivector-decode/dataset",
        s"$tempBasePath/mts-ivector-decode/results"
      )

      printToConfigFile(s"$tempBasePath/mts-ivector-decode/conf/", "exp", expConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-ivector-decode/conf/", "mfcc", mfccConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-ivector-decode/conf/", "decode", decodeConfigMap, "config")

      val dataPrepCommand =
        s"rm -rf $tempBasePath/mts-ivector-decode/dataset && " +
        s"mkdir -p $tempBasePath/mts-ivector-decode/dataset && " +
        s"cp -r ${pathToDataset.replace(":", "\\:")}/* $tempBasePath/mts-ivector-decode/dataset/ &&" +
        s"find $tempBasePath/mts-ivector-decode/dataset -name '*.wav' > $tempBasePath/mts-ivector-decode/dataset/test-list.txt"

      val dataPrepCommandExecution = Seq(
        "bash",
        "-c",
        dataPrepCommand
      )

      log(s"copying temp dataset to mts-ivector-decode/dataset folder")
      val dataPrepProcess = Process(dataPrepCommandExecution).run()
      val exitValueOfDataPrepExecutionExecutor = dataPrepProcess.exitValue()
      handleExitCode(exitValueOfDataPrepExecutionExecutor)

      fu.writeLines(new File(s"$tempBasePath/mts-ivector-decode/dataset/exception_words.txt"), exceptionWords.asJava)
    }

    if (decoder == "mts-fmllr-decode") {

      pathToDecoder = s"$scriptBasePath/kaldi/mts-fmllr-decode/fmllr-dnn-decode.sh"

      expConfigMap += ("EXP_ROOT" -> pathToModel.get.toRealPath().toString.replace(":", "\\:"))

      createDirectoriesIfNotExist(
        s"$tempBasePath/mts-fmllr-decode/conf",
        s"$tempBasePath/mts-fmllr-decode/dataset",
        s"$tempBasePath/mts-fmllr-decode/results"
      )

      printToConfigFile(s"$tempBasePath/mts-fmllr-decode/conf/", "exp", expConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-fmllr-decode/conf/", "mfcc", mfccConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-fmllr-decode/conf/", "decode", decodeConfigMap, "config")
      printToConfigFile(s"$tempBasePath/mts-fmllr-decode/conf/", "decode_dnn", decodeDnnConfigDnnMap, "config")

      val dataPrepCommand =
        s"rm -rf $tempBasePath/mts-fmllr-decode/dataset && " +
        s"mkdir -p $tempBasePath/mts-fmllr-decode/dataset && " +
        s"cp -r ${pathToDataset.replace(":", "\\:")}/* $tempBasePath/mts-fmllr-decode/dataset/ &&" +
        s"find $tempBasePath/mts-fmllr-decode/dataset -name '*.wav' > $tempBasePath/mts-fmllr-decode/dataset/test-list.txt"

      val dataPrepCommandExecution = Seq(
        "bash",
        "-c",
        dataPrepCommand
      )

      log(s"copying temp dataset to mts-fmllr-decode/dataset folder")
      val dataPrepProcess = Process(dataPrepCommandExecution).run()
      val exitValueOfDataPrepExecution = dataPrepProcess.exitValue()
      handleExitCode(exitValueOfDataPrepExecution)

      fu.writeLines(new File(s"$tempBasePath/mts-fmllr-decode/dataset/exception_words.txt"), exceptionWords.asJava)
    }

    Future {

      log(s"Start decoding:")
      // for local running
      //      val command = Seq(
      //        "bash",
      //        "-c",
      //        s"$pathToDecoder"
      //      )
      val dockerCommand = s"$pathToDecoder"
      val command = Seq(
        "docker",
        "exec",
        "-t",
        "kaldi-mts",
        "bash",
        "-c",
        dockerCommand
      )
      process = Process(command).run(processLogger)
      exitValue = process.exitValue()
      handleExitCode(exitValue)

      logger.info(s"finally, deleting prepared dataset")
      assets.FileUtils.deleteRecursively(dataset)

      getTestResult(sortedSoundbankSamples, transcriptionResult)
    }
  }

  override def runTrainAsr(samples: Seq[SoundbankSample],
                           config: Map[String, JsValue])
                          (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = {
    log(s"running train of reference ASR in Future on ${samples.size} samples")

    var decodeConfigMap: Map[String, String] = Map()
    var decodeDnnConfigDnnMap: Map[String, String] = Map()
    var mfccConfigMap: Map[String, String] = Map()
    var expConfigMap: Map[String, String] = Map()
    var nSoundbankSamples: Int = 0
    var pathToTrainScript: String = null

    val dataset = prepareAndLogSoundbankSamples(
      samples,
      config.get("datasetType") match {
        case Some (json) =>
          json.as[String]
        case _ =>
          "voxforge-mts"
      }
    )
    val pathToDataset = dataset.getAbsolutePath

    for ((k, v) <- config) {
      if (k.equals("nSoundbankSamples_test")) {
        if (v.as[Int] > samples.size) {
          nSoundbankSamples = samples.size
        } else {
          nSoundbankSamples = v.as[Int]
        }
      }

      else if (k.startsWith("decode-config")) {
        decodeConfigMap += (k.replace("decode-config-", "") -> v.as[String])
      }

      else if (k.startsWith("decode-dnn-config")) {
        decodeDnnConfigDnnMap += (k.replace("decode-dnn-config-", "") -> v.as[String])
      } else if (k.startsWith("mfcc-config")) {
        mfccConfigMap += (k.replace("mfcc-config", "-") -> v.as[String])
      } else if (k.startsWith("exp-config")) {
        expConfigMap += (k.replace("exp-config-", "") -> v.toString)
      } else if (k.contains("njobs")) {
        if (v.as[Int] > samples.size) {
          expConfigMap += (k.replace("exp-config-", "") -> samples.size.toString)
        } else {
          expConfigMap += (k.replace("exp-config-", "") -> v.as[String])
        }
      }

      else if (k.contains("mono_subset")) {
        if (v.as[Int] > samples.size) {
          expConfigMap += (k.replace("exp-config-", "") -> samples.size.toString)
        } else {
          expConfigMap += (k.replace("exp-config-", "") -> v.as[String])
        }
      }
    }

    val trainScript = config("trainScript").as[String]

    if (trainScript == "mts-fmllr-train") {
      pathToTrainScript = s"$scriptBasePath/kaldi/mts-fmllr-train/run_fmllr_jp.sh"

      expConfigMap += "test_list" -> s"$tempBasePath/mts-fmllr-train/train_test_lists/test-list.txt"
      expConfigMap += "train_list" -> s"$tempBasePath/mts-fmllr-train/train_test_lists/train-list.txt"
      expConfigMap += "EXP_ROOT" -> s"$tempBasePath/mts-fmllr-train/results"
      expConfigMap += "DATA_ROOT" -> s"$tempBasePath/mts-fmllr-train/dataset"

      createDirectoriesIfNotExist(
        s"$tempBasePath/mts-fmllr-train/conf",
        s"$tempBasePath/mts-fmllr-train/dataset",
        s"$tempBasePath/mts-fmllr-train/results",
        s"$tempBasePath/mts-fmllr-train/train_test_lists"
      )

      printToConfigFile(s"$tempBasePath/mts-fmllr-train/conf/", "exp", expConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-fmllr-train/conf/", "mfcc", mfccConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-fmllr-train/conf/", "decode", decodeConfigMap, "config")
      printToConfigFile(s"$tempBasePath/mts-fmllr-train/conf/", "decode_dnn", decodeDnnConfigDnnMap, "config")

      fu.writeLines(new File(s"$tempBasePath/mts-fmllr-train/train_test_lists/exception_words.txt"), exceptionWords.asJava)

      val dataPrepCommand =
        s"rm -rf $tempBasePath/mts-fmllr-train/dataset && " +
        s"mkdir -p $tempBasePath/mts-fmllr-train/dataset && " +
        s"cp -r ${pathToDataset.replace(":", "\\:")}/* $tempBasePath/mts-fmllr-train/dataset/ &&" +
        s"find $tempBasePath/mts-fmllr-train/dataset -name '*.wav' > $tempBasePath/mts-fmllr-train/train_test_lists/train-list.txt && " +
        s"cat $tempBasePath/mts-fmllr-train/train_test_lists/train-list.txt " +
        s"| shuf -n $nSoundbankSamples > $tempBasePath/mts-fmllr-train/train_test_lists/test-list.txt"

      val dataPrepCommandExecution = Seq(
        "bash",
        "-c",
        dataPrepCommand
      )

      log(s"copying temp dataset to mts-fmllr-train/dataset folder; running command: $dataPrepCommand")
      val dataPrepProcess = Process(dataPrepCommandExecution).run()
      val exitValueOfDataPrepExecution = dataPrepProcess.exitValue()
      handleExitCode(exitValueOfDataPrepExecution)
    }

    if (trainScript == "mts-ivector-train") {

      expConfigMap += "test_list" -> s"$tempBasePath/mts-ivector-train/train_test_lists/test-list.txt"
      expConfigMap += "train_list" -> s"$tempBasePath/mts-ivector-train/train_test_lists/train-list.txt"
      expConfigMap += "EXP_ROOT" -> s"$tempBasePath/mts-ivector-train/results"
      expConfigMap += "DATA_ROOT" -> s"$tempBasePath/mts-ivector-train/dataset"

      pathToTrainScript = s"$scriptBasePath/kaldi/mts-ivector-train/train_asr_ivec.sh"

      createDirectoriesIfNotExist(
        s"$tempBasePath/mts-ivector-train/conf",
        s"$tempBasePath/mts-ivector-train/dataset",
        s"$tempBasePath/mts-ivector-train/results",
        s"$tempBasePath/mts-ivector-train/train_test_lists"
      )

      printToConfigFile(s"$tempBasePath/mts-ivector-train/conf/", "exp", expConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-ivector-train/conf/", "mfcc", mfccConfigMap, "conf")
      printToConfigFile(s"$tempBasePath/mts-ivector-train/conf/", "decode", decodeConfigMap, "config")
      printToConfigFile(s"$tempBasePath/mts-ivector-train/conf/", "decode_dnn", decodeDnnConfigDnnMap, "config")

      fu.writeLines(new File(s"$tempBasePath/mts-ivector-train/train_test_lists/exception_words.txt"), exceptionWords.asJava)

      val dataPrepCommand =
        s"rm -rf $tempBasePath/mts-ivector-train/dataset && " +
          s"mkdir -p $tempBasePath/mts-ivector-train/dataset && " +
          s"cp -r ${pathToDataset.replace(":", "\\:")}/* $tempBasePath/mts-ivector-train/dataset/ &&" +
          s"find $tempBasePath/mts-ivector-train/dataset -name '*.wav' > $tempBasePath/mts-ivector-train/train_test_lists/train-list.txt && " +
          s"cat $tempBasePath/mts-ivector-train/train_test_lists/train-list.txt " +
          s"| shuf -n $nSoundbankSamples > $tempBasePath/mts-ivector-train/train_test_lists/test-list.txt"

      val dataPrepCommandExecution = Seq(
        "bash",
        "-c",
        dataPrepCommand
      )

      log(s"copying temp dataset to mts-ivector-train/dataset folder")
      val dataPrepProcess = Process(dataPrepCommandExecution).run()
      val exitValueOfDataPrepExecution = dataPrepProcess.exitValue()
      handleExitCode(exitValueOfDataPrepExecution)
    }

    Future {
      log(s"Start training:")
      // for local running
      //      val command = Seq(
      //        "bash",
      //        "-c",
      //        s"$pathToTrainScript"
      //      )
      val dockerCommand = s"$pathToTrainScript"
      val command = Seq(
        "docker",
        "exec",
        "-t",
        "kaldi-mts",
        "bash",
        "-c",
        dockerCommand
      )
      process = Process(command).run(processLogger)
      exitValue = process.exitValue()
      handleExitCode(exitValue)

      logger.info(s"finally, deleting prepared dataset")
      assets.FileUtils.deleteRecursively(dataset)

      logger.info(s"moving trained models to models/kaldi dir")
      var trainedModelList = new ListBuffer[Path]()

      if (trainScript == "mts-fmllr-train") {
        val resultModelDir: String = config("exp-config-result_model_dir_name").as[String]
        val trainedModelDir = new File(s"$tempBasePath/mts-fmllr-train/results/$resultModelDir")
        trainedModelDir.listFiles.foreach(model => {
          trainedModelList += model.toPath
        })
      }

      if (trainScript == "mts-ivector-train") {
        val trainedModelDir = new File(s"$tempBasePath/mts-ivector-train/results/models/")
        trainedModelDir.listFiles.foreach(model => {
          trainedModelList += model.toPath
        })
      }

      AsrExecutionResultDto(
        Map(),
        None,
        Some(getLogFile),
        Some(trainedModelList.toList)
      )
    }
  }

  override def terminate: Future[_] = {
    logger.info(s"terminating current ASR - stopping process")
    if (process != null) {
      terminated.set(true)
      process.destroy()
    }
    Future
      .successful(None)
  }

  private def getTestResult(SoundbankSamples: Seq[SoundbankSample], transcriptions: Seq[List[String]]): AsrExecutionResultDto = {
    AsrExecutionResultDto(
      Map("Recognized text" -> transcriptions.toString,
        "KALDI'S WER" -> werResult,
        "Warnings" -> warningResult.toList.toString,
        "Errors" -> errorResult.toList.toString,
        "Script time" -> scriptTimeResult,
        "Exit code" -> exitValue.toString
      ),
      Some(
        SoundbankSamples.zipWithIndex.map(pair => {
          (pair._1.id.get, transcriptions(pair._2).mkString(" "))
        }
        ).toMap
      ),
      Some(getLogFile)
    )
  }

  private def handleExitCode(exitValue: Int): Unit = {
    if (exitValue != 0) {
      if (terminated.get()) {
        throw AsrStepTerminationException()
      } else {
        throw new IllegalStateException(s"process execution error")
      }
    }
  }

  private def prepareAndLogSoundbankSamples(SoundbankSamples: Seq[SoundbankSample], datasetType: String): File = {
    logger.info(s"let's convert SoundbankSamples to $datasetType dataset format and log it...")
    val resultFolder = soundbankRepository.samplesToDataset(SoundbankSamples, datasetType)
    logger.info(s"done; listing result directory ${
      resultFolder.getAbsolutePath
    }...")
    // FileUtils.printDirectoryTree(resultFolder)
    resultFolder
  }

  private def createDirectoriesIfNotExist(directories: String*): Unit = {
    directories.foreach(dir => {
      val directory = new File(dir)
      if (!directory.exists()) {
        directory.mkdirs()
      }
    })
  }

  private def printToConfigFile(appPathToConfig: String, configFileName: String, configMap: Map[String, String], extension: String) = {
    val path = appPathToConfig + configFileName + s".${
      extension
    }"
    new PrintWriter(path) {
      for ((k, v) <- configMap)
        write(k + "=" + v + "\n")
      close()
    }
  }
}