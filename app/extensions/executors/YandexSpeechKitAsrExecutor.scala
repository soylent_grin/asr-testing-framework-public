package extensions.executors

import com.google.inject.Inject
import com.typesafe.config.Config
import models.SoundbankSample
import play.api.libs.json.JsValue
import play.api.libs.ws.WSClient
import repositories.SoundbankRepository

import scala.concurrent.{ExecutionContext, Future}


case class ParamsDto(token: String,
                     folder: String,
                     lang: String,
                     topic: String,
                     profanityFilter: Boolean,
                     format: String,
                     sampleRateHertz: Long) {

  def constructUrl(baseUrl: String) = {
    val params = Seq(
      ("folderId", folder),
      ("lang", lang),
      ("topic", topic),
      ("profanityFilter", profanityFilter),
      ("format", format),
      ("sampleRateHertz", sampleRateHertz)
    )
    s"$baseUrl?${params.map(p => {
      s"${p._1}=${p._2}"
    }).mkString("&")}"
  }

}

@AsrExecutorInfo(title = "Yandex SpeechKit", key = "yandex-speechkit")
class YandexSpeechKitAsrExecutor @Inject()(soundbankRepository: SoundbankRepository,
                                           val ws: WSClient,
                                           appConfig: Config) extends CloudAsrExecutor {

  private val API_ENDPOINT = "https://stt.api.cloud.yandex.net/speech/v1/stt:recognize"

  private val MAX_DURATION = 30
  private val MAX_SIZE = 1024 * 1024

  private def getParams(conf: Map[String, JsValue],
                        sample: SoundbankSample): ParamsDto = {
    ParamsDto(
      conf.get("token").flatMap(_.asOpt[String]).getOrElse(
        throwAndLogException(new RuntimeException(s"token is not provided"))
      ),
      conf.get("folder").flatMap(_.asOpt[String]).getOrElse(
        throwAndLogException(new RuntimeException(s"folder is not provided"))
      ),
      conf.get("lang").flatMap(_.asOpt[String]).getOrElse(
        throwAndLogException(new RuntimeException(s"lang is not provided"))
      ),
      conf.get("topic").flatMap(_.asOpt[String]).getOrElse("general"),
      conf.get("profanityFilter").flatMap(_.asOpt[Boolean]).getOrElse(false),
      "lpcm",
      sample.sampleRate.toLong,
    )
  }

  override protected def performRequest(sample: SoundbankSample, config: Map[String, JsValue])
                                       (implicit ec: ExecutionContext): Future[String] = {
    if (sample.duration > MAX_DURATION || sample.audioFile.length() > MAX_SIZE) {
      recognizeLongFile(sample, config)
    } else {
      recognizeShortFile(sample, config)
    }
  }

  private def recognizeLongFile(sample: SoundbankSample, config: Map[String, JsValue])
                               (implicit ec: ExecutionContext): Future[String] = {
    throwAndLogException(new RuntimeException(s"long file recognition is not implemented; try to use files less than " +
      s"$MAX_DURATION seconds and $MAX_SIZE bytes"))
  }

  private def recognizeShortFile(sample: SoundbankSample, config: Map[String, JsValue])
                               (implicit ec: ExecutionContext): Future[String] = {
    val pathToSample = sample.audioFile.toPath
    val params = getParams(config, sample)
    val url = params.constructUrl(API_ENDPOINT)
    log(s"result URL: $url; sending request...")
    ws.url(url)
      .withHttpHeaders(
        ("Authorization", s"Bearer ${params.token}")
      )
      .post(pathToSample.toFile)
      .map(res => {
        log(s"got response: ${res.json}")
        (res.json \ "result").asOpt[String] match {
          case Some(str) =>
            log(s"parsed result: $str")
            str
          case _ =>
            throw new RuntimeException(s"failed to recognize; invalid response")
        }
      })
  }
}
