package extensions.executors

import java.nio.file.Path

import dtos.AsrExecutionResultDto
import models.SoundbankSample
import org.joda.time.DateTime
import play.api.libs.json.JsValue

import scala.concurrent.{ExecutionContext, Future}

trait AsrExecutor extends AsrExecutorLogSupport {

  var timeStart = DateTime.now()

  def runTestAsr(pathToModel: Option[Path],
                 samples: Seq[SoundbankSample],
                 config: Map[String, JsValue])
                 (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = ???

  def runTrainAsr(samples: Seq[SoundbankSample],
                  config: Map[String, JsValue])
                  (implicit ec: ExecutionContext): Future[AsrExecutionResultDto] = ???

  def terminate: Future[_]

  // in seconds
  protected def getDuration: Long = DateTime.now().getMillis - timeStart.getMillis
  protected def setTimeStart: Unit = timeStart = DateTime.now()

}