package extensions.executors

import java.nio.file.Path

import com.google.inject.Inject
import com.typesafe.config.Config
import dtos.AsrExecutionResultDto
import models.SoundbankSample
import play.api.Logger
import play.api.libs.json.{JsValue, Json, OFormat}
import play.api.libs.ws.{WSAuthScheme, WSClient}
import repositories.SoundbankRepository

import scala.concurrent.{ExecutionContext, Future}

/*
* {
   "results": [
      {
         "alternatives": [
            {
               "confidence": 0.881,
               "transcript": "his hands gripped the edges of the table "
            }
         ],
         "final": true
      }
   ],
   "result_index": 0
}
* */
case class WatsonAlternative(confidence: Double,
                             transcript: String)

case class WatsonResult(alternatives: List[WatsonAlternative])

case class WatsonResponse(results: List[WatsonResult])

object WatsonJsonBindings {

  implicit val alternativeFormat: OFormat[WatsonAlternative] = Json.format[WatsonAlternative]
  implicit val resultFormat: OFormat[WatsonResult] = Json.format[WatsonResult]
  implicit val responseFormat: OFormat[WatsonResponse] = Json.format[WatsonResponse]

}

@AsrExecutorInfo(title = "Watson ASR", key = "watson")
class WatsonAsrExecutor @Inject()(soundbankRepository: SoundbankRepository,
                                  val ws: WSClient,
                                  config: Config) extends CloudAsrExecutor {

  private val API_ENDPOINT = "https://gateway-syd.watsonplatform.net/speech-to-text/api/v1/recognize"

  import WatsonJsonBindings._

  override protected def performRequest(sample: SoundbankSample, config: Map[String, JsValue])
                                       (implicit ec: ExecutionContext): Future[String] = {
    val pathToSample = sample.audioFile.toPath
    val apiKey = getValueFromConfig[String](config, "apiKey")
    log(s"sending request to $API_ENDPOINT")
    ws.url(API_ENDPOINT)
      .withAuth("apikey", apiKey, WSAuthScheme.BASIC)
      .withHttpHeaders(
        ("Content-Type", "audio/wav")
      )
      .post(pathToSample.toFile)
      .map(res => {
        log(s"received response: ${res.body}; parsing...")
        res.json.asOpt[WatsonResponse] match {
          case Some(r) =>
            val result = r.results.head.alternatives.head.transcript
            log(s"first alternative: $result")
            result
          case _ =>
            throwAndLogException(new IllegalStateException(s"unexpected JSON format"))
        }
      })
  }
}
