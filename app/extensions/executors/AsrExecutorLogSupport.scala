package extensions.executors

import java.io.File
import java.nio.charset.StandardCharsets
import java.util
import java.util.concurrent.LinkedBlockingQueue
import java.util.{Date, UUID}

import org.apache.commons.io.FileUtils
import play.api.Logger
import utils.DateTimeUtils
import scala.collection.JavaConverters._

trait AsrExecutorLogSupport {

  private var logBuffer = new LinkedBlockingQueue[String]()
  private val logFile = File.createTempFile(getClass.getName,UUID.randomUUID().toString)

  private lazy val logger = Logger(getClass)

  private val bufferMaxSize = 1000

  protected def log(string: String) = {
    logger.debug(string)
    logBuffer.offer(formatString(string))
    if (logBuffer.size() > bufferMaxSize) {
      flushLog()
    }
  }

  def getLogFile: File = {
    if (logBuffer.size() > 0) {
      flushLog()
    }
    logFile
  }

  private def flushLog() = this.synchronized {
    try {
      val buffer = new util.ArrayList[String]()
      logBuffer.drainTo(buffer)
      FileUtils.writeStringToFile(logFile, buffer.asScala.mkString("\n"), StandardCharsets.UTF_8, true)
    } catch {
      case t: Throwable =>
        logger.error(s"failed to write log file; error is: ", t)
      case _ =>
        // do nothing
    }
  }

  private def formatString(str: String): String = {
    s"[${DateTimeUtils.sdf.format(new Date())}] $str"
  }

}
