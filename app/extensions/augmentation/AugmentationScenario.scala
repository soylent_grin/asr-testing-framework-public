package extensions.augmentation

import java.nio.file.{Path, Paths}

import com.typesafe.config.Config
import dtos.AugmentationParamsDto
import models.SoundbankSample
import play.api.Logger
import play.api.libs.json.JsValue

abstract class AugmentationScenario {

  protected var config: Config

  protected implicit lazy val logger = Logger(getClass)

  protected def getResourceDirectory: Path = Paths.get(config.getString("augmentation.resourcePath"))

  logger.info(s"using resource directory: $getResourceDirectory")

  // ---

  // for every audio file in samples, run augmentation scenario
  def run(samples: Seq[SoundbankSample], params: JsValue): Unit

  // ---

}
