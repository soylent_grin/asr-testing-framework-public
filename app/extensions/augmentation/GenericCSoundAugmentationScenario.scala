package extensions.augmentation

import java.nio.file.{Path, Paths}

import com.google.inject.Inject
import com.typesafe.config.Config
import models.SoundbankSample
import play.api.libs.json
import play.api.libs.json.{JsNull, JsValue, Json}
import repositories.NoiseRepository
import services.AudioProcessingService
import services.python.PythonService
import utils.AudioUtils

import scala.concurrent.ExecutionContext
import scala.util.Try

case class ConfigSampleDto(path: String,
                           duration: String,
                           sampleRate: String,
                           maxAmplitude: String,
                           vad: String,
                           backgroundNoise: Option[String],
                           pauseNoise: Option[String],
                           reverbDistance: Option[String],
                           reverbRoomSize: Option[String],
                           reverbLevel: Option[String])

case class ConfigParamsDto(volume: String,
                           speed: String,
                           toneCorrection: String,
                           retriggerProbability: Option[String],
                           pauseNoiseProbability: String,
                           withBackgroundNoise: String,
                           withWhiteNoise: String,
                           whiteNoiseLowFreq: Option[String],
                           whiteNoiseHighFreq: Option[String],
                           reverbMinDistance: Option[String],
                           reverbMaxDistance: Option[String],
                           reverbMinRoomSize: Option[String],
                           reverbMaxRoomSize: Option[String],
                           reverbMinLevel: Option[String],
                           reverbMaxLevel: Option[String],
                           backgroundNoiseGroup: Option[String],
                           pauseNoiseGroup: Option[String])

case class ConfigDto(samples: Seq[ConfigSampleDto],
                     params: ConfigParamsDto)

object ConfigDtoJsonBinding {
  implicit val configSampleDtoJsonFormat = Json.format[ConfigSampleDto]
  implicit val configParamsDtoJsonFormat = Json.format[ConfigParamsDto]
  implicit val configDtoJsonFormat = Json.format[ConfigDto]
}

@AugmentationScenarioInfo(key = "generic", title = "Generic CSound scenario")
class GenericCSoundAugmentationScenario @Inject()(var config: Config,
                                                  noiseRepository: NoiseRepository,
                                                  val pythonService: PythonService,
                                                  implicit val ec: ExecutionContext) extends CSoundAugmentationScenario {

  override protected def getResourceDirectory: Path = Paths.get(super.getResourceDirectory.toString, "generic")

  override protected def getScript: String = Paths.get(getResourceDirectory.toString, "script.py").toString

  import ConfigDtoJsonBinding._

  private val random = scala.util.Random

  override protected def prepareConfig(samples: Seq[SoundbankSample], paramsJson: JsValue): JsValue = {
    paramsJson.validate[ConfigParamsDto] fold (
      invalid = { errors =>
        throw new IllegalArgumentException(s"failed to parse params JSON; errors are: $errors")
      },
      valid = { params =>
        var patchedParams = params
        if (params.pauseNoiseGroup.isEmpty && params.pauseNoiseProbability != "0") {
          patchedParams = patchedParams.copy(pauseNoiseProbability = "0")
        }
        if (params.backgroundNoiseGroup.isEmpty && params.withBackgroundNoise != "-1") {
          patchedParams = patchedParams.copy(withBackgroundNoise = "-1")
        }
        if (params.whiteNoiseHighFreq.isDefined && params.whiteNoiseLowFreq.isDefined) {
          val highFreq = strToInt(params.whiteNoiseHighFreq)
          val lowFreq = strToInt(params.whiteNoiseLowFreq)
          if (lowFreq > highFreq) {
            logger.warn(s"somehow low WN frequency is higher than high freq; this is not good, but patching...")
            patchedParams = patchedParams.copy(whiteNoiseHighFreq = Some(lowFreq.toString))
          }
        }
        val config = ConfigDto(
          samples.map(s => {
            ConfigSampleDto(
              s.audioFile.getAbsolutePath,
              s.duration.toString,
              s.sampleRate.toString,
              prepareMaxAmplitude(s),
              prepareVad(s),
              (patchedParams.backgroundNoiseGroup, patchedParams.withBackgroundNoise) match {
                case (Some(g), flag) if flag != "-1" =>
                  Some(noiseRepository.getRandomSampleFromGroup(g).file.getAbsolutePath)
                case _ =>
                  None
              },
              (patchedParams.pauseNoiseGroup, patchedParams.pauseNoiseProbability) match {
                case (Some(g), p) if p != "0" =>
                  Some(noiseRepository.getRandomSampleFromGroup(g).file.getAbsolutePath)
                case _ =>
                  None
              },
              prepareRandomValueInRange(patchedParams.reverbMinDistance, patchedParams.reverbMaxDistance),
              prepareRandomValueInRange(patchedParams.reverbMinRoomSize, patchedParams.reverbMaxRoomSize),
              prepareRandomValueInRange(patchedParams.reverbMinLevel, patchedParams.reverbMaxLevel)
            )
          }),
          patchedParams
        )
        val resultConfig = json.Json.toJson(config)

        logger.debug(s"config is ready (you can use it in manual script launch): $resultConfig")

        resultConfig
      })
  }

  private def prepareVad(sample: SoundbankSample): String = {
    val command = Seq(
      Paths.get(getResourceDirectory.toString, "vad.py").toString,
      sample.audioFile.getAbsolutePath
    )
    val vad = pythonService.executeScript(command, config.getString("pythonPaths.linux.interpreter"))
    try {
      Json.parse(vad).asOpt[Seq[Seq[Double]]].getOrElse(Nil).flatten.mkString(" ")
    } catch {
      case t: Throwable =>
        logger.warn(s"failed to run vad.py; considering list of pauses as empty; error: ${t.getMessage}")
        ""
    }
  }

  private def prepareMaxAmplitude(sample: SoundbankSample): String = {
    try {
      AudioUtils.getMaxAmplitude(sample.audioFile)
    } catch {
      case t: Throwable =>
        logger.warn(s"failed to calculate max amplitude, error: ${t.getMessage}; returning default 0.7 amplitude")
        "0.7"
    }
  }

  private def prepareRandomValueInRange(min: Option[String], max: Option[String]): Option[String] = {
    val parsedMin = strToInt(min)
    val parsedMax = strToInt(max)
    val result = if (parsedMax < parsedMin) {
      parsedMin
    } else {
      parsedMin + random.nextInt(parsedMax - parsedMin + 1)
    }
    Some((result.toDouble / 100).toString)
  }

  private def strToInt(str: Option[String]): Int = {
    var parsed = str match {
      case Some(d) =>
        Try(d.toInt).toOption.getOrElse(0)
      case _ =>
        0
    }
    if (parsed < 0) {
      parsed = 0
    } else if (parsed > 100) {
      parsed = 100
    }
    parsed
  }

}
