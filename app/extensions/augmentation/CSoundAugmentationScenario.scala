package extensions.augmentation

import java.io.File
import java.nio.file.{Files, Path, Paths}

import com.typesafe.config.Config
import models.SoundbankSample
import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import play.api.libs.json.{JsValue, Json}
import services.python.PythonService

import scala.collection.JavaConverters._
import scala.sys.process.{Process, ProcessLogger}

abstract class CSoundAugmentationScenario extends AugmentationScenario {

  protected var config: Config

  protected val pythonService: PythonService

  protected val processLogger = ProcessLogger(
    (o: String) => {
      logger.debug(o)
    },
    (e: String) => {
      logger.trace(e) // CSound logs all to stderr, so we use `trace` to suppress it by default
    }
  )

  override protected def getResourceDirectory: Path = Paths.get(super.getResourceDirectory.toString, "csound")

  // ---

  override def run(samples: Seq[SoundbankSample], params: JsValue): Unit = {
    logger.debug(s"running on ${samples.size} samples, using script $getScript, at first, preparing config file")
    val confFile = prepareConfigFile(samples, params)
    val command = Seq(
      config.getString("pythonPaths.linux.csoundInterpreter"),
      getScript,
      confFile.getAbsolutePath,
      getResourceDirectory.toString
    )
    logger.info(s"Executing command: $command")

    val start = System.nanoTime()
    val process = Process(command).run(processLogger)
    val exitCode = process.exitValue()
    if (exitCode != 0) {
      throw new IllegalStateException(s"returned non-zero exit code ($exitCode) from python")
    }
    val time = (System.nanoTime() - start).toDouble / (1000 * 1000 * 1000)
    val totalSamplesDuration = samples.map(_.duration).sum
    logger.info(s"done; processing of ${samples.size} with total duration of" +
      s" ${totalSamplesDuration} seconds took $time seconds (real-time factor is ${totalSamplesDuration / time})")
    confFile.delete()
  }

  // ---

  protected def getScript: String = ???

  protected def prepareConfigFile(samples: Seq[SoundbankSample], params: JsValue): File = {
    val file = Files.createTempFile("csound-scenario-config", DateTime.now().getMillis.toString).toFile
    val config = prepareConfig(samples, params)
    FileUtils.writeStringToFile(file, config.toString(), java.nio.charset.StandardCharsets.UTF_8)
    file
  }

  protected def prepareConfig(samples: Seq[SoundbankSample], params: JsValue): JsValue = {
    Json.obj(
      "samples" -> samples.map(s => {
        Json.obj(
          "path" -> s.audioFile.getAbsolutePath,
          "duration" -> s.duration
        )
      }),
      "params" -> params
    )
  }

}
