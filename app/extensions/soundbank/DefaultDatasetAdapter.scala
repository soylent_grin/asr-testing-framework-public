package extensions.soundbank
import java.io.File
import java.nio.file.Files
import java.util.Date

import akka.http.scaladsl.model.DateTime
import dtos.AudioInfoDto
import models.SoundbankSample
import org.apache.commons.io.FileUtils
import play.api.Logger
import play.api.libs.json.Json
import utils.AudioUtils
import utils.DateTimeUtils.sdf

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.io.Source

@DatasetAdapterInfo(key = "default")
class DefaultDatasetAdapter extends DatasetAdapter {

  private val logger = Logger(getClass)

  override def fromFolderToSamples(folder: File): Seq[SoundbankSample] = {
    val begin = System.nanoTime()
    val samples = new ListBuffer[SoundbankSample]()
    folder.listFiles.filter(dd => dd.isDirectory).foreach(s => {
      logger.trace(s"found directory ${s.getAbsolutePath}; trying to process it as sample...")
      try {
        samples += scanSample(s, folder.getName)
      } catch {
        case t: Throwable =>
          logger.error("unexpected error during sample scanning: ", t)
      }
    })
    logger.debug(s"fromFolderToSamples took ${System.nanoTime() - begin}ns")
    samples
  }

  override def fromSamplesToFolder(samples: Seq[SoundbankSample]): File = {
    val begin = System.nanoTime()
    val tempFolder = Files.createTempDirectory(getClass.getName + sdf.format(new Date))
    samples.foreach(s => {
      val samplePath = new File(s"${tempFolder.toFile.getAbsolutePath}/${s.key}").toPath
      Files.createDirectory(samplePath)
      Files.createSymbolicLink(new File(s"${samplePath.toAbsolutePath}/${s.audioFile.getName}").toPath, s.audioFile.toPath)
      val transcriptFile = new File(s"${samplePath.toAbsolutePath}/transcription.txt")
      FileUtils.writeLines(transcriptFile, s.transcription.asJava)
    })
    logger.debug(s"fromFolderToSamples took ${System.nanoTime() - begin}ns")
    tempFolder.toFile
  }

  private def scanSample(d: File, folderName: String): SoundbankSample = {
    val key = d.getName
    var transcript: Option[List[String]] = None
    var audio: Option[File] = None
    var audioInfo: Option[AudioInfoDto] = None
    d.listFiles.foreach(f => {
      val path = f.getAbsolutePath
      if (path.endsWith(".txt")) {
        val source = Source.fromFile(path, "UTF-8")
        try {
          transcript = Some(source.getLines.toList)
        } finally {
          source.close()
        }
      }
      if (path.endsWith(".wav") || path.endsWith(".mp3")) {
        audio = Some(f)
        audioInfo = Some(AudioUtils.getAudioInfo(f))
      }
    })
    if (transcript.isDefined && audio.isDefined) {
      val sample = SoundbankSample(
        Option(s"$folderName/$key"),
        key,
        transcript.get,
        audioInfo.get.length,
        audioInfo.get.sampleRate,
        audio.get,
        tryLoadMeta("", d.getName, key, d.getAbsolutePath)
      )
      logger.debug(s"registered audio sample for key $key (${audioInfo.get}) " +
        s"with ${transcript.get.size} lines of transcription")
      sample
    } else {
      throw new IllegalStateException(s"failed to parse sample $key; audio is $audio, " +
        s"transcript is $transcript, info is $audioInfo")
    }
  }
}
