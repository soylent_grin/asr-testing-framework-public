package extensions.soundbank
import java.io.File
import java.nio.file.Files
import java.util.Date

import akka.http.scaladsl.model.DateTime
import dtos.AudioInfoDto
import models.SoundbankSample
import org.apache.commons.io.FileUtils
import play.api.Logger
import play.api.libs.json.Json
import utils.AudioUtils
import utils.DateTimeUtils.sdf

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.io.Source

@DatasetAdapterInfo(key = "mts-list")
class MtsListDatasetAdapter extends DatasetAdapter {

    private val logger = Logger(getClass)

    private var folder_ = "";
    private val dataListFileName = "data_list.txt"

    override def fromFolderToSamples(folder: File): Seq[SoundbankSample] = {
        folder_ = folder.getAbsolutePath
        val samples = new ListBuffer[SoundbankSample]()
        val dataListFile = Source.fromFile(folder.getAbsolutePath + "/" + dataListFileName, "UTF-8")
        val filesList = dataListFile.getLines.toList
        filesList.foreach(f => {
            logger.info(s"Process file: ${f}")
            try {
                var curFile = new File(f)
                /* May be in future - will process relative paths
                if (!curFile.exists) {
                    val fRel = folder.getAbsolutePath + "/" + f
                    curFile = new File(fRel)
                }
                */
                samples += processWavFile(curFile);
                logger.info(s"Add file: ${f}")
            } catch {
                // empty file error processing
                case eofError: java.io.EOFException => {
                    logger.error(s"${eofError} in file: ${f}")
                }
                // no file error processing
                case notFound: java.io.FileNotFoundException => {
                    logger.error(s"${notFound} in file: ${f}")
                }
            }
        })

        return samples
    }

    def processWavFile(wavFile: File) : SoundbankSample = {
        var audio = Some(wavFile)
        var audioInfo: Option[AudioInfoDto] = Some(AudioUtils.getAudioInfo(wavFile))
        var transcript: Option [ List[ String ]] = None

        val absPath = wavFile.getAbsolutePath

        logger.info(s"found  file ${wavFile.getAbsolutePath}; trying to process it as sample...")
        val suffix = ".orig"
        val transcriptPath = absPath + suffix
        val transcriptFile = new File(transcriptPath)

        // try to get transcription from ".orig" file
        if (transcriptFile.exists) {
            val source =  Source.fromFile(transcriptFile, "UTF-8")
            try {
                transcript = Some(source.getLines.toList)
            } finally {
                source.close()
            }
        } else {
            logger.info(s"Can't find file: ${transcriptPath}")
        }

        // key - file name
        val key = wavFile.getName

        // try to create sample
        if(transcript.isDefined && audio.isDefined) {
            val sample = SoundbankSample (
                Option(s"$key"),
                key,
                transcript.get,
                audioInfo.get.length,
                audioInfo.get.sampleRate,
                audio.get,
                tryLoadMeta("", wavFile.getName, key, wavFile.getAbsolutePath)
            )
            return sample
        } else {
            throw new IllegalStateException ( s"Failed to parse sample $key ; audio is $audio, " +
                 s"transcript is $transcript , info is $audioInfo")
        }
    }

    override def fromSamplesToFolder(samples: Seq[SoundbankSample]): File = {
        // just stub for method
        val tempFolder = Files.createTempDirectory(getClass.getName + sdf.format( new Date ))
        return tempFolder.toFile
    }
}
