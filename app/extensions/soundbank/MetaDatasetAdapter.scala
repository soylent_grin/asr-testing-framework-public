package extensions.soundbank
import java.io.File
import java.nio.file.Files

import dtos.AudioInfoDto
import models.SoundbankSample
import org.apache.commons.io.FileUtils
import play.api.Logger
import utils.AudioUtils
import java.util.Date

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import utils.DateTimeUtils._

@DatasetAdapterInfo(key = "meta")
class MetaDatasetAdapter extends DatasetAdapter {

  private val logger = Logger(getClass)

  override def fromFolderToSamples(folder: File): Seq[SoundbankSample] = {
    val begin = System.nanoTime()
    val samples = new ListBuffer[SoundbankSample]()
    folder.listFiles.filter(dd => dd.isDirectory).foreach(s => {
      logger.trace(s"found directory ${s.getAbsolutePath}; trying to process it as sample...")
      try {
        samples += scanSample(s, folder.getName)
      } catch {
        case t: Throwable =>
          logger.error("unexpected error during sample scanning: ", t)
      }
    })
    logger.debug(s"fromFolderToSamples took ${System.nanoTime() - begin}ns")
    samples
  }

  override def fromSamplesToFolder(samples: Seq[SoundbankSample]): File = {
    val begin = System.nanoTime()
    val dirName = Array[String](getClass.getName, sdf.format(new Date)).mkString("_")
    val tempFolder = Files.createTempDirectory(dirName)
    samples.foreach(s => {
      val samplePath = new File(s"${tempFolder.toFile.getAbsolutePath}/${s.key}").toPath
      Files.createDirectory(samplePath)
      Files.createSymbolicLink(new File(s"${samplePath.toAbsolutePath}/${s.audioFile.getName}").toPath, s.audioFile.toPath)
      val transcriptFile = new File(s"${samplePath.toAbsolutePath}/transcription.txt")
      FileUtils.writeLines(transcriptFile, s.transcription.asJava)
    })
    logger.debug(s"fromFolderToSamples took ${System.nanoTime() - begin}ns")
    tempFolder.toFile
  }

  private def scanSample(d: File, folderName: String): SoundbankSample = {
    val key = d.getName
    var audio: Option[File] = None
    var audioInfo: Option[AudioInfoDto] = None
    var audioFileName: Option[String] = None
    d.listFiles.foreach(f => {
      val path = f.getAbsolutePath
      if (path.endsWith(".wav") || path.endsWith(".mp3")) {
        audio = Some(f)
        audioInfo = Some(AudioUtils.getAudioInfo(f))
        audioFileName = Some(path.split("/").last.split("\\.")(0))
      }
    })
    if (audio.isDefined) {
      val meta = tryLoadMeta("", d.getName, audioFileName.getOrElse(key), d.getAbsolutePath)
      val ref =
        try {
          (meta \ "ref").asOpt[List[String]]
        } catch {
          case e: NoSuchMethodError => {
            logger.error("Can't even try to extract 'ref' from sample.meta")
            logger.error(e.getStackTrace.mkString("\n"))
            None
          }
        }

      if (ref.isEmpty) throw new IllegalStateException(f"Not found property ref inside meta of sample $key")
      val sample = SoundbankSample(
        Option(s"$folderName/$key"),
        key,
        ref.get,
        audioInfo.get.length,
        audioInfo.get.sampleRate,
        audio.get,
        meta
      )
      logger.debug(s"registered audio sample for key $key (${audioInfo.get}) " +
        s"with ${sample.transcription.size} lines of transcription")
      sample
    } else {
      throw new IllegalStateException(s"failed to parse sample $key; audio is $audio, info is $audioInfo")
    }
  }
}
