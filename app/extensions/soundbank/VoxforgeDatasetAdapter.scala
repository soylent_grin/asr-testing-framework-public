package extensions.soundbank
import java.io.File
import java.nio.file.Files

import akka.http.scaladsl.model.DateTime
import models.SoundbankSample
import org.apache.commons.io.{FileUtils, FilenameUtils}
import play.api.Logger
import play.api.libs.json.Json
import utils.AudioUtils

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.io.Source

@DatasetAdapterInfo(key = "voxforge")
class VoxforgeDatasetAdapter extends DatasetAdapter {

  private val logger = Logger(getClass)

  override def fromFolderToSamples(folder: File): Seq[SoundbankSample] = {
    val begin = System.nanoTime()
    val samples = new ListBuffer[SoundbankSample]()
    folder.listFiles.filter(dd => dd.isDirectory).foreach(s => {
      logger.trace(s"found directory ${s.getAbsolutePath}; trying to process it as sample list...")
      try {
        samples ++= scanSamples(s, folder.getName)
      } catch {
        case t: Throwable =>
          logger.error(s"unexpected error during scanning of folder ${s.getName}; ignoring: ${t.getMessage}")
      }
    })
    logger.debug(s"fromFolderToSamples took ${System.nanoTime() - begin}ns")
    samples
  }

  override def fromSamplesToFolder(samples: Seq[SoundbankSample]): File = {
    val begin = System.nanoTime()
    val tempFolder = Files.createTempDirectory(getClass.getName + DateTime.now.toString())
    val sampleGroups = samples.groupBy(s => {
      (s.meta \ "subfolder").as[String]
    })
    sampleGroups.foreach(group => {
      val samplePath = new File(s"${tempFolder.toFile.getAbsolutePath}/${group._1}").toPath
      Files.createDirectory(samplePath)
      val wavPath = new File(s"${samplePath.toFile.getAbsolutePath}/wav").toPath
      Files.createDirectory(wavPath)
      val transcriptionPath = new File(s"${samplePath.toFile.getAbsolutePath}/etc").toPath
      Files.createDirectory(transcriptionPath)
      val transcriptionFile = new File(s"${samplePath.toFile.getAbsolutePath}/etc/PROMPTS")
      transcriptionFile.createNewFile()
      val readmeFile = new File(s"${samplePath.toFile.getAbsolutePath}/etc/README")
      readmeFile.createNewFile()
      val lines = ListBuffer[String]()
      group._2.foreach(s => {
        val fileName = (s.meta \ "originalFilename").as[String]
        lines +=  s"${group._1}/mfc/${FilenameUtils.removeExtension(fileName)} ${s.transcription.mkString(" ")}"
        Files.createSymbolicLink(new File(s"${wavPath.toFile.getAbsolutePath}/$fileName").toPath, s.audioFile.toPath)
      })
      FileUtils.writeLines(transcriptionFile, "UTF-8", lines.asJava)
      if (samples.nonEmpty) {
        // suppose that models in one group have the same readme
        FileUtils.writeLines(readmeFile, "UTF-8", List((group._2.head.meta \ "readme").as[String]).asJava)
      } else {
        logger.warn(s"somehow sample group is empty; wtf?")
      }
    })
    logger.debug(s"fromFolderToSamples took ${System.nanoTime() - begin}ns")
    tempFolder.toFile
  }

  private def scanSamples(d: File, folderName: String): Seq[SoundbankSample] = {
    val key = d.getName
    val samples = ListBuffer[SoundbankSample]()
    val wavFiles = ListBuffer[File]()
    var transcriptLines: List[String] = null
    var readme: String = null
    d.listFiles.foreach(f => {
      if (f.getName == "wav") {
        wavFiles ++= f.listFiles().toList
      } else if (f.getName == "etc") {
        val transcriptSource = Source.fromFile(s"${f.getAbsolutePath}/PROMPTS", "UTF-8")
        val readmeSource = Source.fromFile(s"${f.getAbsolutePath}/README", "UTF-8")
        try {
          transcriptLines = transcriptSource.getLines.toList
          readme = readmeSource.getLines.mkString("\n")
        } finally {
          transcriptSource.close()
          readmeSource.close()
        }
      }
    })
    if (transcriptLines == null || readme == null) {
      throw new IllegalArgumentException(s"not found transcript or readme file for sample $d")
    }
    transcriptLines.foreach(l => {
      val s = l.split(" ", 2)
      if (s.size == 2) {
        val fileName = s.head.split("/").last
        wavFiles.find(f => f.getName.startsWith(fileName)) match {
          case Some(f) =>
            val audioInfo = AudioUtils.getAudioInfo(f)
            val uniqueName = s"$key-$fileName"
            samples += SoundbankSample(
              Option(s"$folderName/$uniqueName"),
              uniqueName,
              List(s.last),
              audioInfo.length,
              audioInfo.sampleRate,
              f,
              tryLoadMeta(readme, d.getName, f.getName, d.getAbsolutePath)
            )
          case _ =>
            logger.warn(s"not found audio file for name: $fileName, folder ${d.getAbsolutePath}")
          }
      } else {
        logger.warn(s"invalid line, unable to parse: $l, folder ${d.getAbsolutePath}")
      }
    })
    samples.sortBy(_.id.getOrElse(""))
  }

}
