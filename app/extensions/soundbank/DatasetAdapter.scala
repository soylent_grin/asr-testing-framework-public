package extensions.soundbank

import java.io.File
import java.nio.file.{Files, Paths}

import models.SoundbankSample
import play.api.libs.json.{JsObject, JsValue, Json}

import scala.io.Source

trait DatasetAdapter {

  def fromFolderToSamples(folder: File): Seq[SoundbankSample]

  def fromSamplesToFolder(samples: Seq[SoundbankSample]): File

  def tryLoadMeta(readme: String, subfolder: String, originalFilename: String,
                  subfolderAbsolutePath: String): JsValue = {
    val jsonPath = f"$subfolderAbsolutePath/${originalFilename.split("\\.")(0)}.json"
    var meta: Option[JsObject] = None
    if (Files.exists(Paths.get(jsonPath))) {
      val source = Source.fromFile(jsonPath, "UTF-8")
      try {
        meta = Some(Json.parse(source.getLines.mkString(" ")).asInstanceOf[JsObject])
      } finally {
        source.close()
      }
    }

    val defaultResult = Json.parse(f"""
              {
                "readme": "${readme.replace("\n", "\\n")}",
                "subfolder": "$subfolder",
                "originalFilename": "$originalFilename"
              }
              """).asInstanceOf[JsObject]

    if (meta.isEmpty){
      defaultResult
    } else {
      meta.get ++ defaultResult
    }
  }
}
