package factories

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.typesafe.config.Config
import models.SoundbankSample
import models.metrics.Mut
import models.metrics.tokens.{Token, TokenSequence, TokenSequenceMeta}
import play.api.Logger
import play.inject.Injector
import services.metrics.MetricsLogger
import services.metrics.alignment.AlignmentService
import services.metrics.mapping.TokenMappingService
import services.metrics.tokenization.TokenizationService
import utils.metrics.tokens.TokenSequenceFactoryUtils.makeTokens
import utils.metrics.tokens.SampleMetaUtils
import utils.metrics.tokens.TokenUtils.mapToken

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[TokenSequenceFactoryImpl])
trait TokenSequenceFactory {
  def make(sample: SoundbankSample, transcription: String)(implicit logger: MetricsLogger): TokenSequence
  def make(transcription: String)(implicit logger: MetricsLogger): TokenSequence
  def make(pairs: Seq[(SoundbankSample, List[String])])(implicit logger: MetricsLogger): Seq[TokenSequence]
  def make(referenceTokens: List[(String, Int, Set[String])], hypothesisWords: List[String], garbageIndices: ListBuffer[(Int, String)],
                meta : ListBuffer[(Int, String)] => TokenSequenceMeta, id: String, duration: Double)(implicit logger: MetricsLogger): TokenSequence
}

@Singleton
class TokenSequenceFactoryImpl @Inject()(injector: Injector,
                                         tokenizationService: TokenizationService,
                                         implicit val alignmentService: AlignmentService,
                                         implicit val tokenMappingService: TokenMappingService,
                                         config: Config,
                                         implicit val ec: ExecutionContext) extends TokenSequenceFactory {

  def make(sample: SoundbankSample, transcription: String)(implicit logger: MetricsLogger): TokenSequence = {
    val tokenTagsForMapping = config.getStringList("metric.tokensMappingTags").asScala.toSet
    // Tokenize
    val (tokens, complexIndices, tags, garbageIndices) = tokenizationService.tokenizeWithTags(logger)(
      SampleMetaUtils.getTaggedRef(sample).getOrElse(SampleMetaUtils.getRef(sample).getOrElse("")).toLowerCase()
    )
    val referenceTokens = tokens.zipWithIndex.map(pair => (mapToken(pair._1, tokenTagsForMapping, tags(pair._2)),
      complexIndices(pair._2), tags(pair._2))).toList

    val hypothesisWords = tokenizationService.tokenizeWithTags(logger)(transcription.toLowerCase())._1.toList

    make(referenceTokens, hypothesisWords, garbageIndices, garbageIndices => new TokenSequenceMeta(sample, garbageIndices.toList), sample.id.get, sample.duration)
  }

  def make(referenceTokens: List[(String, Int, Set[String])], hypothesisWords: List[String], garbageIndices: ListBuffer[(Int, String)],
           meta : ListBuffer[(Int, String)] => TokenSequenceMeta, id: String, duration: Double)(implicit logger: MetricsLogger): TokenSequence = {
    val tokenTagsForMapping = config.getStringList("metric.tokensMappingTags").asScala.toSet
    // Align
    val alignment = alignmentService.align(for (token <- referenceTokens) yield token._1, hypothesisWords)

    logger.debug(s"Reference tokens: ${referenceTokens.mkString(", ")}")
    logger.debug(s"Hypothesis tokens: ${hypothesisWords.mkString(", ")}")
    logger.debug(s"Garbage indices generated while tokenizing: $garbageIndices")
    // Make new token sequence
    val tokenSequence = new TokenSequence(makeTokens(alignment,
      referenceTokens, hypothesisWords, Some(Mut(garbageIndices))).toList,
      meta(garbageIndices), id,
      referenceTokens.length, hypothesisWords.length, duration)

    logger.debug(s"Updated garbage indices: ${tokenSequence.meta.garbageIndices}")
    tokenSequence.mapValuesInplace(tokenTagsForMapping, mapRef = false)
    tokenSequence
  }

  def make(transcription: String)(implicit logger: MetricsLogger): TokenSequence = {
    val (tokens, complexIndices, tags, _) = tokenizationService.tokenizeWithTags(logger)(transcription)
    logger.debug(s"Reference tokens: ${tokens.mkString(", ")}")
    new TokenSequence(tokens.zipWithIndex.map(pair =>
      new Token(pair._1, pair._2, complexIndices(pair._2), tags(pair._2).toSet)).toList)
  }

  def make(pairs: Seq[(SoundbankSample, List[String])])(implicit logger: MetricsLogger): Seq[TokenSequence] =
    pairs.map({
      case (sample, transcription) => try {
        Some(make(sample, transcription.mkString(" ")))
      } catch {
        case error: IllegalStateException =>
          logger.error(s"Cannot make tokenSequence from sample ${sample.id}: $error. Skipping...")
          None
      }
    }).filter(_.isDefined).map(_.get)
}
