package enums

import play.api.libs.json.Format
import utils.EnumUtils

class BaseEnum extends Enumeration {

  type T = Value

  implicit val enumWrites: Format[T] = EnumUtils.enumFormat(this)

  def getForId(id: Int): T = {
    this.values.toList.find(_.id == id).getOrElse(throw new IllegalArgumentException(s"not found enum value for id = $id"))
  }

  def getForName(name: String): T = {
    this.values.toList.find(_.toString == name).getOrElse(throw new IllegalArgumentException(s"not found enum value for name = $name"))
  }

}
