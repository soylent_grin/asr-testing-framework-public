package enums

object MorphoFeature extends Enumeration {
  type Feature = Value
  val PART_OF_SPEECH: Value = Value("POS")
  val GENDER: Value = Value("gender")
  val NUMBER: Value = Value("number")
  val CASE: Value = Value("case")
  val ANIMACY: Value = Value("animacy")
  val ASPECT: Value = Value("aspect")
  val INVOLVEMENT: Value = Value("involvement")
  val MOOD: Value = Value("mood")
  val PERSON: Value = Value("person")
  val TENSE: Value = Value("tense")
  val TRANSITIVITY: Value = Value("transitivity")
  val VOICE: Value = Value("voice")
  val UNKNOWN: Value = Value("unknown")

  def fromString(s: String): MorphoFeature.Value =
    values.find(_.toString == s).getOrElse(MorphoFeature.UNKNOWN)

  def openClasses(): List[String] =
    List[String]("NOUN", "ADJF", "ADJS", "VERB", "INFN", "PRTF", "PRTS", "GRND", "NUMR", "ADVB")
}
