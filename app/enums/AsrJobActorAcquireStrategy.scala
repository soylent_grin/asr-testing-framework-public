package enums

import dtos.AsrJobDto
import models.AsrJob

object AsrJobActorAcquireStrategy extends BaseEnum {

  type AsrJobActorAcquireStrategy = Value

  val perUser, perAsr, perSystem, perJob = Value

  def jobToId(job: AsrJobDto)(implicit strategy: AsrJobActorAcquireStrategy.Value): String = {
    strategy match {
      case AsrJobActorAcquireStrategy.perUser =>
        s"perUser-${job.user.getOrElse("?")}"
      case AsrJobActorAcquireStrategy.perAsr =>
        s"perAsr-${job.asrKey}"
      case AsrJobActorAcquireStrategy.perSystem =>
        s"perSystem"
      case AsrJobActorAcquireStrategy.perJob =>
        s"perJob-${job.id.get.toString}"
      case _ =>
        throw new RuntimeException(s"unknown strategy: $strategy")
    }
  }

}
