package enums

object AsrJobStepInputType extends BaseEnum {
  type AsrJobStepInputType = Value
  val default, previous, current = Value
}
