package enums

object AsrStepSamplesType extends BaseEnum {
  type AsrStepSamplesType = Value
  val current, previous = Value
}
