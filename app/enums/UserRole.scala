package enums

object UserRole extends BaseEnum {
  type UserRole = Value
  val admin, user = Value
}
