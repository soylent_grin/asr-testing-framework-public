package enums

object WerOperation extends Enumeration {
  type Operation = Value
  val INS, DEL, SUB, OK, UNKNOWN = Value

  def withNameOpt(s: String): WerOperation.Value = values.find(_.toString == s).getOrElse(WerOperation.UNKNOWN)
}
