package enums

object Metric extends BaseEnum {
  type Operation = Value
  val SF: Value = Value("SF")
  val WER: Value = Value("WER")
  val INS: Value = Value("INS")
  val DEL: Value = Value("DEL")
  val SUB: Value = Value("SUB")
  val CHER: Value = Value("CHER")
  val PHER: Value = Value("PHER")
  val CER: Value = Value("CER")
  val CXER: Value = Value("CXER")
  val MER: Value = Value("MER")
  val LMER: Value = Value("LMER")
  val INFLER: Value = Value("INFLER")
  val INFLERA: Value = Value("INFLERA")
  val IMER: Value = Value("IMER")
  val IMERA: Value = Value("IMERA")
  val MSTAT: Value = Value("MSTAT")
  val NCR: Value = Value("NCR")
  val OCWR: Value = Value("OCWR")
  val IWERA: Value = Value("IWERA")
  val IWER: Value = Value("IWER")
  val HES: Value = Value("HES")
  val RER: Value = Value("RER")
  val WMER: Value = Value("WMER")
  val WLMER: Value = Value("WLMER")
  val GER: Value = Value("GER")
  val LER: Value = Value("LER")
  val RLER: Value = Value("RLER")

  // scalastyle:off
  def getTitle(v: Metric.Value): String = v match {
    case WER => "Word Error Rate"
    case INS => "WER Insertions"
    case DEL => "WER Deletions"
    case SUB => "WER Substitutions"
    case CHER => "Character Error Rate"
    case PHER => "Phoneme Error Rate"
    case CER => "Concept Error Rate"
    case CXER => "Complex Error Rate"
    case MER => "Morphological Error Rate"
    case LMER => "Morphological Error Rate for tokens with equal lemma"
    case MSTAT => "Morphological Stats"
    case NCR => "Numeric Class Rate"
    case IWERA => "Average Individual WER"
    case IWER => "Individual WER per token"
    case HES => "Hesitate F1 score"
    case RER => "Repairs Error Rate"
    case OCWR => "Open Class Words Rate"
    case SF => "Speed Factor"
    case WMER => "Weighted Morphological ER"
    case WLMER => "Weighted Morphological ER for tokens with equal lemma"
    case INFLERA => "Average Individual Morphological ER for tokens with equal lemma"
    case INFLER => "Individual Morphological ERs for tokens with equal lemma"
    case IMERA => "Average Individual Morphological ER"
    case IMER => "Individual Morphological ERs"
    case GER => "Garbage ER"
    case LER => "Lemma Error Rate"
    case RLER => "Realigned Lemma Error Rate"
    case _ => "Unknown metric"
  }

  def getDescription(m: Value): String = m match {
    case WER =>
      "Cumulative error rate, count by next expression: (INS + DEL + SUB) / (OK + DEL + SUB), where `OK` is count of token " +
        "pairs in accordance with the result of the alignment, in which the reference token is also defined transcriptions and" +
        " the recognized transcription token and at the same time they equal to each other"
    case INS =>
      "Count of tokens of recognized transcription, for which there is no paired token in the reference transcription in" +
        " according to the alignment result"
    case DEL =>
      "Count of reference transcription tokens for which there is no paired token in the recognized transcription in according" +
        " to the alignment result"
    case SUB =>
      "Count of token pairs in accordance with the result of the alignment, in which the reference token is also defined" +
        " transcriptions, and the recognized transcription token, however they are not equal to each other"
    case CHER =>
      "Algorithm for calculating this metric consists of two stages: pre-processing of the reference and recognized " +
        "transcription, which involves splitting two sequences into separate characters, and alignment of the result, " +
        "the second stage is the calculation of WER for the resulting alignment"
    case PHER =>
      "Phoneme recognition error rate. Similar to CHER metric, PHER is calculated by dividing the reference and recognized " +
        "transcriptions into separate phonemes using the language model dictionary, aligning the results and calculating WER"
    case CER =>
      "This metric calculates the recognition error rate for named entities. Requires additional metadata in samples, namely, " +
        "the field “concepts-indices” (indices of token-named entities are indicated) or “concepts-BIO” (a list of reference" +
        " transcription tokens and their types according to the BIO standard is indicated). CER = (1 - recall)"
    case CXER =>
      "Metric calculates the error of difficult word recognition (formed by arbitrary parts of speech), which are written" +
        " with a hyphen. This metric is needed to handle cases where parts of the composite are recognized correctly, but" +
        " written differently in the reference transcription and the ASR hypothesis. When calculating this metric, it is" +
        " assumed that in the reference transcription is fully respected orthography of the Russian language, i.e. compound" +
        " words are hyphenated when required by the rule"
    case MER =>
      "Error rate of the grammatical category recognition (case, number, gender, etc.) is calculated for each grammatical" +
        " category as the ratio of the number of words in which the meaning of the grammatical category was incorrectly" +
        " recognized to the number of words in the reference transcription, for which this grammatical category is contained" +
        " in the morphological information, generated by the morpho-analyzer"
    case LMER =>
      "Error rate for inflectional categories is calculated only for tokens that have the same lemmas in both the reference" +
        " transcription and the ASR transcription hypothesis"
    case MSTAT =>
      "Displays statistics on the morphological information of the ASR transcription hypothesis: for each grammeme of" +
        " each grammatical category, its absolute frequency is calculated, for example, how many words were found in the" +
        " nominative case"
    case NCR =>
      "Accuracy rate of numerals recognition"
    case IWERA =>
      "Calculates the average IWER value as the sum of the IWER values of the reference transcription tokens divided by" +
        " the number of tokens in it"
    case IWER =>
      "Proposed in [Goldwater, et al 2010] to assess the impact of a single word and its immediate context on the quality" +
        " of speech recognition. Requires the presence of the `alpha` field in the sample metadata"
    case HES =>
      "Allows you to evaluate the quality of marker recognition hesitation. It is the result of calculating the F1-score" +
        " based on the reference and recognized transcription for the tokens included in the hesitation list"
    case RER =>
      "Speech failure recognition error rate. Requires additional metadata field - list of indices of speech failures" +
        " in the reference transcription (repetitions, breaks, self-corrections, etc.). Then, taking into account the" +
        " alignment result, only for these complex names, the error is calculated similarly to the CER metric"
    case OCWR =>
      "Сalculates the recognition accuracy of words belonging to the \"open\" parts of speech (nouns, adjectives, etc.)." +
        " The metric value is calculated as (1 - WER (OpenClassWords)). It is used to assess the suitability of a " +
        "recognition hypothesis for analysis by intent classification algorithms"
    case SF =>
      "Allows to compare the time efficiency of ASR and shows how many times the recognition process took longer than" +
        " the duration of the audio file. Only the average value is calculated"
    case WMER =>
      "Weighted arithmetic mean of the MER and the weights of the i grammatical categories (w)"
    case WLMER =>
      "Weighted arithmetic mean of the LMER and grammatical category weights. The result of calculating the metric" +
        " is a real number, which should be interpreted as a weighted error in recognizing inflectional categories"
    case INFLERA =>
      "Average error of recognition of grammatical categories within the token, provided that the lemmas coincide in" +
        " both the reference transcription and the ASR hypothesis. The data source is INFLER, not IMER"
    case INFLER =>
      "Error in recognizing grammatical categories within the token, provided that the lemmas coincide in both the" +
        " reference transcription and the ASR hypothesis, is calculated as the ratio of the sum of the weights of the" +
        " grammatical categories of the token that were incorrectly recognized to the sum of the weights of the" +
        " grammatical categories contained in the morphological information for this token, generated by the spelling" +
        " analyzer automatically, provided that the lexemes in the morphological information of a pair of aligned tokens match"
    case IMERA =>
      "Average error of recognition of grammatical categories in morphological information of each token is calculated " +
        "based on the IMER metric using the arithmetic mean formula (the sum of all IMER values divided by their number)"
    case IMER =>
      "Error in recognizing grammatical categories for each token is calculated as the ratio of the sum of the weights " +
        "of the grammatical categories of the token that were incorrectly recognized to the sum of the weights of the" +
        " grammatical categories contained in the morphological information for the given token, generated by the morpho-analyzer automatically. This metric differs from MER in that it shows how well the grammatical category values were recognized for each token"
    case GER =>
      "Allows you to evaluate the quality of speech recognition, taking into account tokens annotated with tags of" +
        " the \"garbage\" model. The result of calculating the metric consists of the result of calculating the WER," +
        " calculated taking into account the tokens annotated with the tags of the \"garbage\" model."
    case LER =>
      "Allows you to calculate the inflectional error of word recognition, i.e. to estimate the number of" +
        " substitutions that lead to the replacement of the lexeme of the word, and the number of substitutions" +
        " in which the stem of the word is recognized correctly, and the inflection is incorrect, while different" +
        " weights are used depending on whether a pair of tokens that are a replacement has the same or different" +
        " lexeme. The metric was proposed in [Karpov, et al 2012]. After alignment, the lemmas are compared for" +
        " those pairs of tokens of the reference and recognized transcriptions to which the substitution operation corresponds"
    case RLER =>
      "Same as LER, but additional align step is used for lemmatized versions of tokens"
    case _ =>
      "Unknown metric"
  }
  // scalastyle:on

  def morphoMetrics(): List[Metric.Value] = {
    List[Metric.Value](MER, MSTAT, NCR, OCWR, WMER, INFLER,  IMER, INFLERA, IMERA, LMER, WLMER, LER, RLER) //SLER)
  }

  def metricsOnLemma(): List[Metric.Value] = {
    List[Metric.Value](LMER, WLMER, INFLERA)
  }

  def werDetailsBasedMetrics(): List[Metric.Value] = {
    List[Metric.Value](IWER, IWERA, CER, CXER)
  }

  def unaveragedMetrics(): List[Metric.Value] = {
    List[Metric.Value](IWER, INFLER, IMER)
  }

  def indirectMetrics(): List[Metric.Value] = {
    List[Metric.Value](SF)
  }

  def unweightedMetrics(): List[Metric.Value] = {
    List[Metric.Value]()
  }

  def cumulativeMetrics(): List[Metric.Value] = {
    List[Metric.Value](SF)
  }

  def fromString(s: String): Option[Value] = values.find(_.toString == s)
}
