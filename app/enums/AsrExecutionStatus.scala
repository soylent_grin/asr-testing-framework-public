package enums

object AsrExecutionStatus extends BaseEnum {
  type AsrExecutionStatus = Value
  val waiting, running, success, error, terminated = Value
}