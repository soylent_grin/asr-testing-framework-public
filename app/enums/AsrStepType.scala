package enums

object AsrStepType extends BaseEnum {
  type AsrStepType = Value
  val dataset, train, test, measure = Value
}
