package actors

import java.util.UUID

import actors.AsrExecutorActor.RunJob
import akka.actor.{Actor, ActorSystem}
import beans.{AsrJobBean, AsrStepLogBean, SoundbankBean}
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import dtos.{AsrJobDto, AugmentationParamsDto}
import enums.{AsrExecutionStatus, AsrJobStepInputType, AsrStepType}
import models.{AsrMetricInput, AsrStep}
import org.joda.time.DateTime
import play.api.Logger
import play.api.inject.Injector
import repositories.{AsrModelRepository, AsrStepRepository, AsrStepResultRepository, SoundbankRepository}
import services.metrics.AsrMetricService
import models.AsrStepResult
import services.{AsrExecutorService, AsrStepSampleProviderService}
import utils._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

trait AsrExecutorActorFactory {
  def apply(asrJobBean: AsrJobBean): Actor
}

object AsrExecutorActor {
  final val Name: String = "asr-executor-actor"

  case class RunJob(job: AsrJobDto)
}

class AsrExecutorActor @Inject()(@Assisted asrJobBean: AsrJobBean,
                                 injector: Injector,
                                 asrStepLogBean: AsrStepLogBean,
                                 asrExecutorService: AsrExecutorService,
                                 asrMetricService: AsrMetricService,
                                 asrStepRepository: AsrStepRepository,
                                 asrStepResultRepository: AsrStepResultRepository,
                                 asrStepSampleProviderService: AsrStepSampleProviderService,
                                 soundbankRepository: SoundbankRepository,
                                 soundbankBean: SoundbankBean,
                                 actorSystem: ActorSystem,
                                 asrModelRepository: AsrModelRepository) extends Actor {

  private val logger = Logger(getClass)

  private implicit val ec: ExecutionContext = actorSystem.dispatchers.lookup("asr-executor-actor-context")

  import json.AsrJsonBindings._

  {
    logger.debug(s"inited; do nothing (waiting for RunJob message)")
  }

  def receive: PartialFunction[Any, Unit] = {
    case m: RunJob =>
      val job = m.job
      logger.info(s"found waiting job $job; executing...")
      val future = executeJob(job).map(_ => {
        logger.debug(s"job ${job.id} is fully finished; ready to serve other jobs")
      })
      // sorry for this await
      Await.result(future, Duration.Inf)
  }

  // ---

  private def executeJob(job: AsrJobDto): Future[_] = {
    for {
      _ <- {
        logger.debug(s"updating job ${job.id} status to Running...")
        updateJob(
          job.id.get,
          Some(DateTime.now()),
          None,
          AsrExecutionStatus.running
        )
      }
      _ <- {
        var stepsToExecute: Seq[AsrStep] = null
        val a = job.steps
          val b = a.filter(_.getStatus == AsrExecutionStatus.waiting)
            val c = b.sortBy(_.index)
        stepsToExecute = c
        if (stepsToExecute.isEmpty) {
          logger.warn(s"job contains 0 steps; is that ok?")
          Future.successful(None)
        } else {
          val futures = ScalaUtils.chainFutures(
            stepsToExecute.map(s => () => {
              executeStep(job, s)
            }).toList
          ).flatMap(_ => {
            logger.info(s"job ${job.id} successfully finished; updating status to Success...")
            updateJob(
              job.id.get,
              None,
              Some(DateTime.now()),
              AsrExecutionStatus.success
            )
          })
          futures recoverWith {
            case _: AsrTerminationException =>
              logger.info(s"job was terminated; updating job status")
              updateJob(
                job.id.get,
                None,
                Some(DateTime.now()),
                AsrExecutionStatus.terminated
              )
            case _: AsrExecutionException =>
              logger.warn(s"one of steps failed; updating job status")
              updateJob(
                job.id.get,
                None,
                Some(DateTime.now()),
                AsrExecutionStatus.error
              )
            case t: Throwable =>
              logger.error(s"one of steps failed with unhandled exception (that's bad): ", t)
              updateJob(
                job.id.get,
                None,
                Some(DateTime.now()),
                AsrExecutionStatus.error
              )
          }
        }
      }
    } yield ()
  }

  private def updateJob(jobId: UUID,
                        timeBegin: Option[DateTime],
                        timeEnd: Option[DateTime],
                        status: AsrExecutionStatus.Value): Future[_] = {
    asrJobBean.getJob(jobId) flatMap {
      case Some(j) =>
        var newJob = j.copy(
          status = Some(status)
        )
        if (timeBegin.isDefined) {
          newJob = newJob.copy(
            timeBegin = timeBegin
          )
        }
        if (timeEnd.isDefined) {
          newJob = newJob.copy(
            timeEnd = timeEnd
          )
        }
        asrJobBean.updateJob(newJob)
      case _ =>
        Future.failed(
          new RuntimeException(s"failed to update job $jobId; not found in database")
        )
    }
  }

  private def executeStep(job: AsrJobDto, step: AsrStep): Future[_] = {
    logger.debug(s"found next step ${step.id} to perform")
    if (step.stepType == AsrStepType.measure) {
      executeMeasureStep(step, job)
    } else if (step.stepType == AsrStepType.dataset) {
      executeDatasetStep(step, job)
    } else {
      val future = for {
        _ <- updateStep(step, AsrExecutionStatus.running)
        result <- {
          val timeBegin = System.nanoTime()
          asrExecutorService.execute(job.asrKey, step).map(
            _.copy(
              duration = Some((System.nanoTime() - timeBegin) / 1000000000) // in seconds
            )
          )
        }
        _ <- {
          logger.debug(s"saving step result...")
          asrStepResultRepository.create(AsrStepResult.fromAsrExecutionResultDto(result, step.id.get))
        }
        _ <- result.log match {
          case Some(l) =>
            logger.debug(s"received new log from execution result; saving it...")
            asrStepLogBean.saveLog(job.asrKey, step.id.get, l)
          case _ =>
            Future.successful(None)
        }
        _ <- result.models match {
          case Some(m) =>
            logger.debug(s"received new ${m.size} models from execution result; saving it...")
            asrModelRepository.putModels(m, job.asrKey, step.id.get)
          case _ =>
            Future.successful(None)
        }
        _ <- updateStep(step, AsrExecutionStatus.success, result.duration)
      } yield result
      future recoverWith {
        case t: AsrStepTerminationException =>
          logger.info(s"job terminated (suppose to be deleted); updating status and saving log")
          for {
            _ <- updateStep(step, AsrExecutionStatus.terminated)
            _ <- t.log match {
              case Some(f) =>
                logger.info(s"found log file; saving")
                asrStepLogBean.saveLog(job.asrKey, step.id.get, f)
              case _ =>
                logger.info(s"log file not found; do nothing")
                Future.successful(None)
            }
          } yield {
            throw new AsrTerminationException
          }
        case t: AsrStepExecutionException =>
          logger.info(s"job processing failed, but we should have the log; updating")
          for {
            _ <- updateStep(step, AsrExecutionStatus.error)
            _ <- asrStepLogBean.saveLog(job.asrKey, step.id.get, t.log)
          } yield {
            throw new AsrExecutionException
          }
        case t: Throwable =>
          logger.error("step processing has failed with unhandled exception (that's bad):", t)
          for {
            _ <- updateStep(step, AsrExecutionStatus.error)
          } yield {
            throw new AsrExecutionException
          }
      }
    }
  }

  private def executeDatasetStep(step: AsrStep, job: AsrJobDto): Future[_] = {
    logger.debug(s"processing dataset step ${step.id.get}")
    val sourceFolder = (step.config \ "folder").asOpt[String].getOrElse(
      throw new IllegalArgumentException(s"could not parse folder from step config: ${step.config}")
    )
    val augmentationConfig = (step.config \ "augmentationConfig").asOpt[AugmentationParamsDto].getOrElse(
      throw new IllegalArgumentException(s"could not parse augmentation config from step config: ${step.config}")
    )
    val future = soundbankBean.processFolder(
      sourceFolder,
      "generic",
      augmentationConfig,
      job.user.getOrElse(throw new IllegalArgumentException(s"unable to process job without user: $job"))
    ).flatMap(folder => {
      logger.info(s"done processing dataset step; saving result")
      asrStepResultRepository.create(AsrStepResult.fromSoundbankFolder(folder, step.id.get))
    })
    updateStep(step, AsrExecutionStatus.running).flatMap(_ => {
      future.flatMap(_ => {
        updateStep(step, AsrExecutionStatus.success)
      })
    }) recoverWith {
      case t: Throwable =>
        logger.error(s"unexpected exception during dataset processing: ", t)
        updateStep(step, AsrExecutionStatus.error).map(_ => {
          throw new AsrExecutionException()
        })
    }
  }

  private def executeMeasureStep(step: AsrStep, job: AsrJobDto): Future[_] = {
    val future = getInputJobForMeasure(step) flatMap {
      case Some(j) =>
        j.steps.find(s => s.stepType == AsrStepType.test) match {
          case Some(s) if s.duration.isDefined =>
            asrStepResultRepository.findForStep(s.id.get) flatMap {
              case Some(r) if r.transcription.isDefined =>
                asrStepSampleProviderService.getSamples(s).flatMap(samples => {
                  val pairs = samples.map(ss => {
                    (ss, List(r.transcription.get(ss.id.getOrElse(""))))
                  })
                  step.config.validate[AsrMetricInput] fold(
                    invalid = { errors =>
                      Future.failed(new IllegalArgumentException("unable to parse step config" +
                        s"as metrics input: errors are $errors"))
                    },
                    valid = { input =>
                      logger.debug(s"ready to measure metrics on ${pairs.size} pairs on input $input")
                      val asrMetricService = injector.instanceOf[AsrMetricService]
                      asrMetricService.getMetricsFinal(
                        pairs,
                        s.duration.get,
                        input
                      ).flatMap(res => {
                        logger.debug(s"received result for metric calculation; saving it")
                        asrStepResultRepository.create(AsrStepResult.fromMetricOutput(res, step.id.get)).flatMap(_ => {
                          logger.debug(s"done saving result; saving log file...")
                          asrStepLogBean.saveLog(job.asrKey, step.id.get, asrMetricService.getLog)
                        })
                      })
                    }
                  )
                })
              case _ =>
                Future.failed(new IllegalStateException(s"not found test step result for step $s " +
                  s"or transcriptions are empty; unable to calculate metrics"))
            }
          case _ =>
            Future.failed(new IllegalStateException(s"not found test step for job $j " +
              s"or duration is zero; unable to calculate metrics"))
        }
      case _ =>
        Future.failed(new IllegalStateException(s"not found succeeded input job for step $step; " +
          s"unable to calculate metrics"))
    }

    updateStep(step, AsrExecutionStatus.running).flatMap(_ => {
      future.flatMap(_ => {
        updateStep(step, AsrExecutionStatus.success)
      })
    }) recoverWith {
      case t: Throwable =>
        logger.error(s"unexpected exception during metrics calculation: ", t)
        updateStep(step, AsrExecutionStatus.error).flatMap(_ => {
          asrStepResultRepository.create(AsrStepResult.fromMetricFailure(t, step.id.get))
        }).map(_ => {
          throw new AsrExecutionException()
        })
    }
  }

  private def getInputJobForMeasure(step: AsrStep): Future[Option[AsrJobDto]] = {
    val jobId = step.inputType match {
      case Some(AsrJobStepInputType.current) =>
        step.jobId.get
      case Some(AsrJobStepInputType.previous) =>
        step.inputJob.get
      case _ =>
        throw new IllegalArgumentException(s"incorrect input type: ${step.inputType}")
    }
    asrJobBean.getJob(jobId)
  }

  private def updateStep(step: AsrStep,
                         status: AsrExecutionStatus.AsrExecutionStatus,
                         duration: Option[Long] = None): Future[_] = {
    logger.debug(s"updating step ${step.id} status to $status")
    asrStepRepository.updateStep(step.copy(
      status = Some(status),
      duration = duration match {
        case Some(d) =>
          Some(d)
        case _ =>
          step.duration
      }
    ))
  }

}
