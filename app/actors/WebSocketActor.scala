package actors

import akka.actor._
import play.api.libs.json.{JsValue, Json}
import services.MessageBrokerService

case class WebSocketMessage(topic: String, message: JsValue)

object WebSocketMessageJsonBindings {

  implicit val messageFormat = Json.format[WebSocketMessage]

}

object WebSocketActor {
  def props(out: ActorRef, messageService: MessageBrokerService) = Props(new WebSocketActor(out, messageService))
}

class WebSocketActor(out: ActorRef, messageService: MessageBrokerService) extends Actor {

  messageService.registerActor(self)

  import WebSocketMessageJsonBindings._

  def receive: PartialFunction[Any, Unit] = {
    case msg: WebSocketMessage =>
      out ! Json.toJson(msg).toString()
  }

  override def postStop(): Unit = {
    messageService.unregisterActor(self)
  }
}