package exceptions

final case class DicFileDoesNotExistException(private val message: String = "Dic file with given path does not exist",
                                              private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
