package models

import java.io.File

import dtos.RecordSampleDto
import play.api.libs.json.{JsValue, Json}

case class SoundbankSample(id: Option[String],
                           key: String,
                           transcription: List[String],
                           duration: Double, // in seconds
                           sampleRate: Float,
                           audioFile: File,
                           meta: JsValue)

object SoundbankSample {
  def apply(taggedRef: String): SoundbankSample = SoundbankSample(Some("0"), "0", List[String](),
    0, 0, null, Json.obj({
      "tagged-ref" -> List[String](taggedRef)
    }))
}


