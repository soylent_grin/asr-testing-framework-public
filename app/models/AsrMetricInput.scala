package models

case class AsrMetricTagsInput(tags: List[String],
                              metrics: List[String])

case class AsrMetricInput(metrics: List[String],
                          tagGroups: List[AsrMetricTagsInput],
                          shouldCalculateAverageByTags: Option[Boolean] = Some(false))
