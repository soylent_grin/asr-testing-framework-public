package models

import java.util.UUID

import enums.AsrExecutionStatus.AsrExecutionStatus
import org.joda.time.DateTime

case class AsrJob(id: UUID,
                  timeCreate: DateTime,
                  timeBegin: Option[DateTime],
                  timeEnd: Option[DateTime],
                  status: AsrExecutionStatus,
                  asrKey: String,
                  user: String,
                  label: Option[String]) {

}

