package models

import java.io.File

case class SoundbankFolder(key: String,
                           title: String,
                           meta: Map[String, String],
                           file: File,
                           samples: Seq[SoundbankSample] = Nil,
                           user: Option[String] = None)
