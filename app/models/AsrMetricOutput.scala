package models

import enums.Metric

import scala.collection.mutable

case class AsrMetricOutputTagsResult(tags: List[String],
                                    result: Map[String, String])

case class AsrMetricOutputResult(result: Map[String, String],
                                 byTags: Seq[AsrMetricOutputTagsResult])

case class AsrMetricOutput(overall: Seq[AsrMetricOutputResult],
                           single: Seq[(String, AsrMetricOutputResult)])



case class AsrMetricOutputTagsResultAny(var tags: List[String],
                                        var result: Map[Metric.Value, Option[_]])

case class AsrMetricOutputResultAny(var result: Map[Metric.Value, Option[_]],
                                    var byTags: Seq[AsrMetricOutputTagsResultAny])

case class AsrMetricOutputAny(var overall: Seq[AsrMetricOutputResultAny],
                              var single: Seq[(String, AsrMetricOutputResultAny)])
