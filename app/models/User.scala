package models


import com.mohiva.play.silhouette.api.Identity
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import com.typesafe.config.Config
import enums.UserRole
import play.api.libs.json.JsValue
import utils.UserUtils

case class User(id: String,
                name: String,
                email: Option[String],
                avatarUrl: Option[String],
                role: UserRole.Value,
                password: Option[String] = None,
                data: Option[JsValue] = None) extends Identity

object User {

  def apply(profile: CommonSocialProfile)(implicit config: Config): User = {
    val id = UserUtils.makeUserId(profile.loginInfo)
    User(
      id = id,
      name = profile.fullName.getOrElse(profile.firstName.get + " " + profile.lastName.get),
      email = profile.email,
      avatarUrl = profile.avatarURL,
      role = UserRole.user
    )
  }

  def apply(config: Config): User = {
    val username = config.getString("username")
    val password = config.getString("password")
    val role = config.getString("role")
    User(
      id = username,
      name = username,
      email = None,
      avatarUrl = None,
      role = UserRole.getForName(role),
      password = Some(password)
    )
  }

  def apply(id: String): User = {
    User(
      id = id,
      name = id,
      email = None,
      avatarUrl = None,
      role = UserRole.user,
      password = None
    )
  }

}
