package models

import java.util.UUID

import enums.{AsrExecutionStatus, AsrStepSamplesType}
import enums.AsrExecutionStatus.AsrExecutionStatus
import enums.AsrStepType.AsrStepType
import enums.AsrJobStepInputType.AsrJobStepInputType
import play.api.libs.json.JsValue

case class AsrStepSamples(`type`: AsrStepSamplesType.Value,
                          list: List[String])

object AsrStepSamples {

  def getDefault: AsrStepSamples = AsrStepSamples(
    AsrStepSamplesType.current,
    Nil
  )

}

case class AsrStep(id: Option[UUID],
                   jobId: Option[UUID],
                   index: Int,
                   stepType: AsrStepType,
                   status: Option[AsrExecutionStatus],
                   config: JsValue,
                   samples: AsrStepSamples,
                   inputType: Option[AsrJobStepInputType],
                   inputJob: Option[UUID],
                   defaultModelName: Option[String],
                   duration: Option[Long]) {

  def getStatus: AsrExecutionStatus.Value = status.getOrElse(AsrExecutionStatus.waiting)

  def getAsrConfig: Map[String, JsValue] = config.asOpt[Map[String, JsValue]].getOrElse(Map())

  def getMeasureConfig: Map[String, String] = config.asOpt[Map[String, String]].getOrElse(Map())

}