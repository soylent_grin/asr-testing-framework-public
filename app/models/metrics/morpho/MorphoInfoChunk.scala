package models.metrics.morpho

import enums.MorphoFeature

// read response from python script
case class MorphoInfoExtendedChunk(
                                    var token: String,
                                    var lemma: String,
                                    var features: Map[MorphoFeature.Value, String])

// use response from python script for metrics calculation
case class MorphoInfoChunk(
                            var lemma: String,
                            var features: Map[MorphoFeature.Value, String])