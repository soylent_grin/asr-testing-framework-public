package models.metrics

// Wrapper for providing mutable structures to functions
case class Mut[A](var value: A) {}
