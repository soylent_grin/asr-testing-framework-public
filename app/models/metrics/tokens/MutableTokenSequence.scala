package models.metrics.tokens

import enums.WerOperation
import services.metrics.alignment.AlignmentService
import services.metrics.mapping.TokenMappingService
import utils.metrics.tokens.TokenUtils.mapToken

trait MutableTokenSequence {
  self: TokenSequence =>

  def updateLengths(): Int = {
    refLength = 0
    refLengthWithGarbage = 0
    hypLength = 0
    for (token <- tokens) {
      if (token.operation != WerOperation.INS){
        refLength += 1
        refLengthWithGarbage += 1
      }
      if (token.operation != WerOperation.DEL){
        hypLength += 1
      }
    }
    refLength
  }

  def updateLengths[A](values: Seq[A]): Int = {
    refLength = 0
    refLengthWithGarbage = 0
    hypLength = 0
    for (token <- tokens) {
      if ((token.operation != WerOperation.INS) && values.contains(token.value)){
        refLength += 1
        refLengthWithGarbage += 1
      }
      if ((token.operation != WerOperation.DEL) && values.contains(token.hypvalue)){
        hypLength += 1
      }
    }
    refLength
  }

  def mapValuesInplace(tags: Set[String], mapRef: Boolean = true, mapHyp: Boolean = true)
                      (implicit tokenMappingService: TokenMappingService, alignmentService: AlignmentService): Unit = {
    for (token <- tokens){
      // Already mapped before performing alignment
      if (mapRef){
        token.value = mapToken(token.value, tags, token.tags)
      }
      if (mapHyp){
        token.hypvalue = mapToken(token.hypvalue, tags, token.tags)
      }
      token.hypvalue = mapToken(token.hypvalue, tags, token.tags)
      if (token.operation == WerOperation.SUB) {
        token.operation = alignmentService.align(token.value, token.hypvalue)
      }
    }
  }
}
