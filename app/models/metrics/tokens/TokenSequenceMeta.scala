package models.metrics.tokens

import models.SoundbankSample
import utils.metrics.Converter.ListOfPlainMapsToListOfTuples

class TokenSequenceMeta(val alpha: Option[Double], // coefficient for IWER
                        val speechRepairsIndices: Option[List[Int]], // indices of pairs of tokens which are considered as speech repairs
                        val conceptsIndices: Option[List[List[Int]]], val bio: Option[List[(String, String)]], // indices of complex names which are considered as concepts
                        val garbageIndices: List[(Int, String)]) { // indices of pairs of tokens which are considered as garbage with associated garbage tag name

  def this(){
    this(None,None,None,None,List[(Int, String)]())
  }

  def this(sample: SoundbankSample, nrzb: List[(Int, String)]){
    this((sample.meta \ "alpha").asOpt[Double],
      (sample.meta \ "speech-repairs").asOpt[List[Int]],
      (sample.meta \ "concepts-indices").asOpt[List[List[Int]]],
      (sample.meta \ "concepts-BIO").asOpt[List[Map[String, String]]] match {
        case None => None
        case cb => Some(ListOfPlainMapsToListOfTuples(cb.get))
      },
      nrzb)
  }

  def this(tokenSequence: TokenSequence, nrzb: List[(Int, String)]){
    this(tokenSequence.meta.alpha,
      tokenSequence.meta.speechRepairsIndices,
      tokenSequence.meta.conceptsIndices,
      tokenSequence.meta.bio,
      nrzb)
  }
}