package models.metrics.tokens

import enums.MorphoFeature
import models.metrics.morpho.MorphoInfoChunk
import utils.metrics.tokens.TokenSequenceUtils.oneOrMany

import scala.collection.immutable

trait FilterableTokenSequence {
  self: TokenSequence =>

  def filterByPresentedTags(tags: Set[String]): TokenSequence = {
    withCopy {
      tokenSequence => {
        tokenSequence.tokens = tokens.filter(token => token.isRelevant(tags))
        tokenSequence.updateLengths()
      }
    }
  }

  def filterByNotPresentedTags(tags: Set[String]): TokenSequence = {
    withCopy{
      tokenSequence => {
        tokenSequence.tokens = tokens.filter(token => token.nonRelevant(tags))
        tokenSequence.updateLengths()
      }
    }
  }

  def filterByValue[A](values: A): TokenSequence = {
    withCopy{
      tokenSequence => {
        tokenSequence.tokens = tokens.filter(token => {
          oneOrMany[Boolean, A](
            values,
            value => token.value == value,
            values => values.contains(token.value)
          )
        })
        oneOrMany[Unit, A](
          values,
          _ => tokenSequence.updateLengths(),
          values => tokenSequence.updateLengths(values.map(value => value.toString).toList)
        )
      }
    }
  }

  def filterByValues[A](values: A): TokenSequence = {
    withCopy{
      tokenSequence => {
        tokenSequence.tokens = tokens.filter(token => {
          oneOrMany[Boolean, A](
            values,
            value => (token.value == value) || (token.hypvalue == value),
            values => values.contains(token.value) || values.contains(token.hypvalue)
          )
        })
        oneOrMany[Unit, A](
          values,
          _ => tokenSequence.updateLengths(),
          values => tokenSequence.updateLengths(values.map(value => value.toString).toList)
        )
      }
    }
  }

  def filterByCxids(cxids: List[Int]): TokenSequence = {
    withCopy{
      tokenSequence => {
        tokenSequence.tokens = tokens.filter(token => cxids.contains(token.complexId))
        tokenSequence.updateLengths()
      }
    }
  }

  def filterByConcept(concepts: List[List[Int]]): List[Token] = {
    val flatConcepts = concepts.flatten
    tokens.filter(token => flatConcepts.contains(token.complexId))
  }

  def filterByMorphoFeature[A](morphoFeature: MorphoFeature.Value,
                               morphoInfoPacked: Option[immutable.Map[String, MorphoInfoChunk]],
                               values: A): TokenSequence = {
    withCopy{
      tokenSequence => {
        val emptyChunk = MorphoInfoChunk("default", Map[MorphoFeature.Value, String]())
        if (morphoInfoPacked.isDefined) {
          val morphoInfo = morphoInfoPacked.get
          tokenSequence.tokens = tokens.filter(token => {
            val featureValue = morphoInfo.getOrElse(token.value, emptyChunk).features.getOrElse(morphoFeature, "")
            oneOrMany[Boolean, A](
              values,
              value => featureValue == value,
              values => values.contains(featureValue)
            )
          })
          tokenSequence.updateLengths()
        }
      }
    }
  }

  def filterByPOS(values: List[String],
                  morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]]): TokenSequence = {
    filterByMorphoFeature(MorphoFeature.PART_OF_SPEECH, morphoInfo, values)
  }

  def filterByPOS(value: String,
                  morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]]): TokenSequence = {
    filterByMorphoFeature(MorphoFeature.PART_OF_SPEECH, morphoInfo, value)
  }

  def filterByOpenness(morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]]): TokenSequence = {
    filterByPOS(MorphoFeature.openClasses(), morphoInfo)
  }

  def filterByNumeric(morphoInfo: Option[immutable.Map[String, MorphoInfoChunk]]): TokenSequence = {
    filterByPOS("NUMR", morphoInfo)
  }

  def filterAndLog(f: TokenSequence => TokenSequence,
                   log: String => _): TokenSequence = {
    val lengthBeforeFiltering = length()
    val newTokenSequence = f(self)
    if (newTokenSequence.length() != lengthBeforeFiltering){
      log(s"Deleted ${lengthBeforeFiltering - newTokenSequence.length()} tokens")
    } else {
      log("No tokens deleted")
    }
    newTokenSequence
  }
}
