package models.metrics.tokens

import enums.WerOperation

import scala.collection.mutable

trait CountableTokenSequence {
  self: TokenSequence =>

  def countOperations(operation: WerOperation.Value): Int = {
    tokens.count(token => token.operation == operation)
  }

  def countOk(): Int = {
    countOperations(WerOperation.OK)
  }

  def countComplexOperations(operations: List[WerOperation.Value]): Int = {
    var previousId = 0
    var previousIdInitialized = false
    var ok = true
    var result = 0
    for (token <- tokens) {
      if (previousIdInitialized){
        if (token.complexId != previousId){
          if (ok){
            result += 1
          } else {
            ok = true
          }
          previousId = token.complexId
        } else {
          if (!operations.contains(token.operation)){
            ok = false
          }
        }
        result += 1
      } else {
        previousId = token.complexId
        previousIdInitialized = true
      }
    }
    result
  }

  def countComplexOk(): Int = {
    countComplexOperations(List[WerOperation.Value](WerOperation.OK))
  }

  def countComplexNotOk(): Int = {
    countComplexOperations(List[WerOperation.Value](WerOperation.INS, WerOperation.DEL, WerOperation.SUB))
  }

  def countNotOk(): Int = {
    tokens.count(token => token.operation != WerOperation.OK)
  }

  def countIns(): Int = {
    countOperations(WerOperation.INS)
  }

  def countDel(): Int = {
    countOperations(WerOperation.DEL)
  }

  def countSub(): Int = {
    countOperations(WerOperation.SUB)
  }

  def countByPresentedTags(tags: Set[String]): Int = {
    tokens.count(token => token.isRelevant(tags))
  }
}
