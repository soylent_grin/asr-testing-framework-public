package models.metrics.tokens

import enums.WerOperation
import play.api.Logger

class Token(var value: String, // token from the reference transcription
            var hypvalue: String, // appropriate token from the hypothesis transcription according to alignment
            var id: Int, // id of value - hypvalue pair (unique for each pair)
            var complexId: Int, // id of complex name (might be same for some pairs located sequentially)
            var tags: Set[String], // set of tags associated with reference token
            var operation: WerOperation.Value){ // operation according to alignment (OK, SUB, DEL, INS)

  private lazy val logger = Logger(getClass)

  def copy(): Token ={
    new Token(value, hypvalue, id, complexId, tags, operation)
  }

  def this(value: String, id: Int, complexId: Int, tags: Set[String]) = {
    this(value, "", id, complexId, tags, WerOperation.OK)
  }

  def isRelevant(tagsToCompare: Set[String]): Boolean = {
    logger.debug(f"Comparing token's '$value' tagset $tags to $tagsToCompare")
    val result = tags.nonEmpty && tags.intersect(tagsToCompare).nonEmpty
    if (result){
      logger.debug(s"Token '$value' is inside some of required tags")
    } else {
      logger.debug(s"Token '$value' isn't inside any of required tags")
    }
    result
  }

  def nonRelevant(tagsToCompare: Set[String]): Boolean = {
    logger.debug(f"Comparing token's '$value' tagset $tags to $tagsToCompare")
    val result = tags.intersect(tagsToCompare).isEmpty
    if (result){
      logger.debug(s"Token '$value' isn't inside any of required tags")
    } else {
      logger.debug(s"Token '$value' is inside some of required tags")
    }
    result
  }
}
