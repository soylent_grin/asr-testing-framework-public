package models.metrics.tokens

import enums.Metric

import scala.collection.mutable

trait WeightFulTokenSequence {
  self: TokenSequence =>

  def weight(metric: Metric.Value, default: Option[Double] = None, tags: Option[List[String]] = None): Any = {
    var operativeTags = tags
    val metricValue = operativeTags match {
      case None => metrics.result.getOrElse(metric, None)
      case _ => metrics.byTags.filter(asrMetricOutputResultForTags => asrMetricOutputResultForTags.tags == tags.get).head.result.getOrElse(metric, None)
    }
    if (operativeTags.isEmpty){
      operativeTags = Some(List[String]())
    }

    metricValue match {
      case None =>
        if (default.isEmpty){
          throw new IllegalStateException("Not found weight for such metric with given tags")
        } else {
          default.get
        }
      case _ =>
        if (operativeTags.nonEmpty){
          if (weights.contains(operativeTags.get)){
            weights(operativeTags.get).getOrElse(metric, defaultWeights.getOrElse(operativeTags.get, refLength))
          } else {
            refLength
          }
        } else {
          refLength
        }
    }
  }

  def pullWeights(tokenSequence: TokenSequence, tags: List[String] = List[String]()): Unit = {
    weights(tags) = tokenSequence.weights.getOrElse(List[String](), mutable.Map[Metric.Value, Any]())
    defaultWeights(tags) = tokenSequence.refLength
  }

  def isCustomWeightDefined(metric: Metric.Value): Boolean =
    (weights contains List[String]()) && (weights(List[String]()) contains metric)

  def setWeight(metric: Metric.Value, value: Any, tags: Option[List[String]] = None): Unit = {
    var operativeTags = tags
    if (operativeTags.isEmpty){
      operativeTags = Some(List[String]())
    }
    if (operativeTags.isDefined){
      if (weights.contains(operativeTags.get)){
        weights(operativeTags.get)(metric) = value
      } else {
        weights(operativeTags.get) = mutable.Map[Metric.Value, Any](metric -> value)
      }
    }
  }
}
