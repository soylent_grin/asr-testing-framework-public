package models.metrics.tokens

import services.metrics.mapping.TokenMappingService

trait BasicTokenSequence {
  self: TokenSequence =>

  def copy(): TokenSequence = {
    new TokenSequence(for (token <- tokens) yield token.copy(),
      meta, id, refLength, refLengthWithGarbage, hypLength, duration, metrics, weights, defaultWeights)
  }

  def withCopy(x: TokenSequence => Unit): TokenSequence = {
    val tokenSequence = copy()
    x(tokenSequence)
    tokenSequence
  }

//  def mapValues(tokenMappingService: TokenMappingService, tags: Set[String]): TokenSequence = {
//    withCopy {
//      tokenSequence => {
//        tokenSequence.mapValuesInplace(tags)(tokenMappingService = )
//      }
//    }
//  }

  def values(): Set[String] = {
    tokens.filter(token => token.value.forall(_.isLetter)).map(token => token.value).
      union(tokens.filter(token => token.hypvalue.forall(_.isLetter)).
        map(token => token.hypvalue)).toSet
  }

  def length(): Int = {
    tokens.length
  }
}
