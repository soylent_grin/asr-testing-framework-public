package models.metrics.tokens

import enums.Metric
import models.{AsrMetricOutputResultAny, AsrMetricOutputTagsResultAny}
import services.metrics.MetricsLogger

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class TokenSequence (var tokens: List[Token],
                     var meta: TokenSequenceMeta,
                     var id: String,
                     var refLength: Int,
                     var refLengthWithGarbage: Int,
                     var hypLength: Int,
                     var duration: Double,
                     var metrics: AsrMetricOutputResultAny,
                     var weights: mutable.Map[List[String], mutable.Map[Metric.Value, Any]], // weights for each tagset
                     var defaultWeights: mutable.Map[List[String], Any]) // default weights for each tagset in case some metric haven't got associated weights
  extends FilterableTokenSequence with WeightFulTokenSequence with CountableTokenSequence
    with BasicTokenSequence with MutableTokenSequence with GarbageTokenSequence {

  protected var logger: MetricsLogger = MetricsLogger()

  def this(tokens: List[Token], meta: TokenSequenceMeta,
           id: String, refLength: Int, hypLength: Int, duration: Double)(implicit logger: MetricsLogger){
    this(tokens, meta, id, refLength, refLength, hypLength, duration,
      AsrMetricOutputResultAny(Map[Metric.Value, Option[_]](),
        ListBuffer[AsrMetricOutputTagsResultAny]()),
      mutable.Map[List[String], mutable.Map[Metric.Value, Any]](), // weights
      mutable.Map[List[String], Any]()) // default weights
      this.logger = logger
  }

  def this(tokens: List[Token])(implicit logger: MetricsLogger){
    this(tokens, new TokenSequenceMeta(), "<id>", 0, 0, 0)
  }
}
