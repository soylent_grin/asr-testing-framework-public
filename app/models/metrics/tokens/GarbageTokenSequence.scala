package models.metrics.tokens

import enums.WerOperation

import scala.collection.mutable

trait GarbageTokenSequence {
  self: TokenSequence =>

  def countGarbageTokens(tagName: String, alternativeTagNames: Set[String] = Set[String]()): Double = {
    logger.debug(s"Counting garbage tokens for tag $tagName")
    // map of values to divide by quantity of encountered insertions for normalization purposes
    val garbageIndices: mutable.Map[Int, Double] =
      mutable.Map[Int, Double]() ++
        (for (pair <- meta.garbageIndices if tagName == pair._2) yield pair._1 -> 1.0).toMap
    logger.debug(s"Garbage indices: $garbageIndices")

    for (pair <- meta.garbageIndices
         if garbageIndices.contains(pair._1) && // if this index was associated with required tag
           tagName != pair._2 && // and current tag is not equal to required
           (alternativeTagNames.isEmpty || alternativeTagNames.contains(tagName))){ // but it is inside list of alternative tags if it is not empty
      garbageIndices(pair._1) += 1
    }

    var result = 0.0
    val setOfOne = Set[String](tagName)

    for (i <- 0 until this.length()){
      logger.debug(s"Checking token ${tokens(i).value}|${tokens(i).hypvalue}|${tokens(i).tags.mkString(", ")}")
      var j = i
      var counter = 0.0
      var sequenceIsRelevant = false
      if ((tokens(j).operation == WerOperation.INS) && garbageIndices.contains(j)){
        sequenceIsRelevant = true
        counter = 1 / garbageIndices(j)
      }
      while ((j >= 0) && (tokens(j).operation == WerOperation.INS)) { // go down while there are inserts
        j -= 1
        if (garbageIndices.contains(j)){
          sequenceIsRelevant = true
          counter = 1 / garbageIndices(j)
        }
      }
      // If it has tag nrzb, and operation not del
      if (!sequenceIsRelevant && (tokens(i).operation != WerOperation.DEL) && tokens(i).isRelevant(setOfOne)) {
        counter = 1.0
      }
      if (counter > 0){
        logger.debug(s"Token is considered as garbage with weight $counter")
      }
      result += counter
    }
    result
  }

  def removeGarbageInserts(garbageTags: Set[String]): TokenSequence = {
    withCopy{
      tokenSequence => {
        val garbageIndices = for (pair <- meta.garbageIndices if garbageTags.contains(pair._2)) yield pair._1
        tokenSequence.tokens = List[Token]()
        for (i <- 0 until this.length()){
          var j = i
          var shouldRemove = false
          while ((j >= 0) && (tokens(j).operation == WerOperation.INS)) { // go down while there are inserts
            j -= 1
            if (garbageIndices.contains(j)){
              shouldRemove = true
            }
          }
          if (!shouldRemove){
            tokenSequence.tokens :+= tokens(i)
          }
        }
        tokenSequence.updateLengths()
      }
    }
  }
}
