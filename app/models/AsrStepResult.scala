package models

import java.util.UUID

import dtos.AsrExecutionResultDto
import play.api.libs.json.{JsValue, Json}

case class AsrStepResult(id: UUID,
                         stepId: UUID,
                         result: JsValue,
                         transcription: Option[Map[String, String]]) {

  def this(stepId: UUID,
           result: JsValue,
           transcription: Map[String, String]) =
    this(UUID.randomUUID(), stepId, result, Some(transcription))

}

object AsrStepResult {

  import json.AsrJsonBindings._

  def fromAsrExecutionResultDto(dto: AsrExecutionResultDto, stepId: UUID): AsrStepResult = AsrStepResult(
    UUID.randomUUID(),
    stepId,
    Json.toJson(dto.result),
    dto.transcription
  )

  def fromMetricOutput(output: AsrMetricOutput, stepId: UUID): AsrStepResult = AsrStepResult(
    UUID.randomUUID(),
    stepId,
    Json.toJson(output),
    None
  )

  def fromMetricFailure(exception: Throwable, stepId: UUID): AsrStepResult = AsrStepResult(
    UUID.randomUUID(),
    stepId,
    Json.obj(
      "error" -> exception.getMessage
    ),
    None
  )

  def fromSoundbankFolder(folder: SoundbankFolder, stepId: UUID): AsrStepResult = AsrStepResult(
    UUID.randomUUID(),
    stepId,
    Json.obj(
      "folder" -> folder.key,
      "samples" -> folder.samples.map(_.id.get)
    ),
    None
  )

}
