package models.auth

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.{User, UserIdentity}

trait SessionEnv extends Env {
  type I = UserIdentity
  type A = CookieAuthenticator
}