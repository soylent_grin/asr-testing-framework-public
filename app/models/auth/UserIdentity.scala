package models

import com.mohiva.play.silhouette.api.Identity

case class UserIdentity(user: User) extends Identity {}