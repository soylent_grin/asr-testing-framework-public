package dtos

import models.SoundbankSample

case class SampleDto(id: Option[String],
                     key: String,
                     duration: Double,
                     sampleRate: Float) {

  def this(sample: SoundbankSample) = this(
    sample.id,
    sample.key,
    sample.duration,
    sample.sampleRate
  )

}
