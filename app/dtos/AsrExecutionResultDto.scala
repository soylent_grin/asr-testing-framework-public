package dtos

import java.io.File
import java.nio.file.Path

// this case class can be used both for train and test purposes
case class AsrExecutionResultDto(result: Map[String, String],
                                 transcription: Option[Map[String, String]] = None,
                                 log: Option[File] = None,
                                 models: Option[List[Path]] = None,
                                 duration: Option[Long] = None)