package dtos

case class AudioInfoDto(length: Double,
                        sampleRate: Float)
