package dtos

case class AppConfigDto(githubAuthEnabled: Boolean,
                        googleAuthEnabled: Boolean,
                        maxJobsPerUser: Int,
                        maxPrivateFoldersPerUser: Int,
                        maxSamplesPerJob: Int)
