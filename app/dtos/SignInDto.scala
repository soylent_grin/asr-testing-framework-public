package dtos

case class SignInDto(username: String,
                     password: String)
