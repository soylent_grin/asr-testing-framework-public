package dtos

import java.io.File

case class NoiseSampleDto(key: String, file: File) {

  def this(file: File) = this(file.getName, file)

  override def equals(o: Any): Boolean = o match {
    case s: NoiseSampleDto =>
      s.file.getAbsolutePath == file.getAbsolutePath
    case _ =>
      false
  }

  override def hashCode(): Int = file.getAbsolutePath.hashCode

}

case class NoiseGroupDto(key: String,
                         samples: Seq[NoiseSampleDto])
