package dtos

import extensions.augmentation.AugmentationScenario

case class AugmentationScenarioHolderDto(title: String,
                                         key: String,
                                         instance: AugmentationScenario)