package dtos

import extensions.executors.AsrExecutor

case class AsrExecutorCapabilities(train: Boolean)

case class AsrExecutorInfo(title: String,
                           key: String,
                           capabilities: AsrExecutorCapabilities,
                           executorClass: Class[_ <: AsrExecutor])

case class AsrExecutorInfoDto(title: String,
                              key: String,
                              capabilities: AsrExecutorCapabilities,
                              params: List[ParamDto] = Nil)

object AsrExecutorInfoDto {

  def fromModel(m: AsrExecutorInfo): AsrExecutorInfoDto = {
    AsrExecutorInfoDto(m.title, m.key, m.capabilities)
  }

}
