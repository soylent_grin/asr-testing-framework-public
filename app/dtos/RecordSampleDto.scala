package dtos

import java.io.File

import play.api.libs.json.JsValue

case class RecordSampleDto(folder: String,
                           name: String,
                           audio: File,
                           transcription: String,
                           metadata: Option[JsValue])