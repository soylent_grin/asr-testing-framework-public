package dtos

import java.util.UUID

import enums.AsrExecutionStatus
import models.{AsrJob, AsrStep}
import org.joda.time.DateTime
import utils.UserContext

case class AsrJobDto(id: Option[UUID],
                     timeCreate: Option[DateTime],
                     timeBegin: Option[DateTime],
                     timeEnd: Option[DateTime],
                     steps: Seq[AsrStep],
                     status: Option[AsrExecutionStatus.Value],
                     asrKey: String,
                     user: Option[String],
                     label: Option[String]) {

  def toModels: (AsrJob, Seq[AsrStep]) = {
    val jobId = id.getOrElse(UUID.randomUUID())
    val ensuredSteps = steps.map(s => {
      val ensuredId = s.id.getOrElse(UUID.randomUUID())
      s.copy(
        jobId = Some(jobId),
        id = Some(ensuredId)
      )
    })
    (
      AsrJob(
        jobId,
        timeCreate.getOrElse(DateTime.now()),
        timeBegin,
        timeEnd,
        status.getOrElse(AsrExecutionStatus.waiting),
        asrKey,
        user.get,
        label
      ),
      ensuredSteps
    )
  }

}

object AsrJobDto {

  def fromModel(job: AsrJob, steps: Seq[AsrStep]): AsrJobDto = AsrJobDto(
    Some(job.id),
    Some(job.timeCreate),
    job.timeBegin,
    job.timeEnd,
    steps,
    Some(job.status),
    job.asrKey,
    Some(job.user),
    job.label
  )

}
