package dtos

import play.api.libs.json.JsValue

case class AugmentationParamsDto(title: Option[String],
                                 params: Option[JsValue])
