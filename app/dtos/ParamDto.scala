package dtos

import com.typesafe.config.Config
import enums.AsrStepType
import play.api.Logger

import scala.collection.JavaConverters._
import scala.util.Try

case class ParamOption(key: String,
                       title: String)

case class ParamDto(key: String,
                    paramType: String,
                    defaultValue: Option[String],
                    title: Option[String],
                    description: Option[String],
                    options: List[ParamOption],
                    scope: List[AsrStepType.Value] = List(AsrStepType.test))

object ParamDto {

  def fromConfig(params: Seq[Config])
                (implicit logger: Logger): List[ParamDto] = {
    params.map(config => {
      val key = Try(config.getString("key")).toOption.getOrElse("")
      val paramType =Try(config.getString("type")).toOption.getOrElse("")
      val defaultValue = Try(config.getString("default")).toOption
      val title = Try(config.getString("title")).toOption
      val description = Try(config.getString("description")).toOption
      val scope = Try(config.getStringList("scope")).toOption match {
        case Some(l) =>
          l.asScala.map(AsrStepType.getForName)
        case _ =>
          List(AsrStepType.test)
      }

      val resultOptions = Try(config.getConfigList("options")).toOption match {
        case Some(rawOptions) =>
          try {
            rawOptions.asScala.map(op => {
              ParamOption(
                op.getString("key"),
                op.getString("title")
              )
            })
          } catch {
            case t: Throwable =>
              logger.error(s"unexpected error while parsing options param $key", t)
              Nil
          }
        case _ =>
          Nil
      }
      ParamDto(key, paramType, defaultValue, title, description, resultOptions.toList, scope.toList)
    }).toList
  }

}

case class AsrParamListDto(train: Seq[ParamDto],
                           test: Seq[ParamDto]) {
}

object AsrParamListDto {

  def fromList(params: Seq[ParamDto]): AsrParamListDto = {
    AsrParamListDto(
      params.filter(_.scope.contains(AsrStepType.train)),
      params.filter(_.scope.contains(AsrStepType.test))
    )
  }

}
