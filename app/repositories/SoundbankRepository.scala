package repositories

import java.io.File
import java.nio.file.Path

import dtos.RecordSampleDto
import models.{SoundbankFolder, SoundbankSample}
import utils.UserContext

import scala.concurrent.Future

trait SoundbankRepository {

  def getUserFolderRoot(userId: String): Path = ???

  def registerFolder(folder: SoundbankFolder)
                    (implicit userContext: UserContext): Future[SoundbankFolder]

  def findFolder(key: String)
                (implicit userContext: UserContext): Future[Option[SoundbankFolder]]

  def findSample(id: String)
                (implicit userContext: UserContext): Future[Option[SoundbankSample]]

  def findSample(folder: String, key: String)
                (implicit userContext: UserContext): Future[Option[SoundbankSample]] = ???

  def findSamples(ids: Seq[String]): Future[Seq[SoundbankSample]]

  def findAllFolders()
                    (implicit userContext: UserContext): Future[Seq[SoundbankFolder]]

  def findAllSamplesInFolder(key: String)
                            (implicit userContext: UserContext): Future[Seq[SoundbankSample]]

  def scanFolder(folder: File): SoundbankFolder

  def samplesToDataset(samples: Seq[SoundbankSample], datasetType: String): File

  def uploadFolder(folder: File, key: String)(implicit userContext: UserContext): Future[SoundbankFolder]

  def removeFolder(key: String)(implicit userContext: UserContext): Future[Unit]

  def registerRecordedSample(recordSample: RecordSampleDto)
                            (implicit userContext: UserContext): Future[SoundbankSample]

  def registerEmptyFolder(key: String)
                         (implicit userContext: UserContext): Future[SoundbankFolder]

}
