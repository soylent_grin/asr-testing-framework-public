package repositories

import dtos.{NoiseGroupDto, NoiseSampleDto}

trait NoiseRepository {
  def getNoiseGroups: Seq[NoiseGroupDto]
  def getRandomSampleFromGroup(groupKey: String): NoiseSampleDto
}
