package repositories.interpreters.cassandra

import java.util.UUID

import com.outworkers.phantom.connectors.{CassandraConnection, KeySpace}
import com.outworkers.phantom.dsl._
import com.typesafe.config.Config
import javax.inject.Inject
import models.AsrStepResult
import play.api.libs.json.{JsValue, Json}
import repositories.AsrStepResultRepository

import scala.concurrent.{ExecutionContext, Future}

class CassandraAsrStepResultRepository @Inject()(config: Config,
                                                 connection: CassandraConnection,
                                                 implicit val ec: ExecutionContext)
  extends Table[CassandraAsrStepResultRepository, AsrStepResult] with AsrStepResultRepository {

  implicit val session: Session = connection.session
  implicit val space: KeySpace = connection.provider.space
  override val tableName: String = config.getString("db.tables.asrStepResult")

  override def fromRow(r: Row): AsrStepResult = {
    AsrStepResult(
      id(r),
      stepId(r),
      result(r),
      Some(transcript(r))
    )
  }

  import utils.CassandraUtils._

  object id extends UUIDColumn with PartitionKey {
    override def name: String = "id"
  }

  object stepId extends UUIDColumn {
    override def name: String = "step_id"
  }

  object result extends JsonColumn[JsValue] {
    override def name: String = "result"
  }

  object transcript extends MapColumn[String, String] {
    override def name: String = "transcript"
  }

  override def create(res: AsrStepResult): Future[UUID] = {
     insert.value(_.id, res.id)
      .value(_.stepId, res.stepId)
      .value(_.result, res.result)
      .value(_.transcript, res.transcription.getOrElse(Map()))
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .future()
      .map(_ => res.id)
  }

  override def findForStep(stepId: UUID): Future[Option[AsrStepResult]] = {
    select.where(_.stepId is stepId)
      .allowFiltering()
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .one()
  }

  override def removeForStep(stepId: UUID): Future[_] = {
    findForStep(stepId).flatMap(res => {
      if (res.isDefined) {
        delete.where(_.id eqs res.get.id).future()
      } else {
        logger.warn(s"not found any step result for step $stepId; this is unexpected, " +
          s"you should debug this case")
        Future.successful(None)
      }
    })

  }

  def initialize(): Future[String] = {
    this.autocreate(space).future().map(_ => "autocreate table")
  }

  initialize()

}
