package repositories.interpreters.cassandra

import com.outworkers.phantom.connectors.CassandraConnection
import com.outworkers.phantom.dsl._
import com.outworkers.phantom.keys.PartitionKey
import com.typesafe.config.Config
import enums.UserRole
import javax.inject.Inject
import models.User
import play.api.libs.json.Json
import repositories.UserRepository

import scala.concurrent.{ExecutionContext, Future}

class CassandraUserRepository @Inject()(config: Config,
                                          connection: CassandraConnection,
                                          implicit val ec: ExecutionContext)
  extends Table[CassandraUserRepository, User] with UserRepository {

  implicit val session: Session = connection.session
  implicit val space: KeySpace = connection.provider.space
  override val tableName: String = config.getString("db.tables.user")

  override def fromRow(r: Row): User = {
    User(
      id(r),
      name(r),
      email(r),
      avatarUrl(r),
      UserRole.getForName(role(r)),
      None,
      Some(Json.toJson(data(r)))
    )
  }

  object id extends StringColumn with PartitionKey {
    override def name: String = "id"
  }

  /*
  * id: String,
    name: String,
    email: Option[String],
    avatarUrl: Option[String],
    role: UserRole.Value,
    password: Option[String] = None
    *  */

  object name extends StringColumn {
    override def name: String = "name"
  }

  object email extends OptionalStringColumn {
    override def name: String = "email"
  }

  object avatarUrl extends OptionalStringColumn {
    override def name: String = "avatar_url"
  }

  object role extends StringColumn {
    override def name: String = "role"
  }

  object data extends JsonMapColumn[String, String] {
    override def name: String = "data"
  }

  override def upsert(user: User): Future[User] = {
    find(user.id) flatMap {
      case Some(u) =>
        updateUser(u)
      case _ =>
        create(user)
    }
  }

  override def findAll(): Future[Seq[User]] = {
    select.consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM).fetch()
  }

  private def create(user: User): Future[User] = {
    insert.value(_.id, user.id)
      .value(_.name, user.name)
      .value(_.email, user.email)
      .value(_.avatarUrl, user.avatarUrl)
      .value(_.role, user.role.toString)
      .value(_.data, user.data.getOrElse(Json.obj()).asOpt[Map[String, String]].getOrElse(Map()))
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .future()
      .map(_ => user)
  }

  override def find(id: String): Future[Option[User]] = {
    select.where(_.id eqs id).consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM).one()
  }

  private def updateUser(user: User): Future[User] = {
    update.where(_.id eqs user.id)
      .modify(_.role.setTo(user.role.toString))
      .and(_.data.setTo(user.data.getOrElse(Json.obj()).asOpt[Map[String, String]].getOrElse(Map())))
      .future()
      .map(_ => user)
  }

  def initialize(): Future[String] = {
    this.autocreate(space).future().map(_ => "autocreate user table")
  }

  initialize()

}