package repositories.interpreters.cassandra

import java.util.UUID

import com.outworkers.phantom.connectors.CassandraConnection
import com.outworkers.phantom.dsl._
import com.outworkers.phantom.keys.PartitionKey
import com.typesafe.config.Config
import enums.AsrExecutionStatus
import javax.inject.Inject
import enums.AsrExecutionStatus.AsrExecutionStatus
import models.AsrJob
import repositories.AsrJobRepository

import scala.concurrent.{ExecutionContext, Future}

class CassandraAsrJobRepository @Inject()(config: Config,
                                          connection: CassandraConnection,
                                          implicit val ec: ExecutionContext)
  extends Table[CassandraAsrJobRepository, AsrJob] with AsrJobRepository {

  implicit val session: Session = connection.session
  implicit val space: KeySpace = connection.provider.space
  override val tableName: String = config.getString("db.tables.asrJob")

  override def fromRow(r: Row): AsrJob = {
    AsrJob(
      id(r),
      timeCreate(r),
      timeBegin(r),
      timeEnd(r),
      AsrExecutionStatus.getForId(status(r)),
      asrName(r),
      user(r),
      label(r)
    )
  }

  object id extends UUIDColumn with PartitionKey {
    override def name: String = "id"
  }

  /* object userId extends UUIDColumn {
    override def name: String = "userid"
  } */

  object timeCreate extends DateTimeColumn {
    override def name: String = "time_create"
  }

  object timeBegin extends OptionalDateTimeColumn {
    override def name: String = "time_begin"
  }

  object timeEnd extends OptionalDateTimeColumn {
    override def name: String = "time_end"
  }

  object inputSound extends  StringColumn {
    override def name: String = "input_sound"
  }

  object asrName extends  StringColumn with Index{
    override def name: String = "asr_name"
  }

  object configMap extends MapColumn[String,String] {
    override def name: String = "config"
  }

  object status extends IntColumn {
    override def name: String = "status"
  }

  object user extends StringColumn with Index {
    override def name: String = "user"
  }

  object label extends OptionalStringColumn with Index {
    override def name: String = "label"
  }

  override def create(job: AsrJob): Future[UUID] = {
   insert.value(_.id, job.id)
      .value(_.user, job.user)
      .value(_.timeCreate, job.timeCreate)
      .value(_.timeBegin, job.timeBegin)
      .value(_.timeEnd, job.timeEnd)
      .value(_.asrName, job.asrKey)
      .value(_.status, job.status.id)
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .future()
      .map(_ => job.id)
  }

  override def updateJob(job: AsrJob): Future[AsrJob] = {
    find(job.id) flatMap {
      case Some(_) =>
        update.where(_.id eqs job.id)
          .modify(_.status setTo job.status.id)
          .and(_.timeBegin setTo job.timeBegin)
          .and(_.label setTo job.label)
          .and(_.timeEnd setTo job.timeEnd).future().map(_ => job)
      case _ =>
        logger.warn(s"trying to update job that not exists ($job)")
        Future.successful(job)
    }
  }

  override def deleteById(jobId: UUID): Future[_] = {
    delete.where(_.id eqs jobId).future()
    // delete(_.someSet).where(_.id eqs job.id)
    // update.where(_.id eqs jobId).modify(_.someSet setTo Set.empty[String]).future()
  }

  override def find(jobId: UUID): Future[Option[AsrJob]] = {
    select.where(_.id eqs jobId).consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM).one()
  }

  override def findAll(): Future[Seq[AsrJob]] = {
    select.consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM).fetch()
  }

  override def findForStatus(status: AsrExecutionStatus): Future[Seq[AsrJob]] = {
    select.where(_.status is status.id).consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM).fetch()
  }

  override def findForStatuses(statuses: List[AsrExecutionStatus]): Future[Seq[AsrJob]] = {
    val futures = statuses.map(s => select.where(_.status is s.id).consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM).fetch())
    Future.sequence(futures).map(_.flatten.toSeq)
  }

  override def updateLabel(jobId: UUID, label: String): Future[_] = {
    find(jobId) flatMap {
      case Some(_) =>
        update.where(_.id eqs jobId)
          .modify(_.label.setTo(Some(label)))
          .future()
      case _ =>
        Future.failed(new IllegalArgumentException(s"trying to update job that not exists ($jobId)"))
    }
  }

  override def findAllForUser(userId: String): Future[Seq[AsrJob]] = {
    select.where(_.user eqs userId)
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .fetch()
  }

  def initialize(): Future[String] = {
    this.autocreate(space).future().map(_ => "autocreate table")
  }

  initialize()

}