package repositories.interpreters.cassandra

import java.util.UUID

import com.outworkers.phantom.connectors.{CassandraConnection, KeySpace}
import com.outworkers.phantom.dsl._
import com.typesafe.config.Config
import enums.{AsrExecutionStatus, AsrJobStepInputType, AsrStepType}
import javax.inject.Inject
import models.{AsrStep, AsrStepSamples}
import play.api.libs.json.{JsValue, Json}
import repositories.AsrStepRepository

import scala.concurrent.{ExecutionContext, Future}

class CassandraAsrStepRepository @Inject()(config: Config,
                                           connection: CassandraConnection,
                                           implicit val ec: ExecutionContext)
  extends Table[CassandraAsrStepRepository, AsrStep] with AsrStepRepository {

  implicit val session: Session = connection.session
  implicit val space: KeySpace = connection.provider.space
  override val tableName: String = config.getString("db.tables.asrStep")

  import json.AsrJsonBindings.asrStepSamplesJsonFormat

  override def fromRow(r: Row): AsrStep = {
    AsrStep(
      Some(id(r)),
      Some(jobId(r)),
      stepIndex(r),
      AsrStepType.getForId(stepType(r)),
      status(r).map(AsrExecutionStatus.getForId),
      conf(r),
      samples(r).asOpt[AsrStepSamples].getOrElse(AsrStepSamples.getDefault),
      inputType(r).map(AsrJobStepInputType.getForId),
      inputJob(r),
      defaultModelName(r),
      duration(r)
    )
  }

  import utils.CassandraUtils._

  object id extends UUIDColumn with PartitionKey {
    override def name: String = "id"
  }

  object jobId extends UUIDColumn {
    override def name: String = "job_id"
  }

  object stepIndex extends IntColumn {
    override def name: String = "step_index"
  }

  object stepType extends IntColumn {
    override def name: String = "step_type"
  }

  object status extends OptionalIntColumn {
    override def name: String = "status"
  }

  object conf extends JsonColumn[JsValue] {
    override def name: String = "config"
  }

  object samples extends JsonColumn[JsValue] {
    override def name: String = "samples"
  }

  object inputType extends OptionalIntColumn {
    override def name: String = "input_type"
  }

  object inputJob extends OptionalUUIDColumn {
    override def name: String = "input_job"
  }

  object defaultModelName extends OptionalStringColumn {
    override def name: String = "default_model_name"
  }

  object duration extends OptionalLongColumn {
    override def name: String = "duration"
  }

  override def create(res: AsrStep): Future[UUID] = {
    val id = res.id.getOrElse(UUID.randomUUID())
    insert.value(_.id, id)
      .value(_.jobId, res.jobId.get)
      .value(_.stepIndex, res.index)
      .value(_.stepType, res.stepType.id)
      .value(_.status, res.status.map(_.id))
      .value(_.conf, res.config)
      .value(_.samples, Json.toJson(res.samples))
      .value(_.inputType, res.inputType.map(_.id))
      .value(_.inputJob, res.inputJob)
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .future()
      .map(_ => id)
  }

  override def removeForJob(id: UUID): Future[_] = {
    delete.where(_.jobId is id).future()
  }

  override def updateStep(res: AsrStep): Future[_] = {
    findForUuid(res.id.get) flatMap {
      case Some(_) =>
        update.where(_.id eqs res.id.get)
          .modify(_.status setTo res.status.map(_.id))
          .and(_.duration setTo res.duration)
          .future().map(_ => res)
      case _ =>
        logger.warn(s"trying to update non-existing step ($res)")
        Future.successful(None)
    }
  }

  override def findForJob(jobId: UUID): Future[Seq[AsrStep]] = {
    select.where(_.jobId is jobId)
      .allowFiltering()
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .fetch()
  }

  override def findAll(): Future[Seq[AsrStep]] = {
    select
      .allowFiltering()
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .fetch()
  }

  override def findForUuid(stepUuid: UUID): Future[Option[AsrStep]] = {
    select.where(_.id is stepUuid)
      .allowFiltering()
      .consistencyLevel_=(ConsistencyLevel.LOCAL_QUORUM)
      .one()
  }

  private def initialize(): Future[String] = {
    this.autocreate(space).future().map(_ => "autocreate table")
  }

  initialize()

}
