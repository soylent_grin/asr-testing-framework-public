package repositories.interpreters.cassandra

import com.datastax.driver.core._
import com.outworkers.phantom.builder.query.engine.CQLQuery
import com.outworkers.phantom.connectors._
import com.typesafe.config.Config
import javax.inject.{Inject, Provider}

class CassandraConnectionProvider @Inject()(config: Config) extends Provider[CassandraConnection] {
  override def get(): CassandraConnection = {
    val host = config.getString("db.host")
    val port = config.getInt("db.port")
    val username = config.getString("db.username")
    val password = config.getString("db.password")
    val keyspaceName = config.getString("db.keyspace")

    // Use the Cluster Builder if you need to add username/password and handle SSL or tweak the connection
    ContactPoint(host, port)
      .withClusterBuilder(
        _.withCredentials(username, password)
          .withPort(port)
          .withProtocolVersion(ProtocolVersion.V4)
      ).noHeartbeat().keySpace(
      keyspaceName,
      autoinit = true,
      Some(
        CQLQuery(s"CREATE KEYSPACE IF NOT EXISTS $keyspaceName WITH REPLICATION = { 'class': 'SimpleStrategy', 'replication_factor': 1 }")
      )
    )
    //)
    // добавить autoinit
    // ContactPoint.local.keySpace("phantom", autoinit = true, (session, keyspace) => {
    //  s"CREATE KEYSPACE ${keyspace.name} WITH REPLICATION ..."
    //})
  }
}
