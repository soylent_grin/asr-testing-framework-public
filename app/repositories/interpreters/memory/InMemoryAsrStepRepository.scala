package repositories.interpreters.memory

import java.util.UUID

import models.AsrStep
import repositories.AsrStepRepository

import scala.collection.mutable
import scala.concurrent.Future

class InMemoryAsrStepRepository extends AsrStepRepository {

  private val repo: mutable.Map[UUID, AsrStep] = mutable.Map()

  override def create(res: AsrStep): Future[UUID] = {
    val id = res.id.getOrElse(UUID.randomUUID())
    repo(id) = res
    Future.successful(id)
  }

  override def updateStep(res: AsrStep): Future[_] = {
    Future.successful(
      repo(res.id.get) = res
    )
  }

  override def findAll(): Future[Seq[AsrStep]] = {
    Future.successful(
      repo.values.toSeq
    )
  }

  override def findForJob(jobId: UUID): Future[Seq[AsrStep]] = {
    val a = repo.filter(_._2.jobId.contains(jobId))
      val b = a.values
    val c = b.toList
    Future.successful(c)
  }

  override def removeForJob(jobId: UUID): Future[_] = {
    Future.successful(repo.remove(jobId))
  }

  override def findForUuid(stepUuid: UUID): Future[Option[AsrStep]] = {
    val a = repo.filter(_._2.id.contains(stepUuid))
    val b = a.values
    val c = b.headOption
    Future.successful(c)
  }
}
