package repositories.interpreters.memory

import java.io.File

import com.google.inject.Inject
import com.typesafe.config.Config
import dtos.{NoiseGroupDto, NoiseSampleDto}
import org.apache.commons.io.FileUtils
import play.api.Logger
import repositories.NoiseRepository

import scala.collection.JavaConverters._
import scala.util.{Random, Try}

class InMemoryNoiseRepository @Inject()(config: Config) extends NoiseRepository {

  private val logger = Logger(getClass)

  private val rootDir = Try(config.getString(s"noises.rootDir")).toOption.map(new File(_))

  private val withHighOrderClusters = config.getBoolean(s"noises.withHighOrderClusters")

  logger.debug(s"scanning noise repository in $rootDir; withHighOrderClusters = ${withHighOrderClusters}")

  private val groups: Map[String, NoiseGroupDto] = scanRoot().map(g => (g.key, g)).toMap

  private val random = new Random

  val RANDOM_GROUP_KEY = "__random__"

  logger.debug(s"inited with ${groups.size} noise groups " +
    s"and ${getNoiseGroups.flatMap(_.samples).distinct.size} noise samples")

  override def getNoiseGroups: Seq[NoiseGroupDto] = {
    val seq = groups.values.toSeq.sortBy(_.key)
    if (seq.nonEmpty) {
      seq :+ NoiseGroupDto(RANDOM_GROUP_KEY, seq.flatMap(_.samples).distinct)
    } else {
      Nil
    }
  }

  override def getRandomSampleFromGroup(groupKey: String): NoiseSampleDto = {
    if (groups.isEmpty) {
      throw new IllegalArgumentException(s"not found any noise group, unable to choose random for key $groupKey")
    }
    val resultGroupKey = if (groupKey == RANDOM_GROUP_KEY) {
      val k = groups.keys.toSeq(random.nextInt(groups.size))
      logger.debug(s"using random noise group: $k")
      k
    } else {
      groupKey
    }
    groups.get(resultGroupKey) match {
      case Some(g) =>
        g.samples(random.nextInt(g.samples.length))
      case _ =>
        throw new IllegalArgumentException(s"not found noise group for key = $groupKey")
    }
  }

  // ---

  private def scanRoot(): Seq[NoiseGroupDto] = {
    logger.info(s"scanning root noise dir $rootDir...")
    rootDir match {
      case Some(r) =>
        if (r.exists()) {
          try {
            val directories = r.listFiles().filter(_.isDirectory)
            directories.flatMap(d => scanGroups(d, None))
          } catch {
            case t: Throwable =>
              logger.error(s"unexpected error while scanning root noise dir: ", t)
              Nil
          }
        } else {
          logger.info(s"not found root noise directory $r")
          Nil
        }
      case _ =>
        logger.debug(s"no root noise directory found; considering noises list as empty")
        Nil
    }
  }

  private def scanGroups(dir: File, rootKey: Option[String] = None): Seq[NoiseGroupDto] = {
    logger.info(s"scanning noise dir $dir...")
    try {
      val directories = dir.listFiles().filter(_.isDirectory)
      val samples = FileUtils.listFiles(dir, Array("wav"), withHighOrderClusters).asScala.toSeq

      val currentKey = Seq(rootKey, Some(dir.getName)).filter(_.isDefined).map(_.get).mkString(" / ")
      val currentGroup = if (samples.nonEmpty) {
        Some(NoiseGroupDto(
          currentKey,
          samples.map(new NoiseSampleDto(_))
        ))
      } else {
        None
      }
      val childrenGroups = directories.flatMap(d => scanGroups(d, Some(currentKey))).toList
      logger.debug(s"done scanning dir $dir; found ${samples.length} samples and ${childrenGroups.size} subgroups")
      if (currentGroup.isDefined) {
        currentGroup.get :: childrenGroups
      } else {
        childrenGroups
      }
    } catch {
      case t: Throwable =>
        logger.error(s"unexpected error while scanning noise dir $dir: ", t)
        Nil
    }
  }

}
