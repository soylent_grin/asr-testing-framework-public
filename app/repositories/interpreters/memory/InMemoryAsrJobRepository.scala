package repositories.interpreters.memory

import java.util.UUID

import com.google.inject.Inject
import enums.AsrExecutionStatus.AsrExecutionStatus
import models.AsrJob
import repositories.AsrJobRepository

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

class InMemoryAsrJobRepository @Inject()(implicit ec: ExecutionContext) extends AsrJobRepository {

  private val repo: mutable.Map[UUID, AsrJob] = mutable.Map()

  override def create(job: AsrJob): Future[UUID] = {
    repo(job.id) = job
    Future.successful(job.id)
  }

  override def updateJob(job: AsrJob): Future[AsrJob] = {
    repo(job.id) = job
    Future.successful(job)
  }

  override def deleteById(jobId: UUID): Future[_] = {
    repo.remove(jobId)
    Future.successful(None)
  }

  override def updateLabel(jobId: UUID, label: String): Future[_] = {
    find(jobId).flatMap(job => {
      updateJob(
        job.get.copy(
          label = Some(label)
        )
      )
    })
  }

  override def find(jobId: UUID): Future[Option[AsrJob]] = {
    Future.successful(repo.get(jobId))
  }

  override def findAll(): Future[Seq[AsrJob]] = {
    Future.successful(repo.values.toSeq)
  }

  override def findAllForUser(userId: String): Future[Seq[AsrJob]] = {
    findAll.map(_.filter(_.user == userId))
  }

  override def findForStatus(status: AsrExecutionStatus): Future[Seq[AsrJob]] = {
    Future.successful(repo.values.filter(_.status == status).toSeq)
  }

  override def findForStatuses(statuses: List[AsrExecutionStatus]): Future[Seq[AsrJob]] = {
    Future.successful(repo.values.filter(j => statuses.contains(j.status)).toSeq)
  }

}
