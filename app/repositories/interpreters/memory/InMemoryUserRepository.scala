package repositories.interpreters.memory

import com.typesafe.config.Config
import javax.inject.Inject
import models.User
import repositories.UserRepository

import scala.collection.concurrent.TrieMap
import scala.concurrent.{ExecutionContext, Future}

class InMemoryUserRepository @Inject()(config: Config)
                                      (implicit ec: ExecutionContext) extends UserRepository {

  private val users = TrieMap[String, User]()

  def find(id: String): Future[Option[User]] = Future {
    users.get(id)
  }

  def upsert(user: User): Future[User] = Future {
    users(user.id) = user
    user
  }

  def findAll(): Future[Seq[User]] = Future {
    users.values.toSeq
  }

}