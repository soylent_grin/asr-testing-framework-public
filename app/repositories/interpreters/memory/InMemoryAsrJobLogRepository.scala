package repositories.interpreters.memory

import java.io.File
import java.nio.file.{Files, Paths}
import java.util.{Date, UUID}

import com.google.inject.{Inject, Singleton}
import com.typesafe.config.Config
import play.api.Logger
import repositories.AsrJobLogRepository
import utils.DateTimeUtils

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class InMemoryAsrJobLogRepository @Inject()(config: Config,
                                            implicit val ec: ExecutionContext) extends AsrJobLogRepository {

  private val logger = Logger(getClass)

  private val logDirectory = config.getString("asr.logPath")

  init()

  private def init(): Unit = {
    logger.debug(s"initing; ensuring log directory")
    val d = new File(logDirectory)
    if (!d.exists) {
      d.mkdir
    }
  }

  override def saveLog(asrKey: String, stepId: UUID, file: File): Future[_] = {
    logger.debug(s"saving log file ${file.getAbsolutePath} for step $stepId")
    try {
      val path = generateLogName(asrKey, stepId).toPath
      Files.move(file.toPath, path)
      Future.successful(None)
    } catch {
      case t: Throwable =>
        logger.error(s"failed to save log: error is ${t.getMessage}. Possible lack of rights on log directory $logDirectory?")
        Future.failed(t)
    }
  }

  override def removeLog(asrKey: String, stepId: UUID): Future[_] = {
    logger.debug(s"removing log file of step $stepId (if presented)")
    val file = findLog(asrKey, stepId)
    if (file.isDefined) {
      Files.delete(file.get.toPath)
    }
    Future.successful(None)
  }

  override def getLog(asrKey: String, stepId: UUID): Future[Option[File]] = {
    logger.debug(s"getting log file for step $stepId")
    val file = findLog(asrKey, stepId)
    Future.successful(file)
  }


  private def generateLogName(asrKey: String, stepId: UUID): File = {
    val asrFolder = new File(s"$logDirectory/$asrKey")
    if (!asrFolder.exists()) {
      asrFolder.mkdir()
    }
    new File(s"$logDirectory/$asrKey", s"${DateTimeUtils.sdf.format(new Date())}-${stepId.toString}")
  }

  private def findLog(asrKey: String, stepId: UUID): Option[File] = {
    val asrFolder = new File(s"$logDirectory/$asrKey")
    if (asrFolder.exists()) {
      asrFolder.listFiles().find(f => f.getAbsolutePath.contains(stepId.toString))
    } else {
      None
    }
  }

}
