package repositories.interpreters.memory

import java.util.UUID

import models.AsrStepResult
import repositories.AsrStepResultRepository

import scala.collection.mutable
import scala.concurrent.Future

class InMemoryAsrStepResultRepository extends AsrStepResultRepository {

  private val repo: mutable.Map[UUID, AsrStepResult] = mutable.Map()

  override def create(res: AsrStepResult): Future[UUID] = {
    val id = res.id
    repo(res.stepId) = res.copy(id = id)
    Future.successful(id)
  }

  override def findForStep(stepId: UUID): Future[Option[AsrStepResult]] = {
    Future.successful(repo.get(stepId))
  }

  override def removeForStep(stepId: UUID): Future[_] = {
    Future.successful(repo.remove(stepId))
  }
}
