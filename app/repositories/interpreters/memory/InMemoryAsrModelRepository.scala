package repositories.interpreters.memory

import java.io.File
import java.nio.file.{Files, Path}
import java.util.{Date, UUID}

import com.google.inject.Inject
import com.typesafe.config.Config
import play.api.Logger
import repositories.AsrModelRepository
import utils.DateTimeUtils
import utils.assets.FileUtils

import scala.concurrent.Future
import scala.util.Try

class InMemoryAsrModelRepository @Inject()(config: Config) extends AsrModelRepository {

  private val pwd = Try(config.getString("asr.modelPath")).toOption

  private val logger = Logger(getClass)

  init()

  override def getModels(asrKey: String, stepId: UUID): Future[Option[Path]] = {
    val file = findFolder(asrKey, stepId)
    Future.successful(file.map(_.toPath))
  }

  override def removeModels(asrKey: String, stepId: UUID): Future[_] = {
    val dir = findFolder(asrKey, stepId)
    if (dir.isDefined) {
      logger.debug(s"deleting folder $dir recursively")
      FileUtils.deleteRecursively(dir.get)
    }
    Future.successful(None)
  }

  override def putModels(files: List[Path], asrKey: String, stepId: UUID): Future[_] = {
    val dir = generateFolderName(asrKey, stepId)
    if (!dir.exists()) {
      dir.mkdirs()
    } else {
      logger.warn(s"putting models to directory, that already exists (${dir.toPath}); is this expected?")
    }
    files.foreach(f => {
      FileUtils.move(f.toFile, new File(s"$dir/${f.toFile.getName}"))
    })
    Future.successful(None)
  }

  override def getDefaultModels(asrKey: String): Future[Seq[(String, Path)]] = {
    val file = new File(s"$getEnsuredPwd/defaults/$asrKey")
    if (file.exists()) {
      val isSingle = file.listFiles().exists(_.isFile) // for backward compatible
      if (isSingle) {
        Future.successful(Seq((file.getName, file.toPath)))
      } else {
        Future.successful(file.listFiles().filter(_.isDirectory).map(f => (f.getName, f.toPath)))
      }
    } else {
      Future.successful(Nil)
    }
  }

  // ---

  private def init(): Unit = {
    pwd match {
      case Some(_) =>
        logger.info(s"initing; ensuring current working directory")
        val path = new File(s"$getEnsuredPwd")
        if (!path.exists()) {
          path.mkdir()
        }
        logger.debug(s"done; ensuring default models directory")
        val defaultModelPath = new File(s"$getEnsuredPwd/defaults")
        if (!defaultModelPath.exists()) {
          defaultModelPath.mkdir()
        }
        logger.debug(s"done; there are ${Files.list(defaultModelPath.toPath).count()} default models")

      case _ =>
        logger.info(s"not found model dir; do nothing")
    }
  }

  private def generateFolderName(asrKey: String, stepId: UUID): File = {
    new File(s"$getEnsuredPwd/$asrKey", s"${DateTimeUtils.oneMoreSdf.format(new Date())}-${stepId.toString}")
  }

  private def findFolder(asrKey: String, stepId: UUID): Option[File] = {
    val asrFolder = new File(s"$getEnsuredPwd/$asrKey")
    if (asrFolder.exists()) {
      asrFolder.listFiles().find(f => f.getAbsolutePath.contains(stepId.toString))
    } else {
      None
    }
  }

  private def getEnsuredPwd: String = {
    pwd match {
      case Some(p) =>
        p
      case _ =>
        throw new IllegalStateException(s"working with models, but no root directory found; configure it with `asr.modelPath` key")
    }
  }
}
