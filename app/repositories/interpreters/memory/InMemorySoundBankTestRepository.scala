package repositories.interpreters.memory

import java.io.File

import com.google.inject.Inject
import com.typesafe.config.Config
import dtos.RecordSampleDto
import models.{SoundbankFolder, SoundbankSample}
import play.api.Logger
import play.api.libs.json.{JsObject, Json}
import repositories.SoundbankRepository
import services.AudioProcessingService
import utils.UserContext

import scala.collection.mutable
import scala.concurrent.Future
import scala.io.Source
import scala.util.Try

class InMemorySoundBankTestRepository @Inject()(config: Config) extends SoundbankRepository {

  private val samples = mutable.Map[String, SoundbankSample]()

  private implicit val logger = Logger(getClass)

  init()

  private def init(): Unit = {
    scanFolders()
  }

  def findSample(id: String): Future[Option[SoundbankSample]] = {
    Future.successful(
      samples.get(id)
    )
  }

  override def findSamples(ids: Seq[String]): Future[Seq[SoundbankSample]] = ???

  override def registerFolder(folder: SoundbankFolder)
                             (implicit userContext: UserContext): Future[SoundbankFolder] = ???

  override def findAllSamplesInFolder(key: String)
                                     (implicit userContext: UserContext): Future[Seq[SoundbankSample]] = ???

  override def findFolder(key: String)
                         (implicit userContext: UserContext): Future[Option[SoundbankFolder]] = ???

  override def findAllFolders()
                             (implicit userContext: UserContext): Future[Seq[SoundbankFolder]] = ???

  override def scanFolder(d: File): SoundbankFolder = ???

  override def samplesToDataset(samples: Seq[SoundbankSample], datasetType: String): File = ???

  override def findSample(id: String)(implicit userContext: UserContext): Future[Option[SoundbankSample]] = ???

  override def uploadFolder(folder: File, key: String)(implicit userContext: UserContext): Future[SoundbankFolder] = ???

  private def scanFolders(file: Option[File] = None): Unit = {
    val folder = file.getOrElse(new File(config.getString("soundbank.testPath")))
    logger.trace(s"Searching for samples in ${folder.getAbsolutePath}")

    for (file <- folder.listFiles) {
      if (file.exists){
        if (file.isDirectory) {
          scanFolders(Some(file))
        } else if (file.isFile) {
          val wrappedSource = Try(Source.fromFile(file, "UTF-8"))
          if (wrappedSource.isSuccess){
            val source = wrappedSource.get
            val meta = Json.parse(source.getLines().mkString(" ")).asInstanceOf[JsObject]
            val sampleDirectId = (meta \ "id").asOpt[String]
            val sampleId = if (sampleDirectId.isEmpty){
              file.getName
            } else {
              sampleDirectId.get
            }

            logger.trace(s"found sample $sampleId. Adding...")

            samples(sampleId) = SoundbankSample(
              Some(sampleId),
              sampleId,
              (meta \ "ref").asOpt[List[String]].getOrElse(List[String]()),
              (meta \ "duration").asOpt[Double].getOrElse(0),
              0,
              null,
              meta
            )

            source.close()
          } else {
            logger.error(f"Error when reading sample: ${wrappedSource.get}")
          }
        }
      } else {
        logger.error(s"not found file ${file.getAbsolutePath}")
      }
    }
  }

  override def removeFolder(key: String)(implicit userContext: UserContext): Future[Unit] = ???

  override def registerRecordedSample(recordFolder: RecordSampleDto)(implicit userContext: UserContext): Future[SoundbankSample] = ???

  override def registerEmptyFolder(key: String)(implicit userContext: UserContext): Future[SoundbankFolder] = ???
}
