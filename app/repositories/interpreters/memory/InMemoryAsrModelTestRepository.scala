package repositories.interpreters.memory

import java.io.File
import java.nio.file.Path
import java.util.UUID

import com.google.inject.Inject
import com.typesafe.config.Config
import play.api.Logger
import repositories.AsrModelRepository

import scala.concurrent.Future

class InMemoryAsrModelTestRepository @Inject()(config: Config) extends AsrModelRepository {

  private val pwd = config.getString("asr.modelPath")

  private val logger = Logger(getClass)

  init()

  override def getModels(asrKey: String, stepId: UUID): Future[Option[Path]] = ???

  override def removeModels(asrKey: String, stepId: UUID): Future[_] = ???

  override def putModels(files: List[Path], asrKey: String, stepId: UUID): Future[_] = ???

  override def getDefaultModels(asrKey: String): Future[Seq[(String, Path)]] = ???

  // ---

  private def init(): Unit = ???

  private def generateFolderName(asrKey: String, stepId: UUID): File = ???

  private def findFolder(asrKey: String, stepId: UUID): Option[File] = ???
}
