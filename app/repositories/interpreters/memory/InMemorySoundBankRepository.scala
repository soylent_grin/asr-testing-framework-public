package repositories.interpreters.memory

import java.io.{File, FileInputStream, IOException}
import java.lang.reflect.Modifier
import java.nio.charset.Charset
import java.nio.file.{Files, Path, Paths}
import java.util.zip.ZipFile

import com.google.inject.Inject
import com.typesafe.config.Config
import dtos.RecordSampleDto
import enums.UserRole
import extensions.soundbank.DatasetAdapter
import models.{SoundbankFolder, SoundbankSample, User}
import org.apache.commons.io.{FileUtils, FilenameUtils}
import org.reflections.Reflections
import play.api.Logger
import play.api.inject.Injector
import play.api.libs.json.Json
import repositories.SoundbankRepository
import services.{AppConfigService, AudioProcessingService}
import utils.{AudioUtils, UserContext, ZipUtils}

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}

case class NotDefaultDatasetTypeException() extends Exception

class InMemorySoundBankRepository @Inject()(config: Config,
                                            injector: Injector,
                                            appConfigService: AppConfigService,
                                            audioProcessingService: AudioProcessingService)
                                           (implicit ec: ExecutionContext) extends SoundbankRepository {

  private implicit val logger = Logger(getClass)

  private val publicRepo = mutable.Map[String, SoundbankFolder]()
  private val privateRepo = mutable.Map[String, mutable.Map[String, SoundbankFolder]]()
  private val pwd = config.getString("soundbank.rootPath")
  private val publicFoldersRoot = Paths.get(pwd, "public")
  private val privateFoldersRoot = Paths.get(pwd, "private")
  // private val expectedSampleRate: Float = 16000
  private val reflections = new Reflections("extensions.soundbank")
  private val datasetAdapters = mutable.Map[String, DatasetAdapter]()

  logger.info(s"using public root = $publicFoldersRoot, private root = $privateFoldersRoot")

  init()

  override def getUserFolderRoot(userId: String): Path = {
    ensurePrivateFolderRoot(userId)
    Paths.get(privateFoldersRoot.toString, userId)
  }

  override def findSample(id: String)
                         (implicit userContext: UserContext): Future[Option[SoundbankSample]] = {
    findAllFolders().map(folders => {
      folders.flatMap(_.samples).find(s => {
        s.id.contains(id)
      })
    })
  }

  override def findSample(folder: String, key: String)
                         (implicit userContext: UserContext): Future[Option[SoundbankSample]] = {
    findAllFolders().map(folders => {
      folders.find(_.key == folder) match {
        case Some(f) =>
          f.samples.find(_.key == key)
        case _ =>
          None
      }
    })
  }

  // this method supposed to be called without user context (e.g. during job execution)
  override def findSamples(ids: Seq[String]): Future[Seq[SoundbankSample]] = {
    findLiterallyAllFolders().map(folders => {
      folders.flatMap(_.samples).filter(s => {
        s.id.isDefined && ids.contains(s.id.get)
      })
    })
  }

  override def registerFolder(folder: SoundbankFolder)
                             (implicit userContext: UserContext): Future[SoundbankFolder] = {
    logger.debug(s"registering new folder ${folder.key} with ${folder.samples.size} samples for user = ${userContext.user}")
    ensurePrivateFolderRoot(userContext.user.id)
    ensureUserCanCreateFolder(userContext.user.id)
    // probably I should rollback on exception.. but maybe this would be fine
    val newFolder = registerPrivateFolder(folder)
    Future.successful(newFolder)
  }

  override def findAllSamplesInFolder(key: String)
                                     (implicit userContext: UserContext): Future[Seq[SoundbankSample]] = {
    findAllFolders.map(allFolders => {
      allFolders.find(_.key == key).map(_.samples).getOrElse(Nil)
    })
  }

  override def findFolder(key: String)
                         (implicit userContext: UserContext): Future[Option[SoundbankFolder]] = {
    findAllFolders.map(allFolders => {
      allFolders.find(_.key == key)
    })
  }

  override def findAllFolders()(implicit userContext: UserContext): Future[Seq[SoundbankFolder]] = {
    Future.successful(
      publicRepo.values.toSeq ++
      (userContext.user.role match {
        case UserRole.admin =>
          privateRepo.values.flatMap(_.values)
        case _ =>
          privateRepo.getOrElse(userContext.user.id, Map()).values
      }).toSeq)
  }

  private def findLiterallyAllFolders(): Future[Seq[SoundbankFolder]] = {
    Future.successful(
      publicRepo.values.toSeq ++
      privateRepo.values.flatMap(_.values)
    )
  }

  override def scanFolder(d: File): SoundbankFolder = {
    val meta: mutable.Map[String, String] = mutable.Map(getDatasetMeta(d).toSeq: _*)
    val datasetType = meta.getOrElse("datasetType", config.getString("soundbank.testDataset"))
    datasetAdapters.get(datasetType) match {
      case Some(a) =>
        val samples = a.fromFolderToSamples(d)
        val differentSampleRates = samples.map(_.sampleRate).distinct
        val summarySampleRate = differentSampleRates.size match {
          case 1 =>
            differentSampleRates.head.toString
          case _ =>
            "mixed"
        }
        SoundbankFolder(
          d.getName,
          d.getName,
          (
            meta + ("sampleRate" -> summarySampleRate)
          ).toMap,
          d,
          a.fromFolderToSamples(d)
        )
      case _ =>
        throw new IllegalArgumentException(s"not found dataset adapter for type $datasetType; ignoring this folder")
    }
  }

  override def samplesToDataset(samples: Seq[SoundbankSample], datasetType: String): File = {
    logger.info(s"trying to convert ${samples.size} samples to $datasetType dataset type...")
    datasetAdapters.get(datasetType) match {
      case Some(a) =>
        a.fromSamplesToFolder(samples)
      case _ =>
        throw new IllegalArgumentException(s"not found adapter for dataset type ${datasetType}")
    }
  }

  override def uploadFolder(folder: File, key: String)(implicit userContext: UserContext): Future[SoundbankFolder] = {
    logger.info(s"uploading new private folder $folder for user ${userContext.user}")
    val targetFolder = Paths.get(ensurePrivateFolderRoot(userContext.user.id).toString)
    val targetPath = Paths.get(targetFolder.toString, FilenameUtils.getBaseName(folder.getAbsolutePath))
    val desiredPath = Paths.get(targetFolder.toString, key)
    ZipUtils.unzipDirectory(new FileInputStream(folder), targetPath)
    logger.info(s"done unzipping; renaming $targetPath to $desiredPath")
    targetPath.toFile.renameTo(desiredPath.toFile)
    logger.info(s"done renaming; scanning folder...")
    val outputFolder = scanFolder(desiredPath.toFile).copy(
      user = Some(userContext.user.id)
    )
    registerFolder(outputFolder)
  }

  override def removeFolder(key: String)(implicit userContext: UserContext): Future[Unit] = {
    logger.info(s"removing private folder $key for user ${userContext.user}")
    val userId = userContext.user.id
    privateRepo.getOrElse(userId, Map[String, SoundbankFolder]()).get(key) match {
      case Some(f) =>
        logger.debug(s"removed registered repo; removing file")
        val path = Paths.get(privateFoldersRoot.toString, userId, f.title)
        FileUtils.deleteDirectory(path.toFile)
        privateRepo(userId).remove(f.key)
        logger.debug(s"done removing file")
        Future.unit
      case _ =>
        throw new IllegalArgumentException(s"not found folder with key = $key")
    }
  }

  override def registerRecordedSample(recordSample: RecordSampleDto)
                                     (implicit userContext: UserContext): Future[SoundbankSample] = {
    val userId = userContext.user.id

    findFolder(recordSample.folder).flatMap(res => {
      if (res.isEmpty) {
        throw new RuntimeException(s"unable to register recorded sample; folder ${recordSample.folder} does not exists")
      }
      val datasetType = res.get.meta.getOrElse("datasetType", "default")
      if (datasetType != "default") {
        throw NotDefaultDatasetTypeException()
      }
      val folderDirectory = Paths.get(privateFoldersRoot.toString, userId, res.get.title)
      val sampleDirectory = Paths.get(folderDirectory.toString, recordSample.name)
      if (!sampleDirectory.toFile.mkdir()) {
        throw new RuntimeException(s"failed to ensure target sample directory; possible out-of-space?")
      }
      logger.debug(s"copying converted recorded audio to $sampleDirectory'")
      FileUtils.moveFile(
        audioProcessingService.convertToWave(recordSample.audio),
        Paths.get(sampleDirectory.toString, s"${recordSample.name}.wav").toFile
      )
      logger.debug(s"done; creating transcription file...")
      val transcriptionFile = Paths.get(sampleDirectory.toString, s"${recordSample.name}.txt")
      Files.createFile(transcriptionFile)
      FileUtils.write(transcriptionFile.toFile, recordSample.transcription, Charset.defaultCharset())
      logger.debug(s"done; rescanning folder")
      registerFolder(
        scanFolder(folderDirectory.toFile)
      ).map(newFolder => {
        newFolder.samples.find(s => {
          s.key == recordSample.name
        }).get
      })
    })
  }

  override def registerEmptyFolder(key: String)(implicit userContext: UserContext): Future[SoundbankFolder] = {
    val userId = userContext.user.id

    ensurePrivateFolderRoot(userContext.user.id)
    ensureUserCanCreateFolder(userContext.user.id)

    val folderDirectory = Paths.get(privateFoldersRoot.toString, userId, key)
    if (folderDirectory.toFile.exists()) {
      throw new RuntimeException(s"soundbank folder for path $folderDirectory already exists")
    }
    logger.debug(s"okay, folder $folderDirectory does not exists; creating...")
    if (!folderDirectory.toFile.mkdir()) {
      throw new RuntimeException(s"failed to ensure target directory; possible out-of-space?")
    }
    logger.debug(s"done; creating metadata file...")
    val metaFile = Paths.get(folderDirectory.toString, s"info.meta")
    Files.createFile(metaFile)
    FileUtils.write(
      metaFile.toFile,
      "recordedBy=user",
      Charset.defaultCharset()
    )
    logger.debug(s"done; registering folder...")
    registerFolder(
      scanFolder(folderDirectory.toFile)
    )
  }

  // ---

  private def init(): Unit = {
    discoverAdapters()
    scanFolders()
  }

  private def ensurePrivateFolderRoot(userId: String): Path = {
    logger.info(s"ensuring root folder directory for user = $userId")
    val root = Paths.get(privateFoldersRoot.toString, userId)
    if (!privateRepo.contains(userId)) {
      FileUtils.forceMkdir(root.toFile)
      privateRepo(userId) = mutable.Map()
    }
    root
  }

  private def ensureUserCanCreateFolder(userId: String): Unit = {
    val limit = appConfigService.getConfig().maxPrivateFoldersPerUser
    if (
      privateRepo.getOrElse(userId, Map()).size >= limit
    ) {
      throw new RuntimeException(s"unable to register new folder; limit is $limit")
    }
  }

  private def discoverAdapters(): Unit = {
    logger.info(s"initing; reading list of dataset adapters")
    reflections.getSubTypesOf(classOf[DatasetAdapter]).asScala.map(adapterClass => {
      (adapterClass, adapterClass.getAnnotation(classOf[extensions.soundbank.DatasetAdapterInfo]))
    }).filter(pair => {
      pair._2 != null && !Modifier.isAbstract(pair._1.getModifiers)
    }).toList.foreach(pair => {
      logger.debug(s"found new dataset adapter of class ${pair._1.getName}")
      datasetAdapters(pair._2.key()) = injector.instanceOf(pair._1)
    })

    logger.info(s"done; found ${datasetAdapters.size} dataset adapters")
  }

  private def scanFolders(): Unit = {
    logger.info(s"scanning all datasets in $pwd directory...")

    // public
    scanFolderGroup(publicFoldersRoot).foreach(folder => {
      publicRepo(folder.key) = folder
      logger.debug(s"registered public folder for key = ${folder.key}")
    })
    logger.info(s"done; found ${publicRepo.size} public folders")

    // private
    val privateFoldersDir = privateFoldersRoot.toFile
    if (privateFoldersDir.exists() && privateFoldersDir.isDirectory) {
      privateFoldersDir.listFiles().filter(_.isDirectory).foreach(d => {
        val userId = d.getName
        privateRepo(userId) = mutable.Map()
        logger.trace(s"found private folder for user = $userId; scanning")
        scanFolderGroup(d.toPath).foreach(folder => {
          // creating fake user context
          registerPrivateFolder(folder)(UserContext(User(userId)))
        })
        logger.info(s"done; found ${privateRepo(d.getName).size} private folders for user ${d.getName}")
      })
      logger.info(s"done; found ${privateRepo.size} users with private folders")
    } else {
      logger.warn(s"not found private folders dir ${privateFoldersDir.getAbsolutePath} or it is not directory")
    }
  }

  private def scanFolderGroup(path: Path): List[SoundbankFolder] = {
    logger.info(s"scanning all datasets in $path directory...")
    val d = path.toFile
    val folders = ListBuffer[SoundbankFolder]()
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isDirectory).foreach(d => {
        logger.trace(s"found directory ${d.getAbsolutePath}; trying to process it as folder...")
        try {
          folders += scanFolder(d)
        } catch {
          case t: Throwable =>
            logger.warn(s"failed to process folder, it would be ignored; error is: ", t)
        }
      })
    } else {
      logger.warn(s"not found directory $path; considering it as empty")
    }
    folders.toList
  }

  private def getDatasetMeta(file: File): Map[String, String] = {
    var meta: mutable.Map[String, String] = mutable.Map()
    if (file.isDirectory) {
      file.listFiles().foreach(f => {
        if (f.getAbsolutePath.endsWith(".meta")) {
          logger.trace(s"found meta file for dataset; let's find out dataset type")
          try {
            val content = org.apache.commons.io.FileUtils.readFileToString(f, "UTF-8")
            content.split("\n").foreach(line => {
              try {
                val parts = line.split("=")
                meta(parts(0)) = parts(1)
              } catch {
                case t: Throwable =>
                  logger.warn(s"incorrect meta line: $line; error is ${t.getMessage}")
              }
            })
          } catch {
            case t: IOException =>
              logger.warn(s"failed to read meta file; is it exists?")
            case t: Throwable =>
              logger.error(s"unexpected exception: ", t)
          }
        }
      })
    }
    meta.toMap
  }

  private def addUserId(userId: String, key: String): String = {
    s"$userId-$key"
  }

  private def registerPrivateFolder(folder: SoundbankFolder)
                                   (implicit userContext: UserContext): SoundbankFolder = {
    val userId = userContext.user.id
    if (!privateRepo.contains(userId)) {
      privateRepo(userId) = mutable.Map()
    }
    val folderKey = addUserId(userId, folder.key)
    val newFolder = folder.copy(
      user = Some(userId),
      key = folderKey,
      samples = folder.samples.map(s => {
        s.copy(
          id = Some(
            addUserId(userId, s.id.getOrElse(throw new IllegalStateException(s"not found Id for sample $s")))
          )
        )
      })
    )
    privateRepo(userId)(folderKey) = newFolder
    logger.debug(s"registered private folder for key = $folderKey")
    newFolder
  }

}
