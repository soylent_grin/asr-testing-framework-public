package repositories.interpreters.memory

import java.io.File
import java.util.UUID

import com.google.inject.{Inject, Singleton}
import com.typesafe.config.Config
import play.api.Logger
import repositories.AsrJobLogRepository

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class InMemoryAsrJobLogTestRepository @Inject()(config: Config,
                                                implicit val ec: ExecutionContext) extends AsrJobLogRepository {

  private val logger = Logger(getClass)

  private val logDirectory = config.getString("asr.logPath")

  init()

  private def init(): Unit = ???

  override def saveLog(asrKey: String, stepId: UUID, file: File): Future[_] = ???

  override def removeLog(asrKey: String, stepId: UUID): Future[_] = ???

  override def getLog(asrKey: String, stepId: UUID): Future[Option[File]] = ???


  private def generateLogName(asrKey: String, stepId: UUID): File = ???

  private def findLog(asrKey: String, stepId: UUID): Option[File] = ???
}
