package repositories

import models.User

import scala.concurrent.Future

trait UserRepository {
  def find(id: String): Future[Option[User]]
  def findAll(): Future[Seq[User]]
  def upsert(user: User): Future[User]
}