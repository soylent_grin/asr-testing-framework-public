package repositories

import java.util.UUID

import enums.AsrExecutionStatus.AsrExecutionStatus
import models.AsrJob

import scala.concurrent.Future

trait AsrJobRepository {

  def create(job: AsrJob): Future[UUID] = ???

  def updateJob(job: AsrJob): Future[AsrJob] = ???

  def find(jobId: UUID): Future[Option[AsrJob]] = ???

  def findAll(): Future[Seq[AsrJob]] = ???

  def findAllForUser(userId: String): Future[Seq[AsrJob]] = ???

  def findForStatus(status: AsrExecutionStatus): Future[Seq[AsrJob]] = ???

  def findForStatuses(statuses: List[AsrExecutionStatus]): Future[Seq[AsrJob]] = ???

  def deleteById(jobId: UUID): Future[_] = ???

  def updateLabel(jobId: UUID, label: String): Future[_]

}
