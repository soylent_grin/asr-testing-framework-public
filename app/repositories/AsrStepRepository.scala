package repositories

import java.util.UUID

import models.AsrStep

import scala.concurrent.Future

trait AsrStepRepository {

  def create(res: AsrStep): Future[UUID] = ???

  def updateStep(res: AsrStep): Future[_] = ???

  def findAll(): Future[Seq[AsrStep]]

  def findForJob(jobId: UUID): Future[Seq[AsrStep]] = ???

  def findForUuid(stepUuid: UUID): Future[Option[AsrStep]] = ???

  def removeForJob(jobId: UUID): Future[_] = ???

}
