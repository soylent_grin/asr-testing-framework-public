package repositories

import java.util.UUID

import models.AsrStepResult

import scala.concurrent.Future

trait AsrStepResultRepository {

  def create(res: AsrStepResult): Future[UUID] = ???

  def findForStep(stepId: UUID): Future[Option[AsrStepResult]] = ???

  def removeForStep(stepId: UUID): Future[_] = ???

}
