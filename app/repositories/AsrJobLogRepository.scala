package repositories

import java.io.File
import java.util.UUID

import scala.concurrent.Future

trait AsrJobLogRepository {

  def saveLog(asrKey: String, stepId: UUID, file: File): Future[_]

  def getLog(asrKey: String, stepId: UUID): Future[Option[File]]

  def removeLog(asrKey: String, stepId: UUID): Future[_]

}
