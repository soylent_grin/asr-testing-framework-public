package repositories

import java.nio.file.Path
import java.util.UUID

import scala.concurrent.Future

trait AsrModelRepository {

  def putModels(directory: List[Path], asrKey: String, stepId: UUID): Future[_]

  def getModels(asrKey: String, stepId: UUID): Future[Option[Path]]

  def removeModels(asrKey: String, stepId: UUID): Future[_]

  def getDefaultModels(asrKey: String): Future[Seq[(String, Path)]]

}
