package beans

import java.io.File

import com.google.inject.{ImplementedBy, Inject}
import dtos.{AugmentationParamsDto, RecordSampleDto}
import javax.inject.Singleton
import models.{SoundbankFolder, SoundbankSample, User}
import org.apache.commons.io.FileUtils
import play.api.Logger
import repositories.SoundbankRepository
import services.AudioProcessingService
import utils.UserContext

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[SoundbankBeanImpl])
trait SoundbankBean {

  def getFolderList(implicit userContext: UserContext): Future[Seq[SoundbankFolder]]

  def getSample(id: String)(implicit userContext: UserContext): Future[Option[SoundbankSample]]

  def processFolder(folderKey: String, scenarioKey: String, params: AugmentationParamsDto)
                   (implicit userContext: UserContext): Future[SoundbankFolder]

  def processFolder(folderKey: String,
                    scenarioKey: String,
                    params: AugmentationParamsDto,
                    user: String): Future[SoundbankFolder]

  def getFolderSamples(key: String)
                      (implicit userContext: UserContext): Future[Seq[SoundbankSample]]

  def getSample(folder: String, key: String)
               (implicit userContext: UserContext): Future[Option[SoundbankSample]]

  def getSamples(ids: List[String])
               (implicit userContext: UserContext): Future[List[SoundbankSample]]

  def samplesToDataset(samples: Seq[SoundbankSample], datasetType: String): File

  def uploadFolder(folder: File, key: String)(implicit userContext: UserContext): Future[SoundbankFolder]

  def removeFolder(key: String)(implicit userContext: UserContext): Future[Unit]

  def registerRecordedSample(recordFolder: RecordSampleDto)
                            (implicit userContext: UserContext): Future[SoundbankSample]

  def registerEmptyFolder(key: String)
                         (implicit userContext: UserContext): Future[SoundbankFolder]

}

@Singleton
class SoundbankBeanImpl @Inject()(soundbankRepository: SoundbankRepository,
                                  audioProcessingService: AudioProcessingService,
                               implicit val ec: ExecutionContext) extends SoundbankBean {

  implicit val logger = Logger(getClass)

  override def getFolderList(implicit userContext: UserContext): Future[Seq[SoundbankFolder]] = {
    soundbankRepository.findAllFolders()
  }

  override def getSample(id: String)(implicit userContext: UserContext): Future[Option[SoundbankSample]] = {
    soundbankRepository.findSample(id)
  }

  override def getSamples(ids: List[String])(implicit userContext: UserContext): Future[List[SoundbankSample]] = {
    soundbankRepository.findSamples(ids).map(_.toList)
  }

  override def processFolder(folderKey: String,
                             scenarioKey: String,
                             params: AugmentationParamsDto)
                            (implicit userContext: UserContext): Future[SoundbankFolder] = this.synchronized {
    logger.info(s"trying to process folder $folderKey with scenario $scenarioKey; at first, copying entire folder")
    val newKey = params.title.getOrElse(s"$scenarioKey-$folderKey")
    soundbankRepository.findFolder(newKey) flatMap {
      case Some(_) =>
        throw new IllegalArgumentException(s"folder for key $newKey already exist")
      case _ =>
        soundbankRepository.findFolder(folderKey) flatMap {
          case Some(f) =>
            val inputFolder = f.file
            val outputFolderName = params.title.getOrElse(s"$scenarioKey-${inputFolder.getName}")
            val outputFolderPath = s"${soundbankRepository.getUserFolderRoot(userContext.user.id)}/$outputFolderName"
            val outputFolderFile = new File(outputFolderPath)
            if (outputFolderFile.exists()) {
              throw new IllegalArgumentException(s"folder for key ${outputFolderName} already exist")
            }
            try {
              FileUtils.copyDirectory(inputFolder, outputFolderFile)
              logger.debug(s"done; scanning target folder")
              var outputFolder: SoundbankFolder = null
              try {
                outputFolder = soundbankRepository.scanFolder(outputFolderFile)
                logger.info("done; processing each sample of new folder")
                try {
                  audioProcessingService.process(outputFolder.samples, scenarioKey, params)
                } catch {
                  case t: Throwable=>
                    logger.error(s"failed to run scenario $scenarioKey; this is unexpected, performing rollback; error: ${t.getMessage}")
                    FileUtils.deleteDirectory(outputFolderFile)
                    throw t
                }
              } catch {
                case t: Throwable =>
                  logger.error(s"failed to scan output folder; this is unexpected")
                  throw t
              }
              logger.info(s"done processing; scanning folder again in case of audio files length change...")
              outputFolder = soundbankRepository.scanFolder(outputFolderFile)
              soundbankRepository.registerFolder(outputFolder.copy(user = Some(userContext.user.id)))
            } catch {
              case t: Throwable =>
                logger.error(s"could not process dataset; error is ${t.getMessage}")
                throw t
            }
          case _ =>
            throw new IllegalArgumentException(s"not found folder for key $folderKey")
        }
    }

  }

  override def processFolder(folderKey: String,
                             scenarioKey: String,
                             params: AugmentationParamsDto,
                             user: String): Future[SoundbankFolder] = {
    implicit val fakeUserContext: UserContext = UserContext(User(user))
    processFolder(folderKey, scenarioKey, params)
  }

  override def getFolderSamples(key: String)
                               (implicit userContext: UserContext): Future[Seq[SoundbankSample]] = {
    soundbankRepository.findFolder(key) map {
      case Some(f) =>
        f.samples
      case _ =>
        Nil
    }
  }

  override def samplesToDataset(samples: Seq[SoundbankSample], datasetType: String): File = {
    soundbankRepository.samplesToDataset(samples, datasetType)
  }

  override def getSample(folder: String, key: String)
                        (implicit userContext: UserContext): Future[Option[SoundbankSample]] = {
    soundbankRepository.findSample(folder, key)
  }

  override def uploadFolder(folder: File, key: String)(implicit userContext: UserContext): Future[SoundbankFolder] = this.synchronized {
    soundbankRepository.findFolder(key) flatMap {
      case Some(_) =>
        throw new IllegalArgumentException(s"folder for key $key already exist")
      case _ =>
        soundbankRepository.findAllFolders().flatMap(allFolders => {
          if (allFolders.exists(_.key == key)) {
            throw new IllegalArgumentException(s"folder with key = $key already registered")
          }
          soundbankRepository.uploadFolder(folder, key)
        })
    }
  }

  override def removeFolder(key: String)(implicit userContext: UserContext): Future[Unit] = this.synchronized {
    soundbankRepository.findFolder(key) flatMap {
      case Some(f) =>
        if (f.user.isDefined && f.user.contains(userContext.user.id)) {
          soundbankRepository.removeFolder(key)
        } else {
          Future.failed(new RuntimeException(s"not permitted to delete public folder $key"))
        }
      case _ =>
        Future.failed(new RuntimeException(s"unknown folder $key; unable to remove"))
    }

  }

  override def registerRecordedSample(recordSample: RecordSampleDto)(implicit userContext: UserContext): Future[SoundbankSample] = {
    logger.info(s"adding new recorded sample: $recordSample by user ${userContext.user}")
    soundbankRepository.registerRecordedSample(recordSample)
  }

  override def registerEmptyFolder(key: String)(implicit userContext: UserContext): Future[SoundbankFolder] = {
    logger.info(s"registering new empty folder for key $key")
    soundbankRepository.registerEmptyFolder(key)
  }

}


