package beans

import java.util.UUID

import actors.AsrExecutorActor.RunJob
import akka.actor.ActorSystem
import com.google.inject.{ImplementedBy, Inject}
import dtos.AsrJobDto
import enums.{AsrExecutionStatus, AsrStepSamplesType, UserRole}
import javax.inject.Singleton
import models.{AsrJob, AsrStepResult}
import play.api.Logger
import play.api.libs.json.Json
import repositories._
import services.metrics.AsrMetricService
import services.{AppConfigService, AsrExecutorActorFactoryService, AsrExecutorService, MessageBrokerService}
import utils.UserContext

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[AsrJobBeanImpl])
trait AsrJobBean {

  def getJobs(implicit userContext: UserContext): Future[Seq[AsrJob]]

  def getJobDtos(implicit userContext: UserContext): Future[Seq[AsrJobDto]]

  def getJob(id: UUID): Future[Option[AsrJobDto]]

  def getStepResult(id: UUID)(implicit userContext: UserContext): Future[Option[AsrStepResult]]

  def createJob(job: AsrJobDto)(implicit userContext: UserContext): Future[AsrJobDto]

  def removeJob(jobId: UUID)(implicit userContext: UserContext): Future[_]

  def updateJob(job: AsrJobDto): Future[AsrJob]

  def terminateJob(id: UUID)(implicit userContext: UserContext): Future[_]

  def updateJobLabel(id: UUID, label: String)(implicit userContext: UserContext): Future[_]

  def launchJob(id: UUID)(implicit  userContext: UserContext): Unit

}

@Singleton
class AsrJobBeanImpl @Inject()(asrJobRepository: AsrJobRepository,
                               appConfigService: AppConfigService,
                               asrStepRepository: AsrStepRepository,
                               asrStepResultRepository: AsrStepResultRepository,
                               asrExecutorService: AsrExecutorService,
                               asrMetricService: AsrMetricService,
                               messageBrokerService: MessageBrokerService,
                               asrExecutorActorFactoryService: AsrExecutorActorFactoryService,
                               asrJobLogRepository: AsrJobLogRepository,
                               asrModelRepository: AsrModelRepository,
                               implicit val system: ActorSystem,
                               implicit val ec: ExecutionContext) extends AsrJobBean {

  private val logger = Logger(getClass)

  import json.AsrJsonBindings._

  recoverRunningJobs()
  launchWaitingJobs()

  private def recoverRunningJobs(): Unit = {
    logger.debug(s"recovering running jobs...")
    asrJobRepository.findForStatuses(
      List(
        AsrExecutionStatus.running
      )
    ).map(res => {
      Future.sequence(
        res.map(j => {
          updateJob(
            j.copy(
              status = AsrExecutionStatus.terminated
            )
          )
        })
      ).map(_ => {
        logger.info(s"cleaned up ${res.size} running / waiting jobs")
      })
    })
  }

  private def launchWaitingJobs(): Unit = {
    logger.debug(s"finding and launching idle jobs")
    asrJobRepository.findForStatuses(
      List(
        AsrExecutionStatus.waiting
      )
    ).map(res => {
      if (res.nonEmpty) {
        logger.info(s"found ${res.size} waiting jobs; running...")
        res.map(job => {
          getJob(job.id).map(dto => launchAllowedJob(dto.get))
        })
      }
    })
  }

  override def getJobs(implicit userContext: UserContext): Future[Seq[AsrJob]] = {
    if (userContext.user.role == UserRole.admin) {
      asrJobRepository.findAll()
    } else {
      asrJobRepository.findAllForUser(userContext.user.id)
    }
  }

  override def getJobDtos(implicit userContext: UserContext): Future[Seq[AsrJobDto]] = {
    getJobs.flatMap(jobs => {
      asrStepRepository.findAll().map(steps => {
        jobs.map(j => {
          AsrJobDto.fromModel(j, steps.filter(s => s.jobId.isDefined && s.jobId.get == j.id))
        })
      })
    })
  }

  override def getJob(id: UUID): Future[Option[AsrJobDto]] = {
    for {
      job <- asrJobRepository.find(id)
      steps <- asrStepRepository.findForJob(id)
    } yield {
      job.map(j => {
        AsrJobDto.fromModel(
          j,
          steps
        )
      })
    }
  }

  override def getStepResult(stepId: UUID)
                            (implicit userContext: UserContext): Future[Option[AsrStepResult]] = {
    logger.debug(s"getting step result fro id = $stepId")
    for {
      result <- asrStepResultRepository.findForStep(stepId)
      step <- asrStepRepository.findForUuid(stepId)
      job <- asrJobRepository.find(step.get.jobId.get)
    } yield {
      withUserCheck(job.get)
      result
    }
  }

  override def terminateJob(id: UUID)
                           (implicit userContext: UserContext): Future[_] = {
    logger.debug(s"trying to terminate job $id")
    asrJobRepository.find(id).flatMap(j => {
      val job = j.get
      withUserCheck(job)
      if (job.status == AsrExecutionStatus.running) {
        asrExecutorService.terminate(j.get.id)
      } else {
        logger.info(s"job ${job.id} is not in running state; do nothing for now " +
          s"(but later we should do something with jobs in Waiting status)")
        Future.successful(None)
      }
    })
  }

  override def createJob(jobDto: AsrJobDto)
                        (implicit userContext: UserContext): Future[AsrJobDto] = {
    logger.debug(s"creating job $jobDto")
    val (job, steps) = jobDto.copy(
      user = Some(userContext.user.id)
    ).toModels
    val jobLimit = appConfigService.getConfig().maxJobsPerUser
    val sampleLimit = appConfigService.getConfig().maxSamplesPerJob

    steps.foreach(s => {
      if (s.samples.`type` == AsrStepSamplesType.previous && s.samples.list.length > sampleLimit) {
        throw new RuntimeException(s"unable to proceed step $s; sample limit per job ($sampleLimit) exceeded")
      }
    })

    val future = for {
      _ <- asrJobRepository.findAllForUser(userContext.user.id).map(allJobs => {
        if (allJobs.size >= jobLimit) {
          throw new RuntimeException(s"unable to create job; limit ($jobLimit) exceeded for user ${userContext.user}")
        }
      })
      _ <- asrJobRepository.create(job)
      _ <- Future.sequence(
        steps.map(asrStepRepository.create)
      )
    } yield ()
    future.map(_ => {
      val dto = AsrJobDto.fromModel(job, steps)
      logger.debug(s"job ${job.id} registered")
      dto
    })
  }

  override def removeJob(jobId: UUID)
                        (implicit userContext: UserContext): Future[_] = {
    logger.info(s"removing job for id = $jobId with it's result and log")
    for {
      job <- asrJobRepository.find(jobId).map(j => {
        withUserCheck(j.get)
        j
      })
      _ <- job match {
        case Some(j) if j.status == AsrExecutionStatus.running =>
          logger.info(s"trying to remove running job; at first, stopping it")
          asrExecutorService.terminate(job.get.id)
        case _ =>
          Future.successful(None)
      }
      steps <- asrStepRepository.findForJob(jobId)
      _ <- Future.sequence(
        steps.map(s => {
          val asrKey = job.map(_.asrKey).getOrElse("")
          logger.debug(s"removing result, logs and models for step ${s.id.get}")
          asrStepResultRepository.removeForStep(s.id.get).flatMap(_ => {
            asrJobLogRepository.removeLog(asrKey, s.id.get).flatMap(_ => {
              asrModelRepository.removeModels(asrKey, s.id.get)
            })
          })
        })
      )
      res <- asrJobRepository.deleteById(jobId)
    } yield res
  }

  override def updateJob(job: AsrJobDto): Future[AsrJob] = {
    updateJob(job.toModels._1).map(res => {
      messageBrokerService.notify("/topic/jobs/update", Json.toJson(job))
      res
    })
  }

  override def updateJobLabel(id: UUID, label: String)
                             (implicit userContext: UserContext): Future[_] = {
    logger.debug(s"updating label for job $id to $label")
    asrJobRepository.find(id) flatMap {
      case Some(j) =>
        withUserCheck(j)
        asrJobRepository.updateLabel(id, label)
      case _ =>
        Future.failed(new IllegalArgumentException(s"not found job for id = $id"))
    }
  }

  // ---

  private def updateJob(job: AsrJob): Future[AsrJob] = {
    asrJobRepository.updateJob(job).map(res => {
      res
    })
  }

  override def launchJob(id: UUID)(implicit  userContext: UserContext): Unit = {
    getJob(id) map {
      case Some(j) =>
        if (!j.user.contains(userContext.user.id)) {
          throw new IllegalAccessException(s"unable to laucnh job $j by user ${userContext.user}")
        }
        launchAllowedJob(j)
      case _ =>
        throw new IllegalArgumentException(s"not found job for id = $id")
    }
  }

  private def launchAllowedJob(j: AsrJobDto) = {
    logger.debug(s"launching job ${j.id}; acquiring executor actor...")
    asrExecutorActorFactoryService.acquireActor(j)(this) ! RunJob(j)
  }

  private def withUserCheck(job: AsrJob)
                           (implicit userContext: UserContext): Unit = {
    if (job.user != userContext.user.id && userContext.user.role != UserRole.admin) {
      throw new RuntimeException(s"job ${job.id} is not permitted for user ${userContext.user}")
    }
  }

}

