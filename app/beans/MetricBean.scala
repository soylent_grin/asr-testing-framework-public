package beans

import com.google.inject.{ImplementedBy, Inject}
import com.typesafe.config.{Config, ConfigException}
import dtos.ParamDto
import enums.Metric
import javax.inject.Singleton
import play.api.Logger
import services.AsrExecutorService

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@ImplementedBy(classOf[MetricBeanImpl])
trait MetricBean {

  def getAvailableMetrics: Future[List[Metric.Value]]

}

@Singleton
class MetricBeanImpl @Inject()(config: Config,
                               implicit val ec: ExecutionContext) extends MetricBean {

  private implicit val logger = Logger(getClass)

  private var metrics: List[Metric.Value] = Nil

  init()

  private def init(): Unit = {
    logger.debug(s"initing; reading all available metrics...")
    val disabledMetrics = Try(
      config.getStringList("metric.disabled").asScala
    ).getOrElse(Nil)
    metrics = Metric.values.filter(v => {
      if (disabledMetrics.contains(v.toString)) {
        logger.info(s"metric $v is disabled via config")
        false
      } else {
        true
      }
    }).toList
  }

  override def getAvailableMetrics: Future[List[Metric.Value]] = {
    Future.successful(metrics)
  }

}


