package beans

import java.io.File
import java.util.UUID

import com.google.inject.{ImplementedBy, Inject}
import javax.inject.Singleton
import repositories.{AsrJobLogRepository, AsrJobRepository, AsrStepRepository}

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[AsrStepLogBeanImpl])
trait AsrStepLogBean {

  def saveLog(asrKey: String, stepId: UUID, log: File): Future[_]

  def getLog(stepId: UUID): Future[Option[File]]

}

@Singleton
class AsrStepLogBeanImpl @Inject()(asrJobLogRepository: AsrJobLogRepository,
                                   asrStepRepository: AsrStepRepository,
                                   asrJobRepository: AsrJobRepository,
                                   implicit val ec: ExecutionContext) extends AsrStepLogBean {

  override def saveLog(asrKey: String, stepId: UUID, log: File): Future[_] = {
    asrJobLogRepository.saveLog(asrKey, stepId, log)
  }

  override def getLog(stepId: UUID): Future[Option[File]] = {
    asrStepRepository.findForUuid(stepId).flatMap({
      case Some(s) =>
        asrJobRepository.find(s.jobId.get).flatMap({
          case Some(j) =>
            asrJobLogRepository.getLog(j.asrKey, stepId)
          case _ =>
            Future.successful(None)
        })
      case _ =>
        Future.successful(None)
    })
  }

}

