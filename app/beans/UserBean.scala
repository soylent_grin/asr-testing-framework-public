package beans

import com.google.inject.{ImplementedBy, Inject, Singleton}
import models.User
import repositories.UserRepository

import scala.concurrent.Future

@ImplementedBy(classOf[UserBeanImpl])
trait UserBean {

  def findAllUsers: Future[Seq[User]]

  def updateUser(u: User): Future[_]

}

@Singleton
class UserBeanImpl @Inject()(userRepository: UserRepository) extends UserBean {

  override def findAllUsers: Future[Seq[User]] = {
    userRepository.findAll()
  }

  override def updateUser(u: User): Future[_] = {
    userRepository.upsert(u)
  }

}
