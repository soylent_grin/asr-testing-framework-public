package beans

import com.google.inject.{ImplementedBy, Inject}
import com.typesafe.config.{Config, ConfigException}
import dtos.ParamDto
import javax.inject.Singleton
import play.api.Logger
import services.AsrExecutorService

import scala.collection.JavaConverters._
import scala.collection.concurrent.TrieMap
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@ImplementedBy(classOf[AsrBeanImpl])
trait AsrBean {

  def getAllParamTypes: Future[Map[String, List[ParamDto]]]

  def getParamTypes(asrKey: String): Future[List[ParamDto]]

}

@Singleton
class AsrBeanImpl @Inject()(asrExecutorService: AsrExecutorService,
                            config: Config,
                            implicit val ec: ExecutionContext) extends AsrBean {

  private implicit val logger = Logger(getClass)

  private val executorToParams: TrieMap[String, List[ParamDto]] = TrieMap()

  init()

  private def init(): Unit = {
    logger.debug(s"initing; reading all executor parameters...")
    asrExecutorService.getList.foreach(e => {
      logger.debug(s"reading parameters for ")
      executorToParams(e.key) = ParamDto.fromConfig(
        Try(config.getConfigList(s"asrConfig.${e.key}")).toOption match {
          case Some(list) =>
            list.asScala
          case _ =>
            logger.info(s"not found parameters for ASR executor with key = ${e.key}; " +
              s"considering it as empty")
            Nil
        }
      )
    })
  }

  override def getAllParamTypes: Future[Map[String, List[ParamDto]]] = {
    Future.successful(
      executorToParams.toMap
    )
  }

  override def getParamTypes(asrKey: String): Future[List[ParamDto]] = {
    try {
      Future.successful(
        executorToParams(asrKey)
      )
    } catch {
      case t: ConfigException.Missing =>
        logger.debug(s"not found config for $asrKey: ${t.getMessage}")
        Future.successful(Nil)
      case t: Throwable =>
        logger.error(s"unexpected error while parsing param types for ASR $asrKey: ", t)
        Future.successful(Nil)
    }
  }

}


