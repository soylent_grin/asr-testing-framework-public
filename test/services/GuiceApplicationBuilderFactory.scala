package services

import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import repositories.SoundbankRepository
import repositories.interpreters.memory.InMemorySoundBankTestRepository

object GuiceApplicationBuilderFactory {
  def make(): GuiceApplicationBuilder = new GuiceApplicationBuilder()
    .overrides(bind[SoundbankRepository].to(classOf[InMemorySoundBankTestRepository]))
}
