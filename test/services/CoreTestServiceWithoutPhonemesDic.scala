package services

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application

class CoreTestServiceWithoutPhonemesDic extends PlaySpec with GuiceOneAppPerSuite {
  implicit override lazy val app: Application =
    GuiceApplicationBuilderFactory.make()
      .configure("metric.phonemesDicPath" -> "????")
      .build()
}
