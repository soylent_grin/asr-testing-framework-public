package services

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application

class CoreTestServiceWithOneLerWeights extends PlaySpec with GuiceOneAppPerSuite {
  implicit override lazy val app: Application =
    GuiceApplicationBuilderFactory.make()
      .configure(Map[String, Any](
        "metric.lmer.lemmasSimilarWeight" -> 1.0,
        "metric.lmer.lemmasDifferentWeight" -> 1.0
        ))
      .build()
}
