package services

import models._
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.{DefaultAwaitTimeout, FutureAwaits}
import play.api.{Application, Logger}
import services.metrics.AsrMetricService
import services.metrics.morpho.MorphoInfoExtractionService

class AsrMetricServiceSpec extends PlaySpec with GuiceOneAppPerSuite with FutureAwaits with DefaultAwaitTimeout {

  private val logger = Logger(getClass)

  // Override fakeApplication if you need a Application with other than
  // default parameters.
  override def fakeApplication(): Application = {
    GuiceApplicationBuilder().build()
  }

  "getFinalMetrics throws expected exception on empty params" in {
    val service = app.injector.instanceOf[AsrMetricService]
    val pairs = Nil
    val jobDuration = 0
    val input = AsrMetricInput(Nil, Nil, None)
    await(service.getMetricsFinal(pairs, jobDuration, input)) mustBe AsrMetricOutput(List(AsrMetricOutputResult(Map[String, String](), List())), List())
  }

  "dummy succeeded test" in {
    1 + 2 mustBe 3
  }

  /*
  "dummy failed test" in {
    1 / 0
  }
  */

  "MorphoCore test" in {
    val morphoService = app.injector.instanceOf[MorphoInfoExtractionService]
    morphoService.getMorphoInfo(Set[String](), Nil, Nil) mustBe None
  }

}