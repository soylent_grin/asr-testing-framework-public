package services.metrics.complex

import models._
import services.metrics.MetricTestService

class TagsTestService extends MetricTestService {

  "Correct values for sample with no tokens in reference transcription" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-empty-ref" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "-",
          "INS" -> "-",
          "DEL" -> "-",
          "SUB" -> "-",
          "CHER" -> "-",
          "PHER" -> "-",
          "CER" -> "-",
          "CXER" -> "-",
          "MER" -> "-",
          "MSTAT" -> "-",
          "NCR" -> "-",
          "IWERA" -> "-",
          "IWER" -> "-",
          "HES" -> "-",
          "RER" -> "-",
          "OCWR" -> "-",
          "WMER" -> "-",
          "INFLERA" -> "-",
          "INFLER" -> "-",
          "IMERA" -> "-",
          "IMER" -> "-",
          "GER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-chisl" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-chisl" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "2"
        ),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("числ", "фио"),
          Map[String, String](
            "SUB" -> "1",
            "INS" -> "0",
            "DEL" -> "2",
            "WER" -> "0.4286",
            "CHER" -> "0.2449",
            "PHER" -> "0.24",
            "NCR" -> "-",
            "OCWR" -> "0.5"
          )
        ))
      )
    ))
  }

 "Correct WER for test-partial-nrzb" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-partial-nrzb" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег"),
          Map[String, String](
            "WER" -> "0.5"
          )
        ))
      )
    ))
  }

  "Correct WER for test-full-nrzb" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-full-nrzb" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег"),
          Map[String, String](
            "WER" -> "0.3333"
          )
        ))
      )
    ))
  }

  "Correct WER for test-mixed-nrzb" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-mixed-nrzb" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег"),
          Map[String, String](
            "WER" -> "0.5"
          )
        ))
      )
    ))
  }

  "Correct WER for test-irrelevant-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-irrelevant-tags" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег"),
          Map[String, String](
            "WER" -> "0.5"
          )
        ))
      )
    ))
  }

  "Correct WER for test-tag-groups" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tag-groups" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("описание", "что"),
          Map[String, String](
            "WER" -> "0.5"
          )
        ))
      )
    ))
  }

  "Correct WER for test tag with none value" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tag-groups" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("не тег"),
          Map[String, String](
            "WER" -> "-",
            "INS" -> "-",
            "DEL" -> "-",
            "SUB" -> "-",
            "CHER" -> "-",
            "PHER" -> "-",
            "CER" -> "-",
            "CXER" -> "-",
            "MER" -> "-",
            "MSTAT" -> "-",
            "NCR" -> "-",
            "IWERA" -> "-",
            "IWER" -> "-",
            "HES" -> "-",
            "RER" -> "-",
            "OCWR" -> "-",
            "WMER" -> "-",
            "INFLERA" -> "-",
            "INFLER" -> "-",
            "IMERA" -> "-",
            "IMER" -> "-",
            "GER" -> "-"
          )
        ))
      )
    ))
  }

  "Correct WER for test-tag-groups 2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tag-groups" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("описание", "что", "не тег"),
          Map[String, String](
            "WER" -> "0.5"
          )
        ))
      )
    ))
  }

  "Correct CER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CER" -> "0.4167"
          )
        ))
      )
    ))
  }

  "Correct CXER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CXER" -> "0.4167"
          )
        ))
      )
    ))
  }

  "Correct PHER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "PHER" -> "0.2778"
          )
        ))
      )
    ))
  }

  "Correct CHER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CHER" -> "0.1351"
          )
        ))
      )
    ))
  }

  "Correct LMER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "INFLERA" -> "0.0"
          )
        ))
      )
    ))
  }

  "Correct OCWR for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "OCWR" -> "0.6"
          )
        ))
      )
    ))
  }

  "Correct HES for test-tags-hes-rep" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-hes-rep" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "HES" -> "0.6667"
          )
        ))
      )
    ))
  }

  "Correct REP for test-tags-hes-rep" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-hes-rep" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "RER" -> "0.4167"
          )
        ))
      )
    ))
  }

}
