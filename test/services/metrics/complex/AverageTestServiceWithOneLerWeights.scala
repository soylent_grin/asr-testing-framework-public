package services.metrics.complex

import models._
import services.metrics.MetricTestServiceWithOneLerWeights

class AverageTestServiceWithOneLerWeights extends MetricTestServiceWithOneLerWeights {
  // Should be equal to WER
  "Correct average values for set of samples LER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "LER" -> "0.1667"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples FLER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "RLER" -> "0.1667"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }
}
