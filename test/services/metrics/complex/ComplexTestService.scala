package services.metrics.complex

import enums.Metric
import models.{AsrMetricOutput, AsrMetricOutputResult, AsrMetricOutputTagsResult}
import services.metrics.CoreMetricTestServiceImpl

class ComplexTestService extends CoreMetricTestServiceImpl {

  "Correct complex SUB" in test(
    AsrMetricOutput(
      List[AsrMetricOutputResult](
        AsrMetricOutputResult(
          Map[String, String](
            "SUB" -> "2.0",
            "INS" -> "0.0"
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String](
                "INS" -> "-"
              )
            )
          )
        )
      ),
      List[(String, AsrMetricOutputResult)](
        ("test-complex-wer", AsrMetricOutputResult(
          Map[String, String](
            "SUB" -> "2",
            "INS" -> "0"
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String](
                "INS" -> "-"
              )
            )
          )
        ))
      )
    )
  )

  "Iwer is not being calculated without alpha field" in test(
    AsrMetricOutput(
      List[AsrMetricOutputResult](
        AsrMetricOutputResult(
          Map[String, String](),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String]()
            )
          )
        )
      ),
      List[(String, AsrMetricOutputResult)](
        ("test-complex-wer", AsrMetricOutputResult(
          Map[String, String](
            "IWER" -> "-",
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String](
                "IWER" -> "-"
              )
            )
          )
        ))
      )
    )
  )

  "Average Iwer is not being calculated" in test(
    AsrMetricOutput(
      List[AsrMetricOutputResult](
        AsrMetricOutputResult(
          Map[String, String](),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String]()
            )
          )
        )
      ),
      List[(String, AsrMetricOutputResult)](
        ("test-complex-wer-with-alpha", AsrMetricOutputResult(
          Map[String, String](
            "IWER" -> "[0.0,1.0,0.0,0.0,0.0,0.0,0.0,1.4]",
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String](
                "IWER" -> "-"
              )
            )
          )
        ))
      )
    )
  )

  "Speed factor is not being calculated for individual samples nor for tags" in test(
    List[String]("test-sf-1", "test-sf-2"),
    jobDuration = 13,
    List[Metric.Value](Metric.SF),
    Some(Map[List[String], List[Metric.Value]](
      List[String]("тег1", "тег2") -> List[Metric.Value](Metric.SF)
    )),
    AsrMetricOutput(
      List[AsrMetricOutputResult](
        AsrMetricOutputResult(
          Map[String, String](
            "SF" -> "1.0"
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег1", "тег2"),
              Map[String, String]()
            )
          )
        )
      ),
      List[(String, AsrMetricOutputResult)](
        ("test-sf-1", AsrMetricOutputResult(
          Map[String, String](),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег1", "тег2"),
              Map[String, String]()
            )
          )
        )),
        ("test-sf-2", AsrMetricOutputResult(
          Map[String, String](),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег1", "тег2"),
              Map[String, String]()
            )
          )
        ))
      )
    )
  )



  "When running on completely ambiguous ref, it seems like ref is empty considering all metrics except GER" in test(
    AsrMetricOutput(
      List[AsrMetricOutputResult](
        AsrMetricOutputResult(
          Map[String, String](
            "INS" -> "-",
            "GER" -> """{"нрзб-count":4.0,"нрг-count":0.0,"пш-count":0.0}"""
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String](
                "INS" -> "-",
                "GER" -> """{"нрзб-count":2.0,"нрг-count":0.0,"пш-count":0.0}"""
              )
            )
          )
        )
      ),
      List[(String, AsrMetricOutputResult)](
        ("test-completely-ambiguous-ref", AsrMetricOutputResult(
          Map[String, String](
            "INS" -> "-",
            "GER" -> """{"нрзб-count":4.0,"нрг-count":0.0,"пш-count":0.0}"""
          ),
          List[AsrMetricOutputTagsResult](
            AsrMetricOutputTagsResult(
              List[String]("тег"),
              Map[String, String](
                "INS" -> "-",
                "GER" -> """{"нрзб-count":2.0,"нрг-count":0.0,"пш-count":0.0}"""
              )
            )
          )
        ))
      )
    )
  )
}
