package services.metrics.complex

import models._
import services.metrics.MetricTestService

class MixedTestService extends MetricTestService {

  "Correct average WER for test-wer-garbage-tagged-one & test-wer-garbage-tagged-two & test-wer-garbage-tagged-three" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-wer-garbage-tagged-one",
      "test-wer-garbage-tagged-two",
      "test-wer-garbage-tagged-three"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег"),
          Map[String, String](
            "WER" -> "0.6667"
          )
        ))
      )
    )
  }

  "Correct average GER for test-wer-garbage-tagged-one & test-wer-garbage-tagged-two & test-wer-garbage-tagged-three" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-wer-garbage-tagged-one",
      "test-wer-garbage-tagged-two",
      "test-ger-tagged"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег"),
          Map[String, String](
            "GER" -> "{\"нрзб-count\":0.75,\"пш-percentage\":0.0,\"нрзб-percentage\":37.5,\"пш-count\":0.0,\"нрг-count\":0.0,\"нрг-percentage\":0.0,\"WER\":0.75}"
          )
        ))
      )
    )
  }

  "Correct average by tags CER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-cer",
      "mts_test_8kz/TEST_MER_MSTAT"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CER" -> "0.4167"
          )
        ))
      )
    )
  }

  "Correct average by tags CER 2" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-cer",
      "test-tagged-concepts-one"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CER" -> "0.3334"
          )
        ))
      )
    )
  }

  "Correct average by tags CXER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-cer",
      "test-tagged-concepts-one"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CER" -> "0.3334"
          )
        ))
      )
    )
  }

  "Correct average by tags PHER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-cer",
      "test-tags-hes-rep"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "PHER" -> "0.28"
          )
        ))
      )
    )
  }

  "Correct average by tags CHER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-cer",
      "test-tags-hes-rep"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "CHER" -> "0.141"
          )
        ))
      )
    )
  }

  "Correct average by tags LMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tagged-mer",
      "test-tagged-morpho"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "LMER" -> """{"number":0.375,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.0,"case":0.375,"tense":0.0,"transitivity":0.0,"gender":0.1667}"""
          )
        ))
      )
    )
  }

  "Correct average by tags OCWR" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tagged-mer",
      "test-tagged-morpho"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "OCWR" -> "0.375"
          )
        ))
      )
    )
  }

  "Correct average by tags HES" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-hes-rep",
      "test-tags-hes-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "HES" -> "0.7467"
          )
        ))
      )
    )
  }

}
