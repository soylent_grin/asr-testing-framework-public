package services.metrics.complex

import models._
import services.metrics.MetricTestService

class AverageTestService extends MetricTestService {

  "Correct average values for set of samples WER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.1667"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples INS" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "0.6111"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples SUB" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "0.6111"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples DEL" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "DEL" -> "0.6111"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples CER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.1944"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples CER with empty" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags",
      "test-ins"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.1944"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples CHER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "CHER" -> "0.1585"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples CXER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "CXER" -> "0.2917"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples GER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":3.0968,"пш-percentage":6.4516,"нрзб-percentage":29.0322,"пш-count":0.7742,"нрг-count":0.3871,"нрг-percentage":3.2258,"WER":0.4516}"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples HES" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags",
      "test-with-hes"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "0.5714"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples HES with values" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "test-with-garbage-tags",
      "test-with-hes",
      "mts_test_16kz/TEST_IWER_HES_CER_NCR"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "0.6032"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples IMERA" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "0.087"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples IMERA with MER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "mts_test_8kz/TEST_MER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "0.0815"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples INFLERA" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "mts_test_8kz/TEST_MER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "INFLERA" -> "0.0255"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples LMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """{"number":0.125,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0137,"person":0.0,"case":0.1818,"tense":0.0,"transitivity":0.0,"gender":0.0769}"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples MER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.15,"mood":1.0,"animacy":0.0,"involvement":1.0,"aspect":0.25,"voice":0.0,"POS":0.0875,"person":0.0,"case":0.2242,"tense":0.0,"transitivity":0.25,"gender":0.1273}"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples MER 2" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-mer-individual-weights",
      "test-mer-individual-weights-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.3333,"mood":1.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.5,"case":0.4,"tense":1.0,"transitivity":0.0,"gender":0.5}"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples LMER 2" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-mer-individual-weights",
      "test-mer-individual-weights-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """{"number":0.4,"mood":1.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.5,"case":0.5,"tense":1.0,"transitivity":0.0,"gender":0.3333}"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples MSTAT" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """{"number":{"sing":28.0893,"plur":4.8214},"animacy":{"inan":21.6786,"anim":1.0},"aspect":{"impf":0.8036},"voice":{"actv":0.8036},"POS":{"NOUN":22.6786,"ADJS":0.8036,"CONJ":2.4107,"PRTF":0.8036,"PREP":3.0,"ADJF":8.625},"case":{"ablt":21.0893,"gent":4.6071,"datv":1.6071,"nomn":2.6071,"accs":1.0,"loct":1.1964},"tense":{"pres":0.8036},"transitivity":{"intr":0.8036},"gender":{"neut":7.2321,"masc":14.4464,"femn":8.0179}}"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples LER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "LER" -> "0.1339"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples RLER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "RLER" -> "0.1339"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples NCR" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-with-garbage-tags",
      "mts_test_8kz/TEST_NCR-2",
      "mts_test_16kz/TEST_NCR_IWER",
      "mts_test_8kz/TEST_NCR_CHER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "0.8"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples OCWR" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "mts_test_16kz/TEST_CHER",
      "mts_test_16kz/TEST_CXER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.4706"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples PHER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-pher",
      "test-from-mail"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "PHER" -> "0.217"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples REP" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "mts_test_8kz/TEST_MER_MSTAT",
      "mts_test_8kz/TEST_HES_CER",
      "mts_test_16kz/TEST_CXER_CER_OCWR",
      "test-with-hes"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples WLMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WLMER" -> "0.0641"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples WMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-morpho",
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WMER" -> "0.1117"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average CER for mts_test_16kz/TEST_CXER_CER_OCWR & mts_test_16kz/TEST_CXER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "mts_test_16kz/TEST_CXER_CER_OCWR",
      "mts_test_16kz/TEST_CXER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.1667"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    )
  }

  "Correct average DEL for test-del & test-with-garbage-tags" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-del",
      "test-with-garbage-tags"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "DEL" -> "5.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    )
  }

  "Correct average INS for test-ins & mts_test_8kz/TEST_MER_MSTAT" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-ins",
      "mts_test_8kz/TEST_MER_MSTAT"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "2.7419"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    )
  }

  "Correct average GER for test-wer-garbage-tagged-one & test-wer-garbage-tagged-two & test-ger-tagged" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-wer-garbage-tagged-one",
      "test-wer-garbage-tagged-two",
      "test-ger-tagged"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "GER" -> "{\"нрзб-count\":0.9412,\"пш-percentage\":0.0,\"нрзб-percentage\":17.6471,\"пш-count\":0.0,\"нрг-count\":0.0,\"нрг-percentage\":0.0,\"WER\":0.4118}"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    )
  }

  "Error on checking average IMER for test-wer-garbage-tagged-one & test-wer-garbage-tagged-two & test-ger-tagged" in {
    assertThrows[NoSuchElementException](
    coreMetricTestService.testAverageMetrics(List[String](
      "test-wer-garbage-tagged-one",
      "test-wer-garbage-tagged-two",
      "test-ger-tagged"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "IMER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }


  "Correct average values for set of samples having empty reference" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-empty-ref",
      "test-empty-ref"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "-",
          "INS" -> "-",
          "DEL" -> "-",
          "SUB" -> "-",
          "CHER" -> "-",
          "PHER" -> "-",
          "CER" -> "-",
          "CXER" -> "-",
          "MER" -> "-",
          "MSTAT" -> "-",
          "NCR" -> "-",
          "IWERA" -> "-",
          "HES" -> "-",
          "RER" -> "-",
          "OCWR" -> "-",
          "WMER" -> "-",
          "INFLERA" -> "-",
          "IMERA" -> "-",
          "GER" -> "-",
          "SF" -> "0.3333"
        ),
        List[AsrMetricOutputTagsResult]()
      )
      , 5)
  }

}
