package services.metrics.complex.withoutphonemesdic

import models._
import services.metrics.MetricTestServiceWithoutPhonemesDic

class TagsTestService extends MetricTestServiceWithoutPhonemesDic {
  "Correct PHER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "PHER" -> "-"
          )
        ))
      )
    ))
  }
}
