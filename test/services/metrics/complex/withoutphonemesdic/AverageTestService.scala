package services.metrics.complex.withoutphonemesdic

import models._
import services.metrics.{MetricTestService, MetricTestServiceWithoutPhonemesDic}

class AverageTestService extends MetricTestServiceWithoutPhonemesDic {
  "Correct average values for set of samples PHER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-pher",
      "test-from-mail"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "PHER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }
}
