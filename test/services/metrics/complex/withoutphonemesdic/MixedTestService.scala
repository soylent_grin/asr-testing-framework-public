package services.metrics.complex.withoutphonemesdic

import models._
import services.metrics.MetricTestServiceWithoutPhonemesDic

class MixedTestService extends MetricTestServiceWithoutPhonemesDic {
  "Correct average by tags PHER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tags-cer",
      "test-tags-hes-rep"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "PHER" -> "-"
          )
        ))
      )
    )
  }
}
