package services.metrics.complex.withoutmorphoinfo

import models._
import services.metrics.MetricTestServiceWithDisabledMorphoInfo

class AverageTestService extends MetricTestServiceWithDisabledMorphoInfo {
  "Correct average values for set of samples IMERA" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples IMERA with MER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "mts_test_8kz/TEST_MER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples INFLERA" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-from-mail",
      "test-with-garbage-tags-right",
      "mts_test_8kz/TEST_MER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "INFLERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples LMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples MER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples MER 2" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-mer-individual-weights",
      "test-mer-individual-weights-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples LMER 2" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-mer-individual-weights",
      "test-mer-individual-weights-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples MSTAT" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-morpho",
      "test-from-mail"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples NCR" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "test-with-garbage-tags",
      "mts_test_8kz/TEST_NCR-2",
      "mts_test_16kz/TEST_NCR_IWER",
      "mts_test_8kz/TEST_NCR_CHER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples OCWR" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-empty-ref",
      "mts_test_16kz/TEST_CHER",
      "mts_test_16kz/TEST_CXER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples WLMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WLMER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }

  "Correct average values for set of samples WMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-morpho",
      "test-from-mail",
      "mts_test_8kz/TEST_MER",
      "mts_test_8kz/TEST_MER_2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WMER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ))
  }
}
