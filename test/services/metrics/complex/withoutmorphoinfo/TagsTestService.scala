package services.metrics.complex.withoutmorphoinfo

import models._
import services.metrics.{MetricTestService, MetricTestServiceWithDisabledMorphoInfo}

class TagsTestService extends MetricTestServiceWithDisabledMorphoInfo {

  "Correct LMER for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "INFLERA" -> "-"
          )
        ))
      )
    ))
  }

  "Correct OCWR for test-tags-cer" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-tags-cer" -> AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "OCWR" -> "-"
          )
        ))
      )
    ))
  }
}
