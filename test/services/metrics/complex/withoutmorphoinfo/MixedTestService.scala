package services.metrics.complex.withoutmorphoinfo

import models._
import services.metrics.{MetricTestService, MetricTestServiceWithDisabledMorphoInfo}

class MixedTestService extends MetricTestServiceWithDisabledMorphoInfo {

  "Correct average by tags LMER" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tagged-mer",
      "test-tagged-morpho"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "LMER" -> "-"
          )
        ))
      )
    )
  }

  "Correct average by tags OCWR" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-tagged-mer",
      "test-tagged-morpho"
    ),
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          List[String]("тег", "что", "не тег"),
          Map[String, String](
            "OCWR" -> "-"
          )
        ))
      )
    )
  }
}
