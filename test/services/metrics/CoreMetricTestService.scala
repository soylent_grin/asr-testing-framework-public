package services.metrics

import com.google.inject.{ImplementedBy, Singleton}
import enums.Metric
import models._
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.{DefaultAwaitTimeout, FutureAwaits}
import repositories.SoundbankRepository
import services.CoreTestService

@ImplementedBy(classOf[CoreMetricTestServiceImpl])
trait CoreMetricTestService extends PlaySpec with GuiceOneAppPerSuite with FutureAwaits with DefaultAwaitTimeout {
  val metricService: AsrMetricService = app.injector.instanceOf[AsrMetricService]
  val soundbankRepository: SoundbankRepository = app.injector.instanceOf[SoundbankRepository]

  def getMetrics(samples: Seq[String],
                 jobDuration: Long,
                 metrics: List[Metric.Value],
                 taggedMetrics: Option[Map[List[String], List[Metric.Value]]] = None): AsrMetricOutput = {

    val pairs = samples.map(id => (id, await(soundbankRepository.findSample(id)))). // find all required samples
      map(pair => {
      if (pair._2.isEmpty) {
        throw new IllegalStateException(f"Not found sample with id '${pair._1}'")
      }
      pair._2
    }).filter(sample => sample.isDefined). // select only those which are not None
      map(sample => sample.get). // unwrap
      map(sample => (sample,
      (sample.meta \ "transcription-from-asr").asOpt[List[String]].getOrElse(List[String]()))
    ) // convert to required format (list of pairs)
    val input = AsrMetricInput(
      metrics.map(metric => metric.toString), // convert metric to strings
      if (taggedMetrics.isDefined) {
        (for ((tags, metrics) <- taggedMetrics.get) // make list of AsrMetricTagsInput objects from provided map
          yield AsrMetricTagsInput(tags, metrics.map(metric => metric.toString))).toList
      } else {
        List[AsrMetricTagsInput]()
      }, None)
    await(metricService.getMetricsFinal(pairs, jobDuration, input))
  }

  def testMetrics(metrics: Map[String, AsrMetricOutputResult], jobDurationInSeconds: Long = 0): Unit = {
    for ((sampleId, expectedResult) <- metrics) {
      val sampleMetrics = getMetrics(
        List[String](sampleId),
        jobDurationInSeconds,
        expectedResult.result.keys.map(key => Metric.fromString(key).get).toList,
        Some((for (asrMetricOutputTagsResult <- expectedResult.byTags) yield
          asrMetricOutputTagsResult.tags ->
            asrMetricOutputTagsResult.result.keys.map(key => Metric.fromString(key).get).toList).toMap)
      )

      if (sampleMetrics.single.exists(pair => pair._1 == sampleId)) {
        val sampleResult = sampleMetrics.single.filter(pair => pair._1 == sampleId).head._2
        val sampleExpectedResult = metrics(sampleId)

        // check without tags
        for (metric <- metrics(sampleId).result.keys) {
          sampleResult.result(metric) mustBe sampleExpectedResult.result(metric)
        }

        // check with tags
        for (tags <- metrics(sampleId).byTags.map(result => result.tags)) {
          val sampleResultForTags = sampleResult.byTags.filter(tagResult => tagResult.tags == tags).head
          val sampleExpectedResultForTags = sampleExpectedResult.byTags.filter(tagResult => tagResult.tags == tags).head
          for (metric <- sampleResultForTags.result.keys) {
            sampleResultForTags.result(metric) mustBe sampleExpectedResultForTags.result(metric)
          }
        }
      }
    }
  }

  def testAverageMetrics(sampleIds: Seq[String], expectedResult: AsrMetricOutputResult, jobDurationInSeconds: Long = 0): Unit = {
    val sampleMetrics = getMetrics(
      sampleIds,
      jobDurationInSeconds,
      expectedResult.result.keys.map(key => Metric.fromString(key).get).toList,
      Some((for (asrMetricOutputTagsResult <- expectedResult.byTags) yield
        asrMetricOutputTagsResult.tags ->
          asrMetricOutputTagsResult.result.keys.map(key => Metric.fromString(key).get).toList).toMap)
    )

    if (sampleMetrics.overall.nonEmpty) {
      val sampleResult = sampleMetrics.overall.head

      // check without tags
      for (metric <- expectedResult.result.keys) {
        sampleResult.result(metric) mustBe expectedResult.result(metric)
      }

      // check with tags
      for (tags <- expectedResult.byTags.map(result => result.tags)) {
        val sampleResultForTags = sampleResult.byTags.filter(tagResult => tagResult.tags == tags).head
        val sampleExpectedResultForTags = expectedResult.byTags.filter(tagResult => tagResult.tags == tags).head
        for (metric <- sampleResultForTags.result.keys) {
          sampleResultForTags.result(metric) mustBe sampleExpectedResultForTags.result(metric)
        }
      }
    }
  }

  def testSingleMetricOnSingleSample(metricId: String)(sampleId: String, metricValue: String): Unit =
    f"Correct results for $sampleId" in {
      testMetrics(Map[String, AsrMetricOutputResult](
        sampleId -> AsrMetricOutputResult(
          Map[String, String](
            metricId -> metricValue
          ),
          List[AsrMetricOutputTagsResult]()
        )
      ))
    }

  def testSingleMetricOnSeveralSamples(metricId: String)(samplesIds: Seq[String], averageMetricValue: String): Unit =
    "Correct average values for set of samples DEL" in {
      testAverageMetrics(samplesIds,
        AsrMetricOutputResult(
          Map[String, String](
            metricId -> averageMetricValue
          ),
          List[AsrMetricOutputTagsResult]()
        ))
    }


  def testSingleMetricOnSeveralSamples(metricId: String)(samplesIds: Seq[String], averageMetricValue: String, tags: Seq[String]): Unit =
  "Correct average WER for test-wer-garbage-tagged-one & test-wer-garbage-tagged-two & test-wer-garbage-tagged-three" in {
    testAverageMetrics(samplesIds,
      AsrMetricOutputResult(
        Map[String, String](),
        List[AsrMetricOutputTagsResult](AsrMetricOutputTagsResult(
          tags.toList,
          Map[String, String](
            metricId -> averageMetricValue
          )
        ))
      )
    )
  }

  def test(asrMetricOutput: AsrMetricOutput, jobDuration: Long = 0): Unit =
    getMetrics(asrMetricOutput.single.map(_._1),
      jobDuration,
      asrMetricOutput.single.head._2.result.keys.map(Metric.fromString(_).get).toList,
      Some(asrMetricOutput.single.head._2.byTags.map(asrMetricOutputTagsResult =>
        asrMetricOutputTagsResult.tags -> asrMetricOutputTagsResult.result.keys.map(Metric.fromString(_).get).toList).toMap)) mustBe asrMetricOutput

  def test(samplesIds: Seq[String], jobDuration: Long, metrics: List[Metric.Value], taggedMetrics: Option[Map[List[String], List[Metric.Value]]],
           expectedAsrMetricOutput: AsrMetricOutput): Unit =
      getMetrics(samplesIds, jobDuration, metrics, taggedMetrics) mustBe expectedAsrMetricOutput
}

@Singleton
class CoreMetricTestServiceImpl extends CoreTestService with CoreMetricTestService {}