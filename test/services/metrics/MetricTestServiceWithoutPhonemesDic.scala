package services.metrics

import services.CoreTestServiceWithoutPhonemesDic

class MetricTestServiceWithoutPhonemesDic extends CoreTestServiceWithoutPhonemesDic {
  val coreMetricTestService: CoreMetricTestService = app.injector.instanceOf[CoreMetricTestServiceWithoutPhonemesDicImpl]
}
