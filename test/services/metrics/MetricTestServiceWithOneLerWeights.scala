package services.metrics

import services.CoreTestServiceWithOneLerWeights

class MetricTestServiceWithOneLerWeights extends CoreTestServiceWithOneLerWeights {
  val coreMetricTestService: CoreMetricTestService = app.injector.instanceOf[CoreMetricTestServiceWithOneLerWeightsImpl]
}
