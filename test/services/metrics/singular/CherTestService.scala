package services.metrics.singular

import models._
import services.metrics.MetricTestService

class CherTestService extends MetricTestService {

  "Correct results for test-pher" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-pher" -> AsrMetricOutputResult(
        Map[String, String](
          "CHER" -> "0.1591"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "CHER" -> "0.2836"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }


  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "CHER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "CHER" -> "0.2889"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
