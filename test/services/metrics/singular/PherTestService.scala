package services.metrics.singular

import models._
import services.metrics.MetricTestService

class PherTestService extends MetricTestService {

  "Correct results for test-pher" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-pher" -> AsrMetricOutputResult(
        Map[String, String](
          "PHER" -> "0.186"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "PHER" -> "0.2381"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
