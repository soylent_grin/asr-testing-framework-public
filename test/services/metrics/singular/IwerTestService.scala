package services.metrics.singular

import models._
import services.metrics.MetricTestService

class IwerTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_IWER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "IWER" -> "[0.0,0.0,0.0,0.629,0.629,1.0,0.0,0.0,1.0,0.0,1.0,1.0,1.0,0.0,0.629]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "IWER" -> "[0.629,0.629,0.0,0.0,0.0,0.0,1.0,0.0,0.0,1.0,0.0]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "IWER" -> "[0.0,0.0,0.0,0.0,0.0,0.0,0.0]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-hes" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-hes" -> AsrMetricOutputResult(
        Map[String, String](
          "IWER" -> "[0.0,0.0,0.0,1.0,1.0,0.0,0.4,0.4,0.0]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
