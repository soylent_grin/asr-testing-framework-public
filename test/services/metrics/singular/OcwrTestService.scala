package services.metrics.singular

import models._
import services.metrics.MetricTestService

class OcwrTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.1111"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.875"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER_CER_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_CER_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.7368"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.6"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_HES_CER_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.8235"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_NCR_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_NCR_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.6667"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_WER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_WER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "0.7778"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
