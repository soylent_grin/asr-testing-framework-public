package services.metrics.singular

import models._
import services.metrics.MetricTestService

class RerTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_MSTAT" -> AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_HES_CER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_HES_CER" -> AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER_CER_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_CER_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "0.3333"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-hes" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-hes" -> AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-rep" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-rep" -> AsrMetricOutputResult(
        Map[String, String](
          "RER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
