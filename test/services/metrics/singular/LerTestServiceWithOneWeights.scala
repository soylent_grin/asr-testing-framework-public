package services.metrics.singular

import models.{AsrMetricOutputResult, AsrMetricOutputTagsResult}
import services.metrics.MetricTestServiceWithOneLerWeights

class LerTestServiceWithOneWeights extends MetricTestServiceWithOneLerWeights {
  // Should be equal to WER
  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_HES_CER_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "LER" -> "0.2308"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
