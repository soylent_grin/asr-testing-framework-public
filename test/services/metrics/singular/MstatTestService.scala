package services.metrics.singular

import models._
import services.metrics.MetricTestService

class MstatTestService extends MetricTestService {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """{"number":{"sing":33,"plur":6},"animacy":{"inan":26,"anim":1},"aspect":{"impf":1},"voice":{"actv":1},"POS":{"NOUN":27,"ADJS":1,"CONJ":3,"PRTF":1,"PREP":3,"ADJF":10},"case":{"ablt":26,"gent":5,"datv":2,"nomn":3,"accs":1,"loct":1},"tense":{"pres":1},"transitivity":{"intr":1},"gender":{"neut":9,"masc":17,"femn":9}}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """{"number":{"sing":8},"animacy":{"inan":4,"anim":1},"POS":{"NOUN":5,"PREP":3,"ADJF":3},"case":{"ablt":1,"gent":3,"nomn":1,"accs":1,"loct":2},"gender":{"masc":4,"femn":4}}"""
      ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
