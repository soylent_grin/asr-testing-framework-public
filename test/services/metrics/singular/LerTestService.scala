package services.metrics.singular

import models.{AsrMetricOutputResult, AsrMetricOutputTagsResult}
import services.metrics.MetricTestService

class LerTestService extends MetricTestService {
  "Correct results for test-mer-individual-weights" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-mer-individual-weights" -> AsrMetricOutputResult(
        Map[String, String](
          "LER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for permutated-recognition" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "permutated-recognition" -> AsrMetricOutputResult(
        Map[String, String](
          "LER" -> "0.8"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
