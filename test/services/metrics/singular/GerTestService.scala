package services.metrics.singular

import models._
import services.metrics.MetricTestService

class GerTestService extends MetricTestService {

  "Correct results for test-wer-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-wer-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":16.0,"пш-percentage":0.0,"нрзб-percentage":43.2432,"пш-count":0.0,"нрг-count":0.0,"нрг-percentage":0.0,"WER":0.2973}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-wer-with-garbage-tags-and-complex-names" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-wer-with-garbage-tags-and-complex-names" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":16.0,"пш-percentage":0.0,"нрзб-percentage":42.1053,"пш-count":0.0,"нрг-count":0.0,"нрг-percentage":0.0,"WER":0.3421}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":2.0,"пш-percentage":0.0,"нрзб-percentage":22.2222,"пш-count":0.0,"нрг-count":0.0,"нрг-percentage":0.0,"WER":0.0}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":3.0,"пш-percentage":0.0,"нрзб-percentage":30.0,"пш-count":0.0,"нрг-count":0.0,"нрг-percentage":0.0,"WER":0.4}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-2" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":6.0,"пш-percentage":20.0,"нрзб-percentage":60.0,"пш-count":2.0,"нрг-count":2.0,"нрг-percentage":20.0,"WER":1.0}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-3" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-3" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":4.0,"пш-percentage":22.2222,"нрзб-percentage":44.4444,"пш-count":2.0,"нрг-count":1.0,"нрг-percentage":11.1111,"WER":0.8889}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "GER" -> """{"нрзб-count":4.0,"пш-percentage":16.6667,"нрзб-percentage":33.3333,"пш-count":2.0,"нрг-count":1.0,"нрг-percentage":8.3333,"WER":0.8333}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
