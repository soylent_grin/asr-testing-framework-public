package services.metrics.singular

import models._
import services.metrics.MetricTestService

class InsTestService extends MetricTestService {

  "Correct results for test-ins" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-ins" -> AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "7"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-2" -> AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-3" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-3" -> AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_MER_MSTAT" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_MSTAT" -> AsrMetricOutputResult(
        Map[String, String](
          "INS" -> "1"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
