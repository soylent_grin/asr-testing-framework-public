package services.metrics.singular

import models._
import services.metrics.MetricTestService

class DelTestService extends MetricTestService {

  "Correct results for test-del" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-del" -> AsrMetricOutputResult(
        Map[String, String](
          "DEL" -> "7"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "DEL" -> "0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "DEL" -> "1"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
