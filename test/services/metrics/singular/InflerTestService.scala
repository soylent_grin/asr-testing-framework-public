package services.metrics.singular

import models.{AsrMetricOutputResult, AsrMetricOutputTagsResult}
import services.metrics.MetricTestService

class InflerTestService extends MetricTestService {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "INFLER" -> "[0.6818,0.3043,0.0,0.0,0.4737,0.0,0.3462,0.3462,-,0.0,0.0,0.0,0.0,0.0,0.0,0.5833,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "INFLER" -> "[0.0,0.0,0.0,0.0,0.0,0.0,0.0]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "INFLER" -> "[0.0,-,0.0,0.0,0.0,0.0,0.0,-,0.0,0.0,-,0.0]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "INFLER" -> "[0.0,0.0769,0.0,0.0,0.2917,0.0,0.0,0.0,0.0,0.3462]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "INFLER" -> "[0.0,0.0769,0.0,0.0,0.2917,0.0,0.0,0.0,0.0,0.3462,-,-,-,-]"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
