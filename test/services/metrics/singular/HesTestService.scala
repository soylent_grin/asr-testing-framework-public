package services.metrics.singular

import models._
import services.metrics.MetricTestService

class HesTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_HES_CER_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "0.6667"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_HES_CER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_HES_CER" -> AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "0.8571"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER_CER_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_CER_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "0.6667"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-hes" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-hes" -> AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "0.5714"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-hes-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-hes-2" -> AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "HES" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
