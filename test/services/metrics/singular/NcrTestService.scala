package services.metrics.singular

import models._
import services.metrics.MetricTestService

class NcrTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_NCR_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_NCR_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "0.75"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_NCR_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_NCR_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }


  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_NCR-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_NCR-2" -> AsrMetricOutputResult(
        Map[String, String](
          "NCR" -> "0.6667"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
