package services.metrics.singular

import models._
import services.metrics.MetricTestService

class CerTestService extends MetricTestService {

  "Correct results for mts_test_8kz/TEST_MER_MSTAT" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_MSTAT" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_HES_CER_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER_CER_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_CER_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.8"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.2778"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.5833"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-hes" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-hes" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "When concepts-BIO and concepts-indices are set, then concepts-indices field is taken" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-cer-contradictory" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.25"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "When concepts-BIO and concepts-indices are not set, then CER is not being calculated" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-cer-without-bio-and-concepts-indices" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "When concepts-BIO is set and concepts-indices is not set, then concepts-BIO is taken" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-cer-bio-only" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "When concepts-BIO is not set and concepts-indices is set, then concepts-indices is taken" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-cer-concepts-indices-only" -> AsrMetricOutputResult(
        Map[String, String](
          "CER" -> "0.25"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
