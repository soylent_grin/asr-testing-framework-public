package services.metrics.singular

import models._
import services.metrics.MetricTestServiceWithSmallAmountOfMorphoFeatures

class MstatSimplifiedTestService extends MetricTestServiceWithSmallAmountOfMorphoFeatures {
  "No excessive morpho features" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho-closed-pos" -> AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """{}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
