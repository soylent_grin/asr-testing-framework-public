package services.metrics.singular

import models._
import services.metrics.MetricTestService

class LMerTestService extends MetricTestService {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """{"number":0.1316,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0227,"person":0.0,"case":0.1081,"tense":0.0,"transitivity":0.0,"gender":0.0588}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """{"number":0.0,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.0,"case":0.0,"tense":0.0,"transitivity":0.0,"gender":0.0}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """{"number":0.1667,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.0,"case":0.5,"tense":0.0,"transitivity":0.0,"gender":0.1667}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "LMER" -> """{"number":0.1667,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.0,"case":0.5,"tense":0.0,"transitivity":0.0,"gender":0.1667}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
