package services.metrics.singular

import models._
import services.metrics.MetricTestService

class CxerTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_CXER_CER_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_CER_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "CXER" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER" -> AsrMetricOutputResult(
        Map[String, String](
          "CXER" -> "0.2778"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "CXER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "CXER" -> "0.5833"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
