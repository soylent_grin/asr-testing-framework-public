package services.metrics.singular

import models._
import services.metrics.MetricTestService

class SfTestService extends MetricTestService {
  "Correct sf" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-sf-1",
      "test-sf-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "SF" -> "0.1538"
        ),
        List[AsrMetricOutputTagsResult]()
      ),
      2)
  }

  "Correct sf-1" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-sf-1"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "SF" -> "0.8"
        ),
        List[AsrMetricOutputTagsResult]()
      ),
      4)
  }

  "Correct sf-2" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-sf-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "SF" -> "0.125"
        ),
        List[AsrMetricOutputTagsResult]()
      ),
      1)
  }

  "Correct speed factor in case of instantaneous execution" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-sf-2"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "SF" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      ),
      0)
  }

  "Correct speed factor in case of empty audiofile" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "test-sf-0"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "SF" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      ),
      0)
  }
}
