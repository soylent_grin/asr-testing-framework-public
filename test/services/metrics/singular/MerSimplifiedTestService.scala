package services.metrics.singular

import models._
import services.metrics.MetricTestServiceWithSmallAmountOfMorphoFeatures

class MerSimplifiedTestService extends MetricTestServiceWithSmallAmountOfMorphoFeatures {
  "No excessive morpho features" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho-closed-pos" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"gender":0.0,"number":0.0}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
