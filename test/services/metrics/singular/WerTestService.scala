package services.metrics.singular

import models._
import services.metrics.MetricTestService

class WerTestService extends MetricTestService {

  "Correct average results" in {
    coreMetricTestService.testAverageMetrics(List[String](
      "mts_test_8kz/TEST_PHER_CXER_IWER",
      "mts_test_8kz/TEST_WER_CHER",
      "mts_test_16kz/TEST_NCR_IWER",
      "mts_test_8kz/TEST_NCR_CHER",
      "mts_test_8kz/TEST_MER_MSTAT",
      "mts_test_8kz/TEST_MER",
      "mts_test_16kz/TEST_IWER",
      "mts_test_16kz/TEST_IWER_HES_CER_NCR",
      "mts_test_8kz/TEST_HES_CER",
      "mts_test_16kz/TEST_CHER"
    ),
      AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.3497"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    )
  }

  "Correct results for mts_test_8kz/TEST_PHER_CXER_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_PHER_CXER_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-wer-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-wer-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.0345"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-wer-with-garbage-tags-and-complex-names" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-wer-with-garbage-tags-and-complex-names" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.1"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_WER_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_WER_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_NCR_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_NCR_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.4231"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_NCR_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_NCR_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.4286"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_MER_MSTAT" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_MSTAT" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.1818"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.3"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.4"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_HES_CER_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.2308"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_HES_CER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_HES_CER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.12"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.7692"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.375"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-2" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.375"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-3" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-3" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.4286"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "0.2727"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Wer score doesn't exceed 1.0" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "too-many-inserts" -> AsrMetricOutputResult(
        Map[String, String](
          "WER" -> "1.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
