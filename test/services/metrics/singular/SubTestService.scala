package services.metrics.singular

import models._
import services.metrics.MetricTestService

class SubTestService extends MetricTestService {

  "Correct results for test-sub" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-sub" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "5"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "2"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-2" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "2"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-3" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-3" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "2"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_MER_MSTAT" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_MSTAT" -> AsrMetricOutputResult(
        Map[String, String](
          "SUB" -> "3"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
