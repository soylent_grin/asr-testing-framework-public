package services.metrics.singular.withoutphonemesdic

import models._
import services.metrics.MetricTestServiceWithoutPhonemesDic

class PherTestService extends MetricTestServiceWithoutPhonemesDic {

  "Correct results for test-pher" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-pher" -> AsrMetricOutputResult(
        Map[String, String](
          "PHER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "PHER" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
