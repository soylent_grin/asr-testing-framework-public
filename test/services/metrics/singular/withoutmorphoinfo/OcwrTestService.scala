package services.metrics.singular.withoutmorphoinfo

import models._
import services.metrics.MetricTestServiceWithDisabledMorphoInfo

class OcwrTestService extends MetricTestServiceWithDisabledMorphoInfo {

  "Correct results for mts_test_16kz/TEST_CHER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CHER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_CXER_CER_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_CXER_CER_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_IWER_HES_CER_NCR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_HES_CER_NCR" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_NCR_IWER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_NCR_IWER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_16kz/TEST_WER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_WER" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for mts_test_8kz/TEST_OCWR" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_OCWR" -> AsrMetricOutputResult(
        Map[String, String](
          "OCWR" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
