package services.metrics.singular.withoutmorphoinfo

import models._
import services.metrics.MetricTestServiceWithDisabledMorphoInfo

class MstatTestService extends MetricTestServiceWithDisabledMorphoInfo {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "MSTAT" -> """-"""
      ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

}
