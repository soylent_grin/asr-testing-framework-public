package services.metrics.singular.withoutmorphoinfo

import models.{AsrMetricOutputResult, AsrMetricOutputTagsResult}
import services.metrics.MetricTestServiceWithDisabledMorphoInfo

class ImeraTestService extends MetricTestServiceWithDisabledMorphoInfo {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "IMERA" -> "-"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
