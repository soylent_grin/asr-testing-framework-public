package services.metrics.singular.withoutmorphoinfo

import models._
import services.metrics.MetricTestServiceWithDisabledMorphoInfo

class MerTestService extends MetricTestServiceWithDisabledMorphoInfo {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-mer-individual-weights" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-mer-individual-weights" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-mer-individual-weights-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-mer-individual-weights-2" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """-"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
