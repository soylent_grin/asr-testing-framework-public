package services.metrics.singular

import models._
import services.metrics.MetricTestService

class MerTestService extends MetricTestService {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.1282,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0222,"person":0.0,"case":0.1316,"tense":0.0,"transitivity":0.0,"gender":0.0857}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.125,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.1818,"person":0.0,"case":0.25,"tense":0.0,"transitivity":0.0,"gender":0.25}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.1667,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.0,"case":0.5,"tense":0.0,"transitivity":0.0,"gender":0.1667}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.2857,"mood":1.0,"animacy":0.0,"involvement":1.0,"aspect":0.5,"voice":0.0,"POS":0.2857,"person":0.0,"case":0.5,"tense":0.0,"transitivity":0.5,"gender":0.1667}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-mer-individual-weights" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-mer-individual-weights" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.25,"mood":0.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.0,"case":0.5,"tense":0.0,"transitivity":0.0,"gender":0.5}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-mer-individual-weights-2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-mer-individual-weights-2" -> AsrMetricOutputResult(
        Map[String, String](
          "MER" -> """{"number":0.5,"mood":1.0,"animacy":0.0,"involvement":0.0,"aspect":0.0,"voice":0.0,"POS":0.0,"person":0.5,"case":0.0,"tense":1.0,"transitivity":0.0,"gender":0.0}"""
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
