package services.metrics.singular

import models._
import services.metrics.MetricTestService

class IweraTestService extends MetricTestService {

  "Correct results for mts_test_16kz/TEST_IWER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_16kz/TEST_IWER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "IWERA" -> "0.4591"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "IWERA" -> "0.2962"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-garbage-tags-right" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-garbage-tags-right" -> AsrMetricOutputResult(
        Map[String, String](
          "IWERA" -> "0.0"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-with-hes" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-with-hes" -> AsrMetricOutputResult(
        Map[String, String](
          "IWERA" -> "0.3111"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
