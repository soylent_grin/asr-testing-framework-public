package services.metrics.singular

import models._
import services.metrics.MetricTestService

class WmerTestService extends MetricTestService {

  "Correct results for test-morpho" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-morpho" -> AsrMetricOutputResult(
        Map[String, String](
          "WMER" -> "0.0564"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results for test-from-mail" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "test-from-mail" -> AsrMetricOutputResult(
        Map[String, String](
          "WMER" -> "0.1384"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER" -> AsrMetricOutputResult(
        Map[String, String](
          "WMER" -> "0.0935"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }

  "Correct results mts_test_8kz/TEST_MER_2" in {
    coreMetricTestService.testMetrics(Map[String, AsrMetricOutputResult](
      "mts_test_8kz/TEST_MER_2" -> AsrMetricOutputResult(
        Map[String, String](
          "WMER" -> "0.2816"
        ),
        List[AsrMetricOutputTagsResult]()
      )
    ))
  }
}
