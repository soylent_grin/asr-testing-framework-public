package services.metrics

import com.google.inject.Singleton
import services.CoreTestServiceWithSmallAmountOfMorphoFeatures

@Singleton
class CoreMetricTestServiceWithSmallAmountOfMorphoFeatures extends CoreTestServiceWithSmallAmountOfMorphoFeatures with CoreMetricTestService {}