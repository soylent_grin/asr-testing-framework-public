package services.preprocessing

import com.google.inject.{ImplementedBy, Singleton}
import factories.TokenSequenceFactory
import models._
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.{DefaultAwaitTimeout, FutureAwaits}
import services.CoreTestService
import services.metrics.tokenization.TokenizationService

import scala.collection.mutable.ListBuffer

@ImplementedBy(classOf[CorePreprocessingTestServiceImpl])
trait CorePreprocessingTestService extends PlaySpec with GuiceOneAppPerSuite with FutureAwaits with DefaultAwaitTimeout {
  def checkTagsClearing(stringWithTags: String, stringWithoutTags: String, excludeTags: Option[Set[String]] = None): Unit

  def checkGarbageIndexes(stringWithTags: String, garbageList: List[(Int, String)]): Unit

  @deprecated("Potential source of errors")
  def checkTagsClearingLocally(stringWithTags: String, stringWithoutTags: String, excludeTags: Option[Set[String]] = None): Unit
}

@Singleton
class CorePreprocessingTestServiceImpl extends CoreTestService with CorePreprocessingTestService {

  def checkTagsClearing(stringWithTags: String, stringWithoutTags: String, excludeTags: Option[Set[String]] = None): Unit = {
    val tokenSequenceFactory = app.injector.instanceOf[TokenSequenceFactory]

    var tokenSequence = tokenSequenceFactory.make(stringWithTags)

    if (excludeTags.nonEmpty){
      tokenSequence = tokenSequence.filterByNotPresentedTags(excludeTags.get)
    }

    tokenSequence.tokens.map(token => token.value).mkString(" ") mustBe stringWithoutTags
  }

  def checkTagsClearingLocally(stringWithTags: String, stringWithoutTags: String, excludeTags: Option[Set[String]] = None): Unit = {
    var (tokens, complexIndices, tags, nrzb) = app.injector.instanceOf[TokenizationService].tokenizeWithTags(stringWithTags)
    if (excludeTags.nonEmpty){
      val zippedTokens = tokens.zipWithIndex.map(pair =>
        (pair._1, complexIndices(pair._2), tags(pair._2).toSet)).toList
      tokens = zippedTokens.filter(zippedToken => zippedToken._3.intersect(excludeTags.get).isEmpty). // remove all tokens w/ tags intersected with excluded
        map(zippedToken => zippedToken._1).to[ListBuffer] // map result back to list of tokens
    }
    tokens.mkString(" ") mustBe stringWithoutTags
  }

  def checkGarbageIndexes(stringWithTags: String, garbageList: List[(Int, String)]): Unit = {
    val tokenSequenceFactory = app.injector.instanceOf[TokenSequenceFactory]

    val tokenSequence = tokenSequenceFactory.make(SoundbankSample(stringWithTags), "")

    tokenSequence.meta.garbageIndices mustBe garbageList
  }
}