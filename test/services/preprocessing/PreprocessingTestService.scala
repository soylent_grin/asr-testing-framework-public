package services.preprocessing

import services.CoreTestService

class PreprocessingTestService extends CoreTestService {
  val asrCorePreprocessingTestService: CorePreprocessingTestService =
    app.injector.instanceOf[CorePreprocessingTestService]
}