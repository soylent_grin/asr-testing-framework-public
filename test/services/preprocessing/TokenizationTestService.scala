package services.preprocessing

class TokenizationTestService extends PreprocessingTestService {

  "Simple clearing" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(фио: Иванов Иван Иванович)",
      "Иванов Иван Иванович"
    )
  }

  "Simple clearing with excessive spaces" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "( фио   :   Иванов Иван Иванович )",
      "Иванов Иван Иванович"
    )
  }

  "Simple clearing with latin letters" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "( фио   :   Иванoв Иван Иванoвич )",
      "Иванoв Иван Иванoвич"
    )
  }

  "Simple clearing with latin letters in tag name" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "( фиo   :   Иванoв Иван Иванoвич )",
      "Иванoв Иван Иванoвич"
    )
  }

  "Simple clearing with numbers in tag name" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(222:   Иванoв Иван Иванoвич )",
      "Иванoв Иван Иванoвич"
    )
  }

  "Simple clearing with nested tag" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(фио: Иванoв Иван (нрзб: Иванoвич))",
      "Иванoв Иван Иванoвич"
    )
  }

  "Retain ambiguity tags" in {
    for (tag <- List[String]("отр", "числ", "нрзб","нцз","пд","пс","пн","тел","фио","элп","урл","нз","нд","нк", "лс","скн","вр","др","д","кэо", "кс")) {
      asrCorePreprocessingTestService.checkTagsClearing(
        f"($tag: Иванов Иван Иванович)",
        "Иванов Иван Иванович"
      )
    }
  }

  "Delete ambiguity tags" in {
    val ambiguityTagsToRemove = List[String]("пш","нрг")
    for (tag <- ambiguityTagsToRemove) {
      asrCorePreprocessingTestService.checkTagsClearing(
        f"($tag: Иванов Иван Иванович)",
        "",
        Some(ambiguityTagsToRemove.toSet)
      )
    }
  }

  "Full nrzb with spaces in tag name" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "( нрзб)",
      ""
    )
  }

  "Full nrzb with comment" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(нрзб чиво)",
      "нрзб чиво"
    )
  }

  "Partial nrzb" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(нрзб: чиво сказал)",
      "чиво сказал"
    )
  }

  "Excluding nrzb" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "странная жизнь подумал старик, (нрзб: чиво сказал) подумал мальчик",
      "странная жизнь подумал старик подумал мальчик",
      Some(Set[String]("нрзб"))
    )
  }

  "Nrzb as a plain text" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "это просто текст в нем нет ни каких меток но его можно испр нрзб",
      "это просто текст в нем нет ни каких меток но его можно испр нрзб"
    )
  }

  "Replace from brackets" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) я ещё раз те (тебе) его навешу (пш: шум транктора)",
      "сейчас я ещё раз тебе его навешу"
    )
  }

  "Replace from brackets and delete tag" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) я ещё раз те (тебе) его навешу (пш: шум транктора)",
      "сейчас я ещё раз тебе его навешу"
    )
  }

  "seq_nested_brackets" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) я ещё,раз те (тебе) его,навешу (фио: Иван Петрович (нрзб: Кулябякин)) обяза " +
        "(обязательно)",
      "сейчас я ещё раз тебе его навешу Иван Петрович Кулябякин обязательно"
    )
  }

  "seq_nested_brackets_punct" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) я ещё,раз те (тебе) его,навешу (фио: Иван Петрович (нрзб: Кулябякин)) обяза " +
        "(обязательно)",
      "сейчас я ещё раз тебе его навешу Иван Петрович Кулябякин обязательно"
    )
  }

  "seq_nested_cbrackets_mix" in {
    assertThrows[IllegalStateException](
      asrCorePreprocessingTestService.checkTagsClearing(
        "щас {сейчас) я ещё раз те (тебе} его навешу (фио: Иван Петрович (нрзб: Кулябякин}) обяза " +
          "(обязательно)",
        "сейчас я ещё раз тебе его навешу Иван Петрович Кулябякин обязательно"
      )
    )
  }

  "seq_brackets_hisitation" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) я ещё раз те (тебе) его навешу (пш: шум транктора) (рп: угуу)",
      "сейчас я ещё раз тебе его навешу угуу"
    )
  }

  "seq_brackets_hisitation without tag" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) я ещё раз те (тебе) его навешу (пш: шум транктора) (рп: угуу)",
      "сейчас я ещё раз тебе его навешу угуу")
  }

  "reduction_nested_in_numerical" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(числ: триста шейсят (шестьдесят) один) рубль.",
      "триста шестьдесят один рубль")
  }

  // тесты на работоспособность удаления\замены хезитаций и редукций
  "reduction_nested_in_numerical_full_form" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(числ: триста шейсят (шестьдесят) один) рубль.",
      "триста шестьдесят один рубль")
  }

  "test_reduction_multi_nested_in_numerical_full_form" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(числ: триста шейсят (шестьдесят) один и пя (пять)) рубль ща (сейчас) как принесу.",
      "триста шестьдесят один и пять рубль сейчас как принесу")
  }

  "rmv_hesitations_reductions_full" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) (рп: ага) (рп: хм) (рп: а) (рп: ааа) (рп: у) (рп: э) я ещё раз те (тебе) его навешу " +
        "(пш: шум транктора) (рп: угуу)",
      "сейчас я ещё раз тебе его навешу",
      Some(Set[String]("рп")))
  }

  "test_hesitations_keep_multiple" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) (рп: ага) (рп: хм) (рп: а) (рп: ааа) (рп: у) (рп: э) я ещё раз те (тебе) его навешу " +
        "(пш: шум транктора) (рп: угуу)",
      "сейчас ага хм а ааа у э я ещё раз тебе его навешу угуу")
  }

  "test_unk_tag" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "щас (сейчас) (рпп: аааггаа) (рп: ага) (рп: хм) (рп: а) (рп: ааа) (рп: у) (рп: э) я ещё раз те " +
        "(тебе) его навешу (пш: шум транктора) (рп: угуу)",
      "сейчас ага хм а ааа у э я ещё раз тебе его навешу угуу",
      Some(Set[String]( "рпп")))
  }

  "test_nrzb_numerical_t1" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "Такого никогда не было вот у меня у напарника Билайн (нрзб) (числ: триста, четыреста) рублей платит" +
        " и всё,",
      "Такого никогда не было вот у меня у напарника Билайн триста четыреста рублей платит и всё")
  }

  "test_nrzb_numerical_t2" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "Вы можете позвонить на номер (числ: ноль, восемь, восемь, семь)",
      "Вы можете позвонить на номер ноль восемь восемь семь")
  }

  "dashwords_1" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "Вы можете кое-как позвонить на чудо-номер (числ: ноль, восемь, восемь, семь) а  то как-нибудь " +
        "доберусь до вас всё-таки, что-либо это значит для вас, а для нас пути-то известны. любители сим-карт и " +
        "турбо-кнопок",
      "Вы можете кое как позвонить на чудо номер ноль восемь восемь семь а то как нибудь доберусь до " +
        "вас всё таки что либо это значит для вас а для нас пути то известны любители сим карт и турбо кнопок")
  }

  "dash_words_spaces_inside" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "Вы можете кое -как или кое- как позвонить на чудо-номер (числ: ноль, восемь, восемь, семь) а  то " +
        "как-нибудь доберусь до вас всё-таки, что-либо это значит для вас, а для нас пути-то известны. любители " +
        "сим-карт и турбо-кнопок",
      "Вы можете кое как или кое как позвонить на чудо номер ноль восемь восемь семь а то как нибудь " +
        "доберусь до вас всё таки что либо это значит для вас а для нас пути то известны любители сим карт и турбо" +
        " кнопок")
  }

  // ITMO tests
  "Without tag" in {
    assertThrows[IllegalStateException](
      asrCorePreprocessingTestService.checkTagsClearing(
        "щас (  : щас)",
        "щас щас"
      )
    )
  }

  "Basic test" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "а (как) (нрзб: потом что-либо) (потом) (нрзб) ээ к " +
        "памятнику ( пш  : вой сирены) маяковскому " +
        "(пш: я (фио: повернул)) но там было (нрзб: все перекопано) " +
        "(рп: мм) ленты какие-то ограждения вся триумфальная площадь в них",
      "как потом потом ээ к памятнику маяковскому повернул но там было все перекопано мм ленты какие то ограждения вся триумфальная площадь в них"
    )
  }

  //
  // Garbage tests
  //

  "Check garbage indexes" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "а (как) (нрзб: потом что-либо) (потом) (нрзб) ээ к",
      List((1,"нрзб"), (2,"нрзб"))
    )
  }

  "Check garbage indexes 2" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "а (как) (нрзб: потом что-либо) (потом) ээ к",
      List((1,"нрзб"))
    )
  }

  "Check garbage indexes 3" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "а (как) (нрзб: потом что-либо) (нрзб) ээ к",
      List((3,"нрзб"))
    )
  }

  "Check garbage indexes 4" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "а (нрзб: потом ) что-либо (нрзб) ээ к",
      List((1,"нрзб"), (3,"нрзб"))
    )
  }

  "Check garbage indexes 5" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "а (нрзб: потом (нрзб)) что-либо (нрзб) (нрзб) ээ к",
      List((1,"нрзб"), (3,"нрзб"))
    )
  }

  "Check garbage indexes with multiple" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "а (пш: потом (нрзб)) что-либо (нрзб) (нрзб) ээ к (нрг: (пш: чему)) (нрзб)",
      List((0,"нрзб"), (0,"пш"), (2,"нрзб"), (4,"нрг"), (4,"нрзб"), (4, "пш"))
    )
  }

  "Check garbage indexes with multiple 2" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "(рп: (аг: ав) ваыа) а (пш: потом (нрзб)) что-либо (нрзб) (нрзб) ээ к (нрг: алло (нрзб) (пш: чему)) (нрзб)",
      List((2,"нрзб"), (2,"пш"), (4,"нрзб"), (6,"нрг"), (6,"нрзб"), (6, "пш"))
    )
  }

  "Check tags with garbage" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      "(рп: (аг: ав) ваыа) а (пш: потом (нрзб)) что-либо (нрзб) (нрзб) ээ к (нрг: алло (нрзб) (пш: чему)) (нрзб) (нрзб: ваыа) (нрзб)",
      List((2, "нрзб"), (2, "пш"), (4, "нрзб"), (6, "нрг"), (6, "нрзб"), (6, "пш"), (7, "нрзб"))
    )
  }

  "Check tags with garbage 2" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "(рп: (аг: ав) ваыа) а (пш: потом (нрзб)) что-либо (нрзб) (нрзб) ээ к (нрг: алло (нрзб) (пш: чему)) (нрзб) (нрзб: ваыа) (нрзб) ",
      "ав ваыа а что либо ээ к ваыа"
    )
  }

  "Example from doc" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      "я купил диван-кровать (пш: шум трактора) бледно-зеленого цвета (нрзб: вчера вечером). ",
      "я купил диван кровать бледно зеленого цвета вчера вечером"
    )
  }

  "Example from mail" in {
    asrCorePreprocessingTestService.checkTagsClearing(
      " (нрзб: рассмотрим) пример с разной токенизацией (пш: шум поезда) с тэгами мусорной модели (нрг: кашель) в нашем проекте (нрзб)",
      "рассмотрим пример с разной токенизацией с тэгами мусорной модели в нашем проекте"
    )
  }

  "Check tags with garbage from mail" in {
    asrCorePreprocessingTestService.checkGarbageIndexes(
      " (нрзб: рассмотрим) пример с разной токенизацией (пш: шум поезда) с тэгами мусорной модели (нрг: кашель) в нашем проекте (нрзб)",
      List((0, "нрзб"), (4, "пш"), (8, "нрг"), (11, "нрзб"))
    )
  }

}