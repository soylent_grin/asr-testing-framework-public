package services

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application

class CoreTestService extends PlaySpec with GuiceOneAppPerSuite {
  implicit override lazy val app: Application =
    GuiceApplicationBuilderFactory.make().build()
}
