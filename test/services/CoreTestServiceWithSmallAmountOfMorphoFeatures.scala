package services

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application

class CoreTestServiceWithSmallAmountOfMorphoFeatures extends PlaySpec with GuiceOneAppPerSuite {
  implicit override lazy val app: Application =
    GuiceApplicationBuilderFactory.make()
      .configure(Map[String, Any](
        "metric.otherMorphoFeatureList" -> List[String]("POS"),
        "metric.merMorphoFeatureList" -> List[String]("gender", "number"),
        "metric.mstatMorphoFeatureList" -> List[String]("gender", "number"),
        "metric.morphoFeaturesWeights" -> Map[String, Double](
          "gender" -> 0.5,
          "number" -> 0.5
        )))
      .build()
}
