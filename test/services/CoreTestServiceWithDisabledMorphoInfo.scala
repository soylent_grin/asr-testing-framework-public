package services

import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application

class CoreTestServiceWithDisabledMorphoInfo extends PlaySpec with GuiceOneAppPerSuite {
  implicit override lazy val app: Application =
    GuiceApplicationBuilderFactory.make()
      .configure("metric.morphoFeaturesExtractorScriptPath" -> "????")
      .build()
}
