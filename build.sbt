import play.grpc.gen.scaladsl.{ PlayScalaClientCodeGenerator, PlayScalaServerCodeGenerator }

name := """cvoaf"""
organization := "ru.isst-mts"

version := "2.0-SNAPSHOT"

val distName = "cvoaf"
val distVersion = "2.0.2"

lazy val root = (project in file("."))
  .settings(
    // common packaging options
    maintainer := "isst-mts",
    packageName := distName,
    packageSummary := s"$distName summary",
    packageDescription := s"$distName package description",
    executableScriptName := distName,

    // deb-packaging options
    name in Debian := distName,
    version in Debian := distVersion,
    description in Debian := s"$distName description",
  )
  .dependsOn(stcApi, tinkoffApi)
  .aggregate(stcApi, tinkoffApi)
  .enablePlugins(PlayScala, AkkaGrpcPlugin)

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  guice,
  ws,
  "org.scala-lang.modules" %% "scala-java8-compat" % "0.9.0",
  "com.outworkers" %% "phantom-dsl" % "2.26.1", // 2.14.5 2.26.1
  "net.codingwell" %% "scala-guice" % "4.2.8",
  "org.typelevel" %% "cats" % "0.9.0",
  "org.reflections" % "reflections" % "0.9.10",
  "com.datastax.cassandra" % "cassandra-driver-core" % "3.6.0",
  "commons-io" % "commons-io" % "2.7",
  "com.typesafe.play" %% "play-json" % "2.9.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.0" % "test",
  "com.mohiva" %% "play-silhouette" % "7.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "7.0.0",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "7.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "7.0.0",
  "com.lightbend.play" %% "play-grpc-runtime" % "0.9.0"
)


javaOptions in Test += "-Dconfig.resource=test.conf"

PlayKeys.devSettings += "play.server.http.idleTimeout" -> "infinite"

// modules

lazy val stcApi: Project = (project in file("modules/stc-api"))
  .settings(Seq(
    version := "1.0.0",
    libraryDependencies := Seq(
      "com.google.code.gson" % "gson" % "2.8.5",
      "com.neovisionaries" % "nv-websocket-client" % "2.10",
      "com.squareup.okhttp" % "logging-interceptor" % "2.7.5",
      "io.gsonfire" % "gson-fire" % "1.0.1",
      "org.threeten" % "threetenbp" % "1.3.6",
      "io.swagger" % "swagger-annotations" % "1.5.0"
    )
  ))

lazy val tinkoffApi: Project = (project in file("modules/tinkoff-api"))
  .settings(Seq(
    version := "1.0.0",
    // #grpc_client_generators
    // build.sbt
    libraryDependencies := Seq(
      guice,
      "com.google.protobuf" % "protobuf-java" % "3.13.0" % "protobuf",
      "com.google.api.grpc" % "grpc-google-common-protos" % "1.17.0" % "protobuf",
      "com.google.api.grpc" % "grpc-google-common-protos" % "1.17.0",
      "com.lightbend.play" %% "play-grpc-runtime" % "0.9.0",
      "com.lightbend.play" %% "play-grpc-generators" % "0.9.0",
      "com.pauldijou" %% "jwt-core" % "4.2.0"
    ),
    akkaGrpcGeneratedSources := Seq(AkkaGrpc.Client),
    akkaGrpcGeneratedLanguages := Seq(AkkaGrpc.Java),
    // akkaGrpcExtraGenerators += PlayScalaClientCodeGenerator
  ))
  .enablePlugins(PlayScala, AkkaGrpcPlugin)

