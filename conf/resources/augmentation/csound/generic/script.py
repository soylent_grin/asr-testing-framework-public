# -*- coding: utf-8 -*-

import csnd6
import json
import shutil
import sys
reload(sys)
sys.setdefaultencoding('utf8')

conf_file = sys.argv[1]

print("using conf file: ", conf_file)

def getOrc(sample, params):
    return """
; заголовок Csound. установка основных параметров
; sr - частота дискретизации звука
; ksmps - делитель частоты дискретизации для определения частоты обновления сигналов управления
; nchnls - количество выходных каналов
; 0dbfs - нормировка амплитуды
	sr = {sampleRate}
    ksmps = 32
    nchnls = 1
    0dbfs = 1

    seed 454634   ; зерно случайного процесса
    strset 1, "{file}"   ; создание указателей на исходные звуковые файлы
    strset 2, "{backgroundNoise}"
    strset 4, "{pauseNoise}"

    gaL init 0   ; инициализация выходной шины
    gaRevL init 0 ; инициализация шины реверберации

	instr 10 ; контроллер воспроизведения
; в данном инструменте реализуется запуск различных инструментов, отвечащих за воспроизведение
			iMAX = {maxAmplitude}  ; максимальная амплитуда звукового файла
			iflen = ftlen(1) ; длина VAD таблицы
			iVLen filelen 1 ; длина файла с речью
			iCLen filelen 4 ; длина файла со вставкой
			iStrType = p5
  		iVpch = p4
  		iC = 1/iVpch
			if iCLen < 4 goto Next  ; ограничение длительности вставки до 4 с
			 		iCLen = 4
Next:
			irandcough random 0, 1
			iX init 0
			if irandcough < p8 then    ; определение наличия/отсутствия вставки по случайному закону
			 		iX = 1
			endif
			iMaxL = iVLen*iC + iCLen*iX
  		kL init iCLen
  		; воспроизведение блока голосового файла и вставки (при наличии)
  		; если воспроизведение замедлено, это учитывается
  		kL0 init 0
			kndx init 0
			kFla init 0

			if kndx >= iflen goto Over
							kS table kndx, 1
							kF table kndx+1, 1
						event "i", 1,kS*iC+kL0,iC*(kF-kS),1,1,0,iVpch,iStrType,kS
						if (iX != 1)||(kndx+2 == iflen)||(kFla != 0) goto Out  ; <- COUGH PROBABILITY
						  event "i", 1,kF*iC+kL0,kL,4,iMAX,0,1,-1,0
						  kL0 += kL
						  kFla = -1
Out:
				    kndx += 2
Over:
; воспроизведение фонового шума
		if p9 == -1 goto NoAmb
		event_i "i", 2, 0, iMaxL, iMAX  ; запуск контроллера фонового шума
NoAmb:
; воспроизведение белого шума
        if p10 == -1 goto NoWN
		event_i "i", 31, 0, iMaxL, iMAX  ; запуск генератора белого шума
NoWN:
; включение мастер-инструмента и ревербератора
		event_i "i", 4,0,iMaxL
		event_i "i", 5,0,iMaxL*1.1, p7

endin

instr 1 ; основной плеер
    Sfile1 strget p4  ; определение имени файла
    ichn filenchnls Sfile1  ; чтение количества каналов
    ilen filelen Sfile1 ; чтение длины файла

    ipitch	= p7  ; скорость воспроизведения

    if (ichn == 1) then	;чтение файла
       a1 diskin2 Sfile1, ipitch, p9, p6
    else
       a1,a2 diskin2 Sfile1, ipitch, p9, p6
    endif

    if p8 != -1 goto NoStretch   ; определение алгоритма изменения скорорсти
       istrch = 1/ipitch

; параметры Быстрого преобразования Фурье
; ===FFT parameters====
ifft	=	1024	; fft size
ihop	=	ifft*.25	; hop size
iwinsize	=	ifft*2	; analysis window size
iwintype	=	0	; window type (hamming)
inbins	=	(ifft/2) + 1
;======================
; переход в частотную облась
       			fsrc1	pvsanal	a1, ifft, ihop, iwinsize, iwintype	; phase-vocoding analyis of signal
					a1 	pvsadsyn	fsrc1, inbins, istrch	; pitch-shifted resynthesis
NoStretch:
 kEnv linseg 0,0.001, 1, p3-0.101, 1, 0.1, 0  ; общая огибающая
 aOL = a1 * p5 * kEnv
 gaL += aOL * (1 - {reverbDistance})  ; рандомизация прямого сигнала (необработанного)
 gaRevL += a1 * aOL * ({reverbDistance}); посыл на ревербератор
endin

instr 2 ; контроллер скремблинга фонового шума
	k1 gausstrig 1, .07, .8, 1   ; серия случайных импульсов, распределенных по Гауссу
	; .07 - частота, .8 - девиация (0..1)
	if k1 == 0 goto Over
		turnoff2 21, 0, 1    ; остановка предыдущего инстанса инструмнента 21 с возможность отработки огибающих
		event "i", 21, 0, p3, p4  ; запуск нового инстанса инстурмента 21
	Over:
endin

instr 21  ; воспроизведение фрагмента фонового шума
	k1 linsegr 0, 1, 1, p3-2, 1, 1, 0, 1, 0  ; плавная огибающая (подъем и спад длиной 1 с)
	ir random 0, .9   ; рандомизация точки начала воспроизведения 0..0.9
	iLen filelen 2
	ichn filenchnls 2  ; чтение количества каналов

	if (ichn == 1) then	;чтение файла фонового шума с выбранной позиции, в кольце
       a1 diskin2 2, 1, iLen*ir, 2
    else
       a1,a2 diskin2 2, 1, iLen*ir, 2
    endif
	gaL += a1 * p4
endin


instr 3 ; триггер сэмплов по случайному закону
    k1 metro 1  ; клок
    k2 random 0, 1
    if k1 != 1 goto Over
    if k2 > p4 goto Over
    event "i",1,0,1,3,0.9*p5, 0, 1, -1,0,0
    Over:
endin

instr 31 ; WHITE NOISE GEN
    k1 linseg 0, 0.01, 1, p3-0.02, 1, 0.01, 0
    a1 rand p4*k1
    a1 butterhp a1, {whiteNoiseLowFreq}
    a1 butterlp a1, {whiteNoiseHighFreq}
    gaL += a1
endin

instr 4 ; мастер инструмент
        iMASTER = {volume} ; общий уровень громкости
        a1 limit gaL*iMASTER ,-0.99, 0.99 ; ограничитель по уровню
        out a1
        gaL = 0
endin

instr 5 ; ревебератор
        ; TODO Глеб
        ; reverbDistance - расстояние от спикера до микрофона
        ; reverbRoomSize - размер комнаты
        ; reverbLevel - общий уровень реверберации
        ir random 0, 1  ; рандомизация параметров ревербератора
        a1,a2 reverbsc gaRevL,gaRevL, {reverbRoomSize}, 8000
        gaL += a1 * {reverbLevel}
     	gaRevL = 0
endin
    """.format(
        file=sample["path"],
        sampleRate=sample.get("sampleRate", 8000),
        backgroundNoise=sample.get("backgroundNoise", sample["path"]),
        pauseNoise=sample.get("pauseNoise", sample["path"]),
        maxAmplitude=sample.get("maxAmplitude", 0.705),
        volume=params.get("volume", 1),
        whiteNoiseLowFreq=params.get("whiteNoiseLowFreq", 0),
        whiteNoiseHighFreq=params.get("whiteNoiseHighFreq", 0),
        reverbDistance=sample.get("reverbDistance", 0),
        reverbRoomSize=sample.get("reverbRoomSize", 0),
        reverbLevel=sample.get("reverbLevel", 0)
    )

def getScore(sample, params):
    return """
        f1 0 0 -2 {vad}; VAD table
        ; p4 - voice speed (1 - original, >1 faster, <1 slower)
        ; p5 - tone correction (-1 correction off,otherwise correc on)
        ; p6 - retrigger probability (0..1)
        ; p7 - reverb parameter
        ; p8 - voice artefacts probabiliy (0..1)
        ; p9 - ambience on/off (-1 - off)
        ;p1  p2  p3  p4        p5                 p6                      p7        p8                      p9                    p10

        i10  0   .1  {speed}   {toneCorrection}   {retriggerProbability}  {reverb}  {pauseNoiseProbability} {withBackgroundNoise} {withWhiteNoise}
    """.format(
        vad=sample["vad"],
        speed=params.get("speed", "1"),
        toneCorrection=params.get("toneCorrection", "-1"),
        retriggerProbability=params.get("retriggerProbability", ".3"),
        pauseNoiseProbability=params.get("pauseNoiseProbability", "1"),
        withBackgroundNoise=params.get("withBackgroundNoise", "1"),
        withWhiteNoise=params.get("withWhiteNoise", "-1"),
        reverb=params.get("reverb", "0")
    )

with open(conf_file, "r") as file:
    conf_file_content = file.read()
    conf = json.loads(conf_file_content)
    for sample in conf["samples"]:
        print "running on sample: {sample}".format(sample=sample["path"])
        output_file = "{sample}.output".format(sample=sample["path"])

        c = csnd6.Csound()

        orc = getOrc(sample, conf["params"])

        sco = getScore(sample, conf["params"])

        c.SetOption("-o{filename}".format(filename = output_file))
        c.SetOption("-Onull")

        c.CompileOrc(orc)
        c.ReadScore(sco)
        c.Start()
        c.Perform()
        c.Stop()

        shutil.move(output_file, sample["path"])