#!/usr/bin/env python3

import collections
import contextlib
import sys
import wave

import webrtcvad

AGRESIVENESS = 3    # agresiveness of filtering non-speech, vslue: 0-3, 3 is most agressive value
MIN_SILENCE_MS = 4000 # min silence in ms


def read_wave(path):
    """Reads a .wav file.
    Takes the path, and returns (PCM audio data, sample rate).
    """
    with contextlib.closing(wave.open(path, 'rb')) as wf:
        num_channels = wf.getnchannels()
        assert num_channels == 1
        sample_width = wf.getsampwidth()
        assert sample_width == 2
        sample_rate = wf.getframerate()
        assert sample_rate in (8000, 16000, 32000, 48000)
        pcm_data = wf.readframes(wf.getnframes())
        return pcm_data, sample_rate


class Frame(object):
    """Represents a "frame" of audio data."""
    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration


def frame_generator(frame_duration_ms, audio, sample_rate):
    """Generates audio frames from PCM audio data.
    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.
    Yields Frames of the requested duration.
    """
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    timestamp = 0.0
    duration = (float(n) / sample_rate) / 2.0
    while offset + n < len(audio):
        yield Frame(audio[offset:offset + n], timestamp, duration)
        timestamp += duration
        offset += n


def vad_collector(sample_rate, frame_duration_ms,
                  padding_duration_ms, vad, frames,
                  padding_duration_ms_sil):
    """Filters out non-voiced audio frames.
    Given a webrtcvad.Vad and a source of audio frames, yields only
    the voiced audio.
    Uses a padded, sliding window algorithm over the audio frames.
    When more than 90% of the frames in the window are voiced (as
    reported by the VAD), the collector triggers and begins yielding
    audio frames. Then the collector waits until 90% of the frames in
    the window are unvoiced to detrigger.
    The window is padded at the front and back to provide a small
    amount of silence or the beginnings/endings of speech around the
    voiced frames.
    Arguments:
    sample_rate - The audio sample rate, in Hz.
    frame_duration_ms - The frame duration in milliseconds.
    padding_duration_ms - The amount to pad the window, in milliseconds.
    vad - An instance of webrtcvad.Vad.
    frames - a source of audio frames (sequence or generator).
    Returns: A generator that yields PCM audio data.
    """
    num_padding_frames = int(padding_duration_ms / frame_duration_ms)
    num_padding_frames_sil = int(padding_duration_ms_sil / frame_duration_ms)
    # We use a deque for our sliding window/ring buffer.
    ring_buffer = collections.deque(maxlen=num_padding_frames)
    ring_buffer_sil = collections.deque(maxlen=num_padding_frames_sil)
    # We have two states: TRIGGERED and NOTTRIGGERED. We start in the
    # NOTTRIGGERED state.
    triggered = False

    voiced_frames = []
    start = end = 0
    curpos = 0
    add = False
    for frame in frames:
        is_speech = vad.is_speech(frame.bytes, sample_rate)
        if add and not is_speech:
            add=False
            voiced_frames+=[[start, curpos]]
            start = 0
        if not add and is_speech:
            start = curpos
            add=True
        curpos+=frame.duration
    if is_speech:
        voiced_frames += [[start,curpos]]
    if not voiced_frames:
        return []
    vf = [voiced_frames[0]]
    emp = 0
    for i in voiced_frames[1:]:
        if not (i[0] - vf[-1][1]>0.3):
            #merge frames
            emp+=(i[0]-vf[-1][1])
            vf[-1] = [vf[-1][0], i[1]]
        else:
            st = vf[-1][0]-0.03 if vf[-1][0]>0.03 else 0
            end = vf[-1][1]+0.03
            vf[-1] = [st, end]
            vf+=[i]
            emp=0
    return vf


def main(args):
    audio, sample_rate = read_wave(args)
    vad = webrtcvad.Vad(AGRESIVENESS)
    frames = frame_generator(30, audio, sample_rate)
    frames = list(frames)
    segments1 = vad_collector(sample_rate, 30, 300, vad, frames, MIN_SILENCE_MS)
    return segments1


if __name__ == '__main__':
    #usage python3 vad.py file.wav
    if len(sys.argv)!=2:
        print('Usage: vad.py audio.wav')
        exit(1)
    print(main(sys.argv[1]))
 
