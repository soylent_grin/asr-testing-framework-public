#!/bin/bash

# Copyright 2012 Vassil Panayotov
# Apache 2.0
pushd /cvoaf/asr_scripts/kaldi/mts-ivector-train || exit 1

# Copyright 2016 Ubskiy Dmitriy, STC

# NOTE: You will want to download the data set first, before executing this script.
#       This can be done for example by:
#       1. Setting the DATA_ROOT variable to point to a directory with enough free
#          space (at least 20-25GB currently (Feb 2014))
#       2. Running "getdata.sh"

# The second part of this script comes mostly from egs/rm/s5/run.sh
# with some parameters changed

# Changed by Anton Okhotnikov
# Instead fMLLR use MFCC + iVector features


source ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

# The number of parallel jobs to be started for some parts of the recipe
# Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
exp_config=/cvoaf/temp/mts-ivector-train/conf/exp.conf

EXP_ROOT=
DATA_ROOT=
train_list=
test_list=
njobs=
retry_beam_mono=
mono_subset=
# Word position dependent phones?
pos_dep_phones=

CUR_STEP=

# use CUDA true/false
gpu=

source ${exp_config}

# copy steps folder
if [[ ! -d steps ]]; then
  ./make_links.sh
fi

# parse options like CUR_STEP
./utils/parse_options.sh || exit 1;

if [[ ${CUR_STEP} -eq 0 ]]; then
echo "Current step $CUR_STEP: preparing dir structure."
[[ -d ${EXP_ROOT} ]] && rm -rf ${EXP_ROOT}
mkdir -p ${EXP_ROOT}
fi

echo "============= Training process started ============="
echo ${train_list}
echo ${test_list}
echo "===================================================="

start_time=$(date +%s)

# The user of this script could change some of the above parameters. Example:
# /bin/bash run.sh --pos-dep-phones false
source utils/parse_options.sh || exit 1

[[ $# -ge 1 ]] && { echo "Unexpected arguments"; exit 1; }

exp=${EXP_ROOT}
data_txt_dir=${exp}/data_txt
hclg_dir=${exp}/HCLG
log_dir=${exp}/log
models_dir=${exp}/models
conf=/cvoaf/temp/mts-ivector-train/conf

scoring_opts="--min-lmwt 5 --max-lmwt 30"

if [[ ${CUR_STEP} -eq 0 ]]; then
#echo "Current step $CUR_STEP"
mkdir -p ${data_txt_dir}/train
mkdir -p ${data_txt_dir}/test
mkdir -p ${log_dir}
mkdir -p ${models_dir}
let "CUR_STEP = $CUR_STEP + 1"
fi

# Preparing train and test lists and language modelling data
if [[ ${CUR_STEP} -eq 1 ]]; then
echo "Current step $CUR_STEP: Preparing train, test lists and language modelling data"

./HCLG-prepare.sh --train_list ${train_list} --show_AM_error_message false --move_to_dir ${data_txt_dir}/train || exit 1
echo "========================================="
echo "!!! Train text processing accomplished !!!"
echo "========================================="

./HCLG-prepare.sh --train_list ${test_list} --show_AM_error_message false --move_to_dir ${data_txt_dir}/test || exit 1
echo "========================================="
echo "!!! Test text processing accomplished !!!"
echo "========================================="
let "CUR_STEP = $CUR_STEP + 1"
fi

# Now make MFCC features.
# mfccdir should be some place with a largish disk where you
# want to store MFCC features.
if [[ ${CUR_STEP} -eq 2 ]]; then
echo "Current step is $CUR_STEP: make MFCC features."

mfccdir=${exp}/mfcc
for x in train test; do
  steps/make_mfcc.sh --cmd "$train_cmd" --nj ${njobs} --mfcc-config ${conf}/mfcc.conf \
    ${data_txt_dir}/${x}/scp ${log_dir}/make_mfcc/${x} ${mfccdir}/${x} || exit 1

  steps/compute_cmvn_stats.sh ${data_txt_dir}/${x}/scp ${log_dir}/make_mfcc/${x} ${mfccdir}/${x} || exit 1
done

let "CUR_STEP = $CUR_STEP + 1"
fi

# Train monophone models on a subset of the data
if [[ ${CUR_STEP} -eq 3 ]]; then
echo "Current step is $CUR_STEP: Train monophone models on a subset of the data."

utils/subset_data_dir.sh ${data_txt_dir}/train/scp ${mono_subset} ${data_txt_dir}/train.1k  || exit 1
steps/train_mono.sh --retry_beam ${retry_beam_mono} --nj ${njobs} --cmd "$train_cmd" ${data_txt_dir}/train.1k \
                    ${data_txt_dir}/train/lang ${models_dir}/mono  || exit 1

# Monophone decoding (can be skipped).
utils/mkgraph.sh --mono ${data_txt_dir}/test/lang ${models_dir}/mono ${models_dir}/mono/graph || exit 1
# note: local/decode.sh calls the command line once for each
# test, and afterwards averages the WERs into (in this case
# exp/mono/decode/
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
  ${models_dir}/mono/graph ${data_txt_dir}/test/scp ${models_dir}/mono/decode || exit 1


# Get alignments from monophone system
steps/align_si.sh --nj ${njobs} --cmd "$train_cmd" \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/mono ${models_dir}/mono_ali || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi


# train tri1 [[first triphone pass]]
if [[ ${CUR_STEP} -eq 4 ]]; then
echo "Current step $CUR_STEP: train tri1 [[first triphone pass]] "

steps/train_deltas.sh --cmd "$train_cmd" 2000 11000 \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/mono_ali ${models_dir}/tri1 || exit 1

# decode tri1
utils/mkgraph.sh ${data_txt_dir}/test/lang ${models_dir}/tri1 ${models_dir}/tri1/graph || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs} --cmd "$decode_cmd" --scoring_opts "$scoring_opts" \
  ${models_dir}/tri1/graph ${data_txt_dir}/test/scp ${models_dir}/tri1/decode || exit 1

#draw-tree data/lang/phones.txt exp/tri1/tree | dot -Tps -Gsize=8,10.5 | ps2pdf - tree.pdf

# align tri1
steps/align_si.sh --nj ${njobs} --cmd "$train_cmd" \
  --use-graphs true ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri1 ${models_dir}/tri1_ali || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi


# train tri2a [[delta+delta-deltas]]
if [[ ${CUR_STEP} -eq 5 ]]; then
echo "Current step $CUR_STEP: train tri2a [[delta+delta-deltas]] "

steps/train_deltas.sh --cmd "$train_cmd" 2000 11000 \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri1_ali ${models_dir}/tri2a || exit 1

# decode tri2a
utils/mkgraph.sh ${data_txt_dir}/test/lang ${models_dir}/tri2a ${models_dir}/tri2a/graph
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
  ${models_dir}/tri2a/graph ${data_txt_dir}/test/scp ${models_dir}/tri2a/decode || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi


# train and decode tri2b [[LDA+MLLT]]
if [[ ${CUR_STEP} -eq 6 ]]; then
echo "Current step $CUR_STEP: train and decode tri2b [[LDA+MLLT]]"

steps/train_lda_mllt.sh --cmd "$train_cmd" 2000 11000 \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri1_ali ${models_dir}/tri2b || exit 1
utils/mkgraph.sh ${data_txt_dir}/test/lang ${models_dir}/tri2b ${models_dir}/tri2b/graph
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
  ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b/decode || exit 1

# Align all data with LDA+MLLT system (tri2b)
steps/align_si.sh --nj ${njobs} --cmd "$train_cmd" --use-graphs true \
   ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri2b ${models_dir}/tri2b_ali || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi


# Do MMI on top of LDA+MLLT (step 1, it 3 and 4)
if [[ ${CUR_STEP} -eq 7 ]]; then
echo "Current step $CUR_STEP: Do MMI on top of LDA+MLLT (step 1, it 3 and 4)"

steps/make_denlats.sh --nj ${njobs} --cmd "$train_cmd" ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang \
        ${models_dir}/tri2b ${models_dir}/tri2b_denlats || exit 1
steps/train_mmi.sh ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang \
        ${models_dir}/tri2b_ali ${models_dir}/tri2b_denlats ${models_dir}/tri2b_mmi || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --iter 4 --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
   ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b_mmi/decode_it4 || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --iter 3 --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
   ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b_mmi/decode_it3 || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi


# Do the same with boosting (step 2, it 3 and 4)
if [[ ${CUR_STEP} -eq 8 ]]; then
echo "Current step $CUR_STEP: Do the same with boosting (step 2, it 3 and 4)"

steps/train_mmi.sh --boost 0.05 ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang \
   ${models_dir}/tri2b_ali ${models_dir}/tri2b_denlats ${models_dir}/tri2b_mmi_b0.05 || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --iter 4 --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
   ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b_mmi_b0.05/decode_it4 || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --iter 3 --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
   ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b_mmi_b0.05/decode_it3 || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi


# Do MPE (step 3, it 3 and 4)
if [[ ${CUR_STEP} -eq 9 ]]; then
echo "Current step $CUR_STEP: Do MPE (step 3, it 3 and 4)"

steps/train_mpe.sh ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri2b_ali ${models_dir}/tri2b_denlats ${models_dir}/tri2b_mpe || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --iter 4 --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
   ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b_mpe/decode_it4 || exit 1
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --iter 3 --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
   ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b_mpe/decode_it3 || exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi

echo "====================================="
echo "Ready to run iVector and DNN training"
echo "====================================="

# train ivector extractor
if [[ ${CUR_STEP} -eq 10 ]]; then
echo "Current step $CUR_STEP: Train iVector extractor"

local/ivector.sh --njobs ${njobs} || exit 1;

let "CUR_STEP = $CUR_STEP + 1"
fi

# train DNN with ivector
if [[ ${CUR_STEP} -eq 11 ]]; then
echo "Current step $CUR_STEP: Train DNN with MFCC + iVector features"

local/train_dnn_ivector.sh --EXP_ROOT ${EXP_ROOT} --gpu ${gpu} \
                           --njobs ${njobs} || exit 1;

let "CUR_STEP = $CUR_STEP + 1"
fi

end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py ${diff_time}
echo "//////"
