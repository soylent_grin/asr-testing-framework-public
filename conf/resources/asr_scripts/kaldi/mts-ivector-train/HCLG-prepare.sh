#!/bin/bash

# Файл для создания HCLG.fst по входному списку wav-файлов и готовой модели

. ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
. ./cmd.sh || exit 1

# The number of parallel jobs to be started for some parts of the recipe
# Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
njobs=1		 #njobs=2	# original line

# Test-time language model order
lm_order=2

# Word position dependent phones?
pos_dep_phones=true

#List of wav-files. The txt file must be supplied near every wav. Can't be used together with $text_corpus.
train_list=

#use this parameter to create LM from prepared corpus. Can't be used together with $train_list.
text_corpus=
exp_config=/cvoaf/temp/mts-ivector-train/conf/exp.conf

source ${exp_config}
hclg_dir=$EXP_ROOT/HCLG
data_txt_dir=$hclg_dir/data_txt
arpa_dir=$hclg_dir/arpa_dir

#acoustic_model=$EXP_ROOT/dnn5b_pretrain-dbn_dnn_smbr_i1lats
#acoustic_model=$EXP_ROOT/tri3b_ali
acoustic_model=

show_AM_error_message=true

move_to_dir=

# The user of this script could change some of the above parameters. Example:
# /bin/bash run.sh --pos-dep-phones false
. utils/parse_options.sh || exit 1

[[ $# -ge 1 ]] && { echo "Unexpected arguments"; exit 1; }

if [ -z $train_list ] && [ -z $text_corpus ]; then
    echo "One of params train-list or text-corpus must be provided!"
    exit -1
fi

if [ ! -z $train_list ] && [ ! -z $text_corpus ]; then
    echo "Only one of the parameters train-list or text-corpus must be provided!"
    echo -1
fi

[ -d $hclg_dir ] && rm -rf $hclg_dir

mkdir -p $hclg_dir
mkdir -p $hclg_dir/lang
mkdir -p $arpa_dir
mkdir -p $arpa_dir/lang_tmp
mkdir -p $data_txt_dir/dict
mkdir -p $data_txt_dir/scp

# подготавливаем данные
if [ ! -z $train_list ]; then
    # Set first TRUE param to False if you don't want to create TEXT file
    # Set second TRUE param to False if you don't want to use dictor specific information
    local/data-preparation.py $train_list $DATA_ROOT $data_txt_dir/scp True True || exit 1;
else
    cp -rL $text_corpus $data_txt_dir/scp/text
fi

# Prepare ARPA LM and vocabulary using SRILM
local/voxforge_prepare_lm.sh --order ${lm_order} --train_corpus $data_txt_dir/scp/text \
                             --arpa_dir $arpa_dir || exit 1

# Prepare the lexicon and various phone lists
# Pronunciations for OOV words are obtained using a pre-trained Sequitur model
local/voxforge_prepare_dict.sh --dict_dir $data_txt_dir/dict --arpa_dir $arpa_dir || exit 1

# Prepare $arpa_dir/lang_tmp and $hclg_dir/lang directories
utils/prepare_lang.sh --position-dependent-phones $pos_dep_phones \
 $arpa_dir '!SIL' $arpa_dir/lang_tmp $hclg_dir/lang || exit 1

# Prepare G.fst
# Provide arpa_dir hclg_dir
local/hclg_make_Gfst.sh $arpa_dir $hclg_dir

if [ ! -z $acoustic_model ]; then
    [ -d $hclg_dir/model ] && rm -rf $hclg_dir/model
    mkdir -p $hclg_dir/model
    cp -rL $acoustic_model/final.mdl $hclg_dir/model
    cp -rL $acoustic_model/tree $hclg_dir/model

    [ -d $hclg_dir/graph ] && rm -rf $hclg_dir/graph
    mkdir -p $hclg_dir/graph

    # Making HCLG graph
    utils/mkgraph.sh $hclg_dir/lang $hclg_dir/model $hclg_dir/graph
    echo "========================================="
    echo "!!! HCLG prepare mission accomplished !!!"
    echo "========================================="
else
    if [ "$show_AM_error_mesage" = true ]; then
        echo "LG part has been prepared, but HCLG has not."
        echo "Acoustic model has't been provided."
        echo "If you want to create HCLG, please provide acoustic model"
    else
        echo "========================================="
        echo "!!! LG prepare mission accomplished !!!"
        echo "========================================="
    fi
fi

if [ ! -z $move_to_dir ]; then
    mv $hclg_dir/* $move_to_dir
    mv $move_to_dir/data_txt/dict $move_to_dir/dict
    mv $move_to_dir/data_txt/scp $move_to_dir/scp
    rm -rf $move_to_dir/data_txt
    rm -rf $move_to_dir/dict/cmudict-plain.txt
fi


#echo "HERRR!!!" && exit 0
