* To begin training run 
```./train_asr_ivec.sh --njobs 32 --gpu true``` (don't forget to modify your `path.sh` first)
* **iVector**-related code is in scripts `local/ivector.sh`, `local/train_dnn_ivector.sh` (requires Python 2.7)
* To use GPU-server please run `sudo nvidia-smi -c 0` before training so that all GPU's will be set to **Default** mode