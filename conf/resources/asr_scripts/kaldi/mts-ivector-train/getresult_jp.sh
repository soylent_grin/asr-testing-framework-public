#!/usr/bin/env bash

source ./path.sh

res_dir=$EXP_ROOT/models

# Getting results [see RESULTS file]
for x in $res_dir/*/decode*; do
    [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh
done
