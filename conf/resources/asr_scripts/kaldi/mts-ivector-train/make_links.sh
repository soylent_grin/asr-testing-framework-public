#!/usr/bin/env bash

CurDo=`pwd`
KALDI_ROOT=/usr/local/kaldi

ln -s ${KALDI_ROOT}/egs/wsj/s5/steps ${CurDo}
ln -s ${KALDI_ROOT}/egs/wsj/s5/utils ${CurDo}
