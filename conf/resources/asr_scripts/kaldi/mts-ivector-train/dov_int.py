#!/usr/bin/env python3
from sys import argv
from math import sqrt

k = float(argv[1])
n = float(argv[2])

print("k = " + str(k) + ", n = " + str(n))
print("Verojatnost' = " + str(k / n * 100.0) + "%")

dov_int = 1.96 * sqrt((k * (n - k)) / (n * n * (n - 1))) * 100.0

print("Doveritelny interval: " + str(dov_int) + "%")
