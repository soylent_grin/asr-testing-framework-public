#!/usr/bin/env bash

source ./path.sh || exit 1

lmdir=$1
hclg_dir=$2

hclg_lang=$hclg_dir/lang
tmpdir=$hclg_lang/lm_tmp

echo "--- Preparing the grammar transducer (G.fst) ..."

[ ! -d $tmpdir ] && mkdir -p $tmpdir

cat $lmdir/lm.arpa | \
   utils/find_arpa_oovs.pl $hclg_lang/words.txt > $tmpdir/oovs.txt

# grep -v '<s> <s>' because the LM seems to have some strange and useless
# stuff in it with multiple <s>'s in the history.  Encountered some other similar
# things in a LM from Geoff.  Removing all "illegal" combinations of <s> and </s>,
# which are supposed to occur only at being/end of utt.  These can cause
# determinization failures of CLG [ends up being epsilon cycles].
cat $lmdir/lm.arpa | \
  grep -v '<s> <s>' | \
  grep -v '</s> <s>' | \
  grep -v '</s> </s>' | \
  arpa2fst - | fstprint | \
  utils/remove_oovs.pl $tmpdir/oovs.txt | \
  utils/eps2disambig.pl | utils/s2eps.pl | fstcompile --isymbols=$hclg_lang/words.txt \
    --osymbols=$hclg_lang/words.txt  --keep_isymbols=false --keep_osymbols=false | \
  fstrmepsilon | fstarcsort --sort_type=ilabel > $hclg_lang/G.fst
fstisstochastic $hclg_lang/G.fst
# The output is like:
# 9.14233e-05 -0.259833
# we do expect the first of these 2 numbers to be close to zero (the second is
# nonzero because the backoff weights make the states sum to >1).
# Because of the <s> fiasco for these particular LMs, the first number is not
# as close to zero as it could be.


# Everything below is only for diagnostic.
# Checking that G has no cycles with empty words on them (e.g. <s>, </s>);
# this might cause determinization failure of CLG.
# #0 is treated as an empty word.
mkdir -p $tmpdir/g
lexicon=$lmdir/lexicon.txt
awk '{if(NF==1){ printf("0 0 %s %s\n", $1,$1); }} END{print "0 0 #0 #0"; print "0";}' \
  < "$lexicon"  >$tmpdir/g/select_empty.fst.txt
fstcompile --isymbols=$hclg_lang/words.txt --osymbols=$hclg_lang/words.txt \
  $tmpdir/g/select_empty.fst.txt | \
fstarcsort --sort_type=olabel | fstcompose - $hclg_lang/G.fst > $tmpdir/g/empty_words.fst
fstinfo $tmpdir/g/empty_words.fst | grep cyclic | grep -w 'y' &&
  echo "Language model has cycles with empty words" && exit 1
#rm -rf $tmpdir

echo "--- Succeeded in preparing G.fst ---"
