#!/usr/bin/env bash
# This code adapted for NN training on top of MFCC + iVector features
# Author: MTS AI @AntonOkhotnikov

source ./path.sh
source ./cmd.sh


#!/bin/bash

# Copyright 2012-2014  Brno University of Technology (Author: Karel Vesely)
#                2014  Guoguo Chen
# Apache 2.0

# This example script trains a DNN on top of fMLLR features.
# The training is done in 3 stages,
#
# 1) RBM pre-training:
#    in this unsupervised stage we train stack of RBMs,
#    a good starting point for frame cross-entropy trainig.
# 2) frame cross-entropy training:
#    the objective is to classify frames to correct pdfs.
# 3) sequence-training optimizing sMBR:
#    the objective is to emphasize state-sequences with better
#    frame accuracy w.r.t. reference alignment.

source ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

source ./path.sh ## Source the tools/utils (import the queue.pl)

# Config:
conf=/cvoaf/temp/mts-ivector-train/conf
acwt=0.08333                          # NOTE: acwt parameter is redefined while running the script (don't know why)
dbn_dims=2048                         # number of outputs in DBN
max_frames_mpe=15000                  # option for sequential training
scoring_opts="--min-lmwt 5 --max-lmwt 30"
njobs=                              # The number of parallel jobs to be started for some parts of the recipe
                                      # Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
stage=0                               # resume training with --stage N

gpu=                             # To use cuda set this value to true
# End of config.

EXP_ROOT=
source utils/parse_options.sh || exit 1;

exp=${EXP_ROOT}                       # experiment root
gmmdir=${exp}/models/tri2b            # latest trained model
data_mfcc=${exp}/mfcc                 # raw MFCC features
data_ivector=${exp}/ivectors          # dir containing rx-specifier with i-vectors .ark
                                      # "ark:/tmp/transforms.ark"
data_txt_train=${exp}/data_txt/train  # scp list that points to training MFCC feats (feats.scp)
data_txt_test=${exp}/data_txt/test    # scp list that points to testing MFCC feats (feats.scp)

if [[ "$gpu" = true ]] ; then
    skip_cuda_check=false
    # we don't use next parameter anymore as it affects the decoding speed negatively
    nnet_use_gpu="no"
else
    skip_cuda_check=true
    nnet_use_gpu="no"
fi


echo "Current stage: $stage"
echo "Use CUDA: $gpu"
echo "Nnet use gpu param for decoding: $nnet_use_gpu"

if [[ ${stage} -le 0 ]]; then
  echo "Current stage: ${stage}"
  # split the data : 90% train 10% cross-validation (held-out)
  dir=${data_txt_train}/scp
  # just because next script throws an error if USER is empty
  [[ -z ${USER} ]] && export USER=kaldi
  utils/subset_data_dir_tr_cv.sh ${dir} ${data_mfcc}/train_tr90 ${data_mfcc}/train_cv10 || exit 1
  echo "MFCC train data is split into tr90 and cv10"
  let "stage = ${stage} + 1"
fi

if [[ ${stage} -le 1 ]]; then
  echo "Current stage: ${stage}"
  # Pre-train DBN, i.e. a stack of RBMs
  dir=${exp}/models/dnn5b_pretrain-dbn
  (tail --pid=$$ -F ${dir}/log/pretrain_dbn.log 2>/dev/null)& # forward log
  $cuda_cmd $dir/log/pretrain_dbn.log \
    steps/nnet/pretrain_dbn.sh --skip-cuda-check ${skip_cuda_check} \
                               --rbm-iter 1 \
                               --hid-dim ${dbn_dims} \
                               --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                               ${data_txt_train}/scp \
                               ${dir} || exit 1;
  let "stage = ${stage} + 1"
fi

if [[ ${stage} -le 2 ]]; then
  echo "Current stage: ${stage}"
  # Train the DNN optimizing per-frame cross-entropy.
  dir=${exp}/models/dnn5b_pretrain-dbn_dnn
  ali=${gmmdir}_ali
  feature_transform=${exp}/models/dnn5b_pretrain-dbn/final.feature_transform
  dbn=${exp}/models/dnn5b_pretrain-dbn/6.dbn
  (tail --pid=$$ -F $dir/log/train_nnet.log 2>/dev/null)& # forward log
  # Train
  $cuda_cmd $dir/log/train_nnet.log \
    local/nnet/train.sh --feature-transform ${feature_transform} \
                        --dbn ${dbn} \
                        --dbn-dims ${dbn_dims} \
                        --hid-layers 0 \
                        --learn-rate 0.008 \
                        --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                        --skip-cuda-check ${skip_cuda_check} \
                        ${data_mfcc}/train_tr90 \
                        ${data_mfcc}/train_cv10 \
                        ${data_txt_train}/lang \
                        ${ali} ${ali} ${dir} || exit 1;

  # Decode with the trigram swbd language model.
  steps/nnet/decode.sh --nj ${njobs} \
                       --cmd "$decode_cmd" \
                       --config ${conf}/decode_dnn.config \
                       --acwt ${acwt} \
                       --ivector "ark:${data_ivector}/test/ivectors_utt.ark" \
                       --scoring-opts "$scoring_opts" \
                       --use-gpu ${nnet_use_gpu} \
                       ${gmmdir}/graph \
                       ${data_txt_test}/scp \
                       ${dir}/decode || exit 1;
  let "stage = ${stage} + 1"
fi


# Sequence training using sMBR criterion, we do Stochastic-GD
# with per-utterance updates. We use usually good acwt 0.1
# Lattices are re-generated after 1st epoch, to get faster convergence.
dir=${exp}/models/dnn5b_pretrain-dbn_dnn_smbr
srcdir=${exp}/models/dnn5b_pretrain-dbn_dnn
acwt=0.0909

if [[ ${stage} -le 3 ]]; then
  echo "Current stage: ${stage}"
  # First we generate lattices and alignments:
  steps/nnet/align.sh --nj ${njobs} \
                      --cmd "$train_cmd" \
                      --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                      --use-gpu ${nnet_use_gpu} \
                      ${data_txt_train}/scp \
                      ${data_txt_train}/lang \
                      ${srcdir} \
                      ${srcdir}_ali || exit 1;

  steps/nnet/make_denlats.sh --nj ${njobs} \
                             --sub-split 100 \
                             --cmd "$decode_cmd" \
                             --config ${conf}/decode_dnn.config \
                             --acwt ${acwt} \
                             --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                             --use-gpu ${nnet_use_gpu} \
                             ${data_txt_train}/scp \
                             ${data_txt_train}/lang \
                             ${srcdir} \
                             ${srcdir}_denlats || exit 1;
  let "stage = ${stage} + 1"
fi

if [[ ${stage} -le 4 ]]; then
  echo "Current stage: ${stage}"
  # Re-train the DNN by 1 iteration of sMBR
  local/nnet/train_mpe_jp.sh --cmd "$cuda_cmd" \
                             --skip-cuda-check ${skip_cuda_check} \
                             --num-iters 1 \
                             --acwt ${acwt} \
                             --do-smbr true \
                             --max-frames ${max_frames_mpe} \
                             --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                             --nnet-use-gpu ${nnet_use_gpu} \
                             ${data_txt_train}/scp \
                             ${data_txt_train}/lang \
                             ${srcdir} \
                             ${srcdir}_ali \
                             ${srcdir}_denlats \
                             ${dir} || exit 1
  # Decode (reuse HCLG graph)
  for ITER in 1; do
    # Decode with the trigram swbd language model.
    steps/nnet/decode.sh --nj ${njobs} \
                         --cmd "$decode_cmd" \
                         --config ${conf}/decode_dnn.config \
                         --nnet ${dir}/${ITER}.nnet \
                         --acwt ${acwt} \
                         --scoring-opts "$scoring_opts" \
                         --ivector "ark:${data_ivector}/test/ivectors_utt.ark" \
                         --use-gpu ${nnet_use_gpu} \
                         ${gmmdir}/graph \
                         ${data_txt_test}/scp \
                         ${dir}/decode_test || exit 1;
  done
  let "stage = ${stage} + 1"
fi

# Re-generate lattices, run 4 more sMBR iterations
dir=${exp}/models/dnn5b_pretrain-dbn_dnn_smbr_i1lats
srcdir=${exp}/models/dnn5b_pretrain-dbn_dnn_smbr
acwt=0.0909

if [[ ${stage} -le 5 ]]; then
  echo "Current stage: ${stage}"
  # First we generate lattices and alignments:
  steps/nnet/align.sh --nj ${njobs} \
                      --cmd "$train_cmd" \
                      --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                      --use-gpu ${nnet_use_gpu} \
                      ${data_txt_train}/scp \
                      ${data_txt_train}/lang \
                      ${srcdir} \
                      ${srcdir}_ali || exit 1;

  steps/nnet/make_denlats.sh --nj ${njobs} \
                             --sub-split 100 \
                             --cmd "$decode_cmd" \
                             --config ${conf}/decode_dnn.config \
                             --acwt ${acwt} \
                             --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                             --use-gpu ${nnet_use_gpu} \
                             ${data_txt_train}/scp \
                             ${data_txt_train}/lang \
                             ${srcdir} \
                             ${srcdir}_denlats || exit 1;
  let "stage = ${stage} + 1"
fi

if [ $stage -le 6 ]; then
  echo "Current stage: ${stage}"
  # Re-train the DNN by 1 iteration of sMBR
  local/nnet/train_mpe_jp.sh --cmd "$cuda_cmd" \
                             --skip-cuda-check ${skip_cuda_check} \
                             --num-iters 2 \
                             --acwt ${acwt} \
                             --do-smbr true \
                             --max-frames ${max_frames_mpe} \
                             --ivector "ark:${data_ivector}/train/ivectors_utt.ark" \
                             --nnet-use-gpu ${nnet_use_gpu} \
                             ${data_txt_train}/scp \
                             ${data_txt_train}/lang \
                             ${srcdir} \
                             ${srcdir}_ali \
                             ${srcdir}_denlats \
                             ${dir} || exit 1
  # Decode (reuse HCLG graph)
  for ITER in 1 2; do
    # Decode with the trigram swbd language model.
    steps/nnet/decode.sh --nj ${njobs} \
                         --cmd "$decode_cmd" \
                         --config ${conf}/decode_dnn.config \
                         --nnet ${dir}/${ITER}.nnet \
                         --acwt ${acwt} \
                         --scoring-opts "$scoring_opts" \
                         --ivector "ark:${data_ivector}/test/ivectors_utt.ark" \
                         --use-gpu ${nnet_use_gpu} \
                         ${gmmdir}/graph \
                         ${data_txt_test}/scp \
                         ${dir}/decode_test || exit 1;
  done
  let "stage = ${stage} + 1"
fi

# Getting results [see RESULTS file]
# for x in $exp/models/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done

exit 0