#!/bin/bash
# Copyright 2012-2014  Johns Hopkins University (Author: Daniel Povey, Yenda Trmal)
# Apache 2.0

# This script computes the CER (Character Error Rate) as opposed to the script
# local/score_kaldi.sh (which computes WER i.e. Word Error Rate).

[ -f ./path.sh ] && . ./path.sh

# begin configuration section.
cmd=run.pl
min_lmwt=7
max_lmwt=17
#end configuration section.

echo "$0 $@"  # Print the command line for logging
[ -f ./path.sh ] && . ./path.sh
. utils/parse_options.sh || exit 1;

if [ $# -ne 3 ]; then
  echo "Usage: $0 [--cmd (run.pl|queue.pl...)] <data-dir> <lang-dir|graph-dir> <decode-dir>"
  echo " Options:"
  echo "    --cmd (run.pl|queue.pl...)      # specify how to run the sub-processes."
  echo "    --min_lmwt <int>                # minumum LM-weight for lattice rescoring "
  echo "    --max_lmwt <int>                # maximum LM-weight for lattice rescoring "
  exit 1;
fi

data=$1
lang_or_graph=$2
dir=$3

symtab=$lang_or_graph/words.txt

for f in $symtab $dir/lat.1.gz $data/text; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

mkdir -p $dir/scoring_cer

cat $data/text | sed 's:<NOISE>::g' | sed 's:<SPOKEN_NOISE>::g' > $dir/scoring_cer/test_filt.txt

echo "=====READY TO RUN CER=====" || exit 1;

hyp_filtering_cmd="cat"
$cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring_cer/log/best_path.LMWT.log \
        lattice-scale --inv-acoustic-scale=LMWT "ark:gunzip -c $dir/lat.*.gz|" ark:- \| \
        lattice-best-path --word-symbol-table=$symtab ark:- ark,t: \| \
        utils/int2sym.pl -f 2- $symtab \| \
        $hyp_filtering_cmd '>' $dir/scoring_cer/LMWT.txt || exit 1;

#$cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring_cer/log/score.LMWT.log \
#        cat $dir/scoring_cer/LMWT.txt \| \
#        compute-wer --text --mode=present \
#        ark:$dir/scoring_cer/test_filt.txt  ark,p:- ">&" $dir/wer_LMWT || exit 1;

# the stage 2 is intentional, to allow nice coexistence with score_kaldi.sh
# in cases user would be combining calls to these two scripts as shown in
# the example at the top of the file. Otherwise we or he/she would have to
# filter the script parameters instead of simple forwarding.
files=($dir/scoring_cer/test_filt.txt)

for lmwt in $(seq $min_lmwt $max_lmwt); do
    files+=($dir/scoring_cer/${lmwt}.txt)
done

for f in "${files[@]}" ; do
fout=${f%.txt}.chars.txt
if [ -x local/character_tokenizer ]; then
  cat $f |  local/character_tokenizer > $fout
else
  cat $f |  perl -CSDA -ane '
    {
      print $F[0];
      foreach $s (@F[1..$#F]) {
        if (($s =~ /\[.*\]/) || ($s =~ /\<.*\>/) || ($s =~ "!SIL")) {
          print " $s";
        } else {
          @chars = split "", $s;
          foreach $c (@chars) {
            print " $c";
          }
        }
      }
      print "\n";
    }' > $fout
fi
done

$cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring_cer/log/score.cer.LMWT.log \
    cat $dir/scoring_cer/LMWT.chars.txt \| \
    compute-wer --text --mode=present \
    ark:$dir/scoring_cer/test_filt.chars.txt  ark,p:- ">&" $dir/cer_LMWT || exit 1;

echo "=====FINISHED CER=====" || exit 1;

# Show results
for f in $dir/cer_*; do echo $f; egrep  '(WER)|(SER)' < $f; done

exit 0;
