#!/usr/bin/env bash
# The code is written by Anton Okhotnikov 06.12.2018
# changed on 11.01.2019
# reference used /egs/tedlium/s5/local/online/run_nnet2_common.sh
# and zamia-speech's example kaldi-run-ivector-common.sh

source ./path.sh
source ./cmd.sh

# append "src/ivectorbin" to PATH so ivector C-functions can work
PATH=${KALDI_ROOT}/src/ivectorbin:$PATH

exp_config=/cvoaf/temp/mts-ivector-train/conf/exp.conf

EXP_ROOT=

source ${exp_config}

# initialize directories
exp=${EXP_ROOT}
lang_dir=${exp}/data_txt/train/lang
ivector_dir=${exp}/models/ivector

# initialize training
stage=1
njobs=16

# algorithm parameters
num_gauss=1024
num_frames=700000   # number of frames to keep in memory for initialization
num_gselect=10      # Number of Gaussian-selection indices to use while training the model
ivector_dim=100     # Dimensionality of the extracted iVector
max_count=100       # Interpret this as a number of frames times posterior scale...
                    # this config ensures that once the count exceeds this (i.e.
                    # 1000 frames, or 10 seconds, by default), we start to scale
                    # down the stats, accentuating the prior term.   This seems quite
                    # important for some reason.

# parse input parameters
. parse_options.sh || exit 1;

# Train UBM model first
if [[ ${stage} -le 1 ]]; then
  mkdir -p ${ivector_dir}/diag_ubm
  echo "=============================="
  echo "Step: Train UBM model"
  echo "=============================="
  # Usage online/nnet2/train_diag_ubm.sh (also requires online config file):
  # "train_diag_ubm.sh [options] <data> <num-gauss> <srcdir> <output-dir>"
  # <srcdir> = ${exp}/models/tri2b_mpe
  # Usage nnet/ivector/train_diag_ubm.sh:
  # "[options] <data> <num-gauss> <output-dir>"
  steps/nnet/ivector/train_diag_ubm.sh --cmd "$train_cmd" \
                                        --nj ${njobs} \
                                        --num-frames ${num_frames} \
                                        --num-threads $(nproc) \
                                        ${exp}/data_txt/train/scp \
                                        ${num_gauss} \
                                        ${ivector_dir}/diag_ubm || exit 1;
fi

# Train iVector extractor
if [[ ${stage} -le 2 ]]; then
  echo "======================================"
  echo "Step: Train iVector extractor"
  echo "======================================"
  mkdir -p ${ivector_dir}/extractor
  # iVector extractors can in general be sensitive to the amount of data, but
  # this one has a fairly small dim (defaults to 100)
  # "Usage: train_ivector_extractor.sh [options] <data> <diagonal-ubm-dir> <extractor-dir>"
  steps/nnet/ivector/train_ivector_extractor.sh --cmd "$train_cmd" \
                                                --nj ${njobs} \
                                                --num_gselect ${num_gselect} \
                                                --ivector_dim ${ivector_dim} \
                                                --num-threads $(nproc) \
                                                ${exp}/data_txt/train/scp \
                                                ${ivector_dir}/diag_ubm \
                                                ${ivector_dir}/extractor || exit 1;
fi

# Extract training ivectors
if [[ ${stage} -le 3 ]]; then
  echo "========================================"
  echo "Step: Extract training ivectors"
  echo "========================================"
  mkdir -p ${exp}/ivectors/train
  # "Usage: extract_ivectors.sh [options] <data> <lang> <extractor-dir> [<alignment-dir>|<decode-dir>|<weights-archive>] <ivector-dir>"
  # can be used steps/online/nnet2/extract_ivectors_online.sh for online purposes
  steps/nnet/ivector/extract_ivectors.sh --cmd "$train_cmd" \
                                        --nj ${njobs} \
                                        --num_gselect ${num_gselect} \
                                        ${exp}/data_txt/train/scp \
                                        ${lang_dir} \
                                        ${ivector_dir}/extractor \
                                        "" \
                                        ${exp}/ivectors/train || exit 1;
fi

# Extract testing ivectors
if [[ ${stage} -le 4 ]]; then
  echo "======================================="
  echo "Step: Extract testing ivectors"
  echo "======================================="
  mkdir -p ${exp}/ivectors/test
  # "Usage: extract_ivectors.sh [options] <data> <lang> <extractor-dir> [<alignment-dir>|<decode-dir>|<weights-archive>] <ivector-dir>"
  # can be used steps/online/nnet2/extract_ivectors_online.sh for online purposes
  steps/nnet/ivector/extract_ivectors.sh --cmd "$train_cmd" \
                                        --nj ${njobs} \
                                        --num_gselect ${num_gselect} \
                                        ${exp}/data_txt/test/scp \
                                        ${lang_dir} \
                                        ${ivector_dir}/extractor \
                                        "" \
                                        ${exp}/ivectors/test || exit 1;
fi

exit 0;
