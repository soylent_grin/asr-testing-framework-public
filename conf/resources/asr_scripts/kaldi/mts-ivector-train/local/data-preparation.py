#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
from sys import argv, version
from os.path import splitext, isfile, split, join


def print_error():
    print("Program usage:")
    print("data-preparation.py input_list root_wav_dir output_dir create_text do_spk2utt")
    print("input_list - wav-files list with full path's")
    print("root_wav_dir - root dir where wav-files located for unique id extraction")
    print("output_dir - dir for created files")
    print("create_text - True/False")
    print("do_spk2utt - True/False")
    print("In output_dir wav.scp, text, utt2spk, spk2utt files will be created.")
    print("If do_spk2utt==False the files utt2spk, spk2utt will be fake (will not use dictor information).")
    print("Set do_spk2utt to True for normal utt2spk and spk2utt creation in Kaldi manner.")
    print("For TEXT creation *.txt file must exist near *.wav file.")
    print("If you don't want to create TEXT file set create_text flag to False. Test")
    print("With input list file an exception_words.txt (!SIL sil ПДН) file must exist.")
    print("Exception words will be filtered from final text file.")
    print("If you don't want to filter words exception_words.txt must exist, but can be empty.")
    return


def get_id_Kaldi(in_wav_fname):
    idx = in_wav_fname.find('/')
    if in_wav_fname.startswith("anonymous"):
        spk_id = in_wav_fname[:idx]
        spk_id = spk_id.replace('-', '_')
    else:
        idx1 = in_wav_fname.find('-')
        if idx1 != -1:
            spk_id = in_wav_fname[:idx1]
        else:
            spk_id = in_wav_fname[:idx]
    return spk_id


def load_exception_words(input_list):
    exception_words = set()
    dir, _ = split(input_list)
    exceptFN = join(dir, 'exception_words.txt')
    if not isfile(exceptFN):
        print("Can't find exception word list file:\n" + exceptFN)
        print("With input list file an exception_words.txt (!SIL sil ПДН) file must exist.")
        print("Exception words will be filtered from final text file.")
        print("If you don't want to filter words exception_words.txt must exist, but can be empty.")
        print("Making hard exit!!!")
        exit(-1)
    F = open(exceptFN, 'r', encoding='utf8')
    for word in F:
        word = word.replace('\n', '')
        if word in exception_words:
            print("An error occured. Word " + word + " listed more than once in exception_words.txt")
            exit(-1)
        exception_words.add(word)
    F.close()
    return exception_words


def filter_string(inStr, exception_words):
    outStr = ''
    str_list = inStr.split(' ')
    for word in str_list:
        if (len(word) > 0) and not (word in exception_words):
            outStr += word + ' '
    outStr = outStr.rstrip(' ')
    return outStr


def main(input_list, root_wav_dir, output_dir, CrText, do_spk2utt):
    if not root_wav_dir.endswith('/'):
        root_wav_dir += '/'

    exception_words = load_exception_words(input_list)

    ErrNo = 0
    inF = open(input_list, 'r', encoding='utf8')
    wav_scp = open(output_dir + "/wav.scp", 'w', encoding='utf8')
    utt2spk = open(output_dir + "/utt2spk", 'w', encoding='utf8')
    spk2utt = open(output_dir + "/spk2utt", 'w', encoding='utf8')
    txt_file = open(output_dir + "/text", 'w', encoding='utf8')
    spk2utt_dict = dict()
    inFList = sorted(inF.readlines())
    for line in inFList:
        do_scp = True
        line = line.rstrip('\n')
        wav_fname = line.replace(root_wav_dir, "")
        wav_id = wav_fname.replace('/', '-')
        wav_id = wav_id.replace(".wav", "")
        if do_spk2utt:
            spk_id = get_id_Kaldi(wav_fname)
            spk2utt_dict.setdefault(spk_id, []).append(wav_id)
        else:
            spk_id = wav_id
        out_str = "NO_TRANSRIPTION"
        if CrText:
            txt_fname, ext = splitext(line)
            txt_fname += ".txt"
            if isfile(txt_fname):
                out_str = ""
                with open(txt_fname, 'r', encoding='utf8') as input_txt:
                    for txt_line in input_txt:
                        out_str += txt_line.replace('\n', ' ')
                out_str = filter_string(out_str, exception_words)
            else:
                do_scp = False
                print("File not exist:")
                print(txt_fname)
                ErrNo += 1
        if do_scp:
            print(wav_id, spk_id, sep=' ', file=utt2spk)
            if not do_spk2utt:
                print(spk_id, wav_id, sep=' ', file=spk2utt)
            print(wav_id, out_str, sep=' ', file=txt_file)
            # print(wav_id, wav_fname, sep=' ', file=wav_scp)
            print(wav_id, line, sep=' ', file=wav_scp)

    if do_spk2utt:
        for spk_id in sorted(spk2utt_dict.keys()):
            print(spk_id, *spk2utt_dict[spk_id], file=spk2utt)

    txt_file.close()
    spk2utt.close()
    utt2spk.close()
    wav_scp.close()
    inF.close()
    if ErrNo > 0:
        print("Total number of errors: " + str(ErrNo))
    return

# ====================================================
if __name__ == '__main__':
    #old_env = environ['LC_ALL']
    #environ['LC_ALL'] = 'C.UTF-8'
    if len(argv) != 6:
        print_error()
        exit(-1)

    input_list = argv[1]
    root_wav_dir = argv[2]
    output_dir = argv[3]
    CrText = True
    if argv[4] == "False" or argv[4] == "false":
        CrText = False
    do_spk2utt = True
    if argv[5] == "False" or argv[5] == "false":
        do_spk2utt = False

    print(version)
    #print("Ну здравствуй мир!!!")
    print("input_list: " + input_list)
    print("root_wav_dir: " + root_wav_dir)
    print("output_dir: " + output_dir)
    print("Create text flag: " + str(CrText))
    print("do spk2utt and utt2spk files in Kaldi manner: " + str(do_spk2utt))
    print('='*50)

    main(input_list, root_wav_dir, output_dir, CrText, do_spk2utt)
    print('='*50)
    print("Data preparation finished.")
    print('='*50)
    #environ['LC_ALL'] = old_env
