#!/bin/bash

export KALDI_ROOT=/usr/local/kaldi

export PATH=$PWD/utils:${KALDI_ROOT}/src/bin:${KALDI_ROOT}/tools/openfst/bin:${KALDI_ROOT}/src/fstbin:${KALDI_ROOT}/src/gmmbin:${KALDI_ROOT}/src/featbin:${KALDI_ROOT}/src/lm:${KALDI_ROOT}/src/sgmmbin:${KALDI_ROOT}/src/sgmm2bin:${KALDI_ROOT}/src/fgmmbin:${KALDI_ROOT}/src/latbin:${KALDI_ROOT}/src/nnetbin:${KALDI_ROOT}/src/nnet2bin:${KALDI_ROOT}/src/online2bin:${KALDI_ROOT}/src/ivectorbin:${KALDI_ROOT}/src/lmbin:${KALDI_ROOT}/src/chainbin:${KALDI_ROOT}/src/nnet3bin:$PWD:$PATH:${KALDI_ROOT}/tools/sph2pipe_v2.5
export LC_ALL=C

pushd /cvoaf/asr_scripts/kaldi/alphacephei || exit 1


DATA_ROOT=/cvoaf/temp/alphacephei/dataset
test_list=/cvoaf/temp/alphacephei/dataset/test-list.txt
outdir=/cvoaf/temp/alphacephei/results

MODEL_DIR=
txt_ext=.txt
do_filter_test=false
list=${test_list} # list of audiofiles

[[ -d ${outdir} ]] && rm -rf ${outdir}
mkdir -p ${outdir}/txtdata
mkdir -p ${outdir}/decode

frame_subsampling_factor=
frames_per_chunk=
acoustic_scale=
beam=
lattice_beam=
max_active=

source /cvoaf/temp/alphacephei/conf/exp.conf

echo "frame-subsampling-factor=${frame_subsampling_factor}"
echo "frames-per-chunk=${frames_per_chunk}"
echo "acoustic-scale=${acoustic_scale}"
echo "beam=${beam}"
echo "lattice-beam=${lattice_beam}"
echo "max-active=${max_active}"

echo "///////"
echo "// Data preaparation"
echo "///////"

# Set first TRUE param to False if you don't want to create TEXT file
# Set second TRUE param to False if you don't want to use dictor specific information
local/data-preparation.py ${list} ${DATA_ROOT} ${outdir}/txtdata True False ${txt_ext} ${do_filter_test} || exit 1;
[[ -f ${outdir}/txtdata/spk2utt ]] && sort -o ${outdir}/txtdata/spk2utt ${outdir}/txtdata/spk2utt
[[ -f ${outdir}/txtdata/utt2spk ]] && sort -o ${outdir}/txtdata/utt2spk ${outdir}/txtdata/utt2spk
[[ -f ${outdir}/txtdata/wav.scp ]] && sort -o ${outdir}/txtdata/wav.scp ${outdir}/txtdata/wav.scp
[[ -f ${outdir}/txtdata/text ]] && sort -o ${outdir}/txtdata/text ${outdir}/txtdata/text

start_time=$(date +%s)

online2-wav-nnet3-latgen-faster \
    --word-symbol-table=${MODEL_DIR}/exp/tdnn/graph/words.txt \
    --frame-subsampling-factor=${frame_subsampling_factor} \
    --frames-per-chunk=${frames_per_chunk} \
    --acoustic-scale=${acoustic_scale} \
    --beam=${beam} \
    --lattice-beam=${lattice_beam} \
    --max-active=${max_active} \
    --feature-type=mfcc \
    --mfcc-config=/cvoaf/temp/alphacephei/conf/mfcc.conf \
    --ivector-extraction-config=/cvoaf/temp/alphacephei/conf/ivector_extractor.conf \
    --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 \
    --num-threads-startup=$(nproc) \
    ${MODEL_DIR}/exp/tdnn/final.mdl ${MODEL_DIR}/exp/tdnn/graph/HCLG.fst ark:${outdir}/txtdata/utt2spk scp:${outdir}/txtdata/wav.scp ark:- |
    lattice-lmrescore --lm-scale=-1.0 ark:- "fstproject --project_output=true ${MODEL_DIR}/data/lang_test_rescore/G.fst |" ark:- |
    lattice-lmrescore-const-arpa ark:- ${MODEL_DIR}/data/lang_test_rescore/G.carpa ark:- |
    lattice-align-words ${MODEL_DIR}/data/lang_test_rescore/phones/word_boundary.int ${MODEL_DIR}/exp/tdnn/final.mdl ark:- ark:- |
    lattice-best-path --word-symbol-table=${MODEL_DIR}/exp/tdnn/graph/words.txt ark:- ark,t:${outdir}/decode/hyp.tra

end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py ${diff_time}
echo "//////"

echo -e "Recognized text: "
local/int2sym.pl -f 2- ${MODEL_DIR}/exp/tdnn/graph/words.txt ${outdir}/decode/hyp.tra | cut -d " " -f2-

rm -rf /cvoaf/temp/alphacephei/*