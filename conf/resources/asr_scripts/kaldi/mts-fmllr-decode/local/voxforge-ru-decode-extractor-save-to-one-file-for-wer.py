#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from os.path import split, join, isdir
from os import makedirs

def myParser():
    parser = argparse.ArgumentParser(description="Извлекаем результаты распознавания из файлов decode или best_path.",
                                     epilog="Предполагается, что входной список --input-list имеет структуру, которая сохраняется при записи результатов в --output-dir.")
    parser.add_argument('--input-list', dest='inputList', action="store", type=str, required=True,
                        help="Список файлов с полными путями, для которых необходимо получить результаты декодирования.")
    parser.add_argument('--dir-depth', dest='dirDepth', action="store", type=int, required=True,
                        help="До какой глубины извлекать директории при формировании выходной структуры каталогов.")
    parser.add_argument('--decode-file', dest='decodeFile', action="store", type=str, required=True, help="Полный путь к файлу с результатами декодирования.")

    #parser.add_argument('--use-voxforge-struct', dest='useVFstruct', action="store", type=bool, required=True,
    #                    help="True / False - использовать или нет структуру директорий voxforge-ru при разборе директорий.")

    parser.add_argument('--output-dir', dest='outputDir', action="store", type=str, required=True, help="Полный путь к директории для записи результатов.")
    return parser.parse_args()

'''
def findDecodeStr(inMask, File):
    outStr = ""
    outStrExist = False
    for line in File:
        line = line.rstrip('\n')
        idx = line.find(inMask)
        if idx == 0:
            outStr = line.replace(inMask, "")
            outStr = outStr.strip()
            outStrExist = True
            break
    return outStr, outStrExist
'''


def makeOutFileName(fullFName, dirDepth, outputDir):
    outFName = ""
    dirChunk = ""
    [curDir, fName] = split(fullFName)
    for i in range(dirDepth):
        [curDir, chunk] = split(curDir)
        dirChunk = join(chunk, dirChunk)
    outFName = join(outputDir, dirChunk)
    if not isdir(outFName):
        makedirs(outFName)
    fName = fName.replace('.wav', '.txt')
    outFName = join(outFName, fName)
    return outFName


def extract_decode_results(inputList, dirDepth, decodeFile, outputDir):
    inpListDict = dict()
    inpListF = open(inputList, 'r', encoding='utf8')
    for inpStr in inpListF:
        inpStr = inpStr.rstrip('\n')
        [fileMask, fullFName] = inpStr.split(maxsplit=1)
        if fileMask in inpListDict:
            print("Utterance name " + fileMask + " occured more than once.")
            print("Make hard exit.")
            exit(-1)
        inpListDict[fileMask] = fullFName
    inpListF.close()

    decodeF = open(decodeFile, 'r', encoding='utf8')
    outFName = makeOutFileName('decode-result.txt', dirDepth, outputDir)  # change is here as well
    outFile = open(outFName, 'w', encoding='utf8')
    numScoredFiles = 0
    for inpStr in decodeF:
        inpStr = inpStr.rstrip('\n')
        if inpStr.find(' ') < 0:
            continue
        [fileMask, decodeStr] = inpStr.split(maxsplit=1)
        if fileMask in inpListDict:
            # Changes are here. Save to one file
            to_file = ' '.join([fileMask, decodeStr])   # string to save
            print(to_file, file=outFile)
            numScoredFiles += 1
    outFile.close()  # and close the file
    decodeF.close()
    if len(inpListDict) != numScoredFiles:
        print("Keys number in input dict: " + str(len(inpListDict)))
        print("Scored files found: " + str(numScoredFiles))
        print("Some error occurred!!! Making hard exit!!!")
        exit(-1)
    return


# ====================================================
if __name__ == '__main__':
    input_args = myParser()

    extract_decode_results(input_args.inputList, input_args.dirDepth, input_args.decodeFile, input_args.outputDir)

    # print("Слава Тиролю!!!")
    print("Program complete. Results are stored in dir:")
    print(input_args.outputDir)
