#!/usr/bin/env bash
# Script computes WER on the decodings independently and overall
# You should run this script `local/wer-on-decoding-universal.sh`


source $(realpath "$PWD/./path.sh") || exit 1


# Configuration section
input_dir_depth=2
output_dir_depth=1  # leave this as 1 for compatibility
reference_list=$(realpath $EXP_ROOT/../train_test_lists)/test.txt
input_list=$(realpath $EXP_ROOT/../train_test_lists)/test.txt
results_dir=$EXP_ROOT/test_results/fmllr_gmm_dnn_decode_test_list_extended
output_dir=$results_dir/test-okh


. parse_options.sh || exit 1;


# Prepare directories
[ -d $output_dir ] && rm -rf $output_dir
mkdir -p $output_dir


# Run WER
./local/wer-on-decoding-universal.py    --reference-list $reference_list \
                                        --input-list $input_list \
                                        --input-dir-depth $input_dir_depth \
                                        --output-dir-depth $output_dir_depth\
                                        --output-dir $output_dir

# remove here recursively temp folder
rm -rf $output_dir/wav

exit 0

