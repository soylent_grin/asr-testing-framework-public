#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

source $(realpath "$PWD/./path.sh") || exit 1

#input_list=/home/pyyurkov/Kaldi/kaldi-master/egs/Kaldi-VoxForge-ru-work/exp_ext_VoxForge_fmllr/test_results/fmllr_gmm_dnn_decode_NOT_Aelita_list/dnn_txtdata/wav.scp
#dir_depth=2
#wer_dir=/home/pyyurkov/Kaldi/kaldi-master/egs/Kaldi-VoxForge-ru-work/exp_ext_VoxForge_fmllr/test_results/fmllr_gmm_dnn_decode_NOT_Aelita_list/decode_dnn
#score_dir=$wer_dir/scoring/log
#output_dir=/home/pyyurkov/Kaldi/kaldi-master/egs/Kaldi-VoxForge-ru-work/exp_ext_VoxForge_fmllr/test_results/fmllr_gmm_dnn_decode_NOT_Aelita_list/decode_dnn_result/scoring

input_list=$1
dir_depth=$2
wer_dir=$3
score_dir=${wer_dir}/scoring/log
output_dir=$4


[ -d ${output_dir} ] && rm -rf ${output_dir}
mkdir -p ${output_dir}

# getting best wer result
best_wer=$(grep WER ${wer_dir}/wer_* | utils/best_wer.sh | sed -n '/.*wer_/s///p')

echo "Result best wer: $best_wer"

decode_file=${score_dir}/best_path.${best_wer}.log
#echo "Best decode file: $decode_file"

./local/voxforge-ru-decode-extractor.py --input-list ${input_list} \
                                        --dir-depth ${dir_depth} \
                                        --decode-file ${decode_file} \
                                        --output-dir ${output_dir}

#echo "HERRR!!!"
