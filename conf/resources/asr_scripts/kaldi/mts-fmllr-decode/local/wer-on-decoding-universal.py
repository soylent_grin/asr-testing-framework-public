#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from os.path import split, join, isdir
from os import makedirs
import os
import re
import subprocess


def myParser():
    parser = argparse.ArgumentParser(description="Считаем WER для каждого файла в отдельности и фолдера целиком.")
    parser.add_argument('--input-list', dest='inputList', action="store", type=str, required=True,
                        help="Список файлов декодирования с полными путями.")
    parser.add_argument('--reference-list', dest='referenceList', action="store", type=str, required=True,
                        help="Список оригинальных текстовок с полными путями.")
    parser.add_argument('--input-dir-depth', dest='inputDirDepth', action="store", type=int, required=True,
                        help="Входная структура каталогов для корректного составления имени файла.")
    parser.add_argument('--output-dir-depth', dest='outputDirDepth', action="store", type=int, required=True,
                        help="До какой глубины извлекать директории при формировании выходной структуры каталогов.")
    parser.add_argument('--output-dir', dest='outputDir', action="store", type=str, required=True, help="Полный путь к директории для записи результатов.")
    return parser.parse_args()


def makeOutFileName(fullFName, dirDepth, outputDir):
    outFName = ""
    dirChunk = ""
    [curDir, fName] = split(fullFName)
    for i in range(dirDepth):
        [curDir, chunk] = split(curDir)
        dirChunk = join(chunk, dirChunk)
    outFName = join(outputDir, dirChunk)
    if not isdir(outFName):
        makedirs(outFName)
    fName = fName.replace('.wav', '.txt')
    outFName = join(outFName, fName)
    return outFName


def checkFilenames(listOfOriginals, listOfDecodings, inputDirDepth):
    if len(listOfOriginals) != len(listOfDecodings):
        return False
    # NOTE: depth for original files is usually equal to 2
    for idx, fullFilename in enumerate(listOfDecodings):
        filenameList = fullFilename.split('/')
        filenameDecoding = '-'.join(filenameList[-(inputDirDepth + 1):])  # slice only meaningful filename
        filenameOriginal = '-'.join(listOfOriginals[idx].split('/')[-3:])
        print(filenameDecoding, filenameOriginal)
        if filenameDecoding != filenameOriginal:
            return False

    return True


def computeWER(listOfOriginals, listOfDecodings, inputDirDepth, outputDirDepth, outputDir):
    # compute wer for each file independently
    for idx, fullFName in enumerate(listOfOriginals):
        # create subfolders and filename
        filename = makeOutFileName(fullFName, outputDirDepth, outputDir)
        # compute the WER per file
        command = 'compute-wer --text --mode=present ark:{orig_text}  ark:{decoding} > {filename} || exit 1'.format(filename=filename,
                                                                                                                orig_text=fullFName,
                                                                                                                decoding=listOfDecodings[idx])

        p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()

    result = []
    wer_nom, wer_denom, ser_nom, ser_denom = 0, 0, 0, 0
    for root, dirnames, filenames in os.walk(outputDir):
        for filename in filenames:
            if filename.endswith('.txt'):
                with open('/'.join([root, filename]), 'r') as f:
                    data = f.read()
                    match_wer = re.search(r'WER ([\d\.\d]+)', data)
                    match_ser = re.search(r'SER ([\d\.\d]+)', data)
                    match_proportion = re.findall(r'(\d+) / (\d+)', data)
                    if match_wer and match_ser and match_proportion:
                        WER = match_wer.group(1)
                        SER = match_ser.group(1)
                        nameToSave = '-'.join(filename.split('/')[-(inputDirDepth + 1):])
                        result.append('    '.join([nameToSave, WER, SER]))
                        wer_nom += int(match_proportion[0][0])
                        wer_denom += int(match_proportion[0][1])
                        ser_nom += int(match_proportion[1][0])
                        ser_denom += int(match_proportion[1][1])
                    else:
                        raise ValueError("Can't find WER or SER in", filename)

    # add total aggregation in the beginning of the table
    result.insert(0, '    '.join(['Total', '%.2f' % (wer_nom / wer_denom), '%.2f' % (ser_nom / ser_denom)]))
    resultStr = '\n'.join(result)
    return resultStr


def main(referenceList, inputList, inputDirDepth, outputDirDepth, outputDir):

    # read list of original files
    with open(referenceList, 'r', encoding='utf8') as inpListF:
        listOfOriginals = inpListF.read().splitlines()
    print(listOfOriginals)
    listOfOriginals = sorted(listOfOriginals)

    # read list of decodings
    with open(inputList, 'r', encoding='utf8') as inpListF:
        listOfDecodings = inpListF.read().splitlines()
    print(listOfDecodings)
    listOfDecodings = sorted(listOfDecodings)

    # raise error if filenames are different
    if not checkFilenames(listOfOriginals, listOfDecodings, inputDirDepth):
        raise ValueError('Original and decoding files are different')

    # now access the .txt file and compute the WER per file
    wersPerFile = computeWER(listOfOriginals, listOfDecodings, inputDirDepth, outputDirDepth, outputDir)

    # save file to output dir
    outFileName = '/'.join([outputDir, 'WER-per-file.txt'])
    with open(outFileName, 'w') as f:
        f.write(wersPerFile)
    f.close()
    pass


if __name__ == '__main__':
    input_args = myParser()

    main(input_args.referenceList, input_args.inputList, input_args.inputDirDepth, input_args.outputDirDepth, input_args.outputDir)

    print("Program complete. Results are stored in dir:")
    print(input_args.outputDir)
