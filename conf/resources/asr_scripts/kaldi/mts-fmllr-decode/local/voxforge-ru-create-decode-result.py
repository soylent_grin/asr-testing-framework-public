#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from os.path import split, join, isdir, isfile
from os import listdir

def myParser():
    parser = argparse.ArgumentParser(description="В директории с результатами декодирования формируем файл decode.result.",
                                     epilog= "Утилита находит все файлы по маске decode.*.log, сортирует их и после все складывает в один файл decode.result.\n"
                                             "Если в найденных файлах нарушается порядок нумерации, то система выпадает с ошибкой.")
    parser.add_argument('--input-dir', dest='inputDir', action="store", type=str, required=True,
                        help="Директория, в которой должны находиться файлы decode.*.log.")
    return parser.parse_args()

def create_decode_result_file(inputDir):
    if not isdir(inputDir):
        print("Input dir does not exist!!!")
        print(inputDir)
        exit(-1)
    if inputDir.endswith('/'):
        inputDir = inputDir[:-1]
    flist = [x for x in listdir(inputDir) if x.startswith("decode.")]
    dec_res = join(inputDir, "decode.result")
    with open(dec_res, 'w', encoding='utf8') as drF:
        for i in range(1,len(flist)+1):
            fname = "decode." + str(i) + ".log"
            fullName = join(inputDir, fname)
            if not isfile(fullName):
                print("Something wrong with decode.*.log files in dir:")
                print(inputDir)
                exit(-1)
            F = open(fullName, 'r', encoding='utf8')
            flines = F.readlines()
            F.close()
            print(*flines, sep='', end='', file=drF)
            #print(fullName)

    return

# ====================================================
if __name__ == '__main__':
    input_args = myParser()

    create_decode_result_file(input_args.inputDir)

    print("Program complete. Results are stored in file:")
    print(input_args.inputDir + "/decode.result")
