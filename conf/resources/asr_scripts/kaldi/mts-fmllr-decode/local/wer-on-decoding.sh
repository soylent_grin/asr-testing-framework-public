#!/usr/bin/env bash
# Script computes WER on the decoding results
# It requires local/data-preparation.py
#             local/voxforge-ru-decode-extractor-save-to-one-file-for-wer.py
#             local/voxforge-ru-create-decode-result.py
# You should run this script `local/wer-on-decoding.sh`


source $(realpath "$PWD/./path.sh") || exit 1


# Configuration section
dir_depth=1
src_wav_list=$(realpath $EXP_ROOT/../train_test_lists)/voxforge-ru-wav-test-list-extended.txt
results_dir=$EXP_ROOT/test_results/fmllr_gmm_dnn_decode_test_list_extended
decode_dir=$results_dir/decode_fmllr/log
output_dir=$results_dir/decode_fmllr_result/decode


# Prepare directories
[ -d $output_dir ] && rm -rf $output_dir
mkdir -p $output_dir

[ -f $decode_dir/decode.result ] && rm -f $decode_dir/decode.result


# Prepare original text file
# Set first TRUE param to False if you don't want to create TEXT file
# Set second TRUE param to False if you don't want to use dictor specific information
local/data-preparation.py $src_wav_list $DATA_ROOT $results_dir/gmm_txtdata True True || exit 1;
# wav.scp should come from and orig_text come from this function
input_list=$results_dir/gmm_txtdata/wav.scp
orig_text=$results_dir/gmm_txtdata/text


# Prepare decoded data
./local/voxforge-ru-create-decode-result.py --input-dir $decode_dir

./local/voxforge-ru-decode-extractor-save-to-one-file-for-wer.py --input-list $input_list \
                                        --dir-depth $dir_depth \
                                        --decode-file $decode_dir/decode.result \
                                        --output-dir $output_dir
# file decode-result.txt comes from here
decoding=$output_dir/decode-result.txt


# Compute WER and save it to file $outputdir/decode-wer.txt
compute-wer --text --mode=present \
     ark:$orig_text  ark:$decoding > $output_dir/decode-wer.txt || exit 1;

#echo $outputdir/decode-wer.txt;
egrep  '(WER)|(SER)' < $output_dir/decode-wer.txt;

exit 0

