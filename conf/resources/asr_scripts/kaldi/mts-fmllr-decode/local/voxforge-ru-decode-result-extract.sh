#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

source $(realpath "$PWD/./path.sh") || exit 1

#input_list=/home/pyyurkov/Kaldi/kaldi-master/egs/Kaldi-VoxForge-ru-work/exp_ext_VoxForge_fmllr/test_results/fmllr_gmm_dnn_decode_NOT_Aelita_list/dnn_txtdata/wav.scp
#dir_depth=2
#decode_dir=/home/pyyurkov/Kaldi/kaldi-master/egs/Kaldi-VoxForge-ru-work/exp_ext_VoxForge_fmllr/test_results/fmllr_gmm_dnn_decode_NOT_Aelita_list/decode_dnn/log
#output_dir=/home/pyyurkov/Kaldi/kaldi-master/egs/Kaldi-VoxForge-ru-work/exp_ext_VoxForge_fmllr/test_results/fmllr_gmm_dnn_decode_NOT_Aelita_list/decode_dnn_result/decode

input_list=$1
dir_depth=$2
decode_dir=$3
output_dir=$4


[ -d ${output_dir} ] && rm -rf ${output_dir}
mkdir -p ${output_dir}

[ -f ${decode_dir}/decode.result ] && rm -f ${decode_dir}/decode.result

./local/voxforge-ru-create-decode-result.py --input-dir ${decode_dir}

./local/voxforge-ru-decode-extractor.py --input-list ${input_list} \
                                        --dir-depth ${dir_depth} \
                                        --decode-file ${decode_dir}/decode.result \
                                        --output-dir ${output_dir}

#echo "HERRR!!!"
