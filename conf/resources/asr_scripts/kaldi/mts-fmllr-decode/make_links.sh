#!/usr/bin/env bash

source ./path.sh

CurDo=`pwd`

echo $CurDo

ln -s $KALDI_ROOT/egs/wsj/s5/steps $CurDo
ln -s $KALDI_ROOT/egs/wsj/s5/utils $CurDo

ln -sv $CurDo/../v1/local/data-preparation.py $CurDo/local/data-preparation.py
ln -sv $CurDo/../v1/local/get_time.py $CurDo/local/get_time.py
ln -sv $CurDo/../v1/local/score.sh $CurDo/local/score.sh
