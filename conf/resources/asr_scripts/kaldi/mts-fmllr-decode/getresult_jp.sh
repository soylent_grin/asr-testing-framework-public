#!/usr/bin/env bash

source ./path.sh

res_dir=$EXP_ROOT/test_results/$1

# Getting wer results [see RESULTS file]
#for x in $res_dir/*/decode*; do
for x in $res_dir/decode*; do
    echo $x
    [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh
done

# Getting cer results [see RESULTS file]
#for x in $res_dir/*/decode*; do
for x in $res_dir/decode*; do
    echo $x
    [ -d $x ] && grep WER $x/cer_* | utils/best_wer.sh
done

