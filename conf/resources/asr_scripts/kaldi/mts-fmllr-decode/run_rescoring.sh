#!/usr/bin/env bash

# Утилита для запуска процедуры local/scoring
# на случай если на основном проходе в процедуре декодирования
# не было найдено оптимальных параметров.

source ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

# The number of parallel jobs to be started for some parts of the recipe
# Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
njobs=4

exp=$KALDI_ROOT/egs/Kaldi-VoxForge-ru/s5
model_dir=$exp/models/dnn_smbr_i1lats_azure
graph_dir=$model_dir/graph

result_dir=$exp/fmllr_gmm_dnn_decode_NOT_Aelita_list

# Измените эти значения для поиска оптимальных параметров
scoring_opts="--min-lmwt 5 --max-lmwt 30"


[ ! -x local/score.sh ] && { echo "Not scoring because local/score.sh does not exist or not executable."; exit 1; }

local/score.sh --cmd "$cmd" $scoring_opts $data_dir $graph_dir $dir ||
    { echo "$0: Scoring failed. (ignore by '--skip-scoring true')"; exit 1; }

