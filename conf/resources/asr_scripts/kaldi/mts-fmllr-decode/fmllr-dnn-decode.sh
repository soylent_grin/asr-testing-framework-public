#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

pushd /cvoaf/asr_scripts/kaldi/mts-fmllr-decode || exit 1

source ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

exp_config=/cvoaf/temp/mts-fmllr-decode/conf/exp.conf
conf=/cvoaf/temp/mts-fmllr-decode/conf
outdir=/cvoaf/temp/mts-fmllr-decode/results
test_list=/cvoaf/temp/mts-fmllr-decode/dataset/test-list.txt
DATA_ROOT=/cvoaf/temp/mts-fmllr-decode/dataset

CUR_STEP=
scoring_opts=
njobs=
do_filter_test=
njobs_test=
nnet_use_gpu_decode=
acwt=
dir_depth=
test_txt_ext=
EXP_ROOT=
extract_lattice_score=

if [[ -L utils && -L steps ]]; then
    rm -rf utils steps
fi

if [[ ! -L utils && ! -L steps ]]; then
   ./make_links.sh
fi


source utils/parse_options.sh || exit 1
[[ $# -ge 1 ]] && { echo "Unexpected arguments"; exit 1; }

[[ -z ${exp_config} ]] && { echo "Please provide --exp_config option to set up experiment configuration."; exit 1;}

source ${exp_config}

exp=${EXP_ROOT}

wav_root=${DATA_ROOT}

fmllr_model=fmllr_gmm
fmllr_model_dir=${exp}/${fmllr_model}
dnn_model=fmllr_dnn
dnn_model_dir=${exp}/${dnn_model}
graph_dir=${exp}/${dnn_model}/graph
txt_ext=${test_txt_ext}
do_fmllr_gmm_decode=false
dnn_max_mem_decode=100000000

start_time=$(date +%s)

echo "///////"
echo "// Starting from step $CUR_STEP"
echo "///////"

if [[ ${CUR_STEP} -le 0 ]]; then
[[ -d ${outdir} ]] && rm -rf ${outdir}

mkdir -p ${outdir}/mfcc
mkdir -p ${outdir}/log
mkdir -p ${outdir}/cmvn
mkdir -p ${outdir}/decode_gmm
mkdir -p ${outdir}/decode_gmm_result
mkdir -p ${outdir}/decode_dnn
mkdir -p ${outdir}/decode_dnn_result
mkdir -p ${outdir}/fmllr_feats
mkdir -p ${outdir}/gmm_txtdata
mkdir -p ${outdir}/dnn_txtdata
fi

#src_fmllr_dir=./exp/$fmllr_model

if [[ ${CUR_STEP} -le 1 ]]; then
echo "///////"
echo "// Data preaparation"
echo "///////"

# Set first TRUE param to False if you don't want to create TEXT file
# Set second TRUE param to False if you don't want to use dictor specific information
local/data-preparation.py ${test_list} ${wav_root} ${outdir}/gmm_txtdata True False ${txt_ext} ${do_filter_test} || exit 1;
[[ -f ${outdir}/gmm_txtdata/spk2utt ]] && sort -o ${outdir}/gmm_txtdata/spk2utt ${outdir}/gmm_txtdata/spk2utt
[[ -f ${outdir}/gmm_txtdata/utt2spk ]] && sort -o ${outdir}/gmm_txtdata/utt2spk ${outdir}/gmm_txtdata/utt2spk
[[ -f ${outdir}/gmm_txtdata/wav.scp ]] && sort -o ${outdir}/gmm_txtdata/wav.scp ${outdir}/gmm_txtdata/wav.scp
[[ -f ${outdir}/gmm_txtdata/text ]] && sort -o ${outdir}/gmm_txtdata/text ${outdir}/gmm_txtdata/text


echo "///////"
echo "// Computing mfcc and cmvn "
echo "//////"

steps/make_mfcc.sh --cmd "$train_cmd" --nj ${njobs}  --mfcc-config ${conf}/mfcc.conf \
    ${outdir}/gmm_txtdata ${outdir}/log ${outdir}/mfcc || exit 1;

steps/compute_cmvn_stats.sh ${outdir}/gmm_txtdata ${outdir}/log ${outdir}/cmvn || exit 1;
fi

if [[ ${CUR_STEP} -le 2 ]]; then
echo "///////"
echo "// Perform decoding (see log for results)"
echo "//////"

steps/decode_fmllr.sh --config ${conf}/decode.config --nj ${njobs} --cmd "$decode_cmd" \
                      --alignment-model ${fmllr_model_dir}/fmllr/final.alimdl \
                      --adapt-model ${fmllr_model_dir}/fmllr/final.mdl \
                      --scoring-opts "${scoring_opts}" \
                      --num-threads $(nproc) \
                      ${fmllr_model_dir}/graph \
                      ${outdir}/gmm_txtdata \
                      ${fmllr_model_dir}/fmllr/decode_gmm || exit 1;

# move results to outdir
mv -i   ${fmllr_model_dir}/fmllr/decode_gmm/* ${outdir}/decode_gmm
rm -rf  ${fmllr_model_dir}/fmllr/decode_gmm
rm -rf  ${fmllr_model_dir}/fmllr/decode_gmm.si
fi

if [[ ${CUR_STEP} -le 3 ]]; then
echo "///////"
echo "// Computing fMLLR features"
echo "///////"

steps/nnet/make_fmllr_feats.sh --nj ${njobs} --cmd "$train_cmd" \
     --transform-dir ${outdir}/decode_gmm \
     ${outdir}/dnn_txtdata \
     ${outdir}/gmm_txtdata \
     ${fmllr_model_dir} \
     ${outdir}/log \
     ${outdir}/fmllr_feats || exit 1


echo "///////"
echo "// Perform decoding "
echo "//////"

steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
      --config ${conf}/decode_dnn.config --acwt ${acwt} \
      --scoring-opts "$scoring_opts" \
      --use-gpu ${nnet_use_gpu_decode} \
      --max-mem ${dnn_max_mem_decode} \
      --num-threads $(nproc) \
      ${dnn_model_dir}/graph \
      ${outdir}/dnn_txtdata \
      ${dnn_model_dir}/decode_dnn || exit 1;

# move results to outdir
mv -i   ${dnn_model_dir}/decode_dnn/* ${outdir}/decode_dnn
rm -rf  ${dnn_model_dir}/decode_dnn
fi

if [[ ${CUR_STEP} -le 4 ]]; then
echo "///////"
echo "// Decode and scoring result extraction"
echo "//////"

# dir_depth=0 Переменная перенесена на верх
# fmllr results
input_list=${outdir}/gmm_txtdata/wav.scp
if [[ ${do_fmllr_gmm_decode} = "true" ]]; then
    decode_dir=${outdir}/decode_fmllr/log
    result_dir=${outdir}/decode_fmllr_result/decode
    ./local/voxforge-ru-decode-result-extract.sh ${input_list} ${dir_depth} ${decode_dir} ${result_dir}
fi

# gmm results
decode_dir=${outdir}/decode_gmm/log
result_dir=${outdir}/decode_gmm_result/decode
./local/voxforge-ru-decode-result-extract.sh ${input_list} ${dir_depth} ${decode_dir} ${result_dir}

# dnn results
input_list=${outdir}/dnn_txtdata/wav.scp
decode_dir=${outdir}/decode_dnn/log
result_dir=${outdir}/decode_dnn_result/decode
./local/voxforge-ru-decode-result-extract.sh ${input_list} ${dir_depth} ${decode_dir} ${result_dir}

# scoring result extraction if needed
if [[ ${extract_lattice_score} = "true" ]]; then

#    # fmllr results
#    input_list=${outdir}/gmm_txtdata/wav.scp
#    if [[ ${do_fmllr_gmm_decode} = "true" ]]; then
#        wer_dir=${outdir}/decode_fmllr
#        result_dir=${outdir}/decode_fmllr_result/scoring
#        ./local/voxforge-ru-score-result-extract.sh ${input_list} ${dir_depth} ${wer_dir} ${result_dir}
#    fi
#
#    # gmm results
#    wer_dir=${outdir}/decode_gmm
#    result_dir=${outdir}/decode_gmm_result/scoring
#    ./local/voxforge-ru-score-result-extract.sh ${input_list} ${dir_depth} ${wer_dir} ${result_dir}

    # dnn results
    input_list=${outdir}/dnn_txtdata/wav.scp
    wer_dir=${outdir}/decode_dnn
    result_dir=${outdir}/decode_dnn_result/scoring
    ./local/voxforge-ru-score-result-extract.sh ${input_list} ${dir_depth} ${wer_dir} ${result_dir}
fi
fi # of if [ $CUR_STEP -le 4 ]; then


end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py ${diff_time}
echo "//////"

best_wer_gmm=$(grep WER ${outdir}/decode_gmm/wer_* | ./utils/best_wer.sh | sed -n '/.*wer_/s///p')
best_wer_dnn=$(grep WER ${outdir}/decode_dnn/wer_* | ./utils/best_wer.sh | sed -n '/.*wer_/s///p')

#samples_number=$(cat ${outdir}/decode_dnniv/scoring/${best_wer}.tra | wc -l)
echo "Best GMM WER in wer_${best_wer_gmm}:"
cat ${outdir}/decode_gmm/wer_${best_wer_gmm} | egrep  '(WER)|(SER)'

echo "Best DNN WER in wer_${best_wer_dnn}:"
cat ${outdir}/decode_dnn/wer_${best_wer_dnn} | egrep  '(WER)|(SER)'

echo -e "Recognized text: "
int2sym.pl -f 2- ${graph_dir}/words.txt ${outdir}/decode_dnn/scoring/${best_wer_dnn}.tra | cut -d " " -f2-

