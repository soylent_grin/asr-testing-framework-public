#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

source ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

njobs=4
acwt=0.0909

fmllr_model=tri3b_mmi
fmllr_model_dir=./models/$fmllr_model
src_fmllr_dir=./exp/$fmllr_model

model=dnn_smbr_i1lats
model_dir=./models/$model

conf=./conf
datadir=$(realpath ./test_conf)

outputdir=./exp/$model

#src_wav_list=$datadir/voxforge-ru-wav-list-chunk-24-files.txt
#src_wav_list=$datadir/voxforge-ru-wav-list-chunk-25.txt
#src_wav_list=$datadir/voxforge-ru-wav-list.txt
src_wav_list=$datadir/voxforge-ru-wav-test-list.txt
wav_root=$(realpath $DATA_ROOT)


start_time=$(date +%s)

[ -d $outputdir ] && rm -rf $outputdir

mkdir -p $outputdir/mfcc
mkdir -p $outputdir/log
mkdir -p $outputdir/cmvn
mkdir -p $outputdir/decode
# mkdir -p $outputdir/decode_fmllr
mkdir -p $outputdir/fmllr_feats

echo "///////"
echo "// Data preaparation"
echo "///////"

# Set last param to False if you don't want to create TEXT file
local/data-preparation.py $src_wav_list $wav_root $outputdir True || exit 1

echo "///////"
echo "// Computing fMLLR features"
echo "///////"

steps/nnet/make_fmllr_feats.sh --nj $njobs --cmd "$train_cmd" \
     --transform-dir $src_fmllr_dir/decode_fmllr \
     $outputdir $src_fmllr_dir $fmllr_model_dir $outputdir/log $outputdir/fmllr_feats || exit 1

echo "///////"
echo "// Doing decoding (see log for results)"
echo "//////";

steps/nnet/decode.sh --nj $njobs --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
      $model_dir/graph \
      $outputdir \
      $model_dir/decode || exit 1;


# move results to outputdir
mv -i $model_dir/decode/* $outputdir/decode
rm -rf $model_dir/decode

end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py $diff_time
echo "//////";

# echo "HERRR!!!" && exit 0
