#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

source ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

njobs=24
#acwt=0.0909
scoring_opts="--min-lmwt 5 --max-lmwt 45"

exp=$EXP_ROOT
model=callcenter_fmllr_dnn_lm3/gmm_fmllr
model_dir=$exp/$model
#conf=./conf

#datadir=

outputdir=$exp/test_results/gmm_fmllr_decode

#src_wav_list=$datadir/voxforge-ru-wav-test-list.txt
src_wav_list=/usr/local/train_test_lists/cc_test_220_right.txt

txt_ext=".orig.4am.txt"
do_filter=True

wav_root=$DATA_ROOT

dir_depth=0

start_time=$(date +%s)

[ -d $outputdir ] && rm -rf $outputdir

mkdir -p $outputdir/mfcc
mkdir -p $outputdir/log
mkdir -p $outputdir/cmvn
mkdir -p $outputdir/decode_gmm_fmllr
mkdir -p $outputdir/decode_gmm_fmllr_result
mkdir -p $outputdir/decode_fmllr
mkdir -p $outputdir/decode_fmllr_result
mkdir -p $outputdir/gmm_txtdata


echo "///////"
echo "// Data preaparation"
echo "///////"

# Set first TRUE param to False if you don't want to create TEXT file
# Set second TRUE param to False if you don't want to use dictor specific information
local/data-preparation.py $src_wav_list $wav_root $outputdir/gmm_txtdata True False $txt_ext $do_filter || exit 1;
[ -f $outputdir/gmm_txtdata/spk2utt ] && sort -o $outputdir/gmm_txtdata/spk2utt $outputdir/gmm_txtdata/spk2utt
[ -f $outputdir/gmm_txtdata/utt2spk ] && sort -o $outputdir/gmm_txtdata/utt2spk $outputdir/gmm_txtdata/utt2spk
[ -f $outputdir/gmm_txtdata/wav.scp ] && sort -o $outputdir/gmm_txtdata/wav.scp $outputdir/gmm_txtdata/wav.scp
[ -f $outputdir/gmm_txtdata/text ] && sort -o $outputdir/gmm_txtdata/text $outputdir/gmm_txtdata/text


echo "///////"
echo "// Computing mfcc and cmvn "
echo "//////"

steps/make_mfcc.sh --cmd "$train_cmd" --nj $njobs  --mfcc-config conf/mfcc.conf \
    $outputdir/gmm_txtdata $outputdir/log $outputdir/mfcc || exit 1;

steps/compute_cmvn_stats.sh $outputdir/gmm_txtdata $outputdir/log $outputdir/cmvn || exit 1;

echo "///////"
echo "// Doing decoding (see log for results)"
echo "//////";

steps/decode_fmllr.sh --config conf/decode.config --nj $njobs --cmd "$decode_cmd" \
                      --alignment-model $model_dir/fmllr/final.alimdl \
                      --adapt-model $model_dir/fmllr/final.mdl \
                      --scoring-opts "$scoring_opts" \
                      $model_dir/graph \
                      $outputdir/gmm_txtdata \
                      $model_dir/fmllr/decode_fmllr || exit 1;

steps/decode.sh --config conf/decode.config --nj $njobs --cmd "$decode_cmd" \
                --transform-dir $model_dir/fmllr/decode_fmllr \
                --scoring-opts "$scoring_opts" \
                $model_dir/graph \
                $outputdir/gmm_txtdata \
                $model_dir/decode_gmm_fmllr || exit 1;

# move results to outputdir
mv -i $model_dir/fmllr/decode_fmllr/* $outputdir/decode_fmllr
rm -rf $model_dir/fmllr/decode_fmllr
rm -rf $model_dir/fmllr/decode_fmllr.si

mv -i   $model_dir/decode_gmm_fmllr/* $outputdir/decode_gmm_fmllr
rm -rf  $model_dir/decode_gmm_fmllr


input_list=$outputdir/gmm_txtdata/wav.scp

decode_dir=$outputdir/decode_gmm_fmllr/log
result_dir=$outputdir/decode_gmm_fmllr_result/decode
./local/voxforge-ru-decode-result-extract.sh $input_list $dir_depth $decode_dir $result_dir

wer_dir=$outputdir/decode_gmm_fmllr
result_dir=$outputdir/decode_gmm_fmllr_result/scoring
./local/voxforge-ru-score-result-extract.sh $input_list $dir_depth $wer_dir $result_dir



end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py $diff_time
echo "//////";


#echo "HERRR!!!" && exit 0
