#!/bin/bash

# Файл для создания HCLG.fst по входному списку wav-файлов и готовой модели

. ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
. ./cmd.sh || exit 1

# The number of parallel jobs to be started for some parts of the recipe
# Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
#njobs=1		 #njobs=2	# original line

# Test-time language model order
lm_order=

# Word position dependent phones?
pos_dep_phones=

#List of wav-files. The txt file must be supplied near every wav. Can't be used together with $text_corpus.
data_train_list=

#use this parameter to create LM from prepared corpus. Can't be used together with $train_list.
text_corpus=

EXP_ROOT=
DATA_ROOT=

acoustic_model=
txt_ext=
do_filter=

use_ds_info=
show_AM_error_message=true

move_to_dir=

do_garbage=
train_txt_ext=
test_txt_ext=

do_filter_test=
do_filter_train=

dict_name=
g2p_model=

do_train=

exp_config=

# The user of this script could change some of the above parameters. Example:
# /bin/bash run.sh --pos-dep-phones false
. utils/parse_options.sh || exit 1

[[ $# -ge 1 ]] && { echo "Unexpected arguments"; exit 1; }

source ${exp_config} || exit 1

echo "============= Current training settings ============="
echo "exp_config = $exp_config"
echo
echo "DATA_ROOT: $DATA_ROOT"
echo "EXP_ROOT:  $EXP_ROOT"
echo
echo "Test-time language model order"
echo "lm_order = $lm_order"
echo "Use dictor specific info like in VoxForge"
echo "use_ds_info = {$use_ds_info}"
echo
echo "Word position dependent phones?"
echo "pos_dep_phones = {$pos_dep_phones}"
echo "train_txt_ext = $train_txt_ext"
echo "test_txt_ext = $test_txt_ext"
echo
echo "do_filter_train = $do_filter_train"
echo "do_filter_test = $do_filter_test"
echo "do_garbage = $do_garbage"
echo
echo "Dictionary and g2p params:"
echo "dict_name = $dict_name"
echo "g2p_model = $g2p_model"
echo "===================================================="
echo "Do_train param set to = {$do_train}"
[[ -z ${do_train} ]] && { echo "Set do_train param in HCLG call"; exit 1;}
[[ ${do_train}="True" ]] && { do_filter=${do_filter_train}; echo "do_filter set by do_filter_train"; } \
                     || { do_filter=${do_filter_test}; echo "do_filter set by do_filter_test"; }

echo "Do filter = $do_filter"
echo "===================================================="

[[ -z ${data_train_list} ]] && [[ -z ${text_corpus} ]] && { echo "One of params train-list or text-corpus must be provided!"; exit -1;}
[[ ! -z ${data_train_list} ]] && [[ ! -z ${text_corpus} ]] && { echo "Only one of the parameters train-list or text-corpus must be provided!"; echo -1;}
[[ -z ${EXP_ROOT} ]] && { echo "You need to set \"EXP_ROOT\" variable in exp. config file to point to the directory with experiment results"; exit 1;}
[[ -z ${DATA_ROOT} ]] && { echo "You need to set \"DATA_ROOT\" variable in exp. config file to point to the directory to host wav-data"; exit 1;}
[[ -z ${do_garbage} ]] && { echo "Set do_garbage param to True or False"; exit 1;}
[[ -z ${lm_order} ]] && { echo "Set lm_order param."; exit 1;}
[[ -z ${use_ds_info} ]] && { echo "Set use_ds_info param."; exit 1;}
[[ -z ${pos_dep_phones} ]] && { echo "Set pos_dep_phones param."; exit 1;}
[[ -z ${train_txt_ext} ]] || [[ -z ${test_txt_ext} ]] && { echo "Set train_txt_ext or test_txt_ext param."; exit 1;}
[[ -z "$do_filter_test" ]] || [[ -z "$do_filter_train" ]] && { echo "Set do_filter_train or do_filter_test param"; exit 1;}
[[ -z ${dict_name} ]] || [[ -z ${g2p_model} ]] && { echo "Set dict or g2p_model in config_exp file"; exit 1;}
[[ -z ${do_filter} ]] && { echo "do_filter param does't set due to other reason problems"; exit 1;}


hclg_dir=${EXP_ROOT}/HCLG
data_txt_dir=${hclg_dir}/data_txt
arpa_dir=${hclg_dir}/arpa_dir

[[ -d ${hclg_dir} ]] && rm -rf ${hclg_dir}

mkdir -p ${hclg_dir}
mkdir -p ${hclg_dir}/lang
mkdir -p ${arpa_dir}
mkdir -p ${arpa_dir}/lang_tmp
mkdir -p ${data_txt_dir}/dict
mkdir -p ${data_txt_dir}/scp

# подготавливаем данные
if [[ ! -z ${data_train_list} ]]; then
    # Set first TRUE param to False if you don't want to create TEXT file
    # Set second TRUE param to False if you don't want to use dictor specific information
    local/data-preparation.py ${data_train_list} ${DATA_ROOT} ${data_txt_dir}/scp True ${use_ds_info} ${txt_ext} ${do_filter} || exit 1;
    [[ -f ${data_txt_dir}/scp/spk2utt ]] && sort -o ${data_txt_dir}/scp/spk2utt ${data_txt_dir}/scp/spk2utt
    [[ -f ${data_txt_dir}/scp/utt2spk ]] && sort -o ${data_txt_dir}/scp/utt2spk ${data_txt_dir}/scp/utt2spk
    [[ -f ${data_txt_dir}/scp/wav.scp ]] && sort -o ${data_txt_dir}/scp/wav.scp ${data_txt_dir}/scp/wav.scp
    [[ -f ${data_txt_dir}/scp/text ]] && sort -o ${data_txt_dir}/scp/text ${data_txt_dir}/scp/text
else
    cp -rL ${text_corpus} ${data_txt_dir}/scp/text
fi

# Prepare ARPA LM and vocabulary using SRILM
local/voxforge_prepare_lm.sh --order ${lm_order} --train_corpus ${data_txt_dir}/scp/text \
                             --arpa_dir ${arpa_dir} || exit 1

# Prepare the lexicon and various phone lists
# Pronunciations for OOV words are obtained using a pre-trained Sequitur model
local/cc_prepare_dict.sh --dict_dir ${data_txt_dir}/dict --dict_name ${dict_name} --g2p_model ${g2p_model}\
                         --arpa_dir ${arpa_dir} --do-garbage ${do_garbage} || exit 1

# Prepare $arpa_dir/lang_tmp and $hclg_dir/lang directories
utils/prepare_lang.sh --position-dependent-phones ${pos_dep_phones} \
 ${arpa_dir} '!SIL' ${arpa_dir}/lang_tmp ${hclg_dir}/lang || exit 1

#echo "HERRR!!!" && exit 1

# Prepare G.fst
# Provide arpa_dir hclg_dir
local/hclg_make_Gfst.sh ${arpa_dir} ${hclg_dir}

if [[ ! -z ${acoustic_model} ]]; then
    [[ -d ${hclg_dir}/model ]] && rm -rf ${hclg_dir}/model
    mkdir -p ${hclg_dir}/model
    cp -rL ${acoustic_model}/final.mdl ${hclg_dir}/model
    cp -rL ${acoustic_model}/tree ${hclg_dir}/model

    [[ -d ${hclg_dir}/graph ]] && rm -rf ${hclg_dir}/graph
    mkdir -p ${hclg_dir}/graph

    # Making HCLG graph
    utils/mkgraph.sh ${hclg_dir}/lang ${hclg_dir}/model ${hclg_dir}/graph
    echo "========================================="
    echo "!!! HCLG prepare mission accomplished !!!"
    echo "========================================="
else
    if [[ "$show_AM_error_message" = true ]]; then
        echo "LG part has been prepared, but HCLG has not."
        echo "Acoustic model has't been provided."
        echo "If you want to create HCLG, please provide acoustic model"
    else
        echo "========================================="
        echo "!!! LG prepare mission accomplished !!!"
        echo "========================================="
    fi
fi

if [[ ! -z ${move_to_dir} ]]; then
    mv ${hclg_dir}/* ${move_to_dir}
    mv ${move_to_dir}/data_txt/dict ${move_to_dir}/dict
    mv ${move_to_dir}/data_txt/scp ${move_to_dir}/scp
    rm -rf ${move_to_dir}/data_txt
    rm -rf ${move_to_dir}/dict/cmudict-plain.txt
fi


#echo "HERRR_1!!!" && exit 0
