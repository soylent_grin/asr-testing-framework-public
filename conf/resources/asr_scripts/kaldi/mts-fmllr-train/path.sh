#@IgnoreInspection BashAddShebang

KALDI_ROOT=/usr/local/kaldi
#export KALDI_ROOT=/home/pyyurkov/Kaldi/kaldi
#export KALDI_ROOT=/usr/local/kaldi

[ -z ${KALDI_ROOT} ] && { echo "Provide KALDI_ROOT param"; exit 1; }

export PATH=$PWD/utils/:${KALDI_ROOT}/src/bin:${KALDI_ROOT}/tools/openfst/bin:${KALDI_ROOT}/src/fstbin/:${KALDI_ROOT}/src/gmmbin/:${KALDI_ROOT}/src/featbin/:${KALDI_ROOT}/src/lm/:${KALDI_ROOT}/src/lmbin/:${KALDI_ROOT}/src/sgmmbin/:${KALDI_ROOT}/src/sgmm2bin/:${KALDI_ROOT}/src/fgmmbin/:${KALDI_ROOT}/src/latbin/:${KALDI_ROOT}/src/nnetbin/:${KALDI_ROOT}/src/nnet2bin/:$PWD:$PATH

source ${KALDI_ROOT}/tools/env.sh

# Make sure that MITLM shared libs are found by the dynamic linker/loader
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$(pwd)/tools/mitlm-svn/lib

# Needed for "correct" sorting
export LC_ALL=C.UTF-8

