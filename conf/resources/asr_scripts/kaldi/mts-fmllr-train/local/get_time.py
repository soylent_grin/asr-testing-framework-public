#!/usr/bin/env python3
from sys import argv


def get_elapsed_time(in_elapsed_seconds):
    hh = in_elapsed_seconds // 3600
    seconds_rest = in_elapsed_seconds % 3600
    mm = seconds_rest // 60
    ss = seconds_rest % 60
    print("Time elapsed: " + str(hh) + "h:" + str(mm) + "m:" + str(ss) + "s.")
    return


# ====================================================
if __name__ == '__main__':
    if len(argv) != 2:
        print("Specify correct input. Just time in seconds.")

    seconds_elapsed = int(argv[1])
    get_elapsed_time(seconds_elapsed)
