#!/usr/bin/env bash

source ./path.sh

CurDo=`pwd`

ln -s ${KALDI_ROOT}/egs/wsj/s5/steps ${CurDo}
ln -s ${KALDI_ROOT}/egs/wsj/s5/utils ${CurDo}