#!/bin/bash

# Copyright 2012-2014  Brno University of Technology (Author: Karel Vesely)
#                2014  Guoguo Chen
# Apache 2.0

# This example script trains a DNN on top of fMLLR features. 
# The training is done in 3 stages,
#
# 1) RBM pre-training:
#    in this unsupervised stage we train stack of RBMs, 
#    a good starting point for frame cross-entropy trainig.
# 2) frame cross-entropy training:
#    the objective is to classify frames to correct pdfs.
# 3) sequence-training optimizing sMBR: 
#    the objective is to emphasize state-sequences with better 
#    frame accuracy w.r.t. reference alignment.

source ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

source ./path.sh ## Source the tools/utils (import the queue.pl)

# Config:
EXP_ROOT=

stage=0 # resume training with --stage N

# To use cuda set this value to false
skip_cuda_check=false

# The number of parallel jobs to be started for some parts of the recipe
# Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
njobs=24

scoring_opts="--min-lmwt 5 --max-lmwt 30"

nnet_use_gpu="yes" # yes|no|optionaly
nnet_use_gpu_decode="yes"

max_frames_mpe=30000  # option for sequential training
max_frames_dbn=30000 # for dbn pretraining. added by JP
dnn_max_mem_decode=100000000

beam=0
retry_beam=0
# End of config.
source utils/parse_options.sh || exit 1;
#

[ -z ${EXP_ROOT} ] && { echo "You need to set \"EXP_ROOT\" option in run_dnn function call to point to the directory with experiment results"; exit 1;}

exp=${EXP_ROOT}
gmmdir=${exp}/models/tri3b
data_fmllr=${exp}/data-fmllr-tri3b
data_txt_train=${exp}/data_txt/train
data_txt_test=${exp}/data_txt/test
conf=/cvoaf/temp/mts-fmllr-train/conf

echo "Current stage: $stage"
echo "Nnet use gpu param: $nnet_use_gpu"
#echo "HERRR!!!" && exit 0

if [ ${nnet_use_gpu} = "no" ]; then
    skip_cuda_check=true
fi
echo "skip_cuda_check param: $skip_cuda_check"

[ ${beam} -eq 0 ] || [ ${retry_beam} -eq 0 ] && { echo "Please set params beam and retry_beam!"; exit 1; }

#echo "HERRR!!!" && exit 0

if [ ${stage} -le 0 ]; then
  # Store fMLLR features, so we can train on them easily,
  # test
  steps/nnet/make_fmllr_feats.sh --nj ${njobs} --cmd "$train_cmd" \
     --transform-dir ${gmmdir}/decode \
     ${data_fmllr}/test \
     ${data_txt_test}/scp \
     ${gmmdir} \
     ${data_fmllr}/test/log \
     ${data_fmllr}/test/data || exit 1

  # train
  steps/nnet/make_fmllr_feats.sh --nj ${njobs} --cmd "$train_cmd" \
     --transform-dir ${gmmdir}_ali \
     ${data_fmllr}/train \
     ${data_txt_train}/scp \
     ${gmmdir}  \
     ${data_fmllr}/train/log \
     ${data_fmllr}/train/data || exit 1

  # split the data : 90% train 10% cross-validation (held-out)
  dir=${data_fmllr}/train
  utils/subset_data_dir_tr_cv.sh ${dir} ${dir}_tr90 ${dir}_cv10 || exit 1
fi

if [ ${stage} -le 1 ]; then
  # Pre-train DBN, i.e. a stack of RBMs
  dir=${exp}/models/dnn5b_pretrain-dbn
  (tail --pid=$$ -F ${dir}/log/pretrain_dbn.log 2>/dev/null)& # forward log
  ${cuda_cmd} ${dir}/log/pretrain_dbn.log \
      local/nnet/pretrain_dbn.sh --skip-cuda-check ${skip_cuda_check} \
        --rbm-iter 1 --max_frames ${max_frames_dbn} ${data_fmllr}/train ${dir} || exit 1;

    #steps/nnet/pretrain_dbn.sh --skip-cuda-check $skip_cuda_check \
    #    --rbm-iter 1 $data_fmllr/train $dir || exit 1;
fi

if [ ${stage} -le 2 ]; then
  # Train the DNN optimizing per-frame cross-entropy.
  dir=${exp}/models/dnn5b_pretrain-dbn_dnn
  ali=${gmmdir}_ali
  feature_transform=${exp}/models/dnn5b_pretrain-dbn/final.feature_transform
  dbn=${exp}/models/dnn5b_pretrain-dbn/6.dbn
  (tail --pid=$$ -F ${dir}/log/train_nnet.log 2>/dev/null)& # forward log
  # Train
  ${cuda_cmd} ${dir}/log/train_nnet.log \
    steps/nnet/train.sh --feature-transform ${feature_transform} --dbn ${dbn} --hid-layers 0 \
    --learn-rate 0.008 --skip-cuda-check ${skip_cuda_check} \
     ${data_fmllr}/train_tr90 ${data_fmllr}/train_cv10 ${data_txt_train}/lang ${ali} ${ali} ${dir} || exit 1;

  # Decode with the trigram swbd language model.
  if [[ "$nnet_use_gpu_decode" == "yes" || "$nnet_use_gpu_decode" == "wait" ]]; then
      steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
        --config ${conf}/decode_dnn.config --acwt 0.08333 \
        --scoring-opts "$scoring_opts" \
        --use_gpu ${nnet_use_gpu_decode} \
        --max-mem ${dnn_max_mem_decode} \
        --num-threads $(nproc) \
        ${gmmdir}/graph ${data_fmllr}/test \
        ${dir}/decode || exit 1;
  else
      steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
        --config ${conf}/decode_dnn.config --acwt 0.08333 \
        --scoring-opts "$scoring_opts" \
        --use_gpu ${nnet_use_gpu_decode} \
        --max-mem ${dnn_max_mem_decode} \
        ${gmmdir}/graph ${data_fmllr}/test \
        ${dir}/decode || exit 1;
  fi
fi


# Sequence training using sMBR criterion, we do Stochastic-GD 
# with per-utterance updates. We use usually good acwt 0.1
# Lattices are re-generated after 1st epoch, to get faster convergence.
dir=${exp}/models/dnn5b_pretrain-dbn_dnn_smbr
srcdir=${exp}/models/dnn5b_pretrain-dbn_dnn
acwt=0.0909

if [ ${stage} -le 3 ]; then
  # First we generate lattices and alignments:
  steps/nnet/align.sh --nj ${njobs} --cmd "$train_cmd" \
    --use_gpu ${nnet_use_gpu} \
    --beam ${beam} --retry-beam ${retry_beam} \
    ${data_fmllr}/train \
    ${data_txt_train}/lang ${srcdir} \
    ${srcdir}_ali || exit 1;
  #steps/nnet/make_denlats.sh --nj $njobs \
  steps/nnet/make_denlats.sh --nj ${njobs} --sub-split 100 \
    --cmd "$decode_cmd" \
    --config ${conf}/decode_dnn.config \
    --acwt ${acwt} \
    --use_gpu ${nnet_use_gpu} \
    ${data_fmllr}/train \
    ${data_txt_train}/lang \
    ${srcdir} ${srcdir}_denlats || exit 1;
fi

if [ ${stage} -le 4 ]; then
  # Re-train the DNN by 1 iteration of sMBR 
  local/nnet/train_mpe_jp.sh --cmd "$cuda_cmd" \
    --skip-cuda-check ${skip_cuda_check} \
    --num-iters 1 --acwt ${acwt} \
    --do-smbr true \
    --max-frames ${max_frames_mpe} \
    --nnet-use_gpu ${nnet_use_gpu} \
    ${data_fmllr}/train \
    ${data_txt_train}/lang \
    ${srcdir} \
    ${srcdir}_ali \
    ${srcdir}_denlats \
    ${dir} || exit 1
  # Decode (reuse HCLG graph)
  for ITER in 1; do
    # Decode with the trigram swbd language model.
    if [[ "$nnet_use_gpu_decode" == "yes" || "$nnet_use_gpu_decode" == "wait" ]]; then
        steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
          --config ${conf}/decode_dnn.config \
          --nnet ${dir}/${ITER}.nnet --acwt ${acwt} \
          --scoring-opts "$scoring_opts" \
          --use_gpu ${nnet_use_gpu_decode} \
          --max-mem ${dnn_max_mem_decode} \
          --num-threads $(nproc) \
          ${gmmdir}/graph ${data_fmllr}/test \
          ${dir}/decode_test || exit 1;
     else
        steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
          --config ${conf}/decode_dnn.config \
          --nnet ${dir}/${ITER}.nnet --acwt ${acwt} \
          --scoring-opts "$scoring_opts" \
          --use_gpu ${nnet_use_gpu_decode} \
          --max-mem ${dnn_max_mem_decode} \
          ${gmmdir}/graph ${data_fmllr}/test \
          ${dir}/decode_test || exit 1;
     fi
  done 
fi

# Re-generate lattices, run 4 more sMBR iterations
dir=${exp}/models/dnn5b_pretrain-dbn_dnn_smbr_i1lats
srcdir=${exp}/models/dnn5b_pretrain-dbn_dnn_smbr
acwt=0.0909

if [ ${stage} -le 5 ]; then
  # First we generate lattices and alignments:
  steps/nnet/align.sh --nj ${njobs} --cmd "$train_cmd" \
    --use_gpu ${nnet_use_gpu} \
    --beam ${beam} --retry-beam ${retry_beam} \
    ${data_fmllr}/train \
    ${data_txt_train}/lang \
    ${srcdir} ${srcdir}_ali || exit 1;
  #steps/nnet/make_denlats.sh --nj $njobs \
  steps/nnet/make_denlats.sh --nj ${njobs} --sub-split 100 \
    --cmd "$decode_cmd" \
    --config ${conf}/decode_dnn.config \
    --acwt ${acwt} \
    --use_gpu ${nnet_use_gpu} \
    ${data_fmllr}/train \
    ${data_txt_train}/lang \
    ${srcdir} ${srcdir}_denlats || exit 1;
fi

if [ ${stage} -le 6 ]; then
  # Re-train the DNN by 1 iteration of sMBR 
  local/nnet/train_mpe_jp.sh --cmd "$cuda_cmd" \
    --skip-cuda-check ${skip_cuda_check} \
    --num-iters 2 --acwt ${acwt} \
    --do-smbr true \
    --max-frames ${max_frames_mpe} \
    --nnet-use_gpu ${nnet_use_gpu} \
    ${data_fmllr}/train \
    ${data_txt_train}/lang \
    ${srcdir} ${srcdir}_ali \
    ${srcdir}_denlats ${dir} || exit 1
  # Decode (reuse HCLG graph)
  for ITER in 1 2; do
    # Decode with the trigram swbd language model.
    if [[ "$nnet_use_gpu_decode" == "yes" || "$nnet_use_gpu_decode" == "wait" ]]; then
        steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
          --config ${conf}/decode_dnn.config \
          --nnet ${dir}/${ITER}.nnet --acwt ${acwt} \
          --scoring-opts "$scoring_opts" \
          --use_gpu ${nnet_use_gpu_decode} \
          --max-mem ${dnn_max_mem_decode} \
          --num-threads $(nproc) \
          ${gmmdir}/graph ${data_fmllr}/test \
          ${dir}/decode_test${ITER} || exit 1;
    else
        steps/nnet/decode.sh --nj ${njobs} --cmd "$decode_cmd" \
          --config ${conf}/decode_dnn.config \
          --nnet ${dir}/${ITER}.nnet --acwt ${acwt} \
          --scoring-opts "$scoring_opts" \
          --use_gpu ${nnet_use_gpu_decode} \
          --max-mem ${dnn_max_mem_decode} \
          ${gmmdir}/graph ${data_fmllr}/test \
          ${dir}/decode_test${ITER} || exit 1;
    fi
  done 
fi

# Getting results [see RESULTS file]
# for x in $exp/models/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
