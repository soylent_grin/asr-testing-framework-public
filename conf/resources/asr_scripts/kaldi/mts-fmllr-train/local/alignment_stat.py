#!/usr/bin/env python3

'''
Анализирует файлы acc.*.log в указанной папке
и показывает статистику изменения числа успешно обработанных файлов
в зависимости от итерации.
Результат выводится на экран.
Нужно указать число потоков и число выполненных итераций,
а так же директорию для потска log- файлов.
'''

import argparse
import glob
#import numpy as np
from os.path import splitext
from subprocess import check_output


def myParser():
    parser = argparse.ArgumentParser(description="Анализирует файлы acc.*.log в указанной папке\n"
                                        "и показывает статистику изменения числа успешно обработанных файлов\n"
                                        "в зависимости от итерации.",
                                     epilog="Подробное описание смотри в утилите.\n")
    parser.add_argument('--inputdir', dest='inDir', action='store', type=str, required=True,
                        help="Директория для поиска acc.*.log-файлов.")
    parser.add_argument('--njobs', dest='njobs', action='store', type=int, required=True,
                        help="Число потоков, на которые параллелилось вычисление.")
    parser.add_argument('--itersnum', dest='itersnum', action='store', type=int, required=True,
                        help="Число выполненных итераций, на которых формируется статистика.")
    return parser.parse_args()


def get_ietr_job(in_fname):

    fname, ext = splitext(in_fname)
    fname, ext = splitext(fname)
    jobNum = int(ext[1:]) - 1
    fname, ext = splitext(fname)
    iterNum = int(ext[1:]) - 1

    return iterNum, jobNum


def process_file(inFName):
    filesDone = -1
    filesTotal = -1

    exec_command = ['grep', 'Done', inFName]
    res_str = check_output(exec_command).decode('utf-8').rstrip('\n')
    idx = res_str.find('Done')
    res_str = res_str[idx:]
    left_str, right_str = res_str.split(',', maxsplit=1)
    strlist = left_str.split(' ')
    tmp = strlist[1]
    filesDone = int(strlist[1])
    strlist = right_str.strip(' ').split(' ')
    tmp = strlist[0]
    filesTotal = int(strlist[0]) + filesDone

    return filesDone, filesTotal

def get_ali_stat(inDir, njobs, itersnum):

    if not inDir.endswith('/'):
        inDir += '/'

    acc_files_list = sorted(glob.glob(inDir + 'acc.*.log'))

    #stat_mtx = np.zeros((njobs, itersnum), dtype=np.int)
    #filesTotalVec = np.zeros(njobs, dtype=np.int)
    stat_mtx = [[0] * itersnum for i in range(njobs)]
    filesTotalVec = [0] * njobs

    for fname in acc_files_list:
        i, j = get_ietr_job(fname)
        if (i >= itersnum) or (j >= njobs):
            continue
        filesDone, filesTotal = process_file(fname)
        stat_mtx[j][i] = filesDone
        if filesTotalVec[j] == 0:
            filesTotalVec[j] = filesTotal
        elif filesTotalVec[j] != filesTotal:
            print("invalid value for files total in file:")
            print(fname)

    for i in range(njobs):
        print_str = str(i+1) + ": "
        for j in range(itersnum):
            print_str += str(stat_mtx[i][j]) + ", "
        print_str = print_str[:-2]
        print_str += " of files " + str(filesTotalVec[i]) + "."
        print(print_str)

    return


# ====================================================
if __name__ == '__main__':
    input_args = myParser()

    get_ali_stat(input_args.inDir, input_args.njobs, input_args.itersnum)

