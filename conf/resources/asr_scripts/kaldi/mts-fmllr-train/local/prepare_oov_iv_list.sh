#!/usr/bin/env bash

# Copyright 2012 Vassil Panayotov
# Apache 2.0

# Copyright 2016 Ubskiy Dmitriy, STC

source ../path.sh || exit 1

dict_dir=/home/pyyurkov/Templates/chat_exp
check_file=$dict_dir/chat_messages_splited.txt
#arpa_dir=

echo "=== Preparing the dictionary ..."

#source utils/parse_options.sh

#if [ ! -d $locdict/zero_ru_cont_8k_v3 ]; then
#  echo "--- Downloading CMU dictionary ..."
#  mkdir -p $locdict
#  wget -P $locdict http://downloads.sourceforge.net/project/cmusphinx/Acoustic%20and%20Language%20Models/Russian/zero_ru_cont_8k_v3.tar.gz
#  tar xzf $locdict/zero_ru_cont_8k_v3.tar.gz -C $locdict
#fi

[ -z $dict_dir ] && echo "Dictionary dir is not initialised!!! Use --dict_dir option." && exit 1
#[ -z $arpa_dir ] && echo "Arpa dir is not initialised!!! Use --arpa_dir option." && exit 1

# cp $locdict/zero_ru_cont_8k_v3/ru.lm $locdata/lm.arpa
cp data/cmudict-plain.txt $dict_dir/cmudict-plain.txt

cut -f2- -d' ' < $check_file |\
   sed -e 's:[ ]\+: :g' |\
   sort -u > $dict_dir/corpus.txt

src=$KALDI_ROOT/tools/srilm/bin/i686-m64
$src/ngram-count -order 1 -write-vocab $dict_dir/vocab-full.txt -wbdiscount \
  -text $dict_dir/corpus.txt -lm $dict_dir/lm.arpa


echo "--- Searching for OOV words ..."
awk 'NR==FNR{words[$1]; next;} !($1 in words)' \
  $dict_dir/cmudict-plain.txt $dict_dir/vocab-full.txt |\
  egrep -v '<.?s>|-pau-|<unk>' > $dict_dir/vocab-oov.txt

awk 'NR==FNR{words[$1]; next;} ($1 in words)' \
  $dict_dir/vocab-full.txt $dict_dir/cmudict-plain.txt |\
  egrep -v '<.?s>|-pau-|<unk>' > $dict_dir/lexicon-iv.txt

# Show number of words in files
wc -l $dict_dir/vocab-oov.txt
wc -l $dict_dir/lexicon-iv.txt

echo "Mission accomplished!!!"

#echo "HERRR!!!" && exit 0
