#!/usr/bin/env bash

modelsDo=$1

[ -z $modelsDo ] && { echo "Please set input param to fmllr models path"; exit 1; }
#[ -d $modelsDo/models ] || { echo "Отсутствует папка ./models в пути: $modelsDo"; exit 1; }

curdir=`pwd`
cd $modelsDo
find . -xtype f -print0 | xargs -0 md5sum | sort | md5sum > $modelsDo/model_md5.txt
cd $curdir

