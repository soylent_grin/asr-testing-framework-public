#!/usr/bin/env bash

pushd /cvoaf/asr_scripts/kaldi/mts-fmllr-train || exit 1

source ./path.sh || exit 1

DATA_ROOT=
EXP_ROOT=
result_model_dir_name=
train_list=
lm_order=
use_ds_info=
txt_ext=
use_list=

exp_config=/cvoaf/temp/mts-fmllr-train/conf/exp.conf

source ${exp_config}

txt_corpus=${train_list}
models_dir=/cvoaf/temp/mts-fmllr-train/results/models

echo "============= Current training settings ============="
echo "DATA_ROOT: $DATA_ROOT"
echo "EXP_ROOT:  $EXP_ROOT"
echo
echo "Result model dir name. Will be created in exp_root dir."
echo "Result model dir: $result_model_dir_name"
echo
echo "txt_corpus: $txt_corpus"
echo
echo "Test-time language model order"
echo "lm_order = $lm_order"
echo "Use dictor specific info like in VoxForge"
echo "use_ds_info = {$use_ds_info}"
echo
echo "txt_ext = $txt_ext"
echo
echo "Create lm from list of files or from txt-corpus"
echo "use_list = $use_list"
echo "===================================================="

[[ -z ${EXP_ROOT} ]] && { echo "You need to set \"EXP_ROOT\" variable in exp. config file to point to the directory with experiment results"; exit 1;}
[[ -z ${DATA_ROOT} ]] && { echo "You need to set \"DATA_ROOT\" variable in exp. config file to point to the directory to host wav-data"; exit 1;}
[[ -z ${result_model_dir_name} ]] && { echo "Please set result_model_dir_name param to provide output dir name."; exit 1;}
[[ -z ${txt_corpus} ]] && { echo "Please set path for text corpus."; exit 1;}
[[ -z ${lm_order} ]] && { echo "Set lm_order param."; exit 1;}
[[ -z ${use_ds_info} ]] && { echo "Set use_ds_info param."; exit 1;}
[[ -z ${txt_ext} ]] && { echo "Set txt_ext param."; exit 1;}
[[ -z ${use_list} ]] && { echo "Set use_list."; exit 1;}

exp=${EXP_ROOT}
hclg_dir=${exp}/HCLG
res_models_dir=${exp}/${result_model_dir_name}

sat_fmllr_dir=${models_dir}/tri3b_ali
gmm_fmllr_dir=${models_dir}/tri3b_mmi

dnn_dir=${models_dir}/dnn5b_pretrain-dbn_dnn_smbr_i1lats

out_gmm=${res_models_dir}/fmllr_gmm
out_dnn=${res_models_dir}/fmllr_dnn


[[ ! -d ${models_dir} ]] && { echo "Can't locate models dir: ${models_dir}"; exit 1; }

[[ -d ${res_models_dir} ]] && rm -rf ${res_models_dir}
mkdir -p ${res_models_dir}
mkdir -p ${out_gmm}/fmllr
mkdir -p ${out_gmm}/graph
mkdir -p ${out_dnn}/graph

# Копируем sat_gmm модели
[[ -f ${sat_fmllr_dir}/cmvn_opts ]] && cp -rL ${sat_fmllr_dir}/cmvn_opts ${out_gmm}/fmllr \
    || { echo "Can't locate file: ${sat_fmllr_dir}/cmvn_opts "; exit 1; }

[[ -f ${sat_fmllr_dir}/final.alimdl ]] && cp -rL ${sat_fmllr_dir}/final.alimdl ${out_gmm}/fmllr \
    || { echo "Can't locate file: ${sat_fmllr_dir}/final.alimdl "; exit 1; }

[[ -f ${sat_fmllr_dir}/final.mat ]] && cp -rL ${sat_fmllr_dir}/final.mat ${out_gmm}/fmllr \
    || { echo "Can't locate file: ${sat_fmllr_dir}/final.mat "; exit 1; }

[[ -f ${sat_fmllr_dir}/final.mdl ]] && cp -rL ${sat_fmllr_dir}/final.mdl ${out_gmm}/fmllr \
    || { echo "Can't locate file: ${sat_fmllr_dir}/final.mdl "; exit 1; }

[[ -f ${sat_fmllr_dir}/splice_opts ]] && cp -rL ${sat_fmllr_dir}/splice_opts ${out_gmm}/fmllr \
    || { echo "Can't locate file: ${sat_fmllr_dir}/splice_opts "; exit 1; }

[[ -f ${sat_fmllr_dir}/tree ]] && cp -rL ${sat_fmllr_dir}/tree ${out_gmm}/fmllr \
    || { echo "Can't locate file: ${sat_fmllr_dir}/tree "; exit 1; }

# Копируем fmllr_gmm модель
[[ -f ${gmm_fmllr_dir}/cmvn_opts ]] && cp -rL ${gmm_fmllr_dir}/cmvn_opts ${out_gmm} \
    || { echo "Can't locate file: ${gmm_fmllr_dir}/cmvn_opts "; exit 1; }

[[ -f ${gmm_fmllr_dir}/final.mat ]] && cp -rL ${gmm_fmllr_dir}/final.mat ${out_gmm} \
    || { echo "Can't locate file: ${gmm_fmllr_dir}/final.mat "; exit 1; }

[[ -f ${gmm_fmllr_dir}/final.mdl ]] && cp -rL ${gmm_fmllr_dir}/final.mdl ${out_gmm} \
    || { echo "Can't locate file: ${gmm_fmllr_dir}/final.mdl "; exit 1; }

[[ -f ${gmm_fmllr_dir}/splice_opts ]] && cp -rL ${gmm_fmllr_dir}/splice_opts ${out_gmm} \
    || { echo "Can't locate file: ${gmm_fmllr_dir}/splice_opts "; exit 1; }

# Формируем HCLG
echo "Forming HCLG"
if [[ "$use_list" = true ]]; then
    ./HCLG-prepare.sh --exp_config ${exp_config} --data_train_list ${txt_corpus} --acoustic-model ${gmm_fmllr_dir} --do_train False || exit 1
else
    ./HCLG-prepare.sh --exp_config ${exp_config} --text-corpus ${txt_corpus} --acoustic-model ${gmm_fmllr_dir} --do_train False || exit 1
fi

[[ -d ${hclg_dir}/graph ]] && cp -rL ${hclg_dir}/graph ${out_gmm} \
    || { echo "Something wrong with graph dir: ${hclg_dir}/graph "; exit 1; }
rm -rf ${hclg_dir}/*

#echo "Herrr!!!" && exit 1

echo "DNN"
# Теперь занимаемся нейронкой
[[ -f ${dnn_dir}/ali_train_pdf.counts ]] && cp -rL ${dnn_dir}/ali_train_pdf.counts ${out_dnn} \
    || { echo "Can't locate file: ${dnn_dir}/ali_train_pdf.counts "; exit 1; }

[[ -f ${dnn_dir}/final.feature_transform ]] && cp -rL ${dnn_dir}/final.feature_transform ${out_dnn} \
    || { echo "Can't locate file: ${dnn_dir}/final.feature_transform "; exit 1; }

[[ -f ${dnn_dir}/final.mdl ]] && cp -rL ${dnn_dir}/final.mdl ${out_dnn} \
    || { echo "Can't locate file: ${dnn_dir}/final.mdl "; exit 1; }

[[ -f ${dnn_dir}/final.nnet ]] && cp -rL ${dnn_dir}/final.nnet ${out_dnn} \
    || { echo "Can't locate file: ${dnn_dir}/final.nnet "; exit 1; }

# Формируем HCLG
echo "Forming HCLG"
if [[ "$use_list" = true ]]; then

    ./HCLG-prepare.sh --data_train_list ${txt_corpus} --acoustic-model ${gmm_fmllr_dir} \
                      --exp-config ${exp_config} --do_train False || exit 1
else
    ./HCLG-prepare.sh --text-corpus ${txt_corpus} --acoustic-model ${gmm_fmllr_dir} \
                      --exp-config ${exp_config} --do_train False|| exit 1
fi


[[ -d ${hclg_dir}/graph ]] && cp -rL ${hclg_dir}/graph ${out_dnn} \
    || { echo "Something wrong with graph dir: ${hclg_dir}/graph "; exit 1; }
rm -rf ${hclg_dir}/*

echo "Preparing fmllr models complete!!!"
echo "===================================================="
echo
echo "Calculating md5hash of dst dir"
echo

cat ${exp_config} | sed 's/\=/\: /' > ${res_models_dir}/model_settings_info.txt
cd ${res_models_dir}
find . -xtype f -print0 | xargs -0 md5sum | sort | md5sum > ${res_models_dir}/model_md5.txt

