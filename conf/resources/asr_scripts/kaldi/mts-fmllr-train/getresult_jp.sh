#!/usr/bin/env bash

conf=$1

[ -z $conf ] && { echo "Please provide config file as input parameter."; exit 1; }

EXP_ROOT=

echo "conf = $conf"

source $conf

echo "EXP_ROOT = $EXP_ROOT"

res_dir=$EXP_ROOT/models
#res_dir=res_dir=/home/pyyurkov/Kaldi/Kaldi-VoxForge-ru-work/exp_azure/models


# Getting results [see RESULTS file]
for x in $res_dir/*/decode*; do
    [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh
done
