#!/usr/bin/env bash

# Prepare clean acoustic models.
# This script copy only files needed for HCLG creation and further recognition.

# Experiment dir setup
EXP_ROOT=/cvoaf/asr_scripts/kaldi/mts-train/results/

# Result model dir name. Will be created in exp_root dir.
result_am_dir_name=acoustic_models

#exp_config=


# The user of this script could change some of the above parameters. Example:
# /bin/bash run.sh --pos-dep-phones false
source utils/parse_options.sh || exit 1
#
#[[ $# -ge 1 ]] && { echo "Unexpected arguments"; exit 1; }
#
#[ -z $exp_config ] && { echo "Please provide --exp-config option to set up experiment configuration."; exit 1;}

source $exp_config


echo "============= Current training settings ============="
echo "EXP_ROOT:  $EXP_ROOT"
echo
echo "Result acoustic model dir name. Will be created in exp_root dir."
echo "Result model dir: $result_am_dir_name"
echo
echo "===================================================="

[ -z $EXP_ROOT ] && { echo "You need to set \"EXP_ROOT\" variable in exp. config file to point to the directory with experiment results"; exit 1;}
[ -z $result_am_dir_name ] && { echo "Please set result_model_dir_name param to provide output dir name."; exit 1;}

exp=$EXP_ROOT
src_models_dir=$exp/models
dst_models_dir=$exp/$result_am_dir_name

src_sat_fmllr_dir=$src_models_dir/tri3b_ali
src_gmm_fmllr_dir=$src_models_dir/tri3b_mmi
src_dnn_dir=$src_models_dir/dnn5b_pretrain-dbn_dnn_smbr_i1lats

dst_sat_fmllr_dir=$dst_models_dir/tri3b_ali
dst_gmm_fmllr_dir=$dst_models_dir/tri3b_mmi
dst_dnn_dir=$dst_models_dir/dnn5b_pretrain-dbn_dnn_smbr_i1lats


[ ! -d $src_models_dir ] && { echo "Can't locate models dir: $src_models_dir"; exit 1; }

[ -d $dst_models_dir ] && rm -rf $dst_models_dir
mkdir -p $dst_models_dir
mkdir -p $dst_sat_fmllr_dir
mkdir -p $dst_gmm_fmllr_dir
mkdir -p $dst_dnn_dir


# Сначала поработаем с моделью SAT
[ -f $src_sat_fmllr_dir/cmvn_opts ] && cp -rL $src_sat_fmllr_dir/cmvn_opts $dst_sat_fmllr_dir \
    || { echo "Can't locate file: $src_sat_fmllr_dir/cmvn_opts "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_sat_fmllr_dir/final.alimdl ] && cp -rL $src_sat_fmllr_dir/final.alimdl $dst_sat_fmllr_dir \
    || { echo "Can't locate file: $src_sat_fmllr_dir/final.alimdl "; rm -rf $dst_models_dir; exit 1; }


[ -f $src_sat_fmllr_dir/final.mat ] && cp -rL $src_sat_fmllr_dir/final.mat $dst_sat_fmllr_dir \
    || { echo "Can't locate file: $src_sat_fmllr_dir/final.mat "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_sat_fmllr_dir/final.mdl ] && cp -rL $src_sat_fmllr_dir/final.mdl $dst_sat_fmllr_dir \
    || { echo "Can't locate file: $src_sat_fmllr_dir/final.mdl "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_sat_fmllr_dir/splice_opts ] && cp -rL $src_sat_fmllr_dir/splice_opts $dst_sat_fmllr_dir \
    || { echo "Can't locate file: $src_sat_fmllr_dir/splice_opts "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_sat_fmllr_dir/tree ] && cp -rL $src_sat_fmllr_dir/tree $dst_sat_fmllr_dir \
    || { echo "Can't locate file: $src_sat_fmllr_dir/tree "; rm -rf $dst_models_dir; exit 1; }

# Копируем fmllr_gmm модель
[ -f $src_gmm_fmllr_dir/cmvn_opts ] && cp -rL $src_gmm_fmllr_dir/cmvn_opts $dst_gmm_fmllr_dir \
    || { echo "Can't locate file: $src_gmm_fmllr_dir/cmvn_opts "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_gmm_fmllr_dir/final.mat ] && cp -rL $src_gmm_fmllr_dir/final.mat $dst_gmm_fmllr_dir \
    || { echo "Can't locate file: $src_gmm_fmllr_dir/final.mat "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_gmm_fmllr_dir/final.mdl ] && cp -rL $src_gmm_fmllr_dir/final.mdl $dst_gmm_fmllr_dir \
    || { echo "Can't locate file: $src_gmm_fmllr_dir/final.mdl "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_gmm_fmllr_dir/splice_opts ] && cp -rL $src_gmm_fmllr_dir/splice_opts $dst_gmm_fmllr_dir \
    || { echo "Can't locate file: $src_gmm_fmllr_dir/splice_opts "; rm -rf $dst_models_dir; exit 1; }

# Теперь занимаемся нейронкой
[ -f $src_dnn_dir/ali_train_pdf.counts ] && cp -rL $src_dnn_dir/ali_train_pdf.counts $dst_dnn_dir \
    || { echo "Can't locate file: $src_dnn_dir/ali_train_pdf.counts "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_dnn_dir/final.feature_transform ] && cp -rL $src_dnn_dir/final.feature_transform $dst_dnn_dir \
    || { echo "Can't locate file: $src_dnn_dir/final.feature_transform "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_dnn_dir/final.mdl ] && cp -rL $src_dnn_dir/final.mdl $dst_dnn_dir \
    || { echo "Can't locate file: $src_dnn_dir/final.mdl "; rm -rf $dst_models_dir; exit 1; }

[ -f $src_dnn_dir/final.nnet ] && cp -rL $src_dnn_dir/final.nnet $dst_dnn_dir \
    || { echo "Can't locate file: $src_dnn_dir/final.nnet "; rm -rf $dst_models_dir; exit 1; }


echo "Preparing acoustic fmllr models complete!!!"
echo
echo "===================================================="
echo
echo "Calculating md5hash of dst dir"
echo

curdir=`pwd`
cd $dst_models_dir
find . -xtype f -print0 | xargs -0 md5sum | sort | md5sum > $dst_models_dir/model_md5.txt

echo "===================================================="
echo
echo "Making tar of dst dir"
echo

tar_name=$exp/$result_am_dir_name.tar
tar -cf $tar_name *
mv $tar_name $dst_models_dir

#echo "Herrr!!!" && exit 1
