#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

. ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
. ./cmd.sh || exit 1

# The number of parallel jobs to be started for some parts of the recipe
# Make sure you have enough resources(CPUs and RAM) to accomodate this number of jobs
njobs=4

nnet_use_gpu="yes" # yes|no|optionaly

# The user of this script could change some of the above parameters. Example:
# /bin/bash run.sh --pos-dep-phones false
. utils/parse_options.sh || exit 1

[[ $# -ge 1 ]] && { echo "Unexpected arguments"; exit 1; }

start_time=$(date +%s)

export USER=kaldi
beam=50
retry_beam=200
local/nnet/run_dnn.sh --nnet-use-gpu $nnet_use_gpu --stage 0 \
                      --beam $beam --retry_beam $retry_beam


end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py $diff_time
echo "//////";
