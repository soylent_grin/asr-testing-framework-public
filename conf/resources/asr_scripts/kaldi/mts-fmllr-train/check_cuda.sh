#!/usr/bin/env bash

# The second part of this script comes mostly from egs/rm/s5/run.sh
# with some parameters changed

. ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
. ./cmd.sh || exit 1


#if cuda-gpu-available; then
if cuda-compiled; then
    echo "Cuda compiled!!!"
else
    echo "Cuda herr exist!!!"
fi

