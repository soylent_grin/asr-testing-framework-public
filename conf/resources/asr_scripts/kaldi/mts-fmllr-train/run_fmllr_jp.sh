#!/bin/bash

# Copyright 2012 Vassil Panayotov
# Apache 2.0

# Copyright 2016 Ubskiy Dmitriy, STC

# NOTE: You will want to download the data set first, before executing this script.
#       This can be done for example by:
#       1. Setting the DATA_ROOT variable to point to a directory with enough free
#          space (at least 20-25GB currently (Feb 2014))
#       2. Running "getdata.sh"

# The second part of this script comes mostly from egs/rm/s5/run.sh
# with some parameters changed
#
# Code refactoring by Pavel Yurkov aka JurPasha
# We fix train and test list for experiment reproduction

pushd /cvoaf/asr_scripts/kaldi/mts-fmllr-train || exit 1

source ./path.sh || exit 1

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

DATA_ROOT=/cvoaf/temp/mts-fmllr-train/dataset
EXP_ROOT=/cvoaf/temp/mts-fmllr-train/results
train_list=/cvoaf/temp/mts-fmllr-train/train_test_lists/train-list.txt
test_list=/cvoaf/temp/mts-fmllr-train/train_test_lists/test-list.txt

njobs=
njobs_test=
nnet_use_gpu=
nnet_use_gpu_decode=
mfcc_conf=
lm_order=
use_ds_info=
pos_dep_phones=
train_txt_ext=
test_txt_ext=
do_filter_train=
do_filter_test=
do_garbage=
beam=
retry_beam=
mono_start_beam=
mono_next_beam=
mono_subset=
scoring_opts=
CUR_STEP=

exp_config=/cvoaf/temp/mts-fmllr-train/conf/exp.conf

if [[ -L utils && -L steps ]]; then
    rm -rf utils steps
fi

if [[ ! -L utils && ! -L steps ]]; then
   local/make_links.sh
fi

if [[ ! -d results ]]; then
    mkdir results
fi

source ${exp_config}

echo "============= Current training settings ============="
echo "CURRENT STEP: $CUR_STEP"
echo "DATA_ROOT: $DATA_ROOT"
echo "EXP_ROOT:  $EXP_ROOT"
echo
echo "Train list: $train_list"
echo "Test list: $test_list"
echo
echo "njobs = $njobs"
echo "njobs_test = $njobs_test"
echo "nnet_use_gpu = $nnet_use_gpu. Possible values: yes|no|wait|optionaly."
echo "nnet_use_gpu_decode = $nnet_use_gpu_decode"
echo "MFCC config = $mfcc_conf"
echo
echo "Test-time language model order"
echo "lm_order = $lm_order"
echo "Use dictor specific info like in VoxForge"
echo "use_ds_info = {$use_ds_info}"
echo
echo "Word position dependent phones?"
echo "pos_dep_phones = {$pos_dep_phones}"
echo "train_txt_ext = $train_txt_ext"
echo "test_txt_ext = $test_txt_ext"
echo
echo "do_filter_train = $do_filter_train"
echo "do_filter_test = $do_filter_test"
echo "do_garbage = $do_garbage"
echo
echo "beam = $beam"
echo "retry_beam = $retry_beam"
echo "mono_start_beam = $mono_start_beam"
echo "mono_next_beam = $mono_next_beam"
echo "mono_subset = $mono_subset"
echo
echo "scoring_opts = $scoring_opts"
echo "===================================================="

[[ -z ${EXP_ROOT} ]] && { echo "You need to set \"EXP_ROOT\" variable in exp. config file to point to the directory with experiment results"; exit 1;}
[[ -z ${DATA_ROOT} ]] && { echo "You need to set \"DATA_ROOT\" variable in exp. config file to point to the directory to host wav-data"; exit 1;}
[[ -z ${train_list} ]] || [[ -z ${test_list} ]] && { echo "Please set path for train or test lists."; exit 1;}
[[ -z ${njobs} ]] || [[ -z ${njobs_test} ]] && { echo "One of params njobs or njobs_test are not set."; exit 1;}
[[ -z ${nnet_use_gpu} ]] || [[ -z nnet_use_gpu_decode ]] && { echo "Set nnet_use_gpu or nnet_use_gpu_decode param."; exit 1;}
[[ -z ${beam} ]] || [[ -z ${retry_beam} ]] && { echo "Set beam or retry_beam param."; exit 1;}
[[ -z ${mono_start_beam} ]] || [[ -z ${mono_next_beam} ]] && { echo "Set mono_start_beam or mono_next_beam param."; exit 1;}
[[ -z ${mono_subset} ]] && { echo "Set mono_subset param."; exit 1;}
[[ -z "$scoring_opts" ]] && { echo "Set scoring_opts param."; exit 1;}

[[ -z ${train_txt_ext} ]] || [[ -z ${test_txt_ext} ]] && { echo "Set train_txt_ext or test_txt_ext param."; exit 1;}
[[ -z "$do_filter_test" ]] || [[ -z "$do_filter_train" ]] && { echo "Set do_filter_train or do_filter_test param"; exit 1;}

#echo "HERRR!!!" && exit 0

exp=${EXP_ROOT}
data_txt_dir=${exp}/data_txt
hclg_dir=${exp}/HCLG
log_dir=${exp}/log
models_dir=${exp}/models
conf=/cvoaf/temp/mts-fmllr-train/conf

start_time=$(date +%s)

if [[ ${CUR_STEP} -eq 0 ]]; then
echo "============= Training process started ============="
echo
echo "================================================"
echo "Current step $CUR_STEP: preparing dir structure."
echo "================================================"
echo

[[ -d ${EXP_ROOT} ]] && rm -rf ${EXP_ROOT}
mkdir -p ${EXP_ROOT}
mkdir -p ${data_txt_dir}/train
mkdir -p ${data_txt_dir}/test
mkdir -p ${log_dir}
mkdir -p ${models_dir}

echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 1"
fi

# Preparing train and test lists and language modelling data
if [[ ${CUR_STEP} -eq 1 ]]; then
echo
echo "================================================"
echo "Current step $CUR_STEP: Preparing train, test lists and language modelling data"
echo "================================================"
echo

./HCLG-prepare.sh --data_train_list ${train_list} --show_AM_error_message false --move_to_dir ${data_txt_dir}/train \
                  --exp_config ${exp_config} --do_train True || exit 1

echo "========================================="
echo "!!! Train text processing accomplished !!!"
echo "========================================="

./HCLG-prepare.sh --data_train_list ${test_list} --show_AM_error_message false --move_to_dir ${data_txt_dir}/test \
                  --exp_config ${exp_config} --do_train False || exit 1
echo "========================================="
echo "!!! Test text processing accomplished !!!"
echo "========================================="
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo

#echo "HERRR_0!!!" && exit 1

let "CUR_STEP = $CUR_STEP + 1"
fi

# Now make MFCC features.
# mfccdir should be some place with a largish disk where you
# want to store MFCC features.
if [[ ${CUR_STEP} -eq 2 ]]; then
echo
echo "================================================"
echo "Current step is $CUR_STEP: make MFCC features."
echo "================================================"
echo

mfccdir=${exp}/mfcc
for x in train test; do
  steps/make_mfcc.sh --cmd "$train_cmd" --nj ${njobs} --mfcc-config ${conf}/mfcc.conf \
    ${data_txt_dir}/${x}/scp ${log_dir}/make_mfcc/${x} ${mfccdir}/${x} || exit 1

  steps/compute_cmvn_stats.sh ${data_txt_dir}/${x}/scp ${log_dir}/make_mfcc/${x} ${mfccdir}/${x} || exit 1
done
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 1"
fi

# Train monophone models on a subset of the data
if [[ ${CUR_STEP} -eq 3 ]]; then
echo
echo "================================================"
echo "Current step is $CUR_STEP: Train monophone models on a subset of the data."
echo "================================================"
echo

utils/subset_data_dir.sh ${data_txt_dir}/train/scp ${mono_subset} ${data_txt_dir}/train.${mono_subset}  || exit 1
local/steps/train_mono.sh --nj ${njobs} --cmd "$train_cmd" --start_beam ${mono_start_beam} --next_beam ${mono_next_beam} \
                            ${data_txt_dir}/train.${mono_subset} ${data_txt_dir}/train/lang ${models_dir}/mono  || exit 1

# Monophone decoding (can be skipped).
#utils/mkgraph.sh --mono ${data_txt_dir}/test/lang ${models_dir}/mono ${models_dir}/mono/graph || exit 1
# note: local/decode.sh calls the command line once for each
# test, and afterwards averages the WERs into (in this case
# exp/mono/decode/
#steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs_test} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#  ${models_dir}/mono/graph ${data_txt_dir}/test/scp ${models_dir}/mono/decode || exit 1


# Get alignments from monophone system
steps/align_si.sh --nj ${njobs} --cmd "$train_cmd" \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/mono ${models_dir}/mono_ali || exit 1
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 1"
fi

#echo "HERRR!!!" && exit 1

# train tri1 [[first triphone pass]]
if [[ ${CUR_STEP} -eq 4 ]]; then
echo
echo "================================================"
echo "Current step $CUR_STEP: train tri1 [[first triphone pass]] "
echo "================================================"
echo
#beam=200
#retry_beam=500
steps/train_deltas.sh --cmd "$train_cmd" --beam ${beam} --retry_beam ${retry_beam} 2000 11000 \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/mono_ali ${models_dir}/tri1 || exit 1

# decode tri1
utils/mkgraph.sh ${data_txt_dir}/test/lang ${models_dir}/tri1 ${models_dir}/tri1/graph || exit 1
#steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs_test} --cmd "$decode_cmd" --scoring_opts "$scoring_opts" \
#  ${models_dir}/tri1/graph ${data_txt_dir}/test/scp ${models_dir}/tri1/decode || exit 1

#draw-tree data/lang/phones.txt exp/tri1/tree | dot -Tps -Gsize=8,10.5 | ps2pdf - tree.pdf

# align tri1
steps/align_si.sh --nj ${njobs} --cmd "$train_cmd" \
  --use-graphs true ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri1 ${models_dir}/tri1_ali || exit 1
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 2"
fi


# train tri2a [[delta+delta-deltas]]
#if [[ $CUR_STEP -eq 5 ]]; then
#echo "Current step $CUR_STEP: train tri2a [[delta+delta-deltas]] "

#steps/train_deltas.sh --cmd "$train_cmd" 2000 11000 \
#  $data_txt_dir/train/scp $data_txt_dir/train/lang $models_dir/tri1_ali $models_dir/tri2a || exit 1

# decode tri2a
#utils/mkgraph.sh $data_txt_dir/test/lang $models_dir/tri2a $models_dir/tri2a/graph
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#  $models_dir/tri2a/graph $data_txt_dir/test/scp $models_dir/tri2a/decode || exit 1

#let "CUR_STEP = $CUR_STEP + 1"
#fi


# train and decode tri2b [[LDA+MLLT]]
if [[ ${CUR_STEP} -eq 6 ]]; then
echo
echo "================================================"
echo "Current step $CUR_STEP: train and decode tri2b [[LDA+MLLT]]"
echo "================================================"
echo
#beam=200
#retry_beam=500
steps/train_lda_mllt.sh --cmd "$train_cmd" --beam ${beam} --retry_beam ${retry_beam} 2000 11000 \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri1_ali ${models_dir}/tri2b || exit 1
utils/mkgraph.sh ${data_txt_dir}/test/lang ${models_dir}/tri2b ${models_dir}/tri2b/graph
#steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs_test} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#  ${models_dir}/tri2b/graph ${data_txt_dir}/test/scp ${models_dir}/tri2b/decode || exit 1

# Align all data with LDA+MLLT system (tri2b)
steps/align_si.sh --nj ${njobs} --cmd "$train_cmd" --use-graphs true \
   ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri2b ${models_dir}/tri2b_ali || exit 1
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 4"
fi


# Do MMI on top of LDA+MLLT (step 1, it 3 and 4)
#if [[ $CUR_STEP -eq 7 ]]; then
#echo "Current step $CUR_STEP: Do MMI on top of LDA+MLLT (step 1, it 3 and 4)"

#steps/make_denlats.sh --nj $njobs --cmd "$train_cmd" $data_txt_dir/train/scp $data_txt_dir/train/lang \
#        $models_dir/tri2b $models_dir/tri2b_denlats || exit 1
#steps/train_mmi.sh $data_txt_dir/train/scp $data_txt_dir/train/lang \
#        $models_dir/tri2b_ali $models_dir/tri2b_denlats $models_dir/tri2b_mmi || exit 1
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --iter 4 --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#   $models_dir/tri2b/graph $data_txt_dir/test/scp $models_dir/tri2b_mmi/decode_it4 || exit 1
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --iter 3 --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#   $models_dir/tri2b/graph $data_txt_dir/test/scp $models_dir/tri2b_mmi/decode_it3 || exit 1

#let "CUR_STEP = $CUR_STEP + 1"
#fi


# Do the same with boosting (step 2, it 3 and 4)
#if [[ $CUR_STEP -eq 8 ]]; then
#echo "Current step $CUR_STEP: Do the same with boosting (step 2, it 3 and 4)"

#steps/train_mmi.sh --boost 0.05 $data_txt_dir/train/scp $data_txt_dir/train/lang \
#   $models_dir/tri2b_ali $models_dir/tri2b_denlats $models_dir/tri2b_mmi_b0.05 || exit 1
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --iter 4 --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#   $models_dir/tri2b/graph $data_txt_dir/test/scp $models_dir/tri2b_mmi_b0.05/decode_it4 || exit 1
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --iter 3 --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#   $models_dir/tri2b/graph $data_txt_dir/test/scp $models_dir/tri2b_mmi_b0.05/decode_it3 || exit 1

#let "CUR_STEP = $CUR_STEP + 1"
#fi


# Do MPE (step 3, it 3 and 4)
#if [[ $CUR_STEP -eq 9 ]]; then
#echo "Current step $CUR_STEP: Do MPE (step 3, it 3 and 4)"

#steps/train_mpe.sh $data_txt_dir/train/scp $data_txt_dir/train/lang $models_dir/tri2b_ali $models_dir/tri2b_denlats $models_dir/tri2b_mpe || exit 1
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --iter 4 --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#   $models_dir/tri2b/graph $data_txt_dir/test/scp $models_dir/tri2b_mpe/decode_it4 || exit 1
##steps/decode.sh --num-threads $(nproc) --config $conf/decode.config --iter 3 --nj $njobs --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
#   $models_dir/tri2b/graph $data_txt_dir/test/scp $models_dir/tri2b_mpe/decode_it3 || exit 1

#let "CUR_STEP = $CUR_STEP + 1"
#fi


# Do LDA+MLLT+SAT, and decode
if [[ ${CUR_STEP} -eq 10 ]]; then
echo
echo "================================================"
echo "Current step $CUR_STEP: Do LDA+MLLT+SAT, and decode"
echo "================================================"
echo

#beam=200
#retry_beam=500
steps/train_sat.sh --beam ${beam} --retry_beam ${retry_beam} 2000 11000 \
                    ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri2b_ali ${models_dir}/tri3b || exit 1
utils/mkgraph.sh ${data_txt_dir}/test/lang ${models_dir}/tri3b ${models_dir}/tri3b/graph || exit 1
steps/decode_fmllr.sh --config ${conf}/decode.config --nj ${njobs_test} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
  ${models_dir}/tri3b/graph ${data_txt_dir}/test/scp ${models_dir}/tri3b/decode || exit 1

# Align all data with LDA+MLLT+SAT system (tri3b)
steps/align_fmllr.sh --nj ${njobs} --cmd "$train_cmd" --use-graphs true \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri3b ${models_dir}/tri3b_ali || exit 1
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 1"
fi


# MMI on top of tri3b (i.e. LDA+MLLT+SAT+MMI)
if [[ ${CUR_STEP} -eq 11 ]]; then
echo
echo "================================================"
echo "Current step $CUR_STEP: MMI on top of tri3b (i.e. LDA+MLLT+SAT+MMI)"
echo "================================================"
echo

steps/make_denlats.sh --config ${conf}/decode.config \
   --nj ${njobs} --cmd "$train_cmd" --transform-dir ${models_dir}/tri3b_ali \
  ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri3b ${models_dir}/tri3b_denlats || exit 1
steps/train_mmi.sh ${data_txt_dir}/train/scp ${data_txt_dir}/train/lang ${models_dir}/tri3b_ali ${models_dir}/tri3b_denlats ${models_dir}/tri3b_mmi || exit 1

steps/decode_fmllr.sh --config ${conf}/decode.config --nj ${njobs_test} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
  --alignment-model ${models_dir}/tri3b/final.alimdl --adapt-model ${models_dir}/tri3b/final.mdl \
   ${models_dir}/tri3b/graph ${data_txt_dir}/test/scp ${models_dir}/tri3b_mmi/decode || exit 1

# Do a decoding that uses the $models_dir/tri3b/decode directory to get transforms from
steps/decode.sh --num-threads $(nproc) --config ${conf}/decode.config --nj ${njobs_test} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
  --transform-dir ${models_dir}/tri3b/decode  ${models_dir}/tri3b/graph ${data_txt_dir}/test/scp ${models_dir}/tri3b_mmi/decode2 || exit 1
echo
echo "================================================"
echo "Step $CUR_STEP finished."
echo "================================================"
echo
let "CUR_STEP = $CUR_STEP + 1"
fi

if [[ ${CUR_STEP} -eq 12 ]]; then
echo
echo "================================================"
echo "DNN training started."
echo "================================================"
echo

export USER=kaldi
#beam=200
#retry_beam=500
local/nnet/run_dnn.sh --nnet_use_gpu ${nnet_use_gpu} \
                      --nnet_use_gpu_decode ${nnet_use_gpu_decode} \
                      --stage 0 \
                      --beam ${beam} --retry_beam ${retry_beam} \
                      --EXP_ROOT ${exp} \
                      --njobs ${njobs} \

echo
echo "================================================"
echo "DNN training finished."
echo "================================================"
echo
end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py ${diff_time}
echo "//////"
let "CUR_STEP = $CUR_STEP + 1"
fi

if [[ ${CUR_STEP} -eq 13 ]]; then
    ./prepare-fmllr-models-set.sh
fi

#rm -rf ./results/*
# echo "HERRR_0!!!" && exit 0
