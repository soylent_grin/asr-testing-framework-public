#!/usr/bin/env bash

# This is launch of last string from run.sh and making env variables

source $(realpath "$PWD/./path.sh") || exit 1


input_list=
dir_depth=
decode_dir=
output_dir=


source parse_options.sh || exit 1;

# check that all parameters are provided
for x in ${input_list} ${dir_depth} ${decode_dir} ${output_dir}; do
  if [[ -z ${x} ]]; then
    echo "Missing one of compulsory parameters" && exit 1;
  fi
done

[[ -d ${output_dir} ]] && rm -rf ${output_dir}
mkdir -p ${output_dir}

[[ -f ${decode_dir}/decode.result ]] && rm -f ${decode_dir}/decode.result

./local/create-decode-result.py --input-dir ${decode_dir} || exit 1

./local/decode-extractor.py --input-list ${input_list} \
                            --dir-depth ${dir_depth} \
                            --decode-file ${decode_dir}/decode.result \
                            --output-dir ${output_dir} || exit 1
