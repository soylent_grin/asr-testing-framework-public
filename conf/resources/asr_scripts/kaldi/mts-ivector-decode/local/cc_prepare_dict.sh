#!/bin/bash

# Copyright 2012 Vassil Panayotov
# Apache 2.0

# Copyright 2016 Ubskiy Dmitriy, STC

. path.sh || exit 1

dict_dir=
arpa_dir=
do_garbage=

echo "=== Preparing the dictionary ..."

source utils/parse_options.sh

cur_dir=$PWD/local
echo "Cur dir is: "
echo $cur_dir

#if [ ! -d $locdict/zero_ru_cont_8k_v3 ]; then
#  echo "--- Downloading CMU dictionary ..."
#  mkdir -p $locdict
#  wget -P $locdict http://downloads.sourceforge.net/project/cmusphinx/Acoustic%20and%20Language%20Models/Russian/zero_ru_cont_8k_v3.tar.gz
#  tar xzf $locdict/zero_ru_cont_8k_v3.tar.gz -C $locdict
#fi

[ -z $dict_dir ] && echo "Dictionary dir is not initialised!!! Use --dict_dir option." && exit 1
[ -z $arpa_dir ] && echo "Arpa dir is not initialised!!! Use --arpa_dir option." && exit 1
[ -z $do_garbage ] && echo "do_garbage param is not initialised. Use --do-garbage option and set it to True or False"

# cp $locdict/zero_ru_cont_8k_v3/ru.lm $locdata/lm.arpa
cp local/data/cmudict-plain.txt $dict_dir/cmudict-plain.txt

echo "--- Searching for OOV words ..."
awk 'NR==FNR{words[$1]; next;} !($1 in words)' \
  $dict_dir/cmudict-plain.txt $arpa_dir/vocab-full.txt |\
  #egrep -v '<.?s>|-pau-|<unk>' > $dict_dir/vocab-oov.txt
  egrep -v '<.?s>|-pau-|<unk>|!SIL|<SPN>|<NSN>|<SPN_CLAC>|<SPN_CLAC>|<SPN_LAU>|<SPN_COUGH>|<NSN_SOUND>|<NSN_NOISE>|<NSN_MUSIC>|<SPN_BREATH>|<NSN_KEYBOARD>|<UNK>' > $dict_dir/vocab-oov.txt

awk 'NR==FNR{words[$1]; next;} ($1 in words)' \
  $arpa_dir/vocab-full.txt $dict_dir/cmudict-plain.txt |\
  #egrep -v '<.?s>|-pau-|<unk>' > $dict_dir/lexicon-iv.txt
  egrep -v '<.?s>|-pau-|<unk>|!SIL|<SPN>|<NSN>|<SPN_CLAC>|<SPN_CLAC>|<SPN_LAU>|<SPN_COUGH>|<NSN_SOUND>|<NSN_NOISE>|<NSN_MUSIC>|<SPN_BREATH>|<NSN_KEYBOARD>|<UNK>' > $dict_dir/lexicon-iv.txt

# Show number of words in files
wc -l $dict_dir/vocab-oov.txt
wc -l $dict_dir/lexicon-iv.txt

if [[ "$(uname)" == "Darwin" ]]; then
  command -v greadlink >/dev/null 2>&1 || \
    { echo "Mac OS X detected and 'greadlink' not found - please install using macports or homebrew"; exit 1; }
  alias readlink=greadlink
fi

sequitur=$KALDI_ROOT/tools/sequitur
#export PATH=$PATH:$sequitur/bin
#export PYTHONPATH=$PYTHONPATH:`readlink -f $sequitur/lib/python*/site-packages`

if ! g2p=`which g2p.py` ; then
  echo "The Sequitur was not found !"
  echo "Go to $KALDI_ROOT/tools and execute extras/install_sequitur.sh"
  exit 1
fi

echo "--- Preparing pronunciations for OOV words ..."
g2p.py --encoding=utf-8 --model conf/g2p_model --apply $dict_dir/vocab-oov.txt > $dict_dir/lexicon-oov.txt

cat $dict_dir/lexicon-oov.txt $dict_dir/lexicon-iv.txt |\
  sort > $arpa_dir/lexicon.txt

echo "--- Prepare phone lists ..."
if [[ "$do_garbage" == "True" || "$do_garbage" == "true" ]]; then
    echo "We use garbage model!!!"
    cat $cur_dir/data/garbage_phones.txt > $arpa_dir/silence_phones.txt
else
    echo "We don't use garbage model!!!"
    echo SIL > $arpa_dir/silence_phones.txt
fi

echo SIL > $arpa_dir/optional_silence.txt
grep -v -w sil $arpa_dir/lexicon.txt | \
  awk '{for(n=2;n<=NF;n++) { p[$n]=1; }} END{for(x in p) {print x}}' |\
  sort > $arpa_dir/nonsilence_phones.txt

echo "--- Adding SIL to the lexicon ..."
if [[ "$do_garbage" == "True" || "$do_garbage" == "true" ]]; then
    echo "We use garbage model!!!"
    cat $cur_dir/data/garbage_lexicon.txt >> $arpa_dir/lexicon.txt
else
    echo "We don't use garbage model!!!"
    echo -e "!SIL\tSIL" >> $arpa_dir/lexicon.txt
fi

# Some downstream scripts expect this file exists, even if empty
touch $arpa_dir/extra_questions.txt

echo "*** Dictionary preparation finished!"
