#!/usr/bin/env bash

pushd /cvoaf/asr_scripts/kaldi/mts-ivector-decode || exit 1
# This is launch of last string from run.sh and making env variables

source ./path.sh || exit 1

# append "src/ivectorbin" to PATH so ivector C-functions can work
PATH=${KALDI_ROOT}/src/ivectorbin:${PATH}

# If you have cluster of machines running GridEngine you may want to
# change the train and decode commands in the file below
source ./cmd.sh || exit 1

conf=/cvoaf/temp/mts-ivector-decode/conf
outdir=/cvoaf/temp/mts-ivector-decode/results
DATA_ROOT=/cvoaf/temp/mts-ivector-decode/dataset
test_list=/cvoaf/temp/mts-ivector-decode/dataset/test-list.txt

EXP_ROOT=
test_txt_ext=
CUR_STEP=
do_filter_test=
njobs=
ivector_period=
nnet3_use_gpu_decode=
scoring_opts=
dir_depth=
# Source experiment config
source /cvoaf/temp/mts-ivector-decode/conf/exp.conf

exp=${EXP_ROOT}
wav_root=${DATA_ROOT}
models_dir=${exp}/models
graph_dir=${exp}/HCLG/graph
dnn_model=nnet3_dnniv
list=${test_list} # list of audiofiles

if [[ -L utils && -L steps ]]; then
    rm -rf utils steps
fi

if [[ ! -L utils && ! -L steps ]]; then
   local/make_links.sh
fi

source ./utils/parse_options.sh

txt_ext=${test_txt_ext}
dnn_model_dir=${exp}/${dnn_model}
ivec_extr=${dnn_model_dir}/extractor

start_time=$(date +%s)

if [[ -z  ${list} ]]; then
  echo "Please provide list of audiofiles" || exit 1;
fi


echo "///////"
echo "// Starting from step ${CUR_STEP}"
echo "///////"

if [[ ${CUR_STEP} -le 0 ]]; then
    [[ -d ${outdir} ]] && rm -rf ${outdir}

    mkdir -p ${outdir}/ivector_feats
    mkdir -p ${outdir}/dnn_txtdata
fi

if [[ ${CUR_STEP} -le 1 ]]; then
    echo "///////"
    echo "// Data preaparation"
    echo "///////"

    # Set first TRUE param to False if you don't want to create TEXT file
    # Set second TRUE param to False if you don't want to use dictor specific information
    local/data-preparation.py ${list} ${wav_root} ${outdir}/dnn_txtdata True False ${txt_ext} ${do_filter_test} || exit 1;
    [[ -f ${outdir}/dnn_txtdata/spk2utt ]] && sort -o ${outdir}/dnn_txtdata/spk2utt ${outdir}/dnn_txtdata/spk2utt
    [[ -f ${outdir}/dnn_txtdata/utt2spk ]] && sort -o ${outdir}/dnn_txtdata/utt2spk ${outdir}/dnn_txtdata/utt2spk
    [[ -f ${outdir}/dnn_txtdata/wav.scp ]] && sort -o ${outdir}/dnn_txtdata/wav.scp ${outdir}/dnn_txtdata/wav.scp
    [[ -f ${outdir}/dnn_txtdata/text ]] && sort -o ${outdir}/dnn_txtdata/text ${outdir}/dnn_txtdata/text

    echo "///////"
    echo "// Computing mfcc and cmvn "
    echo "//////"


    steps/make_mfcc.sh --cmd "$train_cmd" --nj ${njobs} \
                       --mfcc-config ${conf}/mfcc.conf \
                       ${outdir}/dnn_txtdata || exit 1;
    steps/compute_cmvn_stats.sh ${outdir}/dnn_txtdata || exit 1;

    echo "///////"
    echo "// Computing ivectors "
    echo "//////"

    # "Usage: extract_ivectors.sh [options] <data> <lang> <extractor-dir> [<alignment-dir>|<decode-dir>|<weights-archive>] <ivector-dir>"
    # can be used steps/online/nnet2/extract_ivectors_online.sh for online purposes

    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" \
                                --nj "${njobs}" \
                                --ivector-period ${ivector_period} \
                                ${outdir}/dnn_txtdata ${ivec_extr} \
                                ${outdir}/ivector_feats || exit 1;

fi

if [[ ${CUR_STEP} -le 2 ]]; then
    echo "///////"
    echo "// Perform decoding (see log for results)"
    echo "//////"

    # This is need because nnet3/decode.sh looks for model in 1 level upper than outputdir
    cp ${dnn_model_dir}/final.mdl ${outdir}
    cp ${dnn_model_dir}/cmvn_opts ${outdir}

    steps/nnet3/decode.sh --config ${conf}/decode.config --nj ${njobs} --cmd "$decode_cmd" --scoring-opts "$scoring_opts" \
                          --online-ivector-dir ${outdir}/ivector_feats \
                          --use-gpu ${nnet3_use_gpu_decode} \
                          --num-threads $(nproc) \
                          ${graph_dir}  ${outdir}/dnn_txtdata ${outdir}/decode_dnniv || exit 1

fi

end_time=$(date +%s)
diff_time=$(($end_time - $start_time))
echo "///////"
local/get_time.py ${diff_time}
echo "//////";

wer_dir=${outdir}/decode_dnniv

echo "Deleting temporary dataset in ./dataset dir"
rm -rf ./dataset

best_wer=$(grep WER ${wer_dir}/wer_* | ./utils/best_wer.sh | sed -n '/.*wer_/s///p')

echo "Best IVector WER in wer_${best_wer}:"
cat ${outdir}/decode_dnniv/wer_${best_wer} | egrep  '(WER)|(SER)'

echo -e "Recognized text: "
int2sym.pl -f 2- ${graph_dir}/words.txt ${outdir}/decode_dnniv/scoring/${best_wer}.tra | cut -d " " -f2-

rm -rf /cvoaf/temp/mts-ivector-decode/*