import pymorphy2 as pm
import sys
import json
import pprint as pp


def read_file(filename):
    with open(filename, mode='r', encoding='utf-8', errors='ignore') as file:
        return [line.replace('\n', '') for line in file.readlines()]


def extract_morpho_features(word: str, morpho_features_names: list, morph: object=pm.MorphAnalyzer()) -> dict:
    morpho_result = morph.parse(word)[0]
    return {"token": word, "lemma": morpho_result.normal_form,
            "features": {morpho_feature_name: getattr(morpho_result.tag, morpho_feature_name)
                         for morpho_feature_name in morpho_features_names if getattr(morpho_result.tag, morpho_feature_name)}}


def get_morpho_features(words, morpho_features_names, morph=pm.MorphAnalyzer(), alignment_file_path=None):
    result = []
    for word in words:
        if word not in result:
            result.append(extract_morpho_features(word=word, morpho_features_names=morpho_features_names, morph=morph))
    #pp.pprint(result)
    return result


if __name__ == "__main__":
    if (len(sys.argv) < 3):
        raise ValueError("Not enough arguments")

    morph = pm.MorphAnalyzer()
    words = read_file(sys.argv[1])
    morpho_features_names = sys.argv[2].split(' ')
    result = get_morpho_features(words, morpho_features_names, morph)
    # pp.pprint(result)

    print(json.dumps(result))
