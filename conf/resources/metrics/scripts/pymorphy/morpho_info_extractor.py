import pymorphy2 as pm
from nltk.tokenize import RegexpTokenizer
import itertools
import sys
import json

#
# Wer calculation
#


def make_token_entry(operation, ref, hyp, ref_index, hyp_index):
    return {'operation': operation,
            'ref': ref[ref_index] if not operation == 'INS' else '*' * len(hyp[hyp_index]),
            'hyp': hyp[hyp_index] if not operation == 'DEL' else '*' * len(ref[ref_index])}


def wer(ref, hyp, tokenizer=RegexpTokenizer(r'\w+')):
    ref = ref.lower()
    hyp = hyp.lower()

    r = [token[0] if isinstance(token, tuple) else token for token in tokenizer.tokenize(ref)]
    h = tokenizer.tokenize(hyp)

    # costs will holds the costs, like in the Levenshtein distance algorithm
    costs = [[0 for inner in range(len(h) + 1)] for outer in range(len(r) + 1)]
    # backtrace will hold the operations we've done.
    # so we could later backtrace, like the WER algorithm requires us to.
    backtrace = [[0 for inner in range(len(h) + 1)] for outer in range(len(r) + 1)]
    SUB_PENALTY = 1
    INS_PENALTY = 1
    DEL_PENALTY = 1
    OP_OK = 0
    OP_SUB = 1
    OP_INS = 2
    OP_DEL = 3

    # First column represents the case where we achieve zero
    # hypothesis words by deleting all reference words.
    for i in range(1, len(r) + 1):
        costs[i][0] = DEL_PENALTY * i
        backtrace[i][0] = OP_DEL

    # First row represents the case where we achieve the hypothesis
    # by inserting all hypothesis words into a zero-length reference.
    for j in range(1, len(h) + 1):
        costs[0][j] = INS_PENALTY * j
        backtrace[0][j] = OP_INS

    # computation
    for i in range(1, len(r) + 1):
        for j in range(1, len(h) + 1):
            if r[i - 1] == h[j - 1]:
                costs[i][j] = costs[i - 1][j - 1]
                backtrace[i][j] = OP_OK
            else:
                substitutionCost = costs[i - 1][j - 1] + SUB_PENALTY  # penalty is always 1
                insertionCost = costs[i][j - 1] + INS_PENALTY  # penalty is always 1
                deletionCost = costs[i - 1][j] + DEL_PENALTY  # penalty is always 1

                costs[i][j] = min(substitutionCost, insertionCost, deletionCost)
                if costs[i][j] == substitutionCost:
                    backtrace[i][j] = OP_SUB
                elif costs[i][j] == insertionCost:
                    backtrace[i][j] = OP_INS
                else:
                    backtrace[i][j] = OP_DEL

    # back trace though the best route:
    i = len(r)
    j = len(h)
    numSub = 0
    numDel = 0
    numIns = 0
    numCor = 0

    lines = []
    details = []
    while i > 0 or j > 0:
        if backtrace[i][j] == OP_OK:
            numCor += 1
            i -= 1
            j -= 1
            details.append(make_token_entry('OK', r, h, i, j))
        elif backtrace[i][j] == OP_SUB:
            numSub += 1
            i -= 1
            j -= 1
            details.append(make_token_entry('SUB', r, h, i, j))
        elif backtrace[i][j] == OP_INS:
            numIns += 1
            j -= 1
            details.append(make_token_entry('INS', r, h, i, j))
        elif backtrace[i][j] == OP_DEL:
            numDel += 1
            i -= 1
            details.append(make_token_entry('DEL', r, h, i, j))
    lines = reversed(lines)
    wer_result = round((numSub + numDel + numIns) / (float)(len(r)), 3)
    return {'WER': wer_result, 'Cor': numCor, 'Sub': numSub, 'Ins': numIns, 'Del': numDel,
            'details': list(reversed(details))}

#
# Handling user's input
#


def lemmatize(sentence, morph=pm.MorphAnalyzer(), tokenizer=RegexpTokenizer(r'\w+')):
    parsed_sentence = (morph.parse(word)[0] for word in tokenizer.tokenize(sentence))
    it1, it2, it3 = itertools.tee(parsed_sentence, 3)
    result = {
        'sentence': ' '.join((word.normal_form for word in it1)),
        'tag_properties': [word.tag for word in it2]
    }
    return result


def read_file(filename):
    with open(filename, mode='r', encoding='utf-8') as file:
        return [line.replace('\n', ' ') for line in file.readlines()]


def write_file(filename, lines_to_write):
    with open(filename, mode='a', encoding='utf-8') as file:
        file.writelines(lines_to_write)


def extract_properties(word: str, properties: list, morph: object=pm.MorphAnalyzer()) -> dict:
    return {property_name: getattr(morph.parse(word)[0].tag, property_name) for property_name in properties}


def evaluate_ex(ref, hyp, properties, morph=pm.MorphAnalyzer(), alignment_file_path=None):
    lemmatized_ref = lemmatize(ref, morph)
    lemmatized_hyp = lemmatize(hyp, morph)

    wer_regular = wer(ref, hyp)['details']
    wer_lemmatized = wer(lemmatized_ref['sentence'], lemmatized_hyp['sentence'])['details']

    result = []
    ref_id = 0
    lines = []
    for i in range(len(wer_regular)):
        new_item = {}
        new_item['refTokenId'] = ref_id
        new_item['operation'] = wer_regular[i]['operation']
        new_item['lemmatizedOperation'] = wer_lemmatized[i]['operation']

        if (i < len(wer_regular) - 1) and (wer_regular[i + 1]['operation'] != 'INS'):
            ref_id += 1

        ref_properties = extract_properties(word=wer_regular[i]['ref'], properties=properties + ['POS'], morph=morph)
        hyp_properties = extract_properties(word=wer_regular[i]['hyp'], properties=properties + ['POS'], morph=morph)

        if alignment_file_path:
            lines.append(f"{wer_regular[i]['ref']}\t{wer_regular[i]['hyp']}\t{wer_regular[i]['operation']}\t{ref_properties}\t{hyp_properties}\n")

        new_item['refProperties'] = {property_name: ref_properties[property_name] for property_name in ref_properties if ref_properties[property_name]}
        new_item['hypProperties'] = {property_name: hyp_properties[property_name] for property_name in hyp_properties if hyp_properties[property_name]}
        result.append(new_item)

    if alignment_file_path:
        write_file(alignment_file_path, lines + ['\n\n'])

    return result


def evaluate_mex(refs, hyps, properties, morph=pm.MorphAnalyzer()):
    return [evaluate_ex(pair[0], pair[1], properties, morph) for pair in zip([ref for ref in refs], [hyp for hyp in hyps])]


if __name__ == "__main__":
    if (len(sys.argv) < 4):
        raise ValueError("Not enough arguments")

    morph = pm.MorphAnalyzer()
    refs = read_file(sys.argv[1])
    hyps = read_file(sys.argv[2])
    morpho_features = sys.argv[3].split(' ')

    print(json.dumps(evaluate_mex(refs, hyps, morpho_features, morph)))
