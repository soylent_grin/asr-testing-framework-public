import pymorphy2 as pm
from pymorphy.wer import wer
from nltk.tokenize import RegexpTokenizer
import itertools
import sys

import json


def lemmatize(sentence, morph=pm.MorphAnalyzer(), tokenizer=RegexpTokenizer(r'\w+')):
    parsed_sentence = (morph.parse(word)[0] for word in tokenizer.tokenize(sentence))
    it1, it2, it3 = itertools.tee(parsed_sentence, 3)
    result = {
        'sentence': ' '.join((word.normal_form for word in it1)),
        'tag_properties': [word.tag for word in it2]
    }
    return result


def read_file(filename):
    with open(filename, mode='r', encoding='utf-8') as file:
        return [line.replace('\n', ' ') for line in file.readlines()]


def extract_properties(word: str, properties: list, morph: object=pm.MorphAnalyzer()) -> dict:
    return {property_name: getattr(morph.parse(word)[0].tag, property_name) for property_name in properties}


def evaluate_ex(ref, hyp, properties, morph=pm.MorphAnalyzer()):
    lemmatized_ref = lemmatize(ref, morph)
    lemmatized_hyp = lemmatize(hyp, morph)

    wer_regular = wer(ref, hyp)['details']
    wer_lemmatized = wer(lemmatized_ref['sentence'], lemmatized_hyp['sentence'])['details']

    result = []
    ref_id = 0
    for i in range(len(wer_regular)):
        new_item = {}
        new_item['refTokenId'] = ref_id
        new_item['operation'] = wer_regular[i]['operation']
        new_item['lemmatizedOperation'] = wer_lemmatized[i]['operation']

        if (i < len(wer_regular) - 1) and (wer_regular[i + 1]['operation'] != 'INS'):
            ref_id += 1

        ref_properties = extract_properties(word=wer_regular[i]['ref'], properties=properties + ['POS'], morph=morph)
        hyp_properties = extract_properties(word=wer_regular[i]['hyp'], properties=properties + ['POS'], morph=morph)

        new_item['refProperties'] = {property_name: ref_properties[property_name] for property_name in ref_properties if ref_properties[property_name]}
        new_item['hypProperties'] = {property_name: hyp_properties[property_name] for property_name in hyp_properties if hyp_properties[property_name]}
        result.append(new_item)

    return result


def evaluate_mex(refs, hyps, properties, morph=pm.MorphAnalyzer()):
    return [evaluate_ex(pair[0], pair[1], properties, morph) for pair in zip([ref for ref in refs], [hyp for hyp in hyps])]


if __name__ == "__main__":
    if (len(sys.argv) < 4):
        raise ValueError("Not enough arguments")

    morph = pm.MorphAnalyzer()
    refs = read_file(sys.argv[1])
    hyps = read_file(sys.argv[2])
    morpho_features = sys.argv[3].split(' ')

    print(json.dumps(evaluate_mex(refs, hyps, morpho_features, morph)))
