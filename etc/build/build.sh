#!/bin/bash

echo "start building; first, building UI..."
pushd public.src
    npm i
    npm run build
    rm -rf ../public/*
    cp -rf ./build/* ../public
popd
echo "done; building deb..."
echo "first, backuping application.conf file (if exists)"
if [ -f conf/application.conf ]; then
    mv conf/application.conf conf/application.conf.bak
fi
rm ./target/cvoaf*.deb
sbt clean
sbt debian:packageBin
if [ -f conf/application.conf.bak ]; then
    mv conf/application.conf.bak conf/application.conf
fi
echo "done building"