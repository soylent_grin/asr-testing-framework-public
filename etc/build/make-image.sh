#!/bin/bash

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "start building docker image; script directory is $CWD"
echo "first, building deb package..."
bash $CWD/build.sh
echo "done; buinding docker image..."
docker build -f ./etc/deployment/mts-public/Dockerfile . -t cvoaf:2.2
echo "done; saving docker image to file..."
docker image save cvoaf -o ./target/cvoaf-2.2-image
echo "done; result image is ./target/cvoaf-2.2-image"
