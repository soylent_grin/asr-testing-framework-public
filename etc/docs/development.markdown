### UI

Node.js >= 10 is required

Building UI for production:
```bash
pushd public.src
    npm i
    npm run build
    cp -rf ./build/* ../public
popd
```

Watch mode (for development):
```bash
cd public.src
npm i
npm start 
```

UI would be enabled on port 3000

#### Watch mode with OAuth

OAuth requires redirect to public URL after authorization (see `usage.markdown` for details about OAuth).

For dev purposes you can use host `lvh.me`, which is resolving to `localhost`. Watch mode command to use this host:
```bash
HOST lvh.me npm start
```

Backend use `lvh.me` as callback URL by default

### Backend (Debian-base OS only)

Sbt >= 1.0 is required

Building for production (deb-package)
```
sbt debian:packageBin
```
Result .deb can be found in `target` directory

Development:
```
touch conf/application.conf
sbt run
```

### UI + backend one liner

```bash
./etc/build/build.sh
```

### Docker image one liner
                
```bash
./etc/build/make-image.sh
```