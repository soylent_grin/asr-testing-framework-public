# docker-compose

## build
собрать весь проект:
```
sudo docker-compose build
```

собрать только 1 компонент (на примере основного app):
```
sudo docker-compose build app
```
При сборке основного компонента `id_rsa` - должен находиться в папке с docker-compose

## up
запустить
```
sudo docker-compose up -d
```

# Подготовка
## аудио файлы
Сейчас на сервере в папке `asr_run`:
`audiofiles` - папка с аудио файлами
загрузка из trello аудиофайлов:
```
sudo wget https://trello-attachments.s3.amazonaws.com/5bc9a8cb70c61b538da82442/5be590616488bb42cabd5bd0/53383a4ae4268e4de992800288085335/Aaron-20080318-kdl.zip
sudo unzip Aaron-20080318-kdl.zip
sudo mv Aaron-20080318-kdl/* /home/ubuntu/asr_run/audiofiles
sudo rm -R Aaron-20080318-kdl Aaron-20080318-kdl.zip
```
Аналогично для других записей, сейчас на сервере развернут еще:  `sudo wget https://trello-attachments.s3.amazonaws.com/5bc9a8cb70c61b538da82442/5bf6ea33eacef048312d9f6d/8394ad83bd8a5c880b57cc194ef6248d/libri_speech.zip`

## volume
`/home/ubuntu/compiled_julius` - собранный julius
`/home/ubuntu/asr_run/models` - модели
`/home/ubuntu/asr_run/conf/application.conf` - конфиг для основного компонента
`/home/ubuntu/asr_run/audiofiles` - папка с аудио файлами
`/home/ubuntu/asr_run/asr_logs` - папка с логами
`/home/ubuntu/asr_run/asr_noises` - папка с шумами
`/home/ubuntu/asr-cassandra/config` - папка с конфигом для кассандры
`/home/ubuntu/asr-cassandra/var/lib/cassandra:` - папка для хранения данных кассандры

# Dockerfile с сервером системы (основной компонент)

`id_rsa` - путь к ssh ключу с доступом к репозиторию

example build:
```
sudo docker image build . -t mts-app:1 --build-arg SSH_PRIVATE_KEY=id_rsa
```

example run (в данном примере указывается папка с compiledJulius, папка с моделями, папка с аудиозаписями):
```
sudo docker run -i -td -p 9000:9000 -v /home/ivan/compiled_julius:/home/chvrches/compiledJulius -v /home/ivan/docker_run/asr-core/models:/asr-core/models -v /home/ivan/docker_run/asr-core/asr_audio_samples:/asr-core/asr_audio_samples mts-app:1
```

Пример запуска на сервере:
```
sudo docker run -i -td -p 9000:9000 -v /home/ubuntu/asr_run/conf/application.conf:/asr-core/conf/application.conf -v /home/ubuntu/asr_run/models:/asr-core/models -v /home/ubuntu/asr_run/audiofiles:/asr-core/asr_audio_samples mts-app:1
```

# TODO
    - Для работы rwth надо взять data.zip из https://yadi.sk/d/1-F78sjRcSgVXg и положить в deployment/rwth и собрать контейнер rwth. Потом перенесем на гитлаб, надо будет поправить rwth/Dockerfile
    