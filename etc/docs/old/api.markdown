## Additional HTTP API

### Augmentation

#### Get param list for augmentation scenario
`GET /api/soundbank/augmentation/scenarios/:key/params/`
Response:
```js
[
     {
         key: "key_1",
         paramType: "string" // or "double", or "boolean", or "select" 
         defaultValue: "str",
         title: "Key 1",
         description: "This is key 1 description",
         options: [ // optional
             {
                 "key": "option_1",
                 "title": "Option 1"
             }
         ]
     }
]
```

#### Run augmentation scenario on folder with params
`POST /api/soundbank/folders/:folderKey/augmentate/:scenarioKey/`
Request:
```js
{
    title: "New processed folder name", // optional
    params: { // optional, arbitrary JSON with params (key-value) from previous request
        ...
    }
}
```
Response: new folder JSON
