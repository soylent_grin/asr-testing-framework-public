# ASR Core

## How to run project

If you are not using Docker, see instructions below

### Prerequisites (Ubuntu >= 16.04)

* Install Java 1.8:
```bash
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install openjdk-8-jdk
```

* Install SBT 1.x:
```bash
sudo apt-get install sbt
```

* Install NodeJs 10 and NPM 6 (if you want to access the web UI):
```bash
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs
```

* Install sox (if you want to use normalization and advanced augmentation scenarios)
```bash
sudo apt-get install sox
```

* Install CSound 6.12 (if you want to use any augmentation scenario)
```bash
# assuming you are in project root folder (i. e. `ASRTestingFramework`)
pushd ./lib
    sudo dpkg -i ./*.deb
popd
```

* Install pip3 and python libs (if you want to use morpho metrics and augmentation scenarios using VAD):
```bashl
sudo apt-get install python3-pip
pip3 install nltk==3.4.1 pymorphy2==0.8 webrtcvad
```

* Create `conf/application.conf` file for starting app. Empty file is enough, however, you can redefine any of settings from `conf/reference.conf` file

### Building

- Compile UI:
```bash
cd public.src
npm i
npm run build
```

- Run main app:
```
sbt run
```

And open 9000 port in browser.

### How to launch UI in dev mode (with recompiling)

```
cd public.src
npm i
npm run serve
```
And open 8080 port in browser. App will communicate with API server on 9000 port.

## Dev instructions

### How to add new executor

See `ReferenceAsrExecutor` for example of fully working (test / train) executor.

#### Scala executor

Extend `AsrExecutor` trait, annotate with `AsrExecutorInfo` and put it in `extensions` package, then relaunch.
Now list of executors should contains your executor.

#### Java executor

Extend `JavaAsrExecutor` abstract class, annotate with `AsrExecutorInfo` and put it in `extensions` package, then relaunch.

You can also disable added executor, simple putting it's key in `asr.disabled` param in `application|reference.conf`

### How to log ASR executions

In your executor, call built-in `log` method (from `AsrExecutorLogSupport`) with single log line whenever you want. When execution is ready, log file is accessible through `getLogFile` method

### How to get ASR execution duration

In your executor, call built-in `getDuration` method, that returns duration from job start in seconds.

### How to add config params for executors

In `conf/application.conf` add next rows:

```

asrConfig {
   yourAsr = [ // `yourAsr` must be equal to AsrExecutorInfo.key value
      {
          key = "myParam" // required
          type = "string" // or "double", or "boolean", or "select"
          defaultValue = "abs" // this is optional
          title = "ABS value" // this is optional
          options = [ // this is optional
            {
               key = "key", // but if `options` field exist, this format is required
               title = "title"
            }
          ]
          scope = [ "train", "test" ] // defines if parameter contains in test, train, or both param lists. If not defined - recognizing as [ "test" ]
      }
   }
}

```

### How to add datasets

Platform supports multiple dataset formats. It use dataset adapters - Scala classes, that allow conversion from specific dataset format to inner platform format and vice versa.
Each dataset adapter exposes 2 methods:

* `fromFolderToSamples`
* `fromSamplesToFolder`

Also each dataset adapter has a corresponding key, which is provided in file `info.meta` located in the root directory of a dataset using format `datasetType=$key` where `$key` is a mentioned dataset key. Currently there are three possible values for `$key`:
- `meta` (corresponds to `MetaDatasetAdapter`)
- `default` (corresponds to `DefaultDatasetAdadpter`)
- `voxforge-mts` (corresponds to `VoxforgeMtsDatasetAdapter`)  

Although there is an additional dataset adapter `VoxforgeDatasetAdapter` with associated value for `$key` = `voxforge`, it is deprecated. 

First one is used on start, to scan all datasets. Second can be used inside ASR executors to prepare abstract list of samples to specific format.

There are 3 built-in adapters for now:

* DefaultDatasetAdapter with next format:
    * Root directory contains file `info.meta` with the following content (optional): 
        `datasetType=default`
    * Folder with [0..N] subdirectories-samples
    * Each sample can contain next files:
        - *.txt - transcription (required)
        - *.wav or *.mp3 - audio samples (required)
        - *.json - sample's metadata (optional); file has the same name as the corresponding audio file

* VoxforgeMtsDatasetAdapter
    * Root directory contains file `info.meta` with the following content (required, otherwise dataset will be parsed using `DefaultDatasetAdapter`):   
        `datasetType=voxforge-mts`
    * Folder with [0..N] subdirectories
    * Each subdirectory contains `wav` and `etc` folders
        * `wav` folder contains [0..M] plain .wav-files
        * `etc` folder contains at least `PROMPTS` and `README` text files
            * `PROMPTS` file contains M lines, each line in the next format:
            ```properties
            $WAV_FILE_NAME_WITHOUT_EXTENSION $TRANSCRIPTION
            ```
            * `README` file contains at least this info:
            ```properties
            User Name:1981
            
            Speaker Characteristics:
            
            Gender: Мужчина
            Age Range: до 55
            Language: RU
            Pronunciation dialect: Сибирь
            ```
        * *.json files with sample's metadata; each file has the same name as corresponding audio file in the `wav` subdirectory
* MetaDatasetAdapter
    * Root directory contains file `info.meta` with the following content (required, otherwise dataset will be parsed using `DefaultDatasetAdapter`):  
        `datasetType=meta`
    * Folder with [0..N] subdirectories-samples
    * Each sample can contain next files:
        - *.wav or *.mp3 - audio samples
        - *.json - sample's metadata (must contain at least `ref` field with original transcription; the rest of metadata parameters are described in documentation); file has the same name as the corresponding audio file
                    

To add new datasets:
* Set in `application.conf` param `soundbank.rootPath` to your directory (defaults is `./asr_audio_samples`).
NOTE: to use `fromSamplesToFolder` conversion, this path should be absolute (default is local), because platform creates symbolic links for .wav-files instead of copying them.
* Put dataset directory in this path
* Put file `info.meta` to the root of dataset directory. It contains arbitrary info about dataset, but field `datasetType` is tells the platform which adapter should be used. If no file specified, platform attempts to parse this dataset with `DefaultDatasetAdapter`.

You cat write your own adapters. Extend `DatasetAdapter` trait, annotate with `DatasetAdapterInfo` and put it in `extensions.soundbank` package, then relaunch.

### Augmentation

See `augmentation.markdown`

### How to work with models

All models are contained in directory, that is set in conf file (default is `./models`)

There are 2 types of models, that can be used:

1. Default models - should be put manually and stored at `$ROOT_MODEL_PATH/defaults/$ASR_KEY`
2. Trained models - generated by launching ASR jobs and stored at `$ROOT_MODEL_PATH/$STEP_UUID`

Note that models directory is required (even if there would be no models for executor)

### How test metrics are calculated

You can use hezitates in app; see `metric.hezitationList` key in application|reference.conf
You can use set list of all used speech tags (they are shown in UI); see `asr.tags.list` key in application|reference.conf

For more information, see [metrics.markdown](metrics.markdown)

### How normalization of sample rates works

We use `sox` package, which is preinstalled in main Docker container.

## Example CURL queries (for test purposes)

#### Create job with both train and test steps on sample with id = ID

```bash
curl -X POST -H "Content-Type: application/json"
 \ -d '{"asrKey":"reference","steps":[{"index":1,"stepType":"train","config":{"key":"value"},"modelType":"default","samples":["ID"]},{"index":2,"stepType":"test","config":{"key":"value"},"modelType":"current","samples":["ID"]}]}'
 \ 'http://localhost:9000/api/asr/jobs/'

```

When launching platform itself from .jar, `-Djava.class.path=./lib` would not be enough (you should specify direct full path)

### Testing

We use ScalaTest and GuiceOneAppPerSuite to get fake instance of app with full DI support, that allows us to write unit and functional tests.
Run all tests:
```bash
sbt test
```