# Sound augmentation

Platform provides rich opportunities for datasets augmentation.

For each registered dataset, user can choose one of augmentation scenario and run this scenario with configurable parameters.
As a result, new dataset with given name (which should be unique) is created, which is identical to initial dataset (same metadata, transcription, structure), but each audiofile is processed within given scenario.

Platform supports extendable way to add new scenarios (see below). Although, it contains built-in general-purpose scenario `GenericCSoundAugmentationScenario`

## GenericCSoundAugmentationScenario

Scenario based on [CSound 6.12](https://csound.com/) audio processing library, that allows:

* Change audio speed (from 0.1 to M)
* Change audio volume relative level (from 0.1 to M)
* Use or not use tone-correction
* Set relative range of distance between speaker and microphone
* Set relative range of room size
* Set relative range of reverberation level
* Insert random noise from chosen noise group in pauses (with given probability)
* Insert or not insert random noise from chosen noise group as ambient
* Insert or not insert white noise with given low and high frequency

Pause intervals are calculating automatically with external `vad.py` script. Pause length is expanded to pause noise file, but not more that 4 seconds.

*NOTE*: `vad.py` accept only mono-channel `.wav`-files; if any sample in augmented dataset will be stereo-channel - augmentation would fail on preprocess stage.

*NOTE*: you must have pre-installed both Python2 (for CSound binding) and Python3 (for VAD calculation) to run this scenario. Also, `sox` package is used to parse max amplitude from file, but this dependency is optional (value `0.7` would be used, if value could not be parsed from file). 

Very approximate experiments show, that real-time factor (speech time / processing time) is around 15, but linearly depends on desired speed, input sample rate and used noise files.

## Working with noises

Each augmentation scenario may use large amount of pre-classified noises.

To work with noise bank in platform, put directories with noises (only .wav format is currently supported) to path, defined by `noises.rootDir` configuration key (default is `/asr-core/asr_noises`). Zip archive with noises prepared by ITMO can be found on [Dropbox (3.5 Gb)](https://www.dropbox.com/sh/nhbsi5zes3dpjnf/AAB5j8pqM_ApSdMSbI2vCc0Ea?dl=0).

On start, platform scan this path and register N noise groups.

Each group is characterized with key (which composed by all parent directories, separated by ` / `) and contains M noise samples, that can be used in particular augmentation scenario lately.

Result group composition depends on flag `noises.withHighOrderClusters` flag in *.conf file (default is `false`).

- If `false`, than each group represents a lowest-level directory (of arbitrary depth)
- If `true`, than in addition to groups described above, each directory on each level also is treating as a group.

For now, there is no differentiation between ambient and pause noises; each noise group can be applied both to ambience and pause.

If there is at least one non-empty noise group, for every augmentation scenario there are 2 predefined configuration parameters: `backgroundNoiseGroup` and `pauseNoiseGroup`.
Each of this parameter can be set to one of registered noise group, or `__random__` (if so, random noise would be chosen across all registered noise samples). 

## Troubleshooting

By default, logs of CSound processing (and all `TRACE` logs) are not shown. You can enable them by adding next line `logback.xml`:

```xml
<logger name="extensions.augmentation" level="TRACE" />
```

## Advanced: running GenericCSoundAugmentationScenario manually

Sometimes, you may want to run augmentation in some advanced way: for example, to process single file, not the whole dataset. This can be done by launching main Python2 script `./augmentation_resources/csound/generic/script.py` manually.

Script accepts one argument - path to configuration file with next structure (strip comments if you want to use this as template for your config):
```js
{
  "params": { // overall params
    "pauseNoiseProbability": "1", // 0..1
    "speed": "2", // 0.1..M
    "toneCorrection": "1", // -1 to disable
    "volume": "1", // 0.1..M
    "withBackgroundNoise": "1", // -1 to disable
    "withWhiteNoise": "-1", // 1 to enable
    "whiteNoiseLowFreq": "0", // 0 .. $SAMPLE_RATE / 2
    "whiteNoiseHighFreq": "100" // 0 .. $SAMPLE_RATE / 2
  },
  "samples": [ // arbitrary count of samples
    {
      "reverbDistance": "0.34", // relative distance between speaker and microphone, 0..0.9
      "reverbRoomSize": "0", // relative room size, 0..0.9
      "reverbLevel": "1", // relative reverberation level, 0..0.9
      "backgroundNoise": "./asr_noises/Sounds of household appliances/Washing Machine/27507__schulze__washing-machine.wav", // optional, path to ambient file
      "duration": 8.826375007629395, // duration of file (must be computed externally)
      "maxAmplitude": "0.705292", // maximum amplitude of file (must be computed externally)
      "path": "./asr_audio_samples/test-6/16kz/wav/TEST_CHER.wav", // path to speech file
      "pauseNoise": "./asr_noises/Door/Open Close/182037__omar-alvarado__opening-closing-pantry-door.wav", // optional, path to pause file
      "sampleRate": "16000.0", // output sample rate
      "vad": "0.9900000000000007 4.529999999999993 4.859999999999996 8.04000000000002 8.49000000000001 8.580000000000009" // VAD (must be computed externally)
    }
  ]
}
```

Example of launching (assuming `python` is Python 2.x):

```bash
python ./augmentation_resources/csound/generic/script.py /tmp/conf.json
```

*NOTE*: output of this script rewrites initial speech files. This can be turned off in script itself.

## Advanced: how to add and use new augmentation scenarios in UI

Platform contains base abstract Scala-class is `extensions.augmentation.AugmentationScenario`. It exposes one method: `def run(samples: Seq[Sample], params: JsValue)`, which process all samples with given JSON config.

To add new scenario, simply extend this abstract class, annotate it with `@AugmentationScenarioInfo` annotation, put it in `extensions.augmentation` package and implement your own `run` method. 

You can use arbitrary resource files (scripts, etc.) in your scenario. Each `AugmentationScenario` has protected method `getResourceDirectory`, which is supposed to be separate for each scenario.

Resource directory root is defined in .conf files by key `augmentation.resourcePath`, and by default is set to `./augmentation_resources`

### How to set parameters types for new scenario

All parameters are listed in *.conf files by key: `augmentation.params.$SCENARIO_KEY`, where `$SCENARIO_KEY` equals to key in annotation.

User would see form with this parameters, and all of them would be available in `run` method by argument `params`