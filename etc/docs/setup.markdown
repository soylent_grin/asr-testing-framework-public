### Installing from Docker image

Assuming you have Docker image available (with internet or local tarball).

To wrap app image + database into systemd service, run `setup-systemd-service.sh`.

### Configuration

All settings must be set in `application.conf` file (default location is `/opt/cvoaf/app/config`)

#### OAuth configuration

By default, OAuth is disabled and can be enabled for Google and Github with next lines:

```
  ...
  github {
    redirectURL="http://<PUBLIC_HOSTNAME>/api/authenticate/github"
    clientID="..."
    clientSecret="..."
  }
  google {
    redirectURL="http://<PUBLIC_HOSTNAME>/api/authenticate/google"
    clientID="..."
    clientSecret="..."
  }
```
Where `<PUBLIC_HOSTNAME>` is your public host and port. Client ID and client secret pair can be obtained by registering your own OAuth app within particular provider.

#### Managing preset user

By default, there is one predefined user with username = 'root', password = 'changeme' and role = 'admin'.
You can manage list of predefined users:

```

builtInUsers = [
  {
    username = "root"
    password = "changeme"
    role = "admin"
  }
  {
    username = "user"
    password = "changeme"
    role = "user"
  }
]
```

To disable predefined users, set `builtInUsers = []`

#### Working directories

App requires set of filesystem directories to work with:

* `soundbank.rootPath` (default is `/opt/cvoaf/app/asr_audio_samples`) - root folder for public and private samples
    * To include some public datasets, put them into `${soundbank.rootPath}/public` folder
    * Private datasets, uploaded by users, would be stored in `${soundbank.rootPath}/private`
    
* `noises.rootDir` (default is `/opt/cvoaf/app/asr_noises`) - root folder for samples, that may be used by the augmentation algorithms as background or pause noises
* `asr.modelPath` (default is `/opt/cvoaf/app/models`) - root folder for default and user-trained ASR models
* `asr.logPath` (default is `/opt/cvoaf/app/asr_logs`) - root folder for job results' log files

You can change any of this directories editing `/opt/cvoaf/docker-compose.yml` directory mapping

#### Protecting database with login-password

By default, Cassandra database (main config file is `/opt/cvoaf/cassandra/config/cassandra.yaml`) is accessible for everyone.
If you want expose 9042 port to internet, you should set up login-password authentication for Cassandra:

1. On Cassandra side: https://docs.apigee.com/private-cloud/v4.17.09/enable-cassandra-authentication
2. On app side: add next lines to `application.conf`:

```
db {
  username = "..."
  password = "..."
}
``` 

3. Restart systemd service / docker-compose:

```
sudo systemctl restart cvoaf.service
```

#### Changing list of available ASR

By default, next ASR are **enabled**: `yandex`, `tinkoff`, `stc`, `watson`.

By default, next ASR are **disabled**: `kaldi-alphacephei`

To change enabled and disabled ASR, add next param:

```
asr.disabled = [
  <disabled ASR key>,
  <another disabled ASR key>
]
```

#### Misc configuration

You can vary different settings through `application.conf` file:

```
appConfig.maxJobsPerUser = <number> // default is no restricted
appConfig.maxPrivateFoldersPerUser = <number> // default is not resticted
appConfig.maxSamplesPerJob = <number>  // default is not resticted
```

Also, with setting `asr.jobActorAcquireStrategy` you can vary, how many parallel jobs can be launched:

* `perUser` - one job per user (default)
* `perAsr` - one job per ASR type
* `perSystem` - one parallel job at all
* `perJob` - not restricted