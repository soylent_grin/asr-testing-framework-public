```

|-- app // core source code (backend, scala \ java)
|
|-- conf // general resource directory (global settings, additional scripts, etc)
|  |
|  |-- augmentation // python scripts, used during dataset augmentation
|  |
|  |-- metrics // python scripts, used during metrics calculation
|
|-- etc
|  |
|  |-- build // helper scripts, used during build process (optional)
|  |
|  |-- csound-dependencies // .deb packaged, required for CSound  6.12 (fixed version). Used in Docker image
|  |
|  |-- deployment // scripts and config files, used during production setup (optional)
|  |
|  |-- docs // available docs
|  |
|  |-- python-dependencies // requirements.txt with all python packages. Used in Docker image
|  |
|  |-- test-resources // files for debug purposes
|
|-- modules // some ASR executors modules source code (backend, Scala \ Java)
|
|-- public.src // core source code (frontend, TS \ JS)

```